import Vue from 'vue';
import Router from 'vue-router';
import publicRoutes from '@/router/publicRoutes';
import authRoutes from '@/router/authRoutes';
import store from '@/assets/js/store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  base: process.env.BASE_URL || '/',
  // mode: 'abstract',
  linkActiveClass: 'active',
  routes: [
    ...publicRoutes,
    ...authRoutes,
  ],
});

router.beforeEach((to, from, next) => {
  const { isAuthenticated, isLoggedIn } = store.getters;

  let nextPath = `${to.path}`;
  const nextQuery = to.query;

  if (isLoggedIn) {
    Vue.$log.debug({ isLoggedIn, isAuthenticated });
    if (to.name === 'login' || to.path === '/') {
      nextPath = to.query.redirect || '/app';
    }
  } else if (to.matched.some(route => route.meta.authRequired)) {
    nextPath = `/?redirect=${nextPath}`;
  }

  // Vue.$log.debug({ nextPath });
  if (nextPath === to.path) {
    next();
  } else {
    next({
      path: nextPath,
      query: nextQuery,
    });
  }
});

export default router;
