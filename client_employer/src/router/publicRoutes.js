// import AppNotAuthenticated from '@/views/AppNotAuthenticated';
import Auth from '@/components/Common/Auth';
import EmailVerifyPage from '@/views/EmailVerifyPage';
// import ZoomVerifyPage from '@/views/Public/ZoomVerifyPage';
// import Landing from '@/components/Common/Landing';
// import RequestQuote from '@/components/Common/RequestQuote';
// import FaqPage from '@/views/Public/FaqPage';
// import SurveyPage from '@/views/Public/SurveyPage';
// import OngoPage from '@/views/Public/OngoPage';
// import JobDetailPage from '@/views/Marketplaces/Jobs/JobDetailPage';

const routes = [
  {
    path: '/',
    name: 'login',
    component: Auth,
  },
  {
    path: '/emailVerify',
    name: 'verify',
    component: EmailVerifyPage,
  },
  // {
  //   path: '/zoomverify',
  //   name: 'zoomverify',
  //   component: ZoomVerifyPage,
  // },
];

export default routes;
