import AppAuthenticated from '@/views/AppAuthenticated';
import MarketplaceJobListPage from '@/views/Marketplaces/MarketplaceJobListPage';
import MarketplaceCreatePage from '@/views/Marketplaces/MarketplaceCreatePage';
import MarketplaceJobCreatePage from '@/views/Marketplaces/Jobs/MarketplaceJobCreatePage';
import ProductCreatePage from '@/views/Marketplaces/Products/ProductCreatePage';
import ProductUpdatePage from '@/views/Marketplaces/Products/ProductUpdatePage';
import ProductListPage from '@/views/Marketplaces/Products/ProductListPage';
import JobListPage from '@/views/Jobs/JobListPage';
import JobDetailPage from '@/views/Jobs/JobDetailPage';
import JobCreatePage from '@/views/Jobs/JobCreatePage';
// import JobEditPage from '@/views/Jobs/JobEditPage';
import JobEditPageNew from '@/views/Jobs/JobEditPageNew';
import JobContractPage from '@/views/Jobs/JobContractPage';
import CreateOrganisationPage from '@/views/Organisations/CreateOrganisationPage';
import EditOrganisationPage from '@/views/Organisations/EditOrganisationPage';
import OrganisationSettingsPage from '@/views/Organisations/OrganisationSettingsPage';
import WorkerListPage from '@/views/Workers/WorkerListPage';
import WorkerCreatePage from '@/views/Workers/WorkerCreatePage';
import WorkerAlienCreatePage from '@/views/Workers/WorkerAlienCreatePage';
import WorkerEditPage from '@/views/Workers/WorkerEditPage';
import WorkerAlienEditPage from '@/views/Workers/WorkerAlienEditPage';
import WorkerAssignJobPage from '@/views/Workers/WorkerAssignJobPage';
import WorkerAlienAssignJobPage from '@/views/Workers/WorkerAlienAssignJobPage';
import WorkerProfilePage from '@/views/Workers/WorkerProfilePage';
import BatchUploadPage from '@/views/Utils/BatchUploadPage';
import PaymentTimeSheetsPage from '@/views/Payment/PaymentTimeSheetsPage';
import HybridTimesheetPage from '@/views/Timesheets/HybridTimesheetPage';
import PaymentDisbursementsPage from '@/views/Payment/PaymentDisbursementsPage';
import NotificationsPage from '@/components/Notifications/NotificationList';
import MarketplaceJobMatchingPage from '@/views/Marketplaces/Jobs/JobMatchingPage';
import ExpectedMarketplaceSubscriberUpload from '@/views/Marketplaces/Subscriptions/ExpectedMarketplaceSubscriberUpload';
import MonthendReportPage from '@/views/Reports/MonthendReportPage';
import AddShiftPage from '@/views/Marketplaces/AddShiftPage';
import BraintreePage from '@/views/Payment/BraintreePage';
import MarketplaceDashboardPage from '@/views/Marketplaces/MarketplaceDashboardPage';

const hideSideNav = true;

const routes = [
  {
    path: '/app',
    component: AppAuthenticated,
    children: [
      // Organisation
      {
        path: '',
        redirect: 'view-jobs',
      },
      {
        path: 'create-organisation',
        component: CreateOrganisationPage,
        name: 'create-organisation',
      },
      {
        path: 'edit-organisation',
        component: EditOrganisationPage,
        name: 'edit-organisation',
        props: true,
      },
      {
        path: 'organisation-settings',
        component: OrganisationSettingsPage,
        name: 'organisation-settings',
        props: true,
      },
      // Jobs
      {
        path: 'create-job',
        component: JobCreatePage,
        name: 'create-job',
        meta: { hideSideNav },
        props: true,
      },
      {
        path: 'edit-job',
        component: JobEditPageNew,
        name: 'edit-job',
        meta: { hideSideNav },
        props: true,
      },
      {
        path: 'job-contract',
        component: JobContractPage,
        name: 'job-contract',
        props: true,
      },
      {
        path: 'view-jobs',
        component: JobListPage,
        name: 'view-jobs',
        props: true,
      },
      {
        path: 'marketplace',
        component: MarketplaceJobListPage,
        name: 'marketplace',
      },
      {
        path: 'create-marketplace',
        component: MarketplaceCreatePage,
        name: 'create-marketplace',
      },
      {
        path: 'create-marketplace-job',
        component: MarketplaceJobCreatePage,
        name: 'create-marketplace-job',
        meta: { hideSideNav },
        props: true,
      },
      {
        path: 'marketplace-dashboard',
        component: MarketplaceDashboardPage,
        name: 'marketplace-dashboard',
        props: true,
      },
      {
        path: 'create-product',
        component: ProductCreatePage,
        name: 'create-product',
        props: true,
      },
      {
        path: 'update-product',
        component: ProductUpdatePage,
        name: 'update-product',
        props: true,
      },
      {
        path: 'view-products',
        component: ProductListPage,
        name: 'view-products',
        props: true,
      },
      {
        path: 'view-job/:jobId',
        name: 'view-job',
        component: JobDetailPage,
        props: true,
        meta: { hideSideNav },
      },
      {
        path: 'view-job-detail/:jobId',
        name: 'marketplace-job-matching',
        component: MarketplaceJobMatchingPage,
        props: true,
        meta: {
          hideSideNav,
        },
      },
      {
        path: 'add-shift-page',
        name: 'add-shift-page',
        component: AddShiftPage,
        props: true,
      },
      // Workers
      {
        path: 'view-workers',
        component: WorkerListPage,
        name: 'view-workers',
        props: true,
      },
      {
        path: 'create-worker',
        component: WorkerCreatePage,
        name: 'create-worker',
        props: true,
      },
      {
        path: 'create-alien-worker',
        component: WorkerAlienCreatePage,
        name: 'create-alien-worker',
        props: true,
      },
      {
        path: 'assign-worker-job/:workerId',
        component: WorkerAssignJobPage,
        name: 'assign-worker-job',
        props: true,
      },
      {
        path: 'assign-alien-worker-job/:workerId',
        component: WorkerAlienAssignJobPage,
        name: 'assign-alien-worker-job',
        props: true,
      },
      {
        path: 'edit-worker',
        component: WorkerEditPage,
        name: 'edit-worker',
        props: true,
        meta: {
          hideSideNav,
        },
      },
      {
        path: 'edit-alien-worker',
        component: WorkerAlienEditPage,
        name: 'edit-alien-worker',
        props: true,
      },
      {
        path: 'worker-profile',
        component: WorkerProfilePage,
        name: 'worker-profile',
        props: true,
        meta: { hideSideNav },
      },
      // Payment
      {
        path: 'timesheets',
        component: PaymentTimeSheetsPage,
        name: 'timesheets',
        props: true,
      },
      {
        path: 'hybrid-timehseet',
        component: HybridTimesheetPage,
        name: 'hybrid-timehseet',
        props: true,
      },
      {
        path: 'payment-disbursements',
        component: PaymentDisbursementsPage,
        name: 'payment-disbursements',
        props: true,
      },
      // Utils
      {
        path: 'batch-upload',
        component: BatchUploadPage,
        name: 'batch-upload',
        props: true,
      },
      // Notifications
      {
        path: 'notifications',
        component: NotificationsPage,
        name: 'notifications',
        props: true,
      },
      {
        path: 'expected-marketplace-subscriber-upload',
        component: ExpectedMarketplaceSubscriberUpload,
        name: 'expected-marketplace-subscriber-upload',
      },
      // reporting
      {
        path: 'monthend-report',
        component: MonthendReportPage,
        name: 'monthend-report',
        props: true,
      },
      {
        path: 'braintree',
        component: BraintreePage,
        name: 'braintree',
        props: true,
      },
    ],
    meta: {
      authRequired: true,
    },
  },
];

export default routes;
