// Vue imports
import Vue from 'vue';
import '@/components/Global';
// import './plugins/vuetify';
import App from '@/App.vue';
import router from '@/router/index';
// import { messaging } from '@/assets/js/firebase';
import lodash from 'lodash';
import VeeValidate from 'vee-validate';
import VueLogger from 'vuejs-logger';
import i18next from 'i18next';
import VueI18Next from '@panter/vue-i18next';
import VueClipboard from 'vue-clipboard2';
import * as VueGoogleMaps from 'gmap-vue';
import AsyncComputed from 'vue-async-computed';
import * as firebase from 'firebase/app';
import 'firebase/analytics';
// local imports
import store from '@/assets/js/store';
import apollo from '@/assets/js/apollo';
import AccessService from '@/assets/js/AccessService';
import AnalyticsService from './assets/js/AnalyticsService';
import UserService from '@/assets/js/UserService';
import JobService from '@/assets/js/JobService';
import ProductService from './assets/js/ProductService';
import WorkerService from '@/assets/js/WorkerService';
import OrganisationService from '@/assets/js/OrganisationService';
import NotificationService from '@/assets/js/NotificationService';
import PaymentService from '@/assets/js/PaymentService';
import PayrollService from '@/assets/js/PayrollService';
import UtilityService from '@/assets/js/UtilityService';
import MarketplaceService from '@/assets/js/MarketplaceService';
import TimesheetService from '@/assets/js/TimesheetService';
import IdbService from './assets/js/IdbService';
import RatingService from './assets/js/RatingService';
import ValidateService from './assets/js/ValidateService';
import InterviewService from './assets/js/InterviewService';
import MessageService from './assets/js/MessageService';

import mixins from '@/assets/js/mixins';
import util from '@/assets/js/util';
import io from '@/assets/js/io';
import locales from '@/assets/js/locales';
import vuetify from './plugins/vuetify';
import VueBlobJsonCsv from 'vue-blob-json-csv';
import VueBrainTree from 'vue-braintree';
import Fragment from 'vue-fragment';

require('./assets/js/filters');

const options = {
  isEnabled: true,
  logLevels: ['debug', 'info', 'warn', 'error', 'fatal'],
  logLevel: process.env.LOG_LEVEL,
  stringifyArguments: false,
  showLogLevel: true,
  // showMethodName: true,
  // separator: '|',
  showConsoleColors: true,
};

Vue.use(VueLogger, options);
Vue.use(AsyncComputed);
Vue.use(UserService);
Vue.use(JobService);
Vue.use(ProductService);
Vue.use(WorkerService);
Vue.use(OrganisationService);
Vue.use(NotificationService);
Vue.use(PaymentService);
Vue.use(PayrollService);
Vue.use(VeeValidate);
Vue.use(UtilityService);
Vue.use(MarketplaceService);
Vue.use(TimesheetService);
Vue.use(IdbService);
Vue.use(RatingService);
Vue.use(AccessService);
Vue.use(ValidateService);
Vue.use(InterviewService);
Vue.use(MessageService);
VueClipboard.config.autoSetContainer = true; // add this line
Vue.use(VueClipboard);
Vue.use(VueBlobJsonCsv);
Vue.use(VueBrainTree);
Vue.use(Fragment.Plugin);

const secondaryConfig = {
  apiKey: 'AIzaSyCoMFxGk6yHTiOM00xMZryrYwDbH1DGiCo',
  authDomain: 'dotted-howl-158112.firebaseapp.com',
  databaseURL: 'https://dotted-howl-158112.firebaseio.com',
  projectId: 'dotted-howl-158112',
  storageBucket: 'dotted-howl-158112.appspot.com',
  messagingSenderId: '689075043397',
  appId: '1:689075043397:web:5b59dead6ce0f4343a372c',
  measurementId: 'G-QFVRJ7L28V',
};
const secondaryApp = firebase.initializeApp(secondaryConfig, 'secondary');

secondaryApp.analytics();
Vue.prototype.$analytics = secondaryApp.analytics();

Vue.use(AnalyticsService);

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.GMAP_API,
    libraries: 'places', // This is required if you use the Autocomplete plugin
  },
});
Vue.use(require('vue-moment'));

Vue.mixin(mixins);
Vue.mixin(util);
Vue.mixin(io);

// Vue.prototype.$messaging = messaging;
Vue.prototype.$lodash = lodash;

// import i18next from 'i18next';
// import VueI18Next from '@panter/vue-i18next';

// const locales = {
//   en: {
//     faq: 'FAQ',

//   },
//   mm: {
//     faq: 'အမေးအဖြေများ',
//   },

//   zg: {
//     faq: 'အေမးအေျဖမ်ား',
//   },
// };

Vue.use(VueI18Next);

i18next.init({
  lng: 'en',
  resources: {
    en: { translation: locales.en },
    mm: { translation: locales.mm },
    zg: { translation: locales.zg },
  },
});

const i18n = new VueI18Next(i18next);

Vue.config.productionTip = false;
Vue.config.silent = true;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  vuetify,
  apolloProvider: apollo,
  render: h => h(App),
}).$mount('#app');
