import Vue from 'vue';
import * as firebase from 'firebase/app';
import 'firebase/analytics';
import moment from 'moment';

const analytics = firebase.analytics();
const services = {
  logEvent: async (eventName, param) => {
    Vue.$log.info('log parameters', param);
    // const enableAnalytic = store.getters.getEnableAnalytic;
    analytics.logEvent(eventName, param);
  },
  setUserProperties: async (param) => {
    analytics.setUserProperties(param);
  },
  setUserId: async (userId) => {
    Vue.$log.info('analyticsworkerId', userId);
    analytics.setUserId(userId);
  },
  durationTracker(startTime, endTime) {
    const momentStartTime = moment(startTime);
    const momentEndTime = moment(endTime);
    const duration = moment.duration(
      momentEndTime.diff(momentStartTime),
    );
    return duration.asSeconds() * 1000;
  },
};

const AnalyticsService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local AnalyticsService vue plugin');
    Vue.prototype.$AnalyticsService = services;
  },
};

export default AnalyticsService;
