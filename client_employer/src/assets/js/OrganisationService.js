import Vue from 'vue';
import gql from 'graphql-tag';
import apollo from '@/assets/js/apollo';

export const services = {
  addEmployerOrganisation(args) {
    Vue.$log.info('OrganisationService.addEmployerOrganisation', args);
    const ADD_EMPLOYER_ORGANISATION = gql`
      mutation addEmployerOrganisation(
        $employerId: String!,
        $name: String!,
        $description: String,
        $fee: Float,
        $region: String,
        $payrollManagerName: String,
        $payrollManagerContact: String,
        $payCycleMonthDays: [ Int ],
        $settings: [GraphQLJSON],
        $phone: String,
        $logoUrl: String,
        $industryType: String,
        $videoUrl: String,
        $websiteUrl: String,
        $socialMedia: SocialMediaInput,
      ) {
        addEmployerOrganisation(
          employerId: $employerId,
          name: $name,
          description: $description,
          fee: $fee,
          region: $region,
          payrollManagerName: $payrollManagerName,
          payrollManagerContact: $payrollManagerContact,
          payCycleMonthDays: $payCycleMonthDays,
          settings:$settings,
          phone: $phone,
          logoUrl: $logoUrl,
          industryType: $industryType,
          videoUrl: $videoUrl,
          websiteUrl: $websiteUrl,
          socialMedia: $socialMedia,
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ADD_EMPLOYER_ORGANISATION,
      variables: args,
      optimisticResponse: {
        __typename: 'Mutation',
        addEmployerOrganisation: {
          __typename: 'Employer',
          id: -1,
        },
      },
    });
  },

  updateOrganisation(args) {
    Vue.$log.debug('OrganisationService.updateOrganisation', args);
    const UPDATE_ORGANISATION = gql`
      mutation updateOrganisation(
        $id: ID!,
        $name: String,
        $description: String,
        $accountingStyle: String,
        $payCycleMonthDays: [ Int ],
        $fee: Float,
        $region: String
        $settings: [GraphQLJSON]
        $phone: String
        $address: String
        $leaveSickAccuralRateDaysPerYear: Int
        $leavePersonalAccrualRateDaysPerYear: Int
        $leaveSpecialAccuralRateDaysPerYear: Int
        $logoUrl: String
        $industryType: String
        $websiteUrl: String
        $videoUrl: String
        $socialMedia: SocialMediaInput
        $businessRegFileUrl: String
        $businessRegNumber: String
        $payrollOfficerContact: String
        $disbursementPaymentType: DisbursementPaymentType
        $payrollContractFileUrl: String
        $surveys: GraphQLJSON
        $billingDetails: String
        $payrollOfficerPhone: String
        $payrollOfficerEmail: EmailAddress
        $numberOfStuff: Int
        $approxTotalSalary: Float
        $companyAge: Int
        $organisationRelationChartUrl: String
        $reasonForSelfPayrollAndDisbursement: String
      ) {
        updateOrganisation(
          id: $id,
          name: $name,
          description: $description,
          accountingStyle: $accountingStyle,
          payCycleMonthDays: $payCycleMonthDays,
          fee: $fee,
          region: $region,
          settings: $settings,
          phone: $phone,
          address: $address,
          leaveSickAccuralRateDaysPerYear: $leaveSickAccuralRateDaysPerYear,
          leavePersonalAccrualRateDaysPerYear: $leavePersonalAccrualRateDaysPerYear,
          leaveSpecialAccuralRateDaysPerYear: $leaveSpecialAccuralRateDaysPerYear,
          logoUrl: $logoUrl,
          industryType: $industryType,
          websiteUrl: $websiteUrl,
          videoUrl: $videoUrl,
          socialMedia: $socialMedia,
          businessRegFileUrl: $businessRegFileUrl
          businessRegNumber: $businessRegNumber
          payrollOfficerContact: $payrollOfficerContact
          disbursementPaymentType:$disbursementPaymentType
          payrollContractFileUrl:$payrollContractFileUrl
          surveys: $surveys
          billingDetails: $billingDetails
          payrollOfficerPhone: $payrollOfficerPhone
          payrollOfficerEmail: $payrollOfficerEmail
          numberOfStuff: $numberOfStuff
          approxTotalSalary: $approxTotalSalary
          companyAge: $companyAge
          organisationRelationChartUrl: $organisationRelationChartUrl
          reasonForSelfPayrollAndDisbursement: $reasonForSelfPayrollAndDisbursement
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_ORGANISATION,
      variables: args,
    });
  },

  addOrganisationPayCycleIdentifier(args) {
    const PAYCYCLE_IDENTIFIER = gql`
      mutation addOrganisationPayCycleIdentifier(
        $id: ID!,
        $payCycleIdentifier: GraphQLDateTime!,
      ) {
        addOrganisationPayCycleIdentifier(
          id: $id,
          payCycleIdentifier: $payCycleIdentifier,
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: PAYCYCLE_IDENTIFIER,
      variables: args,
    });
  },

  addOrganisationJob(args) {
    const ADD_ORGANISATION_JOB = gql`
      mutation addOrganisationJob(
        $organisationId: ID!,
        $id: ID!,
        #$to: JOB_STAGE_EMPLOYER!,
      ) {
        addOrganisationJob(
          organisationId: $organisationId,
          id: $id,
          #to: $to,
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ADD_ORGANISATION_JOB,
      variables: {
        organisationId: args.organisationId || '',
        id: args.id || '',
      // to: args.to || '',
      },
    });
  },
};

const OrganisationService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local Organization vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$OrganisationService = services;
  },
};

export default OrganisationService;
