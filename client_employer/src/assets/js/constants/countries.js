export default [
  {
    name: 'Hong Kong',
    code: 'hk',
  },
  {
    name: 'Myanmar',
    code: 'mm',
  },
  {
    name: 'Vietnam',
    code: 'vn',
  },
];
