import gql from 'graphql-tag';
// import store from '@/assets/js/store';
import apollo from '@/assets/js/apollo';
import Vue from 'vue';
// import { isObject } from 'util';

const services = {
  createUnverifiedWorker(args) {
    Vue.$log.info('WorkerService.createUnverifiedWorker', args);
    const CREATE_UNVERIFIED_WORKER = gql`
        mutation createUnverifiedWorker(
          $organisationId:String!,
          $name: String!,
          $phone: String!
          $identification: String,
        ) {
          createUnverifiedWorker (
            organisationId:$organisationId,
            name: $name,
            phone: $phone,
            identification: $identification,
          ) {
            __typename,
            id,
          }
        }
      `;
    /*
    return io.graphqlMutation({
      gql: CREATE_UNVERIFIED_WORKER,
      variables: {
        x:
        y:
      }
    })

    io.graphqlMutation({ gql, args }) {
       io.debug(gql)
       io.info(args)
       // do more things

       await mutation = apollo.defaultClient.mutate({

       })

      // do more things
      return
    }
    */
    return apollo.defaultClient.mutate({
      mutation: CREATE_UNVERIFIED_WORKER,
      variables: args,
      optimisticResponse: {
        __typename: 'Mutation',
        createUnverifiedWorker: {
          __typename: 'Worker',
          id: -1,
        },
      },
    });
  },
  createAlienWorker(args) {
    const CREATE_ALIEN_WORKER = gql`
        mutation createAlienWorker(
          $organisationId:String!,
          $name: String!,
          $identification: String!,
          $phone: String
        ) {
          createAlienWorker (
            organisationId:$organisationId,
            name: $name,
            identification: $identification,
            phone: $phone
          ) {
            __typename,
            id,
          }
        }
      `;
    // return io.graphqlMutation({ gql: CREATE_ALIEN_WORKER });
    return apollo.defaultClient.mutate({
      mutation: CREATE_ALIEN_WORKER,
      variables: args,
      optimisticResponse: {
        __typename: 'Mutation',
        createAlienWorker: {
          __typename: 'AlienWorker',
          id: -1,
        },
      },
    });
  },
  updateWorker(args) {
    const UPDATE_WORKER = gql`
      mutation updateWorker(
        $id: String!,
        $name: String,
        $fcm: String,
        $facebook: String,
        $email: EmailAddress,
        $google: String,
        $identification: String,
        $notificationHiredUpdates: [ String ],
        $notificationNotHiredUpdates: [ String ],
        $notificationChat: [ String ],
        $notificationJobWatch: [ String ],
        $locationFrom: String,
        $paymentCoordinates: String,
        $qualificationIdentificationFileRef: String,
        $paycycleUseAlternatePayment: Boolean,
        $alternatePaymentCoordinates: String,
        $lockCode: String,
      ) {
        updateWorker(
          id: $id,
          name: $name,
          fcm: $fcm,
          facebook: $facebook,
          email: $email,
          google: $google,
          identification: $identification,
          notificationHiredUpdates: $notificationHiredUpdates,
          notificationNotHiredUpdates: $notificationNotHiredUpdates,
          notificationChat: $notificationChat,
          notificationJobWatch: $notificationJobWatch,
          locationFrom: $locationFrom,
          paymentMethod: $paymentMethod,
          paymentCurrency: $paymentCurrency,
          paymentCoordinates: $paymentCoordinates
          qualificationIdentificationFileRef: $qualificationIdentificationFileRef,
          paycycleUseAlternatePayment: $paycycleUseAlternatePayment,
          alternatePaymentMethod:$alternatePaymentMethod,
          alternatePaymentCurrency:$alternatePaymentCurrency,
          alternatePaymentCoordinates:$alternatePaymentCoordinates,
          lockCode: $lockCode,
        ) {
          __typename,
          id,
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: UPDATE_WORKER,
      variables: args,
    });
  },

  updateUnverifiedWorker(args) {
    Vue.$log.info('WorkerService.updateUnverifiedWorker', args);
    const UPDATE_UNVERIFIED_WORKER = gql`
      mutation updateUnverifiedWorker(
        $organisationId: ID!,
        $id: String!,
        $name: String,
        $phone: String
        $fcm: String,
        $facebook: String,
        $email: EmailAddress,
        $google: String,
        $identification: String,
        $notificationHiredUpdates: [ String ],
        $notificationNotHiredUpdates: [ String ],
        $notificationChat: [ String ],
        $notificationJobWatch: [ String ],
        $locationFrom: String,
        $qualificationIdentificationFileRef: String,
      ) {
        updateUnverifiedWorker(
          organisationId: $organisationId,
          id: $id,
          name: $name,
          phone: $phone,
          fcm: $fcm,
          facebook: $facebook,
          email: $email,
          google: $google,
          identification: $identification,
          notificationHiredUpdates: $notificationHiredUpdates,
          notificationNotHiredUpdates: $notificationNotHiredUpdates,
          notificationChat: $notificationChat,
          notificationJobWatch: $notificationJobWatch,
          locationFrom: $locationFrom,
          qualificationIdentificationFileRef: $qualificationIdentificationFileRef,
        ) {
          __typename,
          id,
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: UPDATE_UNVERIFIED_WORKER,
      variables: args,
    });
  },

  updateAlienWorker(args) {
    Vue.$log.info({ args });
    const UPDATE_ALIEN_WORKER = gql`
      mutation updateAlienWorker(
        $organisationId: ID!,
        $alienWorkerId: ID!,
        $name: String,
        $identification: String,
        $phone: String,
        $paymentCoordinates: String,
        $paymentCurrency: CURRENCY
      ) {
        updateAlienWorker(
          organisationId: $organisationId,
          alienWorkerId: $alienWorkerId,
          name: $name,
          identification: $identification,
          phone: $phone,
          paymentCoordinates: $paymentCoordinates,
          paymentCurrency: $paymentCurrency
        ) {
          __typename,
          id,
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: UPDATE_ALIEN_WORKER,
      variables: args,
    });
  },

  verifyAlienWorker(args) {
    Vue.$log.info(args);
    const VERIFY_ALIEN = gql`
        mutation verifyAlienWorker(
          $alienWorkerId: ID!
        ) {
          verifyAlienWorker (
            alienWorkerId: $alienWorkerId
          ) {
            __typename,
            id,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: VERIFY_ALIEN,
      variables: args,
    });
  },

  addWorkerJob(args) {
    Vue.$log.info('JobService.addWorkerJob');
    Vue.$log.info(args);
    const ADD_WORKER_JOB = gql`
        mutation addWorkerJob($workerId: String!, $id: ID!, $to: JOB_STAGE_WORKER!) {
          addWorkerJob (workerId: $workerId, id: $id, to: $to) {
            __typename,
            id,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: ADD_WORKER_JOB,
      variables: {
        workerId: args.workerId || '',
        id: args.id || '',
        to: args.to,
      },
    });
  },

  migrateWorkerJob(args) {
    Vue.$log.info('JobService.migrateWorkerJob');
    Vue.$log.info(args);
    const MIGRATE_WORKER_JOB = gql`
      mutation migrateWorkerJob($workerId: String!, $id: ID!, $from: JOB_STAGE_WORKER!, $to: JOB_STAGE_WORKER!) {
        migrateWorkerJob(workerId: $workerId, id: $id, from: $from, to: $to) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: MIGRATE_WORKER_JOB,
      variables: {
        workerId: args.workerId || '',
        id: args.id || '',
        from: args.from,
        to: args.to,
      },
    });
  },

  dismissWorkerShift(args) {
    Vue.$log.info('JobService.dismissWorkerShift', args);
    const DISMISS_WORKER_SHIFT = gql`
      mutation dismissWorkerShift($id: ID!, $dismissReason: String) {
        dismissWorkerShift(id: $id, dismissReason: $dismissReason) {
          __typename
          id
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: DISMISS_WORKER_SHIFT,
      variables: {
        id: args.id,
        dismissReason: args.dismissReason || '',
      },
    });
  },
};

const WorkerService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local WorkerService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$WorkerService = services;
  },
};

export default WorkerService;
