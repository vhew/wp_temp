import gql from 'graphql-tag';
import Vue from 'vue';
// import store from '@/assets/js/store';
import apollo from '@/assets/js/apollo';

const services = {
  createJob(args) {
    Vue.$log.info('calling JobService.createJob', args);
    const CREATE_JOB = gql`
        mutation createJob(
          $organisationId: ID!,
          $marketplaceId: ID,
          $title: String!,
          $description: String,
          $requirementDescription: String,
          $address: String,
          $location: GraphQLJSON,
          $locationCoordinates: GraphQLJSON,
          $type: JOB_TYPE,
          $role: ROLE_TYPE,
          $jobRoleId: String,
          $paymentMethodsIds: [ ID ],
          $paymentOption: String,
          $renumerationValue: Float,
          $renumerationCurrency: String,
          $hoursPerWeek: Float,
          $isOpenEnded: Boolean,
          $allowFlexibleTime: Boolean,
          $allowAdvancedPay: Boolean,
          $listingStartDate: GraphQLDateTime,
          $listingEndDate: GraphQLDateTime,
          $jobSkillRequirements: [ GraphQLJSON ],
          $requirementMinExperienceLevel: EXPERIENCE_LEVEL,
          $fulltimeStandardHoursStart: String,
          $fulltimeStandardHoursEnd: String,
          $fulltimeOvertimeRenumerationPerHour: Float,
          $fulltimeLeavePersonalAccruePerDay: Float,
          $fulltimeLeaveSickAccruePerDay: Float,
          # $fulltimePublicHolidays: [ Date ],
          $fulltimeNonWorkWeekDays: [ Int ],
          $benefits: [ String ],
          $jobTypeConversion: [ JOB_TYPE_CONVERSION ],
          $contractAcceptanceTest: String,
          $private: Boolean,
          $minShiftsBalance: Int,
          $isPortfolioRequired: Boolean,
          $notes: String,
          $proxyCompanyName: String,
          $proxyCompanyDescription: String,
          $proxyEmployerName: String,
          $proxyCompanyContact: String,
          $proxyCompanyAddress: String,
          $proxyCompanyWebsiteUrl: String,
          $affiliateIds: [ID],
          $purpose: String
          $workingDays: [String]
          $workingHours: [String]
          $lunchHour: [String]
          $sickLeaveDays: Int
          $personalLeaveDays: Int
          $specialLeaveDays: Int
          $requirementDemographicGender: [REQUIREMENT_GENDER]
          $availableCountries: [COUNTRY]
        ) {
          createJob (
            organisationId: $organisationId,
            marketplaceId: $marketplaceId,
            title: $title,
            description: $description,
            requirementDescription: $requirementDescription,
            address: $address,
            location: $location,
            locationCoordinates: $locationCoordinates,
            type: $type,
            role: $role,
            jobRoleId: $jobRoleId,
            paymentMethodsIds: $paymentMethodsIds,
            paymentOption: $paymentOption,
            renumerationValue: $renumerationValue,
            renumerationCurrency: $renumerationCurrency,
            hoursPerWeek: $hoursPerWeek,
            isOpenEnded: $isOpenEnded,
            allowAdvancedPay: $allowAdvancedPay,
            allowFlexibleTime: $allowFlexibleTime,
            listingStartDate: $listingStartDate,
            listingEndDate: $listingEndDate,
            jobSkillRequirements: $jobSkillRequirements,
            requirementMinExperienceLevel: $requirementMinExperienceLevel,
            fulltimeStandardHoursStart: $fulltimeStandardHoursStart,
            fulltimeStandardHoursEnd: $fulltimeStandardHoursEnd,
            fulltimeOvertimeRenumerationPerHour: $fulltimeOvertimeRenumerationPerHour,
            fulltimeLeavePersonalAccruePerDay: $fulltimeLeavePersonalAccruePerDay,
            fulltimeLeaveSickAccruePerDay: $fulltimeLeaveSickAccruePerDay,
            # fulltimePublicHolidays: $fulltimePublicHolidays,
            fulltimeNonWorkWeekDays: $fulltimeNonWorkWeekDays,
            benefits: $benefits,
            jobTypeConversion: $jobTypeConversion,
            contractAcceptanceTest: $contractAcceptanceTest,
            private: $private,
            minShiftsBalance: $minShiftsBalance,
            isPortfolioRequired: $isPortfolioRequired,
            notes: $notes,
            proxyCompanyName: $proxyCompanyName,
            proxyCompanyDescription: $proxyCompanyDescription,
            proxyEmployerName: $proxyEmployerName,
            proxyCompanyContact: $proxyCompanyContact,
            proxyCompanyAddress: $proxyCompanyAddress,
            proxyCompanyWebsiteUrl: $proxyCompanyWebsiteUrl,
            affiliateIds: $affiliateIds,
            purpose: $purpose,
            workingDays: $workingDays,
            workingHours: $workingHours,
            lunchHour: $lunchHour,
            sickLeaveDays: $sickLeaveDays,
            personalLeaveDays: $personalLeaveDays,
            specialLeaveDays: $specialLeaveDays,
            requirementDemographicGender: $requirementDemographicGender,
            availableCountries: $availableCountries,
          ) {
            __typename,
            id,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: CREATE_JOB,
      variables: args,
      optimisticResponse: {
        __typename: 'Mutation',
        createJob: {
          __typename: 'Job',
          id: -1,
        },
      },
    });
  },

  updateJob(args) {
    Vue.$log.debug('calling JobService.updateJob', args);
    const UPDATE_JOB = gql`
      mutation updateJob(
        $id: ID!,
        $organisationId: ID!,
        $marketplaceId: ID,
        $title: String,
        $description: String,
        $requirementDescription: String,
        $address: String,
        $location: GraphQLJSON,
        $locationCoordinates: GraphQLJSON,
        $type: JOB_TYPE,
        $role: ROLE_TYPE,
        $paymentMethodsIds: [ ID ],
        $paymentOption: String,
        $renumerationValue: Float,
        $renumerationCurrency: String,
        $minShiftsBalance: Int,
        $fulltimeStandardHoursStart: String,
        $fulltimeStandardHoursEnd: String,
        $fulltimeOvertimeRenumerationPerHour: Float,
        $fulltimeLeavePersonalAccruePerDay: Float,
        $fulltimeLeaveSickAccruePerDay: Float,
        $fulltimeNonWorkWeekDays: [ Int ],
        $fulltimePublicHolidays: [ GraphQLDate ],
        $benefits: [ String ],
        $jobTypeConversion: [ JOB_TYPE_CONVERSION ],
        $contractAcceptanceTest: String,
        $requirementNumberOfContracts: Int,
        $requirementSpokenLanguages: [ String ],
        $requirementWrittenLanguages: [ String ],
        $requirementDocuments: [ String ],
        $requirementScreeningChannels: [ String ],
        $requirementRequiredSkills: [ REQUIREMENT_SKILL ],
        $requirementVenueWorked: [ REQUIREMENT_VENUE_EXPERIENCE ],
        $requirementMinExperienceLevel: EXPERIENCE_LEVEL,
        $requirementPreferredSkills: [ REQUIREMENT_SKILL ],
        $requirementDressCodes: [ REQUIREMENT_DRESS_CODE ],
        $requirementCustomDressCode: String,
        $requirementDemographicAgeGroup: [ REQUIREMENT_AGE_GROUP ],
        $requirementDemographicGender: [REQUIREMENT_GENDER],
        $allowAdvancedPay: Boolean,
        $isOpenEnded: Boolean,
        $proxyCompanyName: String,
        $proxyCompanyDescription: String,
        $proxyEmployerName: String,
        $proxyCompanyContact: String,
        $proxyCompanyAddress: String,
        $affiliateIds: [ID]
        $purpose: String
        $jobSkillRequirements: [ GraphQLJSON ],
        $jobRoleId: String,
        $workingDays: [String]
        $workingHours: [String]
        $lunchHour: [String]
        $sickLeaveDays: Int
        $personalLeaveDays: Int
        $specialLeaveDays: Int
        $listingStartDate: GraphQLDateTime,
        $listingEndDate: GraphQLDateTime,
        $availableCountries: [COUNTRY]
      ) {
        updateJob(
          id: $id,
          organisationId: $organisationId,
          marketplaceId: $marketplaceId,
          title: $title,
          description: $description,
          requirementDescription: $requirementDescription,
          address: $address,
          location: $location,
          locationCoordinates: $locationCoordinates,
          type: $type,
          role: $role,
          paymentMethodsIds: $paymentMethodsIds,
          paymentOption: $paymentOption,
          renumerationValue: $renumerationValue,
          renumerationCurrency: $renumerationCurrency,
          minShiftsBalance: $minShiftsBalance,
          fulltimeStandardHoursStart: $fulltimeStandardHoursStart,
          fulltimeStandardHoursEnd: $fulltimeStandardHoursEnd,
          fulltimeOvertimeRenumerationPerHour: $fulltimeOvertimeRenumerationPerHour,
          fulltimeLeavePersonalAccruePerDay: $fulltimeLeavePersonalAccruePerDay,
          fulltimeLeaveSickAccruePerDay: $fulltimeLeaveSickAccruePerDay,
          fulltimePublicHolidays: $fulltimePublicHolidays,
          fulltimeNonWorkWeekDays: $fulltimeNonWorkWeekDays,
          benefits: $benefits,
          jobTypeConversion: $jobTypeConversion,
          contractAcceptanceTest: $contractAcceptanceTest,
          requirementNumberOfContracts: $requirementNumberOfContracts,
          requirementSpokenLanguages: $requirementSpokenLanguages,
          requirementWrittenLanguages: $requirementWrittenLanguages,
          requirementDocuments: $requirementDocuments,
          requirementScreeningChannels: $requirementScreeningChannels,
          requirementRequiredSkills: $requirementRequiredSkills,
          requirementVenueWorked: $requirementVenueWorked,
          requirementMinExperienceLevel: $requirementMinExperienceLevel,
          requirementPreferredSkills: $requirementPreferredSkills,
          requirementDressCodes: $requirementDressCodes,
          requirementCustomDressCode: $requirementCustomDressCode,
          requirementDemographicAgeGroup: $requirementDemographicAgeGroup,
          requirementDemographicGender: $requirementDemographicGender,
          allowAdvancedPay: $allowAdvancedPay,
          isOpenEnded: $isOpenEnded,
          proxyCompanyName: $proxyCompanyName,
          proxyCompanyDescription: $proxyCompanyDescription,
          proxyEmployerName: $proxyEmployerName,
          proxyCompanyContact: $proxyCompanyContact,
          proxyCompanyAddress: $proxyCompanyAddress,
          affiliateIds: $affiliateIds,
          purpose: $purpose,
          jobSkillRequirements: $jobSkillRequirements,
          jobRoleId: $jobRoleId,
          workingDays: $workingDays,
          workingHours: $workingHours,
          lunchHour: $lunchHour,
          sickLeaveDays: $sickLeaveDays,
          personalLeaveDays: $personalLeaveDays,
          specialLeaveDays: $specialLeaveDays,
          listingStartDate: $listingStartDate,
          listingEndDate: $listingEndDate,
          availableCountries: $availableCountries,
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_JOB,
      variables: args,
      optimisticResponse: {
        __typename: 'Mutation',
        updateJob: {
          id: -1,
          __typename: 'Job',
        },
      },
    });
  },

  updateJobsMarketplace(args) {
    Vue.$log.debug('JobService.updateJobsMarketplace', args);
    const UPDATE_JOBS_MARKETPLACE = gql`
      mutation updateJobsMarketplace(
        $organisationId: ID!
      ) {
        updateJobsMarketplace(
          organisationId: $organisationId,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_JOBS_MARKETPLACE,
      variables: args,
    });
  },

  createMarketplace(args) {
    Vue.$log.debug('JobService.createMarketplace', args);
    const CREATE_MARKETPLACE = gql`
      mutation createMarketplace(
        $name: String!,
        $organisationId: ID,
        $imageUrl: String
        $description: String
      ) {
        createMarketplace(
          name: $name,
          organisationId: $organisationId,
          imageUrl: $imageUrl,
          description: $description,
        ) {
          __typename
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CREATE_MARKETPLACE,
      variables: args,
    });
  },

  createJobContract(args) {
    Vue.$log.debug('JobService.createJobContract', args);
    const CREATE_JOB_CONTRACT = gql`
      mutation createJobContract(
        $jobId: ID,
        $jobStartDate: GraphQLDateTime,
        $renumerationValue: String
        $employerId: ID,
        $signingDateEmployer: GraphQLDateTime,
        $contract: String,
      ) {
        createJobContract(
          jobId: $jobId,
          jobStartDate: $jobStartDate,
          renumerationValue: $renumerationValue,
          employerId: $employerId,
          signingDateEmployer: $signingDateEmployer,
          contract: $contract,
        ) {
          __typename
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: CREATE_JOB_CONTRACT,
      variables: args,
    });
  },

  addJobShift(args) {
    Vue.$log.info('JobService.addJobShift', args);
    const ADD_JOB_SHIFT = gql`
      mutation addJobShift($jobId: ID!, $startTime: GraphQLDateTime!, $endTime: GraphQLDateTime, $divisible: Boolean) {
      # mutation addJobShift(
      #   $jobId: ID!,
      #   $startTime: GraphQLGraphQLGraphQLDateTime!,
      #   $endTime: GraphQLGraphQLGraphQLDateTime
      #  ) {
        addJobShift(
          jobId: $jobId,
          startTime: $startTime,
          endTime: $endTime,
          divisible: $divisible
        ) {
          __typename,
          id,
          shifts {
            id
          }
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ADD_JOB_SHIFT,
      variables: args,
      // variables: {
      //   jobId: args.jobId || '',
      //   startTime: args.startTime || '',
      //   endTime: args.endTime || '',
      // },
    });
  },

  updateShift(args) {
    Vue.$log.info('JobService.updateShift', args);
    const UPDATE_SHIFT = gql`
      mutation updateShift(
        $id: ID!
        $startTime: GraphQLDateTime
        $endTime: GraphQLDateTime
        $deductions: [DeductionInput]
        $taxConfigurations: TaxConfigurationInput
        $divisible: Boolean
        $leaveSickDays: Float
        $leaveSickUpdateDate: GraphQLDateTime
        $leavePersonalDays: Float
        $leavePersonalUpdateDate: GraphQLDateTime
        $leaveSpecialDays: Float
        $leaveSpecialUpdateDate: GraphQLDateTime
        $currentTimesheetConfirm: String
        $dismissReason: String
        $completedAt: GraphQLDateTime
        $salary: Float
        $renumerationCurrency: String
      ) {
        updateShift(
          id: $id,
          startTime: $startTime,
          endTime: $endTime,
          deductions: $deductions,
          taxConfigurations: $taxConfigurations,
          divisible: $divisible,
          leaveSickDays: $leaveSickDays,
          leaveSickUpdateDate: $leaveSickUpdateDate,
          leavePersonalDays: $leavePersonalDays,
          leavePersonalUpdateDate: $leavePersonalUpdateDate,
          leaveSpecialDays: $leaveSpecialDays,
          leaveSpecialUpdateDate: $leaveSpecialUpdateDate,
          currentTimesheetConfirm: $currentTimesheetConfirm,
          dismissReason: $dismissReason
          completedAt: $completedAt
          salary: $salary
          renumerationCurrency: $renumerationCurrency
        ) {
          __typename
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_SHIFT,
      variables: args,
    });
  },

  offerShift(args) {
    Vue.$log.info('JobService.offerShift', args);
    const OFFER_SHIFT = gql`
      mutation offerShift($id: ID!, $workerId: String!) {
        offerShift(
          id: $id,
          workerId: $workerId
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: OFFER_SHIFT,
      variables: {
        id: args.id,
        workerId: args.workerId || '',
      },
    });
  },

  rejectShift(args) {
    Vue.$log.info('JobService.rejectShift', args);
    const REJECT_SHIFT = gql`
      mutation rejectShift($id: ID!, $workerId: String!) {
        rejectShift(
          id: $id,
          workerId: $workerId
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: REJECT_SHIFT,
      variables: {
        id: args.id,
        workerId: args.workerId,
      },
    });
  },

  updateLeave(args) {
    Vue.$log.info('JobService.updateLeave', args);
    const UPDATE_LEAVE = gql`
      mutation updateLeave(
        $id: ID!,
        $leaveSickDays: Float,
        $leaveSickUpdateDate: GraphQLDateTime,
        $leavePersonalDays: Float,
        $leavePersonalUpdateDate: GraphQLDateTime,
        $leaveSpecialDays: Float,
        $leaveSpecialUpdateDate: GraphQLDateTime,
        $currentTimesheetConfirm: String,
      ) {
        updateLeave(
          id: $id,
          leaveSickDays: $leaveSickDays,
          leaveSickUpdateDate: $leaveSickUpdateDate,
          leavePersonalDays: $leavePersonalDays,
          leavePersonalUpdateDate: $leavePersonalUpdateDate,
          leaveSpecialDays: $leaveSpecialDays,
          leaveSpecialUpdateDate: $leaveSpecialUpdateDate,
          currentTimesheetConfirm: $currentTimesheetConfirm,
        ) {
          __typename,
          id,
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: UPDATE_LEAVE,
      variables: args,
    });
  },

  assignUnverifiedWorkerToShift(args) {
    const ASSIGNUNVERIFIEDWORKERTOSHIFT = gql`
      mutation assignUnverifiedWorkerToShift($id: ID!, $workerId: String!) {
        assignUnverifiedWorkerToShift(
          id: $id,
          workerId: $workerId
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ASSIGNUNVERIFIEDWORKERTOSHIFT,
      variables: args,
    });
  },

  assignAlienWorkerToShift(args) {
    const ASSIGN_ALIEN_WORKER_TOSHIFT = gql`
      mutation assignAlienWorkerToShift($id: ID!, $alienWorkerId: ID!) {
        assignAlienWorkerToShift(
          id: $id,
          alienWorkerId: $alienWorkerId
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ASSIGN_ALIEN_WORKER_TOSHIFT,
      variables: args,
    });
  },

  calculateSiblingShifts: (args) => {
    Vue.$log.info('JobService.calculateSiblingShifts', args);
    const CALCULATE_SIBLING_SHIFTS = gql`
      mutation calculateSiblingShifts($jobId: ID!) {
        calculateSiblingShifts(jobId: $jobId)
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CALCULATE_SIBLING_SHIFTS,
      variables: args,
    });
  },

  acceptLeave: (args) => {
    Vue.$log.info('JobService.acceptLeave', args);
    const ACCEPT_LEAVE = gql`
      mutation acceptLeave(
        $id: ID!,
        $leaveToken: GraphQLJSON!,
      ) {
        acceptLeave(
          id: $id,
          leaveToken: $leaveToken,
        ) {
          id
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: ACCEPT_LEAVE,
      variables: args,
    });
  },

  rejectLeave: (args) => {
    Vue.$log.info('JobService.rejectLeave', args);
    const REJECT_LEAVE = gql`
      mutation rejectLeave(
        $id: ID!,
        $leaveToken: GraphQLJSON!,
      ) {
        rejectLeave(
          id: $id,
          leaveToken: $leaveToken,
        ) {
          id
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: REJECT_LEAVE,
      variables: args,
    });
  },
};

const JobService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local JobService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$JobService = services;
  },
};

export default JobService;
