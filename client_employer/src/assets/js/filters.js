import Vue from 'vue';
import moment from 'moment';
import MarkedDownIt from 'markdown-it';
import store from './store';

const getFormattedPaymentOption = (paymentOption) => {
  switch (paymentOption) {
    case 'MONTHLY':
      return '/month';
    case 'HOURLY':
      return '/hour';
    case 'FIXED':
      return '/fixed';
    default:
      return '';
  }
};

Vue.filter('currency', (value, c, paymentOptionType = '') => {
  const roundedAmount = Number(parseFloat(value).toFixed(0)).toLocaleString('en');
  const currency = c || store.getters.localCurrency;
  const paymentOption = getFormattedPaymentOption(paymentOptionType);

  if (currency === 'MMK') return `${roundedAmount} Ks${paymentOption}`;
  if (currency === 'HKD') return `HKD ${roundedAmount}${paymentOption}`;
  if (currency === 'VND') return `${roundedAmount}${paymentOption} VND`;
  if (currency === 'USD') return `$${roundedAmount}${paymentOption}`;
  return `$${roundedAmount}${paymentOption}`;
});

Vue.filter('dateTime', (date, format) => moment(date).format(format));

Vue.filter('fromNow', date => moment(date).fromNow());

Vue.filter('militaryTimeToMeridian', (time24hr) => {
  // Check correct time format and split into components
  let time = time24hr;
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice(1); // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join(''); // return adjusted time or original string
});

const md = new MarkedDownIt();
Vue.filter('markedDown', text => md.render(text));
