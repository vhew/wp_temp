// isEmptyArray :: a -> Boolean
const isObject = v => typeof v === 'object';
const isEmptyArray = v => Array.isArray(v) && v.length === 0;
const isEmptyObject = v => isObject(v) && JSON.stringify(v) === '{}';

// hasValue :: a -> Boolean
const hasValue = v => !(isEmptyArray(v) || !v);

const services = {
  // required :: a -> true | String
  required: v => hasValue(v) || '* Required',
  integer: v => (v ? Number.isSafeInteger(Number.parseFloat(v, 10)) || 'Integers only' : true),
  number: v => (v ? Number.isFinite(Number.parseFloat(v, 10)) || 'Invalid Number' : true),
  phone: v => (v ? !!v.match(/^(?!,)(,?\s?([+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[- ./0-9]{6,20}))+$/) || 'Invalid Phone Number' : true),
  dayOfMonth: v => (v ? !!v.match(/^3[01]|[12][0-9]|[1-9]$/) || 'Invalid day of month' : true),
  payCycle: v => (v ? !!v.match(/^(?!,)(,?\s?(3[01]|[12][0-9]|[1-9]))+$/) || 'Invalid Format' : true),
  email: v => (v ? !!v.match(/^((([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))*$/) || 'Invalid email' : true),
  url: v => (v ? !!v.match(/^([(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*))*$/) || 'Invalid URL' : true),
  shortString: v => `${v}`.length <= 60 || 'Text too long. Min 60 characters',
  mediumString: v => `${v}`.length <= 500 || 'Text too long. Min 500 characters',
  longString: v => `${v}`.length <= 1000 || 'Text too long. Min 1000 characters',
  renumerationValue: v => Number.parseFloat(v, 10) > 0 || 'Invalid renumeration value',
  file: size => (file) => {
    if (file && file.size) {
      return file.size <= size * 1000000 ? true : `File can't be larger than ${size}MB`; // 5 MegaByte
    }
    return true;
  },
};

const ValidateService = {
  install(Vue) {
    Vue.$log.info('Installing the local ValidateService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$Validate = services;
  },
};

export default ValidateService;
