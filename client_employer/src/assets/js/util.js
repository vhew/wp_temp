/* eslint-disable no-console */
// const XLSX = require('xlsx');
// const PhoneValidator = require('awesome-phonenumber');
// const validator = require('validator');
// const moment = require('moment');
import XLSX from 'xlsx';
import PhoneValidator from 'awesome-phonenumber';
import validator from 'validator';
import moment from 'moment';
import lodash from 'lodash';
// import { truncate } from 'fs';

const util = {
  methods: {
    test: ({ a, b }) => a + b,

    /* Function Rules
    -----------------------------------------------------------------
    isX validateX {} => Boolean
    createX deleteX updateX readX format filterX getXfromY {} => {}
    generic: isX builtin_types => Boolean
    generic: filter builtin_types => builtin_types */

    // batchUpload
    readExcelFile: ({ file, fileResult }) => {
      let result = {};
      const reader = new FileReader();
      reader.onload = async () => {
        const data = new Uint8Array(fileResult);
        const workbook = await XLSX.read(data, { type: 'array' });
        const firstWorksheet = await workbook.Sheets[workbook.SheetNames[0]];
        result = await XLSX.utils.sheet_to_json(firstWorksheet, { header: 0 });
      };
      reader.readAsArrayBuffer(file);
      return {
        data: result,
      };
    },

    // validateWorkerJob: ({ data }) => Boolean,

    // batchUpload, xxxx ,xxx
    // isFileValid: ({file}) => Boolean,
    isValidFormat: ({ field, type }) => {
      let result = false;
      // let paymentCoordinates = new PhoneValidator(data.paymentCoordinates);
      // eslint-disable-next-line no-useless-escape
      const expDate = /[0-9]{4}\-[0-9]{2}\-[0-9]{2}/;
      // eslint-disable-next-line no-useless-escape
      const expTime = /[0-9]{2}\:[0-9]{2}\+[0-9]{2}\:[0-9]{2}/;
      const paymentMethods = ['ONGO'];
      const paymentCurrencys = ['MMK', 'USD'];
      const jobTypes = ['PART_TIME', 'FULL_TIME', 'CONTRACT'];
      if (type === 'PHONE') {
        const phone = new PhoneValidator(field);
        result = (!phone.isPossible());
      } else if (type === 'PAYMENT_METHOD') {
        result = (paymentMethods.filter(x => x === field).length > 0);
      } else if (type === 'PAYMENT_CURRENCY') {
        result = (paymentCurrencys.filter(x => x === field).length > 0);
      } else if (type === 'JOB_TYPE') {
        result = (jobTypes.filter(x => x === field).length > 0);
      } else if (type === 'NUMBER') {
        result = (validator.isNumeric(field.toString()));
      } else if (type === 'DATE') {
        result = (expDate.test(field));
      } else if (type === 'TIME') {
        result = (expTime.test(field));
      }
      return result;
    },

    isNull: ({ field }) => (field == null),

    // workerCreatePage
    formatPhoneNo: ({ phoneCode, phoneNo }) => {
      let result = '';
      const phone = phoneCode + phoneNo;
      if (phone.includes('+95') && phone.length === 11) {
        result = `${phone.slice(0, 3)}0${phone.slice(3)}`;
      }
      return {
        data: result,
      };
    },

    // workerListPage
    filterWorkers: ({ workers, hasJob }) => {
      let result = [];
      if (workers.length === 0) {
        result = [];
      }
      if (hasJob === true) {
        result = workers.filter(worker => worker.jobsHired.length > 0
          && worker.adoptedByWorker == null);
      } else {
        result = workers
          .filter(worker => worker.jobsHired.length === 0 && worker.adoptedByWorker == null)
          .sort((a, b) => ((a.name > b.name) ? 1 : -1));
      }
      return {
        data: result,
      };
    },

    // filterWorkerWithoutJob: ({ workers }) => {
    //   const result = [];
    //   return {
    //     data: result,
    //   };
    // },

    // filterArray: ({ String, condition }) => [String],

    // paymentDisbursementPage
    getBonusInfo: () => {
      const result = {};
      return {
        data: result,
      };
    },

    getShiftsFromJobs: ({ jobs }) => {
      let result = [];
      result = jobs.map(job => job.shifts);
      return {
        data: Array.prototype.concat.apply([], result),
      };
    },
    getShiftsFromWorkTokens: ({ workTokens }) => {
      let result = [];
      result = workTokens.map(workToken => workToken.shifts);
      return {
        data: Array.prototype.concat.apply([], result),
      };
    },
    filterShiftsByWorker: ({ shifts, worker }) => {
      // eslint-disable-next-line no-param-reassign
      shifts = shifts.filter(shift => shift.worker != null);
      let result = [];
      result = shifts.filter(shift => shift.worker.id === worker.id);
      return {
        data: result,
      };
    }, // TimeSheet Also Need
    getWorkTokenValuesFromShifts: ({ shifts }) => {
      let result = [];
      result = shifts.map(shift => shift.workTokenValues);
      return {
        data: Array.prototype.concat.apply([], result),
      };
    }, // TimeSheet Also Need
    // getPayInfoFromXXX: ({customerXXX: data}) => {pay:[]},
    getUniqueWorkersFromShifts: ({ shifts }) => {
      let workers = shifts.map(shift => shift.worker);
      workers = lodash.uniq(workers, worker => worker.id);
      const result = workers;

      return {
        data: result,
      };
    },
    getWorkersFromShifts: ({ shifts }) => {
      const workers = shifts.map(shift => shift.worker);
      const result = workers;
      return {
        data: result,
      };
    },
    getWorkerFromShifts: ({ shifts }) => {
      const workers = shifts.map(shift => shift.worker);
      const result = workers[0];
      return {
        data: result,
      };
    },

    getPayCycleFromMonth: (month, organisation) => {
      let result = {};
      const paycycle = organisation.payCycleMonthDays;
      const startDate = moment(`${month}-${paycycle[0]}`);
      // .subtract(1, 'months');
      const endDate = moment(`${month}-${paycycle[1]}`)
        .date(paycycle[1])
        .add(1, 'months');
        // .subtract(1, 'days');

      result = {
        startDate,
        endDate,
      };
      return result;
    },

    getCurrentPayCycleFromOragnisation: ({ organisation }) => {
      let result = {};
      const paycycle = organisation.payCycleMonthDays;
      const startDate = moment()
        .date(paycycle[0])
        .subtract(1, 'months')
        .startOf('day');
      const endDate = moment()
        .date(paycycle[1])
        .add(1, 'months')
        .subtract(1, 'days');

      result = {
        startDate,
        endDate,
      };
      return {
        data: result,
      };
    },
    // Old
    calendarStartDate(organisation, wroktokens) {
      const payCycle = organisation.payCycleMonthDays;
      const today = moment();
      let startTime = moment().set('date', payCycle[0]);
      const payCycleStart = moment().set('date', payCycle[0]);
      if (wroktokens.length > 0) {
        startTime = moment(wroktokens[0].checkinTime).add(1, 'day');
        if (startTime >= payCycleStart) {
          return payCycleStart;
        }
        return payCycleStart.add(-1, 'M');
      }
      this.$log.debug('startTime... ', startTime);
      this.$log.debug('payCycleStart... ', payCycleStart);
      if (today >= startTime) {
        return payCycleStart;
      }
      // this.$log.debug(moment(this.filteredWorkTokens[0].checkinTime).format('YYYY-MM-DD'));
      return payCycleStart.add(-1, 'M');
    },
    calendarEndDate(organisation, wroktokens) {
      const payCycle = organisation.payCycleMonthDays;
      let startTime = moment().set('date', payCycle[0]);
      const today = moment();
      const payCycleStart = moment().set('date', payCycle[0]);
      if (wroktokens.length > 0) {
        startTime = moment(wroktokens[0].checkinTime).add(1, 'day');
        if (startTime >= payCycleStart) {
          return moment().set('date', payCycle[1]).add(1, 'M');
        }
        return moment().set('date', payCycle[1]);
      }
      if (today >= startTime) {
        return moment().set('date', payCycle[1])
          .add(1, 'M');
      }
      return moment().set('date', payCycle[1]);
    },
    getCurrentPayCycleFromOragnisationForMTD: async ({ organisation }) => {
      let result = {};
      let startDate;
      let endDate;
      let currentPayCycleIdentifier;
      const paycycle = organisation.payCycleMonthDays;
      const payCycleIdentifiers = await Promise.all(organisation
        .payCycleAdvancements.map(payCycleAdvancement => payCycleAdvancement.payCycleIdentifier));
      // eslint-disable-next-line no-console
      console.log({ payCycleIdentifiers });
      if (payCycleIdentifiers.length === 0) {
        currentPayCycleIdentifier = moment().subtract(1, 'months');
      } else if (payCycleIdentifiers.length === 1) {
        // eslint-disable-next-line prefer-destructuring
        currentPayCycleIdentifier = moment(payCycleIdentifiers[0]);
        // eslint-disable-next-line no-console
        console.log({ currentPayCycleIdentifier });
      } else {
        currentPayCycleIdentifier = moment(payCycleIdentifiers[payCycleIdentifiers.length - 1]);
      }
      // eslint-disable-next-line no-console
      console.log({ currentPayCycleIdentifier });
      // console.log({ ass: (currentPayCycleIdentifier.format('MM') === moment().format('MM')) });
      if (payCycleIdentifiers.length > 0
      && currentPayCycleIdentifier.format('MM') === moment().format('MM')) {
        startDate = moment()
          .date(paycycle[0])
          .add(3, 'days');
        endDate = moment()
          .date(paycycle[1])
          .add(1, 'months')
          .subtract(1, 'days');
      } else {
        startDate = moment()
          .date(paycycle[0])
          .subtract(1, 'months')
          .add(3, 'days');
        endDate = moment()
          .date(paycycle[1])
          .subtract(1, 'days');
      }
      result = {
        startDate,
        endDate,
      };
      // eslint-disable-next-line no-console
      console.log({ resultDate: result });
      return {
        data: result,
      };
    },

    getMTDFromFiatTokens: ({ fiatTokens, timeRange }) => {
      // eslint-disable-next-line no-console
      console.log(({ _getFiatTokens: timeRange }));
      let result = {};
      let filteredFiatTokens = [];
      // eslint-disable-next-line no-console
      console.log({ timeRange });
      // eslint-disable-next-line arrow-body-style
      filteredFiatTokens = fiatTokens.filter((fiattoken) => {
        return (moment.utc(fiattoken.workToken.checkinTime).isAfter(moment.utc(timeRange.from).add(-1, 'day'))
          && moment.utc(fiattoken.workToken.checkoutTime).isBefore(moment.utc(timeRange.to).add(1, 'day')));
      });
      const amountArray = filteredFiatTokens.map(fiattoken => fiattoken.amount);
      result = amountArray.reduce((a, b) => a + b, 0);
      return {
        data: result,
      };
    },

    filterBonusWorkTokens: ({ workTokens }) => {
      let result = [];
      result = workTokens.filter((workToken) => {
        if (workToken.type === 'BONUS') {
          return true;
        }
        return false;
      });
      return {
        data: result,
      };
    },

    filterStandardWorkTokenValues: ({ workTokenValues }) => {
      let result = [];
      result = workTokenValues.filter((workTokenValue) => {
        if (workTokenValue == null) {
          return false;
        }
        if (workTokenValue.expectedWorkTokenType == null) {
          return false;
        }
        if (workTokenValue.expectedWorkTokenType === 'STANDARD') {
          return true;
        }
        if (workTokenValue.expectedWorkTokenType === 'PRODUCT') {
          return true;
        }
        return false;
      });
      return {
        data: result,
      };
    },

    filterBonusWorkTokenValues: ({ workTokenValues }) => {
      let result = [];
      result = workTokenValues.filter((workTokenValue) => {
        if (workTokenValue == null) {
          return false;
        }
        if (workTokenValue.expectedWorkTokenType == null) {
          return false;
        }
        if (workTokenValue.expectedWorkTokenType === 'BONUS') {
          return true;
        }
        return false;
      });
      return {
        data: result,
      };
    },

    filterDeductionWorkTokenValues: ({ workTokenValues }) => {
      let result = [];
      result = workTokenValues.filter((workTokenValue) => {
        if (workTokenValue == null) {
          return false;
        }
        if (workTokenValue.expectedWorkTokenType == null) {
          return false;
        }
        if (workTokenValue.expectedWorkTokenType === 'DEDUCTION') {
          return true;
        }
        return false;
      });
      return {
        data: result,
      };
    },

    getPaymentDataFromWorkTokenValues: ({ workTokenValues }) => {
      let result = {};
      if (workTokenValues.length > 0) {
        result = {
          method: workTokenValues[0].method,
          currency: workTokenValues[0].currency,
          coordinates: workTokenValues[0].coordinates,
        };
      }
      return {
        data: result,
      };
    },

    getTotalAmountFromWorkTokenValues: ({ workTokenValues }) => {
      let result = 0;
      const amounts = [];
      let totalAmount = 0;
      workTokenValues.forEach((e) => {
        if (e.workToken.confirmed === true) {
          amounts.push(e.amount);
        }
      });
      totalAmount = amounts.reduce((a, b) => a + b, 0);
      result = totalAmount;
      return { data: result };
    },

    getDifferenceAmountFromWorkTokenValues: ({ workTokenValues }) => {
      let result = 0;
      const amounts = [];
      let differenceAmount = 0;
      workTokenValues.forEach((e) => {
        if (e.workToken.confirmed === true) {
          amounts.push(e.amountConflictDifference || 0);
        }
      });
      differenceAmount = amounts.reduce((a, b) => a + b, 0);
      result = differenceAmount;
      return { data: result };
    },

    getTotalFeeFromWorkTokenValues: ({ workTokenValues }) => {
      const fees = [];
      let totalFee = 0;
      workTokenValues.forEach((e) => {
        if (e.fee != null && e.workToken.confirmed === true) {
          fees.push(e.fee);
        }
      });
      totalFee = fees.reduce((a, b) => a + b, 0);
      return {
        data: {
          // method: workTokenValues[0].method,
          // currency: workTokenValues[0].currency,
          amount: totalFee,
        },
      };
    },

    getWorkTokenValuesFromWorkTokens: ({ workTokens }) => {
      let result = [];
      result = workTokens.map(workToken => workToken.workTokenValue);
      return {
        data: Array.prototype.concat.apply([], result),
      };
    },

    findShiftByWorker: ({ shifts, worker }) => {
      let result = {};
      result = shifts.find(shift => shift.worker.id === worker.id);
      return {
        data: result,
      };
    },

    findJobByShift: ({ jobs, shift }) => {
      let result = {};
      result = jobs.find(job => job.shift.id === shift.id);
      return {
        data: result,
      };
    },

    // // timeSheet
    filterUncomfirmedJobsByWorkTokens: ({ jobs, workTokens }) => {
      let result = [];
      if (jobs == null && workTokens == null) {
        result = [];
      }
      const uncomfirmedWorkTokens = workTokens
        .filter(workToken => workToken.confirmed == null);
      const unconfirmedShiftIds = uncomfirmedWorkTokens
        .map(workToken => workToken.shift.id);
      result = jobs.filter((job) => {
        let includeCount = 0;
        job.shifts.forEach((shift) => {
          if (unconfirmedShiftIds.includes(shift.id)) {
            includeCount += 1;
          }
        });
        if (includeCount > 0) {
          return true;
        }
        return false;
      });
      return {
        data: result,
      };
    },

    filterUnpaidJobsByWorkTokens: ({ jobs, workTokens }) => {
      let result = [];
      if (jobs == null && workTokens == null) {
        result = [];
      }
      // eslint-disable-next-line no-console
      console.log({ workTokens });
      const unpaidWorkTokens = workTokens
        .filter(workToken => (workToken.confirmed === true
            && workToken.fiatTokens.length === 0));
      const unpaidShiftIds = unpaidWorkTokens
        .map(workToken => workToken.shift.id);
      result = jobs.filter((job) => {
        let includeCount = 0;
        job.shifts.forEach((shift) => {
          if (unpaidShiftIds.includes(shift.id)) {
            includeCount += 1;
          }
        });
        if (includeCount > 0) {
          return true;
        }
        return false;
      });
      return {
        data: result,
      };
    },

    filterShiftsByWorkToken: () => {
      const result = [];
      return {
        data: result,
      };
    },

    filterJobsByShift: ({ jobs, shift }) => {
      let result = [];
      result = jobs.filter(job => job.shift.id === shift.id);
      return {
        data: result,
      };
    },

    getCreatedDateFromWorkTokenValues: ({ workTokenValues }) => {
      let result = null;
      result = workTokenValues[0].createdAt;
      return {
        data: result,
      };
    },
    getWorkTokensFromShifts: ({ shifts }) => {
      let result = [];
      result = shifts.map(shift => shift.workTokenValue);
      return {
        data: result,
      };
    },

    filterUnconfirmedWorkTokens: () => {
      const result = [];
      return {
        data: result,
      };
    },

    isShiftsAlreadyChecked: () => {
      const result = true;
      return {
        data: result,
      };
    },
    isEachWorkTokenHasFiatToken: ({ workTokens }) => {
      let result = true;
      let count = 0;
      workTokens.forEach((workToken) => {
        if (workToken.fiatTokens.length > 0) {
          count += 1;
        }
      });
      if (count > 0) {
        result = true;
      } else {
        result = false;
      }
      return {
        data: result,
      };
    },
    isEachWorkTokenValueHasFee: ({ workTokenValues }) => {
      let result = true;
      let count = 0;
      workTokenValues.forEach((workTokenValue) => {
        if (workTokenValue.fee > 0) {
          count += 1;
        }
      });
      if (count > 0) {
        result = true;
      } else {
        result = false;
      }
      return {
        data: result,
      };
    },

    formatAmount(value) {
      const val = (value / 1).toFixed(0);
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    },

    hasDuplicates(array, targetProp) {
      let unique;
      if (targetProp) unique = [...new Set(array.map(e => e[targetProp]))];
      else unique = [...new Set(Array.from(array))];

      return unique.length !== array.length;
    },

    getUniqueShiftByTimes: (shifts) => {
      const startTimes = shifts.map(shift => shift.startTime);
      const uniqueStartTimes = startTimes
        .map(startTime => startTime.toString())
        .filter((startTime, i, arr) => arr.indexOf(startTime) === i)
        .map(startTime => startTime.toString());

      const endTimes = shifts.map(shift => shift.endTime);
      const uniqueEndTimes = endTimes
        .map(endTime => endTime.toString())
        .filter((endTime, i, arr) => arr.indexOf(endTime) === i)
        .map(endTime => endTime.toString());

      // if (uniqueStartTimes.length !== uniqueEndTimes.length) {
      //   throw new Error('Faulty algorithm');
      // }

      const uniqueShifts = [];
      let unique = {};
      for (let i = 0; i < uniqueEndTimes.length; i += 1) {
        unique = shifts.find(shift => shift.startTime.toString() === uniqueStartTimes[i]
          && shift.endTime.toString() === uniqueEndTimes[i]);

        uniqueShifts.push(unique);
      }
      return uniqueShifts;
    },
    getUniqueShiftByTimesNotIncludeHired: (shifts) => {
      const startTimes = shifts.map(shift => shift.startTime);
      const uniqueStartTimes = startTimes
        .map(startTime => startTime.toString())
        .filter((startTime, i, arr) => arr.indexOf(startTime) === i)
        .map(startTime => startTime.toString());

      const endTimes = shifts.map(shift => shift.endTime);
      const uniqueEndTimes = endTimes
        .map(endTime => endTime.toString())
        .filter((endTime, i, arr) => arr.indexOf(endTime) === i)
        .map(endTime => endTime.toString());

      // if (uniqueStartTimes.length !== uniqueEndTimes.length) {
      //   throw new Error('Faulty algorithm');
      // }

      const uniqueShifts = [];
      let unique = {};
      for (let i = 0; i < uniqueEndTimes.length; i += 1) {
        unique = shifts.find(shift => shift.startTime.toString() === uniqueStartTimes[i]
          && shift.endTime.toString() === uniqueEndTimes[i]
          && !shift.workerAccepted);

        uniqueShifts.push(unique);
      }
      uniqueShifts.push(...shifts.filter(e => e.workerAccepted === true));
      return uniqueShifts;
    },

    // // generic
    // isInArray: [] => Boolean,
  },
  data() {
    return {
      get PUBLIC_MARKETPLACE_ID() {
        return '5e1d54053998d418ddc644ba';
      },
      get NOT_POSTED_MARKETPLACE_ID() {
        return '5fdb677bb9aa853f791bab64';
      },
    };
  },
};
export default util;
