import Vue from 'vue';
import gql from 'graphql-tag';
import apollo from '@/assets/js/apollo';
import moment from 'moment';

const services = {
  calculatePreviousPayrollWorkDays(previousPayrollPeriod, nonWorkDays) {
    let payCycleStartTime = moment().set('date', previousPayrollPeriod[0]);
    const payCycleEndTime = moment().set('date', previousPayrollPeriod[1]);

    // Period starts in last month or [1, 1]
    if (previousPayrollPeriod[0] >= previousPayrollPeriod[1]) {
      payCycleStartTime = payCycleStartTime.subtract(1, 'M');
    }

    const workDays = [];
    for (let thisDay = moment.utc(payCycleStartTime); thisDay.isSameOrBefore(payCycleEndTime); thisDay.add(1, 'days')) {
      if (!nonWorkDays.includes(thisDay.day())) {
        workDays.push(moment.utc(thisDay).startOf('day'));
      }
    }

    return workDays;
  },

  async getMissingWorkTokens(workerId) {
    const { data } = await apollo.defaultClient.query({
      query: gql`query worker($myId: String!) {
              worker (id: $myId) {
                id
                name
                phone
                email
                locationFrom
                paymentCoordinates
                paymentMethod
                paymentCurrency
                qualificationIdentificationFileRef
              }
            }`,
      variables: {
        myId: workerId,
      },
    })
      .catch((err) => {
        Vue.$log.error(err);
      });

    Vue.$log.info(data);
  },

  async getMissingWorkTokensForShift(shift, workDays) {
    const workTokenDays = shift.workTokens.map(workToken => moment.utc(workToken.checkinTime).startOf('day'));
    Vue.$log.info({ workDays });

    const missingWorkTokenDays = [];
    let found;
    workDays.forEach((workDay) => {
      found = workTokenDays.find(workTokenDay => workTokenDay.isSame(workDay));
      if (found == null) {
        missingWorkTokenDays.push(workDay);
      }
    });
    Vue.$log.info({ missingWorkTokenDays });
  },

  getPreviousPayCycle(payCycleMonthDays) {
    let payCycleEndTime;
    let payCycleStartTime;
    let today = moment();

    if (payCycleMonthDays.length === 2 && payCycleMonthDays[0] === payCycleMonthDays[1]) {
      payCycleStartTime = moment(today).subtract(1, 'month').startOf('month');
      payCycleEndTime = moment(today).subtract(1, 'month').endOf('month');

      return {
        payCycleEndTime,
        payCycleStartTime,
      };
    }

    const daysInEachPayCycle = payCycleMonthDays[1] - payCycleMonthDays[0];
    today = today.subtract(daysInEachPayCycle, 'day');

    const search = today.date();
    let startDate;
    let endDate;
    for (let i = 0; i < payCycleMonthDays.length - 1; i += 1) {
      if (payCycleMonthDays[i] < search && search <= payCycleMonthDays[i + 1]) {
        startDate = payCycleMonthDays[i];
        endDate = payCycleMonthDays[i + 1];
        break;
      }
      if (i === payCycleMonthDays.length - 2) {
        startDate = payCycleMonthDays[i];
        endDate = payCycleMonthDays[i + 1];
      }
    }

    payCycleStartTime = moment(today).set('date', startDate);
    if (endDate === 1) {
      payCycleEndTime = moment(today).add(1, 'month').set('date', endDate - 1);
    } else {
      payCycleEndTime = moment(today).set('date', endDate - 1);
    }

    return {
      payCycleEndTime,
      payCycleStartTime,
    };
  },

  runTimeSheetService: (args) => {
    Vue.$log.info('PayrollService.runTimeSheetService', args);
    const RUN_TIMESHEET_SERVICE = gql`
      mutation runTimeSheetService(
        $jobId: String!,
        $workerId: String!
        $forPreviousPayCycle: Boolean,
      ) {
        runTimeSheetService(
          jobId: $jobId,
          workerId: $workerId,
          forPreviousPayCycle: $forPreviousPayCycle,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: RUN_TIMESHEET_SERVICE,
      variables: args,
    });
  },
};

const PayRollService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local PayRollService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$PayrollService = services;
  },
};

export default PayRollService;
