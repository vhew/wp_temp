import gql from 'graphql-tag';
import apollo from '@/assets/js/apollo';
import Vue from 'vue';

const services = {
  healthcheck() {
    Vue.$log.info('Health Checking...');
    return apollo.defaultClient.query({
      query: gql`query healthcheck {
        healthcheck
      }`,
    });
  },
  workerBatchUpload(args) {
    Vue.$log.info('workerBatchUpload', args);
    const WORKER_BATCH_UPLOAD = gql`
      mutation workerBatchUpload(
        $organisationId: ID!,
        $batchData: GraphQLJSON,
      ) {
        workerBatchUpload(
          organisationId: $organisationId,
          batchData: $batchData,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: WORKER_BATCH_UPLOAD,
      variables: args,
    });
  },
  alienBatchUpload(args) {
    Vue.$log.info('alienBatchUpload', args);
    const ALIEN_BATCH_UPLOAD = gql`
      mutation alienBatchUpload(
        $organisationId: ID!,
        $batchData: GraphQLJSON,
      ) {
        alienBatchUpload(
          organisationId: $organisationId,
          batchData: $batchData,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ALIEN_BATCH_UPLOAD,
      variables: args,
    });
  },
  getRequiredLevel(error) {
    const [errorData] = error.graphQLErrors;
    return errorData && errorData.extensions.exception.requiredLevel;
  },
};

const UtilityService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local UtilityService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$UtilityService = services;
  },
};

export default UtilityService;
