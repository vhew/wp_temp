import gql from 'graphql-tag';

const queries = {
  organisationsAdminQuery: gql`query organisationsAdmin {
    organisationsAdmin {
      id
      name
      region
    }
  }`,

  marketplacesQuery: gql`query marketplaces(
    $organisationId: ID
  ) {
    marketplaces(
      organisationId: $organisationId,
    ) {
      id
      name
    }
  }`,

  paymentMethodQuery: gql`query paymentMethods(
    $region: String
    $currencies: [String]
  ) {
    paymentMethods(
      region: $region,
      currencies: $currencies
    ) {
      id
      name
      currency
      paymentType
    }
  }`,
  jobCategoryQuery: gql`query jobCategories {
    jobCategories {
      id
      name
      jobRoles {
        id
        name
        titles
        jobSkills {
          jobSkill {
            id
            name
            levels {
              level
              definition
            }
          }
        }
      }
    }
  }`,
  jobRoleQuery: gql`query jobRoles {
    jobRoles {
      id
      name
      titles
      jobSkills {
        jobSkill {
          id
          name
          levels {
            level
            definition
          }
        }
      }
    }
  }`,

  jobSkillsQuery: gql`query jobSkills{
    jobSkills {
      id
      name
      levels {
        level
        definition
      }
      description
    }
  }`,

  enumQuery: gql`query __type(
    $name: String!
  ) {
    __type(
      name: $name,
    ) {
      name
      enumValues {
        name
      }
    }
  }`,

  ledgerEntryQuery: gql`query ledgerEntry(
    $event: String
    $documentId: String
    $update: String
  ) {
    ledgerEntry(
      event: $event,
      documentId: $documentId,
      update: $update,
    ) {
      event
      createdAt
    }
  }`,

  ledgerEntriesQuery: gql`query ledgerEntries(
    $event: String
    $doumentId: String,
    $update: String
  ) {
    ledgerEntries(
      event: $event,
      documentId: $doumentId,
      update: $update,
    ) {
      event
    }
  }`,

  workerQuery: gql`
    query worker($id: String!) {
      worker(id: $id) {
        id
        name
        phone
        profileUrl
        identification
        identificationBackFileRef
        identificationFrontFileRef
        youtubeUrl
        description
        facebook
        email
        google
        paymentMethod
        paymentCurrency
        paymentCoordinates
        paycycleUseAlternatePayment
        alternatePaymentMethod
        alternatePaymentCurrency
        alternatePaymentCoordinates
        verified
        jobSkills
        cvFileUrl
        digitalLiteracyScore
        personalityType
        conscientiousnessLevel
        feedbacks {
          value
        }
        jobHistories {
          position
          companyName
          startDate
          endDate
          currentlyWorking
          description
        }
        workerPaymentMethods{
          id
          paymentMethod{
            id
            code
          }
          coordinates
        }
        adoptedByWorker{
          id
        }
        # {
        #   jobSkill {
        #     id
        #     name
        #     levels {
        #       level
        #       definition
        #     }
        #     urls
        #   }
        #   level
        #   definition
        #   uploads {
        #     requirementName
        #     url
        #   }
        # }
      }
    }
  `,
  timesheetShiftsQuery: gql`query shifts(
    $filter: FilterType,
    $orderBy: String,
    $skip: Int,
    $first: Int,
  ) {
      shifts(
        filter: $filter,
        orderBy: $orderBy,
        skip: $skip,
        first: $first,
      ) {
      id
      unpaid
      unconfirmed
      startTime
      endTime
      currentTimesheetConfirm
      job {
        id
        renumerationValue
      }
      worker {
        id
        name
        phone
        verified
        identification
        paymentCurrency
        paymentCoordinates
        languageSetting
      }
      organisation {
        id
        name
      }
      unconfirmedLeaves {
        id
      }
    }
  }
  `,
  disbursementWorkTokensQuery: gql`query workTokens(
    $filter: FilterType,
    $orderBy: String,
    $skip: Int,
    $first: Int,
  ) {
      workTokens(
        filter: $filter,
        orderBy: $orderBy,
        skip: $skip,
        first: $first,
      ) {
        id
        createdAt
        worker {
          id
        }
        workTokenValue{
          id
          amount
          amountConflictDifference
          fee
          method
          currency
          coordinates
          expectedWorkTokenShift {
            id
          }
          expectedWorkTokenType
          expectedWorkTokenCheckinTime
          expectedWorkTokenCheckoutTime
          notes
          workToken{
            id
            confirmed
            payNow
            product {
              id
              title
            }
            worker{
              id
            }
          }
        }
        fiatTokens{
          id
          workToken{
            checkinTime
            checkoutTime
          }
          createdAt
          amount
          currency
          method
          coordinates
          transactionRef
        }
        checkinTime
        checkoutTime
        type
        confirmed
        payNow
      }
    }
  `,
};

export default queries;
