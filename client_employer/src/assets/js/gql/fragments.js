import gql from 'graphql-tag';

const fragments = {
  worker: gql`
    fragment worker on Worker {
      id
      name
      phone
      locationFrom
    }
  `,
  job: gql`
    fragment job on Job {
      id
      title
      type
      role
      address
      location {
        township
        state
        country
      }
      renumerationValue
      minShiftsBalance
      private
      organisation {
        id
      }
    }
  `,
};

export default fragments;
