import gql from 'graphql-tag';
import apollo from '@/assets/js/apollo';
import Vue from 'vue';

export const services = {
  createFeedbackToken(args) {
    Vue.$log.info('creating feedback token', args);
    const CREATE_FEEDBACK_TOKEN = gql`
      mutation createFeedbackToken(
        $fromType: String 
        $fromId: String
        $targetType: String 
        $targetId: String
        $feedbackType: String 
        $feedbackValue: String
      ) {
        createFeedbackToken(
          fromType: $fromType,
          fromId: $fromId,
          targetType: $targetType,
          targetId: $targetId,
          feedbackType: $feedbackType,
          feedbackValue: $feedbackValue,
        ){
            __typename,
            fromId,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CREATE_FEEDBACK_TOKEN,
      variables: args,
    });
  },
  rateWorker(args) {
    Vue.$log.info('rating worker', args);
    const RATE_WORKER = gql`
      mutation rateWorker(
        $jobContractId: String!
        $workerId: String!
        $value: String!
        $reference: GraphQLJSON
      ) {
        rateWorker(
          jobContractId: $jobContractId,
          workerId: $workerId,
          value: $value,
          reference: $reference
        ){
            __typename,
            fromId,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: RATE_WORKER,
      variables: args,
    });
  },
};

const RatingService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local RatingService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$RatingService = services;
  },
};

export default RatingService;
