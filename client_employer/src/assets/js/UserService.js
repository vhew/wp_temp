import gql from 'graphql-tag';
import {
  auth,
  googleAuthProvider,
  facebookAuthProvider,
} from '@/assets/js/firebase';
import store from '@/assets/js/store';
import apollo from '@/assets/js/apollo';
import router from '@/router/index';
import Vue from 'vue';
import timeouts from '../utils/timeouts';
// eslint-disable-next-line no-unused-vars
import { services as OrganisationService } from '@/assets/js/OrganisationService';

const services = {
  async onAuthStateChanged(user) {
    Vue.$log.info({ user });
    if (user) {
      if (user.emailVerified) {
        Vue.$log.info('onAuthStateChanged - logged in');
        await user.getIdToken()
          .then((token) => {
            store.dispatch('setToken', token);
          });

        const employer = await services.queryEmployer(user.uid)
          .catch((err) => {
            Vue.$log.error(err);
          });
        if (employer == null) {
          Vue.$log.info('createEmployer');
          try {
            const { data } = await services.createEmployer({
              myId: user.uid,
              name: user.displayName || user.email.split('@').shift(),
              email: user.email,
            });
            if (data) {
              const { id } = data.createEmployer;
              store.dispatch('setMyId', id);
              services.updateEmployer({
                id: data.createEmployer.id,
                fcm: store.getters.fcm,
                notificationHiredUpdates: ['PUSH'],
                notificationNotHiredUpdates: ['PUSH'],
                notificationChat: ['PUSH'],
                notificationJobWatch: ['PUSH'],
              });
            }
          } catch (err) {
            Vue.$log.error(err);
          }
        } else {
          store.dispatch('setMyId', user.uid);
        }
      } else {
        router.push('/emailVerify');
        user.sendEmailVerification();
      }
    } else {
      Vue.$log.info('onAuthStateChanged - logged out');
      store.dispatch('setMyId', null);
      store.dispatch('setToken', null);
      store.dispatch('setIsAdminUser', false);
    }
  },
  sendEmailVerification() {
    return auth.currentUser.sendEmailVerification();
  },
  loginGoogle() {
    return auth.signInWithPopup(googleAuthProvider)
      .then((result) => {
        Vue.$log.info({ userInfo: result.additionalUserInfo });
        if (result.additionalUserInfo.isNewUser) {
          store.dispatch('setIsNewUser', true);
        }
      });
  },
  loginGoogleForAdmin(emails) {
    auth.signInWithPopup(googleAuthProvider)
      .then((result) => {
        Vue.$log.info('loginAdmin');
        const emailAccount = result.additionalUserInfo.profile.email;
        Vue.$log.info({ emailAccount });
        Vue.$log.info(emails.includes(emailAccount));
        if (emails.includes(emailAccount)) {
          store.dispatch('setIsAdminUser', true);
          Vue.$log.info('include');
          if (result.additionalUserInfo.isNewUser) {
            store.dispatch('setIsNewUser', true);
          }
        } else {
          this.logout();
        }
      })
      .catch((err) => {
        Vue.$log.error(err);
      });
  },
  async signInEmail(email, password) {
    const networkTimeouts = timeouts.setNetworkTimeouts();
    return auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        timeouts.clearTimeouts(networkTimeouts);
        Vue.$log.info({ result });
      }).catch((e) => {
        timeouts.clearTimeouts(networkTimeouts);
        throw e;
      });
  },
  isSignInWithEmailLink(link) {
    return auth.isSignInWithEmailLink(link);
  },
  async signInWithEmailLink(link) {
    let ret;
    // Additional state parameters can also be passed via URL.
    // This can be used to continue the user's intended action before triggering
    // the sign-in operation.
    // Get the email if available. This should be available if the user completes
    // the flow on the same device where they started it.
    const email = window.localStorage.getItem('emailForSignIn');
    Vue.$log.info({ email });
    // if (!email) {
    //   // User opened the link on a different device. To prevent session fixation
    //   // attacks, ask the user to provide the associated email again. For example:
    //   email = window.prompt('Please provide your email for confirmation');
    // }
    if (email) {
      // The client SDK will parse the code from the link for you.
      ret = await auth.signInWithEmailLink(email, link)
        .then(() => {
          // Clear email from storage.
          window.location.href = window.location.href.split('?').shift();
          window.localStorage.removeItem('emailForSignIn');
          window.localStorage.removeItem('passwordForSignIn');
          // You can access the new user via result.user
          // Additional user info profile not available via:
          // result.additionalUserInfo.profile == null
          // You can check if the user is new or existing:
          // result.additionalUserIno.isNewUser
        })
        .catch((error) => {
          // Some error occurred, you can inspect the code: error.code
          // Common errors could be invalid email and invalid or expired OTPs.
          Vue.$log.error(error);
        });
    }
    return ret;
  },
  async signUpEmail(email, password) {
    // const baseUrl = window.location.origin;
    // const actionCodeSettings = {
    //   url: `${baseUrl}/#/?signInMode=emailLink`,
    //   handleCodeInApp: true,
    // };
    // return auth.sendSignInLinkToEmail(email, actionCodeSettings)
    //   .then(() => {
    //     // The link was successfully sent. Inform the user.
    //     // Save the email locally so you don't need to ask the user for it again
    //     // if they open the link on the same device.
    //     window.localStorage.setItem('emailForSignIn', email);
    //     window.localStorage.setItem('passwordForSignIn', password);
    //   })
    //   .catch(e => console.error(e));
    const networkTimeouts = timeouts.setNetworkTimeouts();
    return auth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        timeouts.clearTimeouts(networkTimeouts);
        Vue.$log.info({ result });
      })
      .catch((e) => {
        timeouts.clearTimeouts(networkTimeouts);
        Vue.$log.info(e);
        throw e;
      });
  },
  loginFacebook() {
    auth.signInWithPopup(facebookAuthProvider)
      .then((result) => {
        if (result.additionalUserInfo.isNewUser) {
          store.dispatch('setIsNewUser', true);
        }
      })
      .catch((err) => {
        Vue.$log.info(err);
      });
  },
  logout() {
    return auth.signOut();
  },
  updateUserPermissions: (args) => {
    Vue.$log.info('UserPermissionService.updateUserPermissions', args);
    const UPDATE_USER_PERMISSIONS = gql`
      mutation updateUserPermissions(
        $resourceName: String!
        $resourceId: String!,
        $userId: String!
        $permissions: [String!]
      ) {
        updateUserPermissions(
          resourceName: $resourceName,
          resourceId: $resourceId,
          userId: $userId,
          permissions: $permissions,
        ) {
          id
          permissions
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_USER_PERMISSIONS,
      variables: args,
    });
  },
  async createEmployer(args) {
    Vue.$log.info('UserService.createEmployer', args);
    const CREATE_EMPLOYER = gql`
        mutation createEmployer($myId: String!, $name: String!, $email: EmailAddress!) {
          createEmployer (id: $myId, name: $name, email: $email) {
            __typename,
            id,
            name,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: CREATE_EMPLOYER,
      variables: {
        myId: args.myId,
        name: args.name,
        email: args.email || '',
      },
      optimisticResponse: {
        __typename: 'Mutation',
        createEmployer: {
          id: -1,
          __typename: 'Employer',
          name: 'name placeholder',
        },
      },
    });
  },
  async updateEmployer(args) {
    Vue.$log.info('UserService.updateEmployer', args);
    const UPDATE_EMPLOYER = gql`
      mutation updateEmployer(
        $id: String!,
        $name: String,
        $phone: String,
        $fcm: String,
        $facebook: String,
        $google: String,
        $notificationHiredUpdates: [ NOTIFICATION_METHOD ],
        $notificationNotHiredUpdates: [ NOTIFICATION_METHOD ],
        $notificationChat: [ NOTIFICATION_METHOD ],
        $workerWatchNotifyThreshold: Float,
        $organisationsIds: [ID],
        $surveyTriggers: GraphQLJSON
        $zoomAuthCode: String
      ) {
        updateEmployer(
          id: $id,
          name: $name,
          phone: $phone,
          fcm: $fcm,
          facebook: $facebook,
          google: $google,
          notificationHiredUpdates: $notificationHiredUpdates,
          notificationNotHiredUpdates: $notificationNotHiredUpdates,
          notificationChat: $notificationChat,
          workerWatchNotifyThreshold: $workerWatchNotifyThreshold,
          organisationsIds: $organisationsIds,
          surveyTriggers: $surveyTriggers,
          zoomAuthCode: $zoomAuthCode
        ) {
          __typename,
          id,
        }
      }
    `;

    const data = await apollo.defaultClient.mutate({
      mutation: UPDATE_EMPLOYER,
      variables: args,
      optimisticResponse: {
        __typename: 'Mutation',
        updateEmployer: {
          id: -1,
          __typename: 'Employer',
        },
      },
    }).catch((err) => {
      Vue.$log.info('updateEmployer mutation error:');
      Vue.$log.error(err);
    });
    return data;
  },

  queryEmployer(employerId) {
    return apollo.defaultClient.query({
      query: gql`query employer($myId: String!) {
        employer (id: $myId) {
          id
          email
        }
      }`,
      variables: {
        myId: employerId,
      },
    });
  },
};

auth.onAuthStateChanged(services.onAuthStateChanged);
// auth.onIdTokenChanged(services.onAuthStateChanged);

const UserService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local UserService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$UserService = services;
  },
};

export default UserService;
