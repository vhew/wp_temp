import Vue from 'vue';
import gql from 'graphql-tag';
import apollo from '@/assets/js/apollo';

const services = {

  confirmWorkToken(args) {
    Vue.$log.info('PaymentService.confirmWorkToken :', args);
    const CONFIRM_WORKTOKEN = gql`
      mutation confirmWorkToken(
        $id: ID!,
        $shiftId: ID!,
        $confirmed: Boolean,
        $payNow: Boolean,
      ) {
        confirmWorkToken(
          id: $id,
          shiftId: $shiftId,
          confirmed: $confirmed
          payNow: $payNow
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CONFIRM_WORKTOKEN,
      variables: args,
    });
  },

  confirmWorkTokens(args) {
    const CONFIRM_WORKTOKENS = gql`
      mutation confirmWorkTokens(
        $shiftId: ID!,
        $workTokenIds: [ID!]!
        $confirmed: Boolean,
        $payNow: Boolean,
      ) {
        confirmWorkTokens(
          shiftId: $shiftId,
          workTokenIds: $workTokenIds,
          confirmed: $confirmed
          payNow: $payNow
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CONFIRM_WORKTOKENS,
      variables: args,
    });
  },

  updateWorkTokenValues(args) {
    const UPDATE_WORK_TOKEN_VALUES = gql`
      mutation updateWorkTokenValues(
        $workTokenValuesIds: [ID!]!,
        $amountConflictDifference: Float!,
      ) {
        updateWorkTokenValues(
          workTokenValuesIds: $workTokenValuesIds,
          amountConflictDifference: $amountConflictDifference,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_WORK_TOKEN_VALUES,
      variables: args,
    });
  },

  updateDiscountToken(args) {
    Vue.$log.info(`PaymentService.createDiscountToken : (${args.id})`);
    const UPDATE_DISCOUNT_TOKEN = gql`
      mutation updateDiscountToken(
        $id: String!,
        $status: DISCOUNT_STATUS
      ) {
        updateDiscountToken (
          id:$id,
          status: $status
        ) {
          __typename,
          id
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_DISCOUNT_TOKEN,
      variables: args,
    });
  },

  deleteWorkTokenValues(args) {
    const DELETE_WORKTOKENVALUES = gql` mutation deleteWorkTokenValues($shiftId: ID!, $workTokenValuesIds: [ID!]) {
        deleteWorkTokenValues(
          shiftId: $shiftId,
          workTokenValuesIds: $workTokenValuesIds
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: DELETE_WORKTOKENVALUES,
      variables: args,
    });
  },

  addBonus: (args) => {
    Vue.$log.debug('PaymentService.addBonus', args);
    const ADD_BONUS = gql`
      mutation addBonus(
        $workerId: ID!,
        $shiftId: ID!,
        $amount: Int!,
        $notes: String,
        $forPreviousPayCycle: Boolean,
        $receiptId: String,
      ) {
        addBonus(
          workerId: $workerId,
          shiftId: $shiftId,
          amount: $amount,
          notes: $notes
          forPreviousPayCycle: $forPreviousPayCycle,
          receiptId: $receiptId,
        ) {
          __typename,
          id
          createdAt
          worker {
            id
          }
          workTokenValue{
            id
            amount
            amountConflictDifference
            fee
            method
            currency
            coordinates
            expectedWorkTokenShift {
              id
            }
            expectedWorkTokenType
            expectedWorkTokenCheckinTime
            expectedWorkTokenCheckoutTime
            notes
            workToken{
              id
              confirmed
              payNow
              product {
                id
                title
              }
              worker{
                id
              }
            }
          }
          fiatTokens{
            id
            workToken{
              checkinTime
              checkoutTime
            }
            createdAt
            amount
            currency
            method
            coordinates
            transactionRef
          }
          checkinTime
          checkoutTime
          type
          confirmed
          payNow
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ADD_BONUS,
      variables: args,
      // update: (cache, { data }) => {
      //   const saved = data.addBonus;
      //   const queryVariables = store.getters.queryVariables
      //     .find(e => e.name === 'disbursementWorkTokensQuery');
      //   const cached = cache.readQuery({
      //     query: queries.disbursementWorkTokensQuery,
      //     variables: queryVariables.variables,
      //   });
      //   const updated = cached.workTokens.push(saved);
      //   cache.writeQuery({
      //     query: queries.disbursementWorkTokensQuery,
      //     variables: queryVariables.variables,
      //     data: updated,
      //   });
      // },
    });
  },

  createFiatTokens(args) {
    const CREATE_FIAT_TOKENS = gql`
      mutation createFiatTokens(
        $workTokenValueId: ID!,
        $transactionRef: String!,
        $feeTransactionRef: String,
        # $shiftId: String,
        $notifyWorker: Boolean,
        ) {
          createFiatTokens(
          workTokenValueId: $workTokenValueId,
          transactionRef: $transactionRef,
          feeTransactionRef: $feeTransactionRef,
          # shiftId: $shiftId,
          notifyWorker: $notifyWorker
        ) {
          __typename,
          id,
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CREATE_FIAT_TOKENS,
      variables: args,
    });
  },

  requestPay(args) {
    const REQUEST_PAY = gql`
      mutation requestPay(
        $jobId: ID!,
        $workerId: String,
        $numShiftsPay: Int
        $dryRun: Boolean,
        $noFee: Boolean,
      ) {
        requestPay (
          jobId: $jobId,
          workerId: $workerId,
          numShiftsPay: $numShiftsPay,
          dryRun: $dryRun,
          noFee: $noFee,
        ) {
          __typename,
          expectedWorkTokenShift {
            id
          }
          expectedWorkTokenWorker {
            name
          }
          expectedWorkTokenType
          expectedWorkTokenCheckinTime
          expectedWorkTokenCheckoutTime
          workToken {
            shift {
              id
            }
            worker {
              name
            }
          }
          confirmRequest
          amount
          fee
          currency
          method
          coordinates
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: REQUEST_PAY,
      variables: args,
    });
  },

  runPayroll(args) {
    const RUN_PAYROLL = gql`
      mutation runPayroll(
        $jobId: ID!,
        $workerId: String,
        $dryRun: Boolean
      ) {
        runPayroll(
          jobId: $jobId,
          workerId: $workerId,
          dryRun: $dryRun,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: RUN_PAYROLL,
      variables: args,
    });
  },

  addExtraWorkingDay(args) {
    const ADD_EXTRA_WORKING_DAY = gql`
      mutation addExtraWorkingDay(
        $jobId: ID!,
        $shiftId: String,
        $workerId: String,
        $numberRequested: Int,
        $requestedDay: GraphQLDateTime,
      ) {
        addExtraWorkingDay(
          jobId: $jobId
          shiftId: $shiftId
          workerId: $workerId
          numberRequested: $numberRequested
          requestedDay: $requestedDay
        ) {
          id
          confirmed
          checkinTime
          checkoutTime
          payNow
          workTokenValue {
            fee
            amount
          }
          shift {
            id
            startTime
            endTime
            job {
              id
              locationCoordinates {
                type
                coordinates
              }
            }
          }
          references
          type
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ADD_EXTRA_WORKING_DAY,
      variables: args,
    });
  },

  addWorkerPaymentMethod: (args) => {
    Vue.$log.debug('PaymentService.addWorkerPaymentMethod', args);
    const ADD_WORKER_PAYMENT_METHOD = gql`
      mutation addWorkerPaymentMethod (
        $workerId: String!,
        $paymentMethodId: ID!,
        $coordinates: String!
      ) {
        addWorkerPaymentMethod (
          workerId: $workerId,
          paymentMethodId: $paymentMethodId,
          coordinates: $coordinates,
        ) {
          __typename
          id
          workerPaymentMethods{
            id
            coordinates
            paymentMethod{
              id
              code
            }
          }
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ADD_WORKER_PAYMENT_METHOD,
      variables: args,
    });
  },

  editWorkerPaymentMethod: (args) => {
    Vue.$log.debug('PaymentService.editWorkerPaymentMethod', args);
    const EDIT_WORKER_PAYMENT_METHOD = gql`
      mutation editWorkerPaymentMethod (
        $workerPaymentMethodId: String!,
        $paymentMethodId: ID!,
        $coordinates: String!
      ) {
        editWorkerPaymentMethod (
          workerPaymentMethodId: $workerPaymentMethodId,
          paymentMethodId: $paymentMethodId,
          coordinates: $coordinates,
        ) {
          __typename
          id
          coordinates
          paymentMethod{
            id
            code
          }
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: EDIT_WORKER_PAYMENT_METHOD,
      variables: args,
    });
  },

  runCampaign(args) {
    Vue.$log.info(`PaymentService.runCampaign : (${args})`);
    const RUN_CAMPAIGN = gql`
      mutation runCampaign(
        $campaignId: String,
        $campaignData: GraphQLJSON,
      ) {
        runCampaign (
          campaignId: $campaignId,
          campaignData: $campaignData,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: RUN_CAMPAIGN,
      variables: args,
    });
  },

  createPaymentMethod(args) {
    const CREATE_PAYMENT_METHOD = gql`
      mutation createPaymentMethod(
        $name: String!
        $region: String!
        $transactionFee: String
        $currencies: [ CURRENCY ]
      ) {
        createPaymentMethod(
          name: $name,
          region: $region,
          transactionFee: $transactionFee,
          currencies: $currencies,
        ) {
          id
        }
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: CREATE_PAYMENT_METHOD,
      variables: args,
    });
  },

  payWithBraintree(args) {
    const PARAMS = gql`
      mutation payWithBraintree(
        $amount: Float
        $payload: GraphQLJSON
      ) {
        payWithBraintree(
          amount: $amount,
          payload: $payload,
        )
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: PARAMS,
      variables: args,
    });
  },
};

const PaymentService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local PaymentService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$PaymentService = services;
  },
};

export default PaymentService;
