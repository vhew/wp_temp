import gql from 'graphql-tag';
import apollo from '@/assets/js/apollo';
import Vue from 'vue';

export const services = {
  sendTopicNotification(args) {
    Vue.$log.info('sendTN', args);
    const SEND_TOPIC_NOTIFICATION = gql`
      mutation sendTopicNotification(
        $topic: String!,
        $title: String!,
        $body: String!,
      ) {
        sendTopicNotification(
          topic: $topic,
          title: $title,
          body: $body,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: SEND_TOPIC_NOTIFICATION,
      variables: args,
    });
  },

  sendNotification(args) {
    Vue.$log.info('sendN', args);
    const SEND_NOTIFICATION = gql`
      mutation sendNotification(
        $workerId: ID!,
        $title: String!,
        $body: String!,
        $messageData: GraphQLJSON,
      ) {
        sendNotification(
          workerId: $workerId,
          title: $title,
          body: $body,
          messageData: $messageData,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: SEND_NOTIFICATION,
      variables: args,
    });
  },
};

const NotificationService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local NotificationService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$NotificationService = services;
  },
};

export default NotificationService;
