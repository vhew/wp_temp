import Vue from 'vue';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/messaging';
import 'firebase/functions';
import 'firebase/storage';
import 'firebase/analytics';
import store from '@/assets/js/store';

// firebase init
const config = {
  apiKey: 'AIzaSyA5m_XMaeAHs3nars-eKmF5CLuYdYDVsyQ',
  authDomain: 'jobdoh2employer.firebaseapp.com',
  databaseURL: 'https://jobdoh2employer.firebaseio.com',
  projectId: 'jobdoh2employer',
  storageBucket: 'jobdoh2employer.appspot.com',
  messagingSenderId: '996263494846',
  appId: '1:996263494846:web:9d65cd2bcd09ca862b4aef',
  measurementId: 'G-GWLX7HBZXS',
};
firebase.initializeApp(config);
// firebase utils
export const auth = firebase.auth();
let firebaseMessaging = {};
if (firebase.messaging.isSupported()) {
  firebaseMessaging = firebase.messaging();
}
export const messaging = firebaseMessaging;
export const functions = firebase.functions();
export const storage = firebase.storage();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();

const initMessage = async () => {
  await navigator.serviceWorker
    .register('/static/sw.js')
    .then(registration => firebase.messaging().useServiceWorker(registration))
    .catch(err => Vue.$log.error(err));
  messaging.requestPermission()
    .then(() => messaging.getToken())
    .then((token) => {
      Vue.$log.debug('FCM', token); // Receiver Token to use in the notification
      store.dispatch('setFCM', token);
    })
    .catch((err) => {
      Vue.$log.error('Unable to get permission to notify.', err);
    });
};

if (firebase.messaging.isSupported()) {
  initMessage();
}

export default {
  auth,
  messaging,
  functions,
};
