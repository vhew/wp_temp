import Vue from 'vue';

const services = {
  async getDb() {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open('jobdoh', 1);

      request.onerror = (e) => {
        Vue.$log.info('Error opening db', e);
        // eslint-disable-next-line prefer-promise-reject-errors
        reject('Error');
      };

      request.onsuccess = (e) => {
        resolve(e.target.result);
      };

      request.onupgradeneeded = (e) => {
        Vue.$log.info('onupgradeneeded');
        const db = e.target.result;
        db.createObjectStore('notifications', { autoIncrement: true, keyPath: 'id' });
      };
    });
  },
  async getJobDb() {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open('jobdohJob', 1);

      request.onerror = (e) => {
        Vue.$log.info('Error opening db', e);
        // eslint-disable-next-line prefer-promise-reject-errors
        reject('Error');
      };

      request.onsuccess = (e) => {
        resolve(e.target.result);
      };

      request.onupgradeneeded = (e) => {
        Vue.$log.info('onupgradeneeded');
        const db = e.target.result;
        db.createObjectStore('tempJobs', { autoIncrement: true, keyPath: 'id' });
      };
    });
  },
  async getNotificationsFromDb() {
    const db = await services.getDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['notifications'], 'readonly');
      trans.oncomplete = () => {
        // eslint-disable-next-line no-use-before-define
        resolve(notifications);
      };

      const store = trans.objectStore('notifications');
      let notifications = [];

      store.openCursor().onsuccess = (e) => {
        const cursor = e.target.result;
        if (cursor) {
          notifications.push(cursor.value);
          cursor.continue();
        }
      };
    });
  },
  async addNotificaitonsToDb(notification) {
    const db = await services.getDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['notifications'], 'readwrite');
      trans.oncomplete = () => {
        resolve();
      };

      const store = trans.objectStore('notifications');
      store.add(notification);
    });
  },
  async editNotificaitonsToDb(notification) {
    const db = await services.getDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['notifications'], 'readwrite');
      trans.oncomplete = () => {
        resolve();
      };

      const store = trans.objectStore('notifications');
      store.put(notification);
    });
  },
  async getTempJobsFromDb() {
    const db = await services.getJobDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['tempJobs'], 'readonly');
      trans.oncomplete = () => {
        // eslint-disable-next-line no-use-before-define
        resolve(tempJobs);
      };

      const store = trans.objectStore('tempJobs');
      let tempJobs = [];

      store.openCursor().onsuccess = (e) => {
        const cursor = e.target.result;
        if (cursor) {
          tempJobs.push(cursor.value);
          cursor.continue();
        }
      };
    });
  },
  async addTempJobToDb(job) {
    const db = await services.getJobDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['tempJobs'], 'readwrite');
      trans.oncomplete = () => {
        resolve();
      };

      const store = trans.objectStore('tempJobs');
      Vue.$log.info({ store });
      const addRequest = store.add(job);
      addRequest.onerror = () => store.put(job);
    });
  },
  async editTempJobToDb(job) {
    const db = await services.getJobDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['tempJobs'], 'readwrite');
      trans.oncomplete = () => {
        resolve();
      };

      const store = trans.objectStore('tempJobs');
      Vue.$log.info({ store });
      const putRequest = store.put(job);
      putRequest.onerror = () => store.add(job);
    });
  },
  async deleteTempJobFromDb(job) {
    Vue.$log.info('deleteJob...', job);
    const db = await services.getJobDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['tempJobs'], 'readwrite');
      trans.oncomplete = () => {
        resolve();
      };

      const store = trans.objectStore('tempJobs');
      const deleteRequest = store.delete(job.id);
      deleteRequest.onerror = () => store.delete(job.id);
    });
  },

};

const IdbService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local IDB Service vue plugin');
    Vue.prototype.$IdbService = services;
  },
};

export default IdbService;
