
// import gql from 'graphql-tag';
import axios from 'axios';
// import Vue from 'vue';

const services = {
  sendMessageToSlack: (channel, message) => {
    const url = 'https://hooks.slack.com/services/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';
    let data = {};
    if (process.env.NODE_ENV !== 'production') {
      data = {
        text: message,
        username: 'Testing bot',
        icon_emoji: ':robot_face:',
        channel: '#testing_slack_messages',
      };
    } else {
      data = {
        text: message,
        username: 'KhuPay bot',
        icon_emoji: ':star-struck:',
        channel,
      };
    }

    return axios.post(url, JSON.stringify(data), {
      withCredentials: false,
      transformRequest: [(result, headers) => {
        // eslint-disable-next-line no-param-reassign
        delete headers.post['Content-Type'];
        return result;
      }],
    });
  },
};

const MessageService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local MessageService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$MessageService = services;
  },
};

export default MessageService;
