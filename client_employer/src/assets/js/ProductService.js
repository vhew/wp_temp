import gql from 'graphql-tag';
import Vue from 'vue';
// import store from '@/assets/js/store';
import apollo from '@/assets/js/apollo';

const services = {
  createProduct(args) {
    Vue.$log.info('calling ProductService.createProduct', args);
    const CREATE_PRODUCT = gql`
        mutation createProduct(
          $lumpSum: Int
          $organisationId: String
          $listingStartDate: GraphQLDateTime 
          $listingEndDate: GraphQLDateTime
          $categoryHierarchies: [GraphQLJSON]
          $title: String
          $description: String
          $deposit: Float
          $price: Float
          $priceRRP: Float
          $fee: Float
          $currency: String
          $tagLine: String
          $tags: [GraphQLJSON]
          $productMetadata: GraphQLJSON
          $benefits: [String]
          $itemAttributes: [GraphQLJSON]
          $pickupInstructions: String
          $pickupContact: String
        ) {
          createProduct (
            lumpSum: $lumpSum,
            organisationId: $organisationId,
            listingStartDate: $listingStartDate,
            listingEndDate: $listingEndDate,
            categoryHierarchies: $categoryHierarchies,
            title: $title,
            description: $description,
            deposit: $deposit,
            price: $price,
            priceRRP: $priceRRP
            fee: $fee,
            currency: $currency
            tagLine: $tagLine
            tags: $tags,
            productMetadata: $productMetadata,
            benefits: $benefits,
            itemAttributes: $itemAttributes,
            pickupInstructions: $pickupInstructions,
            pickupContact: $pickupContact,
          ) {
            __typename,
            id,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: CREATE_PRODUCT,
      variables: args,
    });
  },
  updateProduct(args) {
    Vue.$log.info('calling ProductService.updateProduct', args);
    const UPDATE_PRODUCT = gql`
        mutation updateProduct(
          $id: String
          $lumpSum: Int
          $organisationId: String
          $listingStartDate: GraphQLDateTime 
          $listingEndDate: GraphQLDateTime
          $categoryHierarchies: [GraphQLJSON]
          $title: String
          $description: String
          $deposit: Float
          $price: Float
          $priceRRP: Float
          $benefits: [String]
          $fee: Float
          $tags: [GraphQLJSON]
          $productMetadata: GraphQLJSON
          $itemAttributes: [GraphQLJSON]
          $pickupInstructions: String
          $pickupContact: String
        ) {
          updateProduct (
            id: $id,
            lumpSum: $lumpSum,
            organisationId: $organisationId,
            listingStartDate: $listingStartDate,
            listingEndDate: $listingEndDate,
            categoryHierarchies: $categoryHierarchies,
            title: $title,
            description: $description,
            deposit: $deposit,
            price: $price,
            priceRRP: $priceRRP,
            benefits: $benefits,
            fee: $fee,
            tags: $tags,
            productMetadata: $productMetadata,
            itemAttributes: $itemAttributes,
            pickupInstructions: $pickupInstructions,
            pickupContact: $pickupContact,
          ) {
            __typename,
            id,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: UPDATE_PRODUCT,
      variables: args,
    });
  },
  createReceipt(args) {
    Vue.$log.info('calling ProductService.createReceipt', args);
    const CREATE_RECEIPT = gql`
        mutation createReceipt(
          $productId: String
          $workerId: String
          $jobId: String
          $code: String
          $paidLevel: Int
          $shiftId: String
          $transactionRef: String
          $password: String
          $complete: Boolean
          $organisationId: String
          $payCycleStart: String
          $payCycleEnd: String
        ) {
          createReceipt (
            productId: $productId,
            workerId: $workerId,
            jobId: $jobId,
            code: $code,
            shiftId: $shiftId,
            transactionRef: $transactionRef,
            password: $password,
            paidLevel: $paidLevel,
            complete: $complete,
            organisationId: $organisationId,
            payCycleStart: $payCycleStart,
            payCycleEnd: $payCycleEnd,
          ) {
            __typename,
            id,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: CREATE_RECEIPT,
      variables: args,
    });
  },
  updateReceipt(args) {
    Vue.$log.info('calling ProductService.updateReceipt', args);
    const UPDATE_RECEIPT = gql`
        mutation updateReceipt(
          $productId: String
          $workerId: String
          $organisationId: String
          $paidLevel: Int
          $receivedAt: GraphQLDateTime
        ) {
          updateReceipt (
            productId: $productId,
            workerId: $workerId,
            organisationId: $organisationId,
            receivedAt: $receivedAt,
            paidLevel: $paidLevel,
          ) {
            __typename,
            id,
          }
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: UPDATE_RECEIPT,
      variables: args,
    });
  },
  runProductTimeSheetService: (args) => {
    Vue.$log.info('ProductService.runProductTimeSheetService', args);
    const RUN_PRODUCT_TIMESHEET_SERVICE = gql`
      mutation runProductTimeSheetService(
        $jobId: String,
        $workerId: String,
        $productId: String,
      ) {
        runProductTimeSheetService(
          jobId: $jobId,
          workerId: $workerId,
          productId: $productId,
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: RUN_PRODUCT_TIMESHEET_SERVICE,
      variables: args,
    });
  },
};

const ProductService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local JobService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$ProductService = services;
  },
};

export default ProductService;
