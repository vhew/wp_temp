import axios from 'axios';

const io = {
  methods: {
    showSuccessAlert: ({ message, _this }) => {
      _this.$eventHub.$emit('show-alert', {
        alert: true,
        type: 'success',
        message,
      });
    },
    showErrorAlert: ({ message, _this }) => {
      _this.$eventHub.$emit('show-alert', {
        alert: true,
        type: 'error',
        message,
      });
    },
    showWarningAlert: ({ message, _this }) => {
      _this.$eventHub.$emit('show-alert', {
        alert: true,
        type: 'warn',
        message,
      });
    },
    showInfoAlert: ({ message, _this }) => {
      _this.$eventHub.$emit('show-alert', {
        alert: true,
        type: 'info',
        message,
      });
    },
    debug: message => message,
    info: message => message,
    error: message => message,
    warn: message => message,
    roundTo: (n, digits) => {
      const multiplicator = 10 ** digits;
      const nFixed = parseFloat((n * multiplicator).toFixed(11));
      return Math.round(nFixed) / multiplicator;
    },
    // sendMessageToSlack: (message, channel) => {},
    sendMessageToSlackOperationsChannel: ({ message, _this }) => {
      const url = 'https://hooks.slack.com/services/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';
      let data = {};
      if (_this.$store.getters.getEnv !== 'production') {
        data = {
          text: message,
          username: 'Testing bot',
          icon_emoji: ':robot_face:',
          channel: '#testing_slack_messages',
        };
      } else {
        data = {
          text: message,
          username: 'KhuPay bot',
          icon_emoji: ':star-struck:',
        };
      }

      axios.post(url, JSON.stringify(data), {
        withCredentials: false,
        transformRequest: [(result, headers) => {
          // eslint-disable-next-line no-param-reassign
          delete headers.post['Content-Type'];
          return result;
        }],
      });
    },

    // normalBatchUpload
    test: ({ a, b }) => a + b,

    getNotificationText(lang, messageConst, _this) {
      _this.$i18n.i18next.changeLanguage(lang);
      return _this.$t(`${messageConst}`);
    },
  },
};
export default io;
