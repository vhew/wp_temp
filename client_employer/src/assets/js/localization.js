import en from '@/assets/js/languages/en';
import mm from '@/assets/js/languages/mm';
import zg from '@/assets/js/languages/zg';

const locales = {
  en,
  mm,
  zg,
};

export default locales;
