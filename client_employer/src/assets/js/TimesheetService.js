// import Vue from 'vue';
import moment from 'moment';

const services = {
  haversineDistanceKilometer: (coords1, coords2) => {
    // Vue.$log.debug('TimesheetService.haversineDistanceKilometer', coords1, coords2);
    const toRad = x => x * Math.PI / 180;

    const lng1 = coords1.lng;
    const lat1 = coords1.lat;
    const lng2 = coords2.lng;
    const lat2 = coords2.lat;

    const R = 6371; // km

    const x1 = lat2 - lat1;
    const dLat = toRad(x1);
    const x2 = lng2 - lng1;
    const dLng = toRad(x2);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
      + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2))
      * Math.sin(dLng / 2) * Math.sin(dLng / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = R * c;

    return Math.abs(d);
  },

  compareTimeOnlyDifference: (date1, date2) => {
    if (date1 == null || date2 == null) return null;
    const moment1 = moment.utc(date1).set({ year: 2020, month: 1, date: 1 });
    const moment2 = moment.utc(date2).set({ year: 2020, month: 1, date: 1 });

    const diff = moment2.diff(moment1, 'minute');
    return diff;
  },
};

const TimesheetService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local TimesheetService vue plugin');
    Vue.prototype.$TimesheetService = services;
  },
};

export default TimesheetService;
