
import Vue from 'vue';
import store from './store';
import eventHub from '../utils/event-bus';


const services = {
  checkAccess({ permissions, onPass, onDeny }) {
    const currentUserLevel = store.state.userLevel;
    const requiredLevels = store.state.employerLevels
      .filter(e => e.permissions.some(p => permissions.includes(p)));
    const largestRequiredLevel = requiredLevels
      .map(l => l.userLevel)
      .reduce((p, c) => (p > c ? p : c));

    Vue.$log.info('User Level', currentUserLevel);
    Vue.$log.info('Required Level', largestRequiredLevel);
    Vue.$log.info('Required Levels', requiredLevels);

    if (largestRequiredLevel <= currentUserLevel) return onPass();
    return onDeny(permissions);
  },
  // checkFeatures({ featureNames, onPass, onDeny }) {
  //   const currentUserLevel = store.state.userLevel;
  //   const requiredLevels = store.state.employerLevels
  //     .filter(e => featureNames.includes(e.feature.name));

  //   const lockedLevels = requiredLevels.filter(l => l.userLevel > currentUserLevel);

  //   Vue.$log.debug('User Level', currentUserLevel);
  //   Vue.$log.debug('Required Levels', requiredLevels);
  //   Vue.$log.debug('Locked Levels', lockedLevels);

  //   if (lockedLevels.length) return onDeny();
  //   return onPass();
  // },
  isPermittedPayment(payment) {
    return services.isUnlocked(services.getPaymentFeature(payment));
  },
  getPaymentFeature(payment) {
    if (typeof payment !== 'object') return '';
    if (payment.paymentType === 'CASH_TRANSFER') {
      return ('DISBURSEMENT');
    } return ('TIMESHEET');
  },
  isUnlocked(featureName) {
    if (services.isAdminLevel()) return true;
    if (featureName === 'WRITE_ACCESS') return services.hasWriteAccess();
    return store.getters.unlockedFeatures.some(f => f.name === featureName);
  },
  isLocked(featureName) {
    if (services.isAdminLevel()) return false;
    if (featureName === 'WRITE_ACCESS') return !services.hasWriteAccess();
    return store.getters.unlockedFeatures.every(f => f.name !== featureName);
  },
  isPermitted(permission) {
    return store.getters.givenPermissions.includes(permission);
  },
  notPermittedFor(permissions) {
    const { givenPermissions } = store.getters;
    return !permissions.every(p => givenPermissions.includes(p));
  },
  requiredLevelFor(permission) {
    return store.state.employerLevels
      .filter(e => e.permissions.includes(permission))
      .map(e => e.userLevel)
      .shift();
  },
  isAdminLevel() {
    const adminLevel = store.state.employerLevels.find(e => e.adminLevel);
    const currentUserLevel = store.state.userLevel;
    return adminLevel.userLevel === currentUserLevel;
  },
  hasWriteAccess() {
    return store.state.employer.access.writable;
  },
  getFeaturesOfLevel(level) {
    return store.state.employerLevels.find(e => e.userLevel === level).features.map(f => f.name);
  },
  getUnlockedFeaturesOfLevel(level) {
    return store.state.employerLevels.filter(e => e.userLevel <= level).flatMap(e => e.features);
  },
  getLockedFeaturesOfLevel(level) {
    const currentLevel = store.state.userLevel;
    return store.state.employerLevels
      .filter(e => e.userLevel > currentLevel && e.userLevel <= level)
      .flatMap(e => e.features);
  },
};

const AccessService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local AccessControl vue plugin');
    Vue.prototype.$AccessService = services;

    Vue.directive('access', {
      inserted: (element, binding) => {
        const features = binding.value;
        if (Array.isArray(features)) {
          const lockedFeatures = features.filter(services.isLocked);
          if (lockedFeatures.length) {
            const parent = element.parentNode;
            const wrapper = document.createElement('div');
            parent.replaceChild(wrapper, element); // parent -[ wrapper
            wrapper.appendChild(element); // parent -[ wrapper -[ element

            wrapper.addEventListener(
              'click',
              (ev) => {
                ev.stopPropagation();
                ev.preventDefault();
                eventHub.$emit('show-tutorial-feature', lockedFeatures[0], true);
              },
              true,
            );
          }
        }
      },
    });
  },
};

export default AccessService;
