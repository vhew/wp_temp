const en = {
  faq: 'FAQ',
  question1: 'What is KhuPay?',
  answer1: `Built by the company JOBDOH, KhuPay is an app to help employees get their earned pay faster.
  Even if the employee normally gets paid by the employer once a month,
  he or she can still get paid through KhuPay more frequently for the days they have already worked. Yay!`,
  question2: 'Is KhuPay free to use?',
  answer2: `KhuPay app is free to download.
  There is no fee to withdraw salary on your normal pay day.
  However there will be a fee for all withdrawals before the normal pay day.
  All the fees will be clearly stated on the app when you request to withdraw money.`,
  question3: 'What do I need to use Khypay?',
  answer3: 'A non-IOS* smart phone, a valid sim card with sufficient data or steady internet access.',
  question4: 'Do I need to pay a deposit to use KhuPay?',
  answer4: 'No.',
  question5: 'Do I need to apply to use KhuPay?',
  answer5: `You can only use KhuPay if your employer has enrolled in our program.
  (So ask your employer about it if your company is not enrolled yet!) After that,
  as long as you have a valid non-IOS smartphone and an official ID card,
  you can activate the KhuPay app and start getting your earned pay early!`,
  question6: 'How do I know how much I have earned?',
  answer6: 'Once you activate your KhuPay, you can view your earned salary in the app.',
  question7: 'How does KhuPay work?',
  answer7: `KhuPay is an app that helps employees get paid faster.
  (To use KhuPay, an employer must first enroll with JOBDOH Limited.)
  It connects with employer partners and calculates every employee’s daily earned salary,
  allowing employees to calculate and withdraw their salary.
  It partners with e-wallet providers so employees can then
  i) transfer money to their bank accounts,
  ii) withdraw cash via a list of cash agents or
  iii) remits money to another person.`,
  question8: 'How come I can’t sign up on KhuPay?',
  answer8: `You may not sign up on KhuPay if you
   i) do not belong to a company who has enrolled with KhuPay,
   ii) do not have a smartphone and
   iii) only have an IOS phone.`,
  question9: 'How come I can’t withdraw money from my KhuPay app?',
  answer9: `First check that you have enough balance in your KhuPay app and if there are restrictions stated on your app.
  If there is enough balance and you are still working for the same employer,
  FB or viber us at xxxxx.`,
  question10: 'I am lost. What do I do after confirming to withdraw my money from KhuPay?',
  answer10: `     
  a. Wait for approval from KhuPay app (up to 24 hours)
  b. Download the pre-set e-wallet from Ongo, OK Dollar or Wave Money (check with your employer) (first time users only)
  c. Sign up and verify your e-wallet account (first time users only)
  d. Confirm how you’d like to access the cash received from your e-wallet (i.e. connect to your bank account or withdraw from an agent)
  e. After receiving KhuPay withdrawal approval, check your e-wallet for the amount deposited to your bank account
  `,
  question11: 'What if I can’t get an e-wallet?',
  answer11: 'Please FB or viber us at xxx for assistance.',
  question12: 'How can I get my salary?',
  answer12: `
  To get money before your normal pay day:<br>
  &ensp;  I. Download the KhuPay App (first time users only)<br>
  &ensp;  II. Sign up and verify your KhuPay account (first time users only)<br>
  &ensp;  III. Check your available balance in the app<br>
  &ensp;  IV. Check the amount of withdrawal allowed<br>
  &ensp;  V. Confirm the amount of withdrawal in the app<br>
  &ensp;  VI. Wait for approval from KhuPay app (up to 24 hours)<br>
  &ensp;  VII. Download the pre-set e-wallet from Ongo, OK Dollar or Wave Money (check with your employer) (first time users only)<br>
  &ensp;  VIII. Sign up and verify your e-wallet account (first time users only)<br>
  &ensp;  IX. Confirm how you’d like to access the cash received from your e-wallet (i.e. connect to your bank account or withdraw from an agent)<br>
  &ensp;  X. After receiving KhuPay withdrawal approval, check your e-wallet for the amount deposited to your bank account<br>
  &ensp;  XI. Withdraw money from your bank account, a cash agent or send money to someone else<br>
  <br>
  To get money on your normal pay day:<br>
  &ensp;  I. Download the KhuPay App (first time users only)<br>
  &ensp;  II. Sign up and verify your KhuPay account (first time users only)<br>
  &ensp;  III. Check your available balance in the app<br>
  &ensp;  IV. Confirm the amount of withdrawal (should be your monthly salary plus overtime and bonuses) in the app<br>
  &ensp;  V. Wait for approval from KhuPay app (up to 24 hours)<br>
  &ensp;  VI. Download the pre-set e-wallet from Ongo, OK Dollar or Wave Money (check with your employer) (first time users only)<br>
  &ensp;  VII. Sign up and verify your e-wallet account (first time users only)<br>
  &ensp;  VIII. Confirm how you’d like to access the cash received from your e-wallet (i.e. connect to your bank account or withdraw from an agent)<br>
  &ensp;  IX. After receiving KhuPay withdrawal approval, check your e-wallet for the amount deposited to your bank account<br>
  &ensp;  X. Withdraw money from your bank account, a cash agent or send money to someone else<br>
  `,
  question13: 'How do I know if my employer is already enrolled in KhuPay?',
  answer13: 'Feel free to Viber us at xxxx.',
  question14: 'Where can I receive my money?',
  answer14: `
  You may withdraw money from your bank account, a cash agent or send money to someone else.
  Depending on which e-wallet providers you have, the location of withdrawal might be different.
  Here’s a list of the cash agents in Myanmar…..
  `,
  question15: 'How much money can I withdraw each time?',
  answer15: `
  Different companies and employees will have different withdrawal limits.
  Please check your KhuPay app for the most updated amount.
  `,
  question16: 'How much will it cost me to withdraw?',
  answer16: `
  Different salary levels and timing of withdrawal will have different withdrawal fees.
  Please check your KhuPay app for the most updated amount.
  `,
  question17: 'Is getting money from KhuPay a loan?  Do I need to pay back?',
  answer17: `
  No, KhuPay app is not a lender. It is a software to help process salary payment faster.
  The money you withdraw is money you have earned so there is no need to pay it back.
  `,
  question18: 'How many times can we request money before my normal pay day?',
  answer18: `
  Different companies might have different withdrawal restrictions.
  Please check your KhuPay app for the most updated frequency.
  `,
  question19: 'Is it possible to get a loan from KhuPay?',
  answer19: `
  No, KhuPay app is not a lender.
  It is a software to help process salary payment faster.
  The money you withdraw is money you have earned.
  If you’d like to get a loan, please viber us at XXX, and we can recommend our lending partners to you.
  `,
  question20: 'Is KhuPay legal?',
  answer20: 'KhuPay is a legally registered company under the Republic of Myanmar company law.',
  question21: 'What kind of benefits can we get by using KhuPay?',
  answer21: `
  You don’t have to ask your boss or your HR manager; you don’t have to ask your friends or your family.
  You can get your hard earned money much quicker.
  `,
  question22: 'Why should I use KhuPay?',
  answer22: `
  i) If you sometimes need money before your normal pay day and you don’t want to:
    • Ask your boss
    • Ask your colleagues
    • Ask your mother, father or sibling
    • Ask your friends
    • Ask your boyfriend or girlfriend
    • Ask Facebook
    • Borrow from strangers

Then you need KhuPay.
KhuPay helps you get your hard earned salary when you need them, not just at the end of the month.
KhuPay helps you de-stress about cash in the middle of the month.  You may also use KhuPay to save money
  `,
  question23: 'Is KhuPay safe?',
  answer23: `
  Yes, there is no risk to use KhuPay since KhuPay pays you first and does not keep your money.
  KhuPay also works with trust e-wallet providers in the country to transfer your pay.
  `,
  question24: 'Will other people know that I use KhuPay to take out money before payday?',
  answer24: `
  No one except your HR manager/ boss and you will know that you take out money before payday,
  as long as you safeguard your phone and do not show your details on your KhuPay app to others.
  `,
  question25: 'What happens if I lose my phone?',
  answer25: 'Please viber us at xxx for further assistance.',
  question26: 'What if I disagree with the amount shown on my KhuPay app?',
  answer26: `
  Please take a screen cap of the amount shown on your KhuPay app
   and contact us via FB or viber at xxx for further assistance.
  `,
  question27: 'What happens if I lose my phone?',
  answer27: 'Please FB or viber us at xxx for further assistance.',
  question28: 'I want to get some help; to whom should I talk?',
  answer28: 'Please FB or viber us at xxx for further assistance.',
  question29: 'How fast can I get my money?',
  answer29: `
  If you request your salary via the KhuPay app on a working day,
   your salary should arrive in your e-wallet on the next working day
  `,
  totalENQna: 29,

  ongoInfo1: 'Please visit an ONGO agent to get cash out.',
  ongoInfo2: 'Relevant agents for I-Land workers in BAGO',
  ongoInfo3: '- Nilar Yoma: 09 865 1562',
  ongoInfo4: ' Please refer to the ONGO app for a more complete agent list',
  // question: '',
  // answer: '',

  // KhuPay Notification
  khupayStatus: 'Status',
  confirmSuccessNotification: '@@@ has confirmed your work attendance for ### days. Your KhuPay|Salary will soon be disbursed.',
  confirmRejectNotification: '@@@ has confirmed your work attendance for ### days. Your KhuPay request was not successful.Today you may request KhuPay for *** Ks, with instant approval.',

  disbursementNotification: 'KhuPay has been paid.',
};

export default en;
