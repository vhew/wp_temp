import moment from 'moment';

export default class Job {
  constructor(job) {
    this.job = job;
  }

  get jobTitle() {
    return this.getFormattedExperienceLabel(
      this.job.requirementMinExperienceLevel,
    ) + this.job.title;
  }

  get earliestStartDate() {
    const startDates = this.job.shifts.map(shift => new Date(shift.startTime));
    return moment(Math.min.apply(null, startDates));
  }

  get latestEndDate() {
    const endDates = this.job.shifts.map(shift => new Date(shift.endTime));
    return moment(Math.max.apply(null, endDates));
  }

  get companyInfo() {
    const { township, country } = this.job.location || {};
    let companyInfo = `At ${this.companyName}`;
    if (township) companyInfo = companyInfo.concat(`, ${township} township`);
    if (country) companyInfo = companyInfo.concat(`, ${country}`);
    return companyInfo;
  }

  get companyName() {
    if (this.job.organisation == null
        || this.job.proxyCompanyName == null) return '';

    if (this.job.proxyCompanyName.length > 0) {
      return this.job.proxyCompanyName;
    }
    return this.job.organisation.name;
  }

  get paymentInfo() {
    const { renumerationValue, renumerationCurrency, paymentOption } = this.job;
    return `${this.getFormattedRenumerationValue(renumerationValue, renumerationCurrency)}${this.getFormattedPaymentOption(paymentOption)}`;
  }

  get applyBefore() {
    return moment(this.job.listingEndDate).format('[Apply before - ]MMM DD YYYY');
  }

    getFormattedExperienceLabel = (experienceLevel) => {
      if (experienceLevel === 'INTERN') return 'Intern ';
      if (experienceLevel === 'JUNIOR') return 'Junior ';
      if (experienceLevel === 'MID_LEVEL') return 'Mid-Level ';
      if (experienceLevel === 'SENIOR') return 'Senior ';
      return '';
    }

    getFormattedPaymentOption = (paymentOption) => {
      switch (paymentOption) {
        case 'MONTHLY':
          return '/month';
        case 'HOURLY':
          return '/hour';
        case 'FIXED':
          return '/fixed';
        default:
          return '';
      }
    }

    getFormattedRenumerationValue = (rValue, rCurrency) => {
      const roundedAmount = Number(parseFloat(rValue).toFixed(0)).toLocaleString('en');
      if (rCurrency === 'MMK') return `${roundedAmount} Ks`;
      if (rCurrency === 'HKD') return `HKD ${roundedAmount}`;
      if (rCurrency === 'USD') return `$${roundedAmount}`;
      if (rCurrency === 'VND') return `${roundedAmount} VND`;
      return `$${roundedAmount}`;
    }

  // this.timeline = () => {
  //   const startDate = this.earliestStartDate().format('MMM D');
  //   const endDate = this.latestEndDate().format('MMM D');

  //   if (this.job.type.includes('FULL_TIME')) {
  //     return `${startDate} - ${i18next.t('permanent')}`;
  //   }
  //   return `${startDate} - ${endDate}`;
  // };
  // this.shiftTimeline = () => {
  //   const startTime = this.earliestStartDate().format('hh:mmA');
  //   const endTime = this.latestEndDate().format('hh:mmA');
  //   return `${startTime} - ${endTime}`;
  // };
  // this.type = () => {
  //   switch (job.type) {
  //     case 'FULL_TIME':
  //       return i18next.t('fullTime');
  //     case 'PART_TIME':
  //       return i18next.t('partTime');
  //     case 'CONTRACT':
  //       return i18next.t('contract');
  //     default:
  //       return '';
  //   }
  // };
}
