/* eslint-disable camelcase */
import en from './en';
import vn from './vn';
import mm from './mm';
import zg from './zg';
import zh_Hans from './zh_Hans';
import zh_Hant from './zh_Hant';

const locales = {
  en,
  mm,
  vn,
  zg,
  zh_Hans,
  zh_Hant,
  translate: (langVariableName, lang) => {
    let translation;
    if (lang === 'en') {
      translation = locales.en[langVariableName];
    }
    if (lang === 'vn') {
      translation = locales.vn[langVariableName];
    }
    if (lang === 'mm') {
      translation = locales.mm[langVariableName];
    }
    if (lang === 'zg') {
      translation = locales.zg[langVariableName];
    }
    if (lang === 'zh_Hant') {
      translation = locales.zh_Hant[langVariableName];
    }
    return translation;
  },
};

export default locales;
