import Vue from 'vue';
import VueApollo from 'vue-apollo';
import { ApolloClient } from 'apollo-client';
import { ApolloLink, from } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { RetryLink } from 'apollo-link-retry';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';
import store from './store';
import eventHub from '../utils/event-bus';
import { auth } from './firebase';
import timeouts from '../utils/timeouts';

const httpLink = new HttpLink({
  uri: process.env.API_URL,
});
// dynamic header for when tokens change
const middlewareLink = setContext(async () => {
  // const { getters } = store;
  // console.log('getters', store.getters);
  if (auth.currentUser) {
    const token = await auth.currentUser.getIdToken();
    // eslint-disable-next-line import/no-named-as-default-member
    if (store.getters.token !== token) {
      await store.dispatch('setToken', token);
      // eslint-disable-next-line no-console
      console.log('INFO: token is different');
    }
  }
  return ({
    headers: {
      // eslint-disable-next-line import/no-named-as-default-member
      authorization: store.getters.token,
      version: process.env.VUE_APP_VERSION,
      app: 'client_employer',
    },
  });
});
const authHttpLink = middlewareLink.concat(httpLink);

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors != null && graphQLErrors.length === 1) {
    if (graphQLErrors[0].message.includes('Incompatible version')) {
      // eslint-disable-next-line no-alert
      alert('Please clear your browser cache');
    }
  }
  if (graphQLErrors) {
    graphQLErrors.map(
      // eslint-disable-next-line no-unused-vars
      ({ message, locations, path }) => true,
    );
  }
  if (networkError) Vue.$log.error(`[Network error]: ${networkError}`);
});

const retryLink = new RetryLink({
  delay: {
    initial: 100,
    max: Infinity,
    jitter: true,
  },
  attempts: {
    max: 5,
    retryIf: error => !!error,
  },
});

const setNetworkTimeoutLink = new ApolloLink((operation, forward) => {
  const networkTimeouts = timeouts.setNetworkTimeouts();
  operation.setContext({ networkTimeouts });
  return forward(operation);
});

const clearNetworkTimeoutLink = new ApolloLink(
  (operation, forward) => forward(operation).map((data) => {
    const context = operation.getContext();
    timeouts.clearTimeouts(context.networkTimeouts);
    return data;
  }),
);

const networkTimeoutLink = from([
  setNetworkTimeoutLink,
  clearNetworkTimeoutLink,
]);

const cache = new InMemoryCache();

const apolloClient = new ApolloClient({
  cache,
  rejectUnauthorized: false,
  name: 'client_employer',
  version: '28.02.20',
  link: ApolloLink.from([errorLink, retryLink, networkTimeoutLink, authHttpLink]),
});

// VueApollo (graphql implmentation) provider holds the apollo client instances
Vue.use(VueApollo);
export default new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
    $query: {
      fetchPolicy: 'cache-and-network',
      loadingKey: 'loading',
      prefetch: false,
    },
  },
});
