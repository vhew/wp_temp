import gql from 'graphql-tag';
import apollo from '@/assets/js/apollo';
import Vue from 'vue';

const services = {
  createMarketplaceSubscriber(args) {
    Vue.$log.info('MarketplaceService.createMarketplaceSubscriber', args);
    const CREATE_EXPECTED_MARKETPLACE_SUBSCRIBER = gql`
      mutation createMarketplaceSubscriber(
        $name: String!,
        $phone: String,
        $email: String,
        $identification: String!,
        $marketplaceId: ID!,
      ) {
        createMarketplaceSubscriber(
          name: $name,
          phone: $phone,
          email: $email,
          identification: $identification,
          marketplaceId: $marketplaceId,
        ) {
          id
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CREATE_EXPECTED_MARKETPLACE_SUBSCRIBER,
      variables: args,
    });
  },

  updateMarketplaceSubscriberList(args) {
    Vue.$log.info('MarketplaceService.updateMarketplaceSubscriberList', args);
    const UPDATE_EXPECTED_MARKETPLACE_SUBSCRIBER_LIST = gql`
      mutation updateMarketplaceSubscriberList(
        $marketplaceId: ID!,
        $subscriberList: [MarketplaceSubscriberInput]
      ) {
        updateMarketplaceSubscriberList(
          marketplaceId: $marketplaceId,
          subscriberList: $subscriberList
        )
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_EXPECTED_MARKETPLACE_SUBSCRIBER_LIST,
      variables: args,
    });
  },

  createJobRole: (args) => {
    Vue.$log.info('MarketplaceService.createJobRole', args);
    const CREATE_JOB_ROLE = gql`
      mutation createJobRole(
        $name: String!,
        $description: String,
      ) {
        createJobRole(
          name: $name,
          description: $description,
        ) {
          id
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: CREATE_JOB_ROLE,
      variables: args,
    });
  },

  addJobRoleJobSkill: (args) => {
    Vue.$log.info('MarketplaceService.addJobRoleJobSkill', args);
    const ADD_JOB_ROLE_JOB_SKILL = gql`
      mutation addJobRoleJobSkill(
        $jobRoleId: ID!,
        $name: String,
        $description: String,
        $levels: [ GraphQLJSON ]
        $requiredDocuments: [ String! ]
      ) {
        addJobRoleJobSkill(
          jobRoleId: $jobRoleId,
          name: $name,
          description: $description,
          levels: $levels,
          requiredDocuments: $requiredDocuments,
        ) {
          id
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: ADD_JOB_ROLE_JOB_SKILL,
      variables: args,
    });
  },
};

const MarketplaceService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local MarketplaceService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$MarketplaceService = services;
  },
};

export default MarketplaceService;
