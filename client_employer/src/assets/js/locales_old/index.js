/* eslint-disable camelcase */
import en from './en';
import mm from './mm';
import zg from './zg';

const locales = {
  en,
  mm,
  zg,
  translate: (langVariableName, lang) => {
    let translation;
    if (lang === 'en') {
      translation = locales.en[langVariableName];
    }
    if (lang === 'mm') {
      translation = locales.mm[langVariableName];
    }
    if (lang === 'zg') {
      translation = locales.zg[langVariableName];
    }
    if (lang === 'zh_Hant') {
      translation = locales.zh_Hant[langVariableName];
    }
    return translation;
  },
};

export default locales;
