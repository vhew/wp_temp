const en = {
  khupayStatus: 'ခုေပးမွသတင္းပို႔ျခင္း',
  confirmSuccessNotification: '@@@ မွသင္အလုပ္ဆင္းရက္ ### ရက္ကိုအတည္ျပဳေပးၿပီးပါၿပီ။ သင့္ရဲ႕ခု‌ေပးလစာကို မၾကာမီမွာထုတ္ယူႏိုင္မွာျဖစ္ပါတယ္။',
  confirmRejectNotification: '@@@ မွသင္အလုပ္ဆင္းရက္ ### ရက္ကိုအတည္ျပဳၿပီးပါၿပီ။ သင့္ရဲ႕ခု‌ေပး‌ေတာင္းခံျခင္းမေအာင္ျမင္ပါ။ဒီေန႔မွာ‌ေတာ့ အတည္ျပဳၿပီး *** ကိုခု‌ေပးေတာင္းခံႏိုင္ပါၿပီ။',
  disbursementNotification: 'ခုေပး ေပးျပီးပါျပီ။',

  choosePaymentMethod: 'Choose Payment Method',
  paymentOption: 'Payment Option',
  runPayroll: 'Run Payroll',
  previousPayCyclePayroll: 'Previous Pay Cycle Payroll',
  currentPayCyclePayroll: 'Current Pay Cycle Payroll',
  update: 'Update',
  organisationSettingsUpdated: 'Organization Settings Updated',
};

export default en;
