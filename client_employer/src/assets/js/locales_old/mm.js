const en = {
  khupayStatus: ' ခုပေးမှသတင်းပို့ခြင်း',
  confirmSuccessNotification: '@@@ မှသင်အလုပ်ဆင်းရက် ### ရက်ကိုအတည်ပြုပေးပြီးပါပြီ။ သင့်ရဲ့ခု‌ပေးလစာကို မကြာမီမှာထုတ်ယူနိုင်မှာဖြစ်ပါတယ်။',
  confirmRejectNotification: '@@@ မှသင်အလုပ်ဆင်းရက် ### ရက်ကိုအတည်ပြုပြီးပါပြီ။ သင့်ရဲ့ခု‌ပေး‌တောင်းခံခြင်းမအောင်မြင်ပါ။ဒီနေ့မှာ‌တော့ အတည်ပြုပြီး *** ကိုခု‌ပေးတောင်းခံနိုင်ပါပြီ။ ',
  disbursementNotification: 'ခုပေး ပေးပြီးပါပြီ။',

  choosePaymentMethod: 'Choose Payment Method',
  paymentOption: 'Payment Option',
  runPayroll: 'Run Payroll',
  previousPayCyclePayroll: 'Previous Pay Cycle Payroll',
  currentPayCyclePayroll: 'Current Pay Cycle Payroll',
  update: 'Update',
  organisationSettingsUpdated: 'Organization Settings Updated',
};

export default en;
