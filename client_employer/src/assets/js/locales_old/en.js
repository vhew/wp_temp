const en = {
  khupayStatus: 'Status',
  confirmSuccessNotification: '@@@ has confirmed your work attendance for ### days. Your KhuPay|Salary will soon be disbursed.',
  confirmRejectNotification: '@@@ has confirmed your work attendance for ### days. Your KhuPay request was not successful.Today you may request KhuPay for *** Ks, with instant approval.',
  disbursementNotification: '@@@ has been sent to ### ($$$).',

  choosePaymentMethod: 'Choose Payment Method',
  paymentOption: 'Payment Option',
  runPayroll: 'Run Payroll',
  previousPayCyclePayroll: 'Previous Pay Cycle Payroll',
  currentPayCyclePayroll: 'Current Pay Cycle Payroll',
  update: 'Update',
  organisationSettingsUpdated: 'Organization Settings Updated',
};

export default en;
