import lodash from 'lodash';

const mixins = {
  methods: {
    getSample: () => 'sample',

    formatCurrency: amount => amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','),


    // shiftsByWorker(job, workerId) {
    //   const shifts = [];
    //   job.shifts.forEach((shift) => {
    //     if (shift.worker) {
    //       if (shift.worker.id === workerId) {
    //         if (shift.fiatTokens.length === 0) {
    //           shifts.push(shift);
    //         }
    //       }
    //     }
    //   });
    //   return shifts;
    // },

    groupBy: (arr, property) => arr.reduce((memo, x) => {
      // eslint-disable-next-line no-param-reassign
      if (!memo[x[property]]) { memo[x[property]] = []; }
      memo[x[property]].push(x);
      return memo;
    }, {}),

    extractWorkersFromJobs: (jobs, params) => {
      let workers = [];
      workers = jobs.map(job => job.shifts.map(shift => shift.worker));
      workers = workers.flat();

      if (params.indexOf('verified') > -1) {
        workers = workers.filter(worker => !worker.id.includes('unverified'));
      }
      return lodash.uniq(workers, worker => worker.id);
    },

    roundTo: (n, digits) => {
      const multiplicator = 10 ** digits;
      const nFixed = parseFloat((n * multiplicator).toFixed(11));
      return Math.round(nFixed) / multiplicator;
    },
  },
};

export default mixins;
