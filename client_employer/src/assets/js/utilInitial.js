/* eslint-disable no-unused-vars */
import XLSX from 'xlsx';
import PhoneValidator from 'awesome-phonenumber';
import validator from 'validator';
import lodash from 'lodash';

const util = {
  test: ({ a, b }) => a + b,

  /* Function Rules
  -----------------------------------------------------------------
  isX validateX {} => Boolean
  createX deleteX updateX readX format filterX getXfromY {} => {}
  generic: isX builtin_types => Boolean
  generic: filter builtin_types => builtin_types */

  // batchUpload
  readExcelFile: ({ file, fileResult }) => {
    const result = {};
    return {
      data: result,
    };
  },

  // validateWorkerJob: ({ data }) => Boolean,

  // batchUpload, xxxx ,xxx
  // isFileValid: ({file}) => Boolean,
  isValidFormat: ({ field, type }) => {
    const result = true;
    return result;
  },

  isNull: ({ field }) => true,

  // workerCreatePage
  formatPhoneNo: ({ phoneCode, phoneNo }) => {
    const result = '';
    return {
      data: result,
    };
  },

  // workerListPage
  filterWorkersWithJob: ({ workers }) => {
    const result = [];
    return {
      data: result,
    };
  },

  filterWorkerWithoutJob: ({ workers }) => {
    const result = [];
    return {
      data: result,
    };
  },
  // filterArray: ({ String, condition }) => [String],

  // paymentDisbursementPage
  getBonusInfo: ({ data }) => {
    const result = {};
    return {
      data: result,
    };
  },

  getShiftsFromJobs: ({ jobs }) => {
    const result = [];
    return {
      data: result,
    };
  },
  filterShiftsByWorker: ({ shifts, worker }) => {
    const result = [];
    return {
      data: result,
    };
  }, // TimeSheet Also Need
  getWorkTokenValuesFromShifts: ({ shifts }) => {
    const result = [];
    return {
      data: result,
    };
  }, // TimeSheet Also Need
  // getPayInfoFromXXX: ({customerXXX: data}) => {pay:[]},
  getWorkerFromShifts: ({ shifts }) => {
    const result = {};
    return {
      data: result,
    };
  },

  getMTDFromShifts: ({ shifts }) => {
    const result = {};
    return {
      data: result,
    };
  },
  getPaymentDataFromWorkTokenValues: ({ workTokenValues }) => {
    const result = {};
    return {
      data: result,
    };
  },

  getTotalAmountFromWorkTokenValues: ({ workTokenValues }) => {
    const result = 0;
    return { data: result };
  },

  getTotalFeeFromWorkTokenValues: ({ workTokenValues }) => {
    const result = 0;
    return { data: result };
  },

  findShiftByWorker: ({ shifts, worker }) => {
    const result = {};
    return {
      data: result,
    };
  },

  findJobByShift: ({ jobs, shift }) => {
    const result = {};
    return {
      data: result,
    };
  },

  // // timeSheet
  filterShiftsByWorkToken: ({ workTokens }) => {
    const result = [];
    return {
      data: result,
    };
  },

  filterJobsByShifts: ({ shifts }) => {
    const result = [];
    return {
      data: result,
    };
  },
  getCreatedDateFromWorkTokenValues: ({ workTokenValues }) => {
    const result = {};
    return {
      data: result,
    };
  },
  getWorkTokensFromShifts: ({ shifts }) => {
    const result = [];
    return {
      data: result,
    };
  },
  filterUnconfirmedWorkTokens: ({ workTokens }) => {
    const result = [];
    return {
      data: result,
    };
  },
  isShiftsAlreadyChecked: ({ shifts }) => {
    const result = true;
    return {
      data: result,
    };
  },
  isEachWorkTokenHasFiatToken: ({ workTokens }) => {
    const result = true;
    return {
      data: result,
    };
  },

  // // generic
  // isInArray: [] => Boolean,

};
module.exports = util;
