import gql from 'graphql-tag';
// import store from '@/assets/js/store';
import apollo from '@/assets/js/apollo';
import Vue from 'vue';

const services = {
  migrateWorker(args) {
    Vue.$log.info('OperationsService.migrateWorker', args);
    const MIGRATE_WORKER = gql`
      mutation migrateWorker($oldWorker: String!, $newWorker: String!) {
        migrateWorker(oldWorker: $oldWorker, newWorker: $newWorker) {
          __typename
          id
          name
        }
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: MIGRATE_WORKER,
      variables: {
        oldWorker: args.oldWorker || '',
        newWorker: args.newWorker || '',
      },
    });
  },

  marketplaceQuickHack(args) {
    Vue.$log.info('OperationsService.marketplaceQuickHack', args);
    const MARKETPLACE_QUICK_HACK = gql`
      mutation marketplaceQuickHack($workerId: String) {
        marketplaceQuickHack(workerId: $workerId)
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: MARKETPLACE_QUICK_HACK,
      variables: args,
    });
  },

  deleteWorkTokensAndWorkTokenValues(args) {
    Vue.$log.info('OperationsService.deleteWorkTokensAndWorkTokenValues', args);
    const GQL_MUTATION_STRING = gql`
      mutation deleteWorkTokensAndWorkTokenValues(
        $shiftId: ID!,
        $checkinTime: GraphQLDateTime!
      ) {
        deleteWorkTokensAndWorkTokenValues(
          shiftId: $shiftId,
          checkinTime: $checkinTime,
        )
      }
    `;
    return apollo.defaultClient.mutate({
      mutation: GQL_MUTATION_STRING,
      variables: args,
    });
  },

  updateServerConfig(args) {
    Vue.$log.info('OperationsService.updateServerConfig', args);
    const UPDATE_SERVER_CONFIG = gql`
      mutation updateServerConfig($identifier: String!, $feePercent: Float!) {
        updateServerConfig(identifier: $identifier, feePercent: $feePercent) {
          __typename
        }
      }`;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_SERVER_CONFIG,
      variables: args,
    });
  },

  deleteFiatTokens(args) {
    Vue.$log.debug('OperationsService.deleteFiatTokens', args);
    const DELETE_FIAT_TOKENS = gql`
      mutation deleteFiatTokens($transactionRef: String!) {
        deleteFiatTokens(transactionRef: $transactionRef)
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: DELETE_FIAT_TOKENS,
      variables: {
        transactionRef: args.transactionRef || '',
      },
    });
  },

  updateFeeByRegion(args) {
    const UPDATE_FEE_BY_REGION = gql`
      mutation updateFeeByRegion($region: String!, $fee: Float!) {
        updateFeeByRegion(region: $region, fee: $fee)
      }
    `;

    return apollo.defaultClient.mutate({
      mutation: UPDATE_FEE_BY_REGION,
      variables: {
        region: args.region || '',
        fee: args.fee || 0,
      },
    });
  },
};

const OperationsService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local Operations vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$OperationsService = services;
  },
};

export default OperationsService;
