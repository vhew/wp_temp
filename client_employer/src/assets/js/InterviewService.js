import gql from 'graphql-tag';
import Vue from 'vue';
// import store from '@/assets/js/store';
import apollo from '@/assets/js/apollo';

const services = {
  requestInterview(args) {
    Vue.$log.debug('calling InterviewService.requestInterview', args);
    const REQUEST_INTERVIEW = gql`
        mutation requestInterview(
          $workerId: ID!
          $jobId: ID!
          $shiftId: ID!
          $confirmed: Boolean
          $name: String
          $requirements: [String]
          $times: [GraphQLJSON]
          $interviewLength: Float 
          $interviewType: INTERVIEW 
          $address: String
        ) {
          requestInterview (
            workerId: $workerId,
            jobId: $jobId,
            shiftId: $shiftId,
            confirmed: $confirmed,
            name: $name,
            requirements: $requirements,
            times: $times,
            interviewLength: $interviewLength
            interviewType: $interviewType
            address: $address
          )
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: REQUEST_INTERVIEW,
      variables: args,
    });
  },
  updateInterview(args) {
    Vue.$log.debug('calling InterviewService.updateInterview', args);
    const REQUEST_INTERVIEW = gql`
        mutation updateInterview(
          $workerId: ID!
          $jobId: ID!
          $shiftId: ID!
          $requirements: [String]
          $times: [GraphQLJSON]
          $interviewLength: Float 
          $address: String
        ) {
          updateInterview (
            workerId: $workerId,
            jobId: $jobId,
            shiftId: $shiftId,
            requirements: $requirements,
            times: $times,
            interviewLength: $interviewLength
            address: $address
          )
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: REQUEST_INTERVIEW,
      variables: args,
    });
  },
  withdrawInterview(args) {
    Vue.$log.debug('calling InterviewService.withdrawInterview', args);
    const REQUEST_INTERVIEW = gql`
        mutation withdrawInterview(
          $workerId: ID!
          $jobId: ID!
          $shiftId: ID!
        ) {
          withdrawInterview (
            workerId: $workerId,
            jobId: $jobId,
            shiftId: $shiftId,
          )
        }
      `;
    return apollo.defaultClient.mutate({
      mutation: REQUEST_INTERVIEW,
      variables: args,
    });
  },
};

const InterviewService = {
  // eslint-disable-next-line no-shadow
  install(Vue) {
    Vue.$log.info('Installing the local InterviewService vue plugin');
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$InterviewService = services;
  },
};

export default InterviewService;
