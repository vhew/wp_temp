const CONTRACTS = {
  template1: `
Employment Agreement<br>
THIS AGREEMENT made as of the day of <h3>@@startDate@@</h3>, between
[name of employer] a corporation incorporated under the laws of the Province of Ontario,
<br>
WHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the
Employee desires to render such services on the terms and conditions set forth.
IN CONSIDERATION of the promises and other good and valuable consideration (the
sufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>
1. Employment<br>
The Employee agrees that he will at all times faithfully, industriously, and to the best of his skill,
ability, experience and talents, perform all of the duties required of his position. In carrying out
these duties and responsibilities, the Employee shall comply with all Employer policies,
procedures, rules and regulations, both written and oral, as are announced by the Employer from
time to time. It is also understood and agreed to by the Employee that his assignment, duties and
responsibilities and reporting arrangements may be changed by the Employer in its sole
discretion without causing termination of this agreement.<br>
2. Position Title<br>
As a <h3>@@title@@</h3>, the Employee is required to perform the following duties and undertake
the following responsibilities in a professional manner.
(a) As full compensation for all services provided the employee shall be paid at the
 rate of <h3>@@renumerationValue@@</h3>. Such payments shall be subject to such normal statutory deductions
 by the Employer.
(b) (may wish to include bonus calculations or omit in order to exercise discretion).
(c) The salary mentioned in paragraph (l)(a) shall be review on an annual basis.
(d) All reasonable expenses arising out of employment shall be reimbursed assuming
 same have been authorized prior to being incurred and with the provision of
 appropriate receipts.<br>
 <br>
 <br>3. Working Arrangements<br>
 Working Days: Mon to Fri <br><br>
 Working Hours: 09:00 AM to 06:00 PM<br><br>
 Lunch Hour: 12:00 PM to 01:00 PM <br><br>
 Sick Leave Days: 5 <br><br>
 Annual Leave Days: 10 <br><br>
 Other Leaves Days: 10 <br><br>
SIGNED, SEALED AND DELIVERED in the presence of:
<br><br>
<h3>@@companyName@@</h3><br>
----------------------------------------<br><br>
<h3>[Name of Employee]</h3><br>
----------------------------------------<br>
`,

  template2: `
Employment Agreement
THIS AGREEMENT made as of the ______day of__________________, 20__ , between
[name of employer] a corporation incorporated under the laws of the Province of Ontario,
and having its principal place of business at _______________________(the "Employer");
and [name of employee], of the City of ____________________in the Province of Ontario
(the "Employee").
WHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the
Employee desires to render such services on the terms and conditions set forth.
IN CONSIDERATION of the promises and other good and valuable consideration (the
sufficiency and receipt of which are hereby acknowledged) the parties agree as follows:
1. Employment
The Employee agrees that he will at all times faithfully, industriously, and to the best of his skill,
ability, experience and talents, perform all of the duties required of his position. In carrying out
these duties and responsibilities, the Employee shall comply with all Employer policies,
procedures, rules and regulations, both written and oral, as are announced by the Employer from
time to time. It is also understood and agreed to by the Employee that his assignment, duties and
responsibilities and reporting arrangements may be changed by the Employer in its sole
discretion without causing termination of this agreement.
2. Position Title
As a _____________, the Employee is required to perform the following duties and undertake
the following responsibilities in a professional manner.
(a)-.
(b) -
(c) -
(d) -
(e) Other duties as may arise from time to time and as may be assigned to the employee.
3. Compensation
(a) As full compensation for all services provided the employee shall be paid at the
 rate of ____. Such payments shall be subject to such normal statutory deductions
 by the Employer.
(b) (may wish to include bonus calculations or omit in order to exercise discretion).
(c) The salary mentioned in paragraph (l)(a) shall be review on an annual basis.
(d) All reasonable expenses arising out of employment shall be reimbursed assuming
 same have been authorized prior to being incurred and with the provision of
 appropriate receipts.
2
4. Vacation
The Employee shall be entitled to vacations in the amount of ____ weeks per annum.
5. Benefits
The Employer shall at its expense provide the Employee with the Health Plan that is currently in
place or as may be in place from time to time.
6. Probation Period
It is understood and agreed that the first ninety days of employment shall constitute a
probationary period during which period the Employer may, in its absolute discretion, terminate
the Employee's employment, for any reason without notice or cause.
7. Performance Reviews
The Employee will be provided with a written performance appraisal at least once per year and
said appraisal will be reviewed at which time all aspects of the assessment can be fully discussed.
8. Termination
(a) The Employee may at any time terminate this agreement and his employment by
giving not less than two weeks written notice to the Employer.
(b) The Employer may terminate this Agreement and the Employee’s employment at
any time, without notice or payment in lieu of notice, for sufficient cause.
(c) The Employer may terminate the employment of the Employee at any time
without the requirement to show sufficient cause pursuant to (b) above, provided
the Employer pays to the Employee an amount as required by the Employment
Standards Act 2000 or other such legislation as may be in effect at the time of
termination. This payment shall constitute the employees entire entitlement
arising from said termination.
(d) The employee agrees to return any property of ___________________________
at the time of termination.
3
9. Non- Competition
(1) It is further acknowledged and agreed that following termination of the
employee’s employment with ________________ for any reason the employee
shall not hire or attempt to hire any current employees of _________________.
(2) It is further acknowledged and agreed that following termination of the
employee’s employment with ________________ for any reason the employee
shall not solicit business from current clients or clients who have retained
________________ in the 6 month period immediately preceding the employee’s
termination.
10. Laws
This agreement shall be governed by the laws of the Province of Ontario.
11. Independent Legal Advice
The Employee acknowledges that the Employer has provided the Employee with a reasonable
opportunity to obtain independent legal advice with respect to this agreement, and that either:
(a) The Employee has had such independent legal advice prior to executing
 this agreement, or;
(b) The Employee has willingly chosen not to obtain such advice and to
execute this agreement without having obtained such advice.
12. Entire Agreement
This agreement contains the entire agreement between the parties, superseding in all respects any
and all prior oral or written agreements or understandings pertaining to the employment of the
Employee by the Employer and shall be amended or modified only by written instrument signed
by both of the parties hereto.
13. Severability
The parties hereto agree that in the event any article or part thereof of this agreement is held to be
unenforceable or invalid then said article or part shall be struck and all remaining provision shall
remain in full force and effect.

IN WITNESS WHEREOF the Employer has caused this agreement to be
executed by its duly authorized officers and the Employee has set his hand
as of the date first above written.
SIGNED, SEALED AND DELIVERED in the presence of:
________________________________________
[Name of employee]
________________________________________
[Signature of Employee]
________________________________________
[Name of Employer Rep]
________________________________________
[Signature of Employer Rep]
[Title]
`,

  template_mm: `
  Employment Agreement (Sample)<br>
  <br>
  THIS AGREEMENT made as of the day of <h3>@@startDate@@</h3>, between <h3>@@employerName@@</h3> a corporation incorporated under the laws of the legal jurisdiction of Myanmar with business registration of <h3>@@businessRegNumber@@</h3> and having its principal place of business at <h3>@@organisationAddress@@</h3> (the "Employer") and <h3>[Name of Employee]</h3> (the "Employee").<br>
  <br>
  WHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.
  IN CONSIDERATION of the promises and other good and valuable consideration (the
  sufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>
  1. Employment<br>
  The Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.
  <br><br>
  2. Position Title<br>
  As a <h3>@@title@@</h3>, the Employee is required to perform the following duties and undertake
  the following responsibilities in a professional manner.<br>
  (a) As full compensation for all services provided the employee shall be paid at the
  rate of <h3>@@renumerationValue@@</h3>. Such payments shall be subject to such normal statutory deductions
  by the Employer.<br>
  (b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>
  (c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>
  (d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br><br>
  3. Working Arrangements<br>
  @@workingArrangements@@<br><br>
  Sick Leave Days: <b>@@sickLeaveDays@@</b><br>
  Annual Leave Days: <b>@@personalLeaveDays@@</b><br>
  Other Leaves Days: <b>@@specialLeaveDays@@</b><br>

  <br>
  SIGNED, SEALED AND DELIVERED in the presence of:<br>
  <br>
  <h3>@@employerName@@</h3><br>
  ----------------------------------------<br>
  <br>
  <h3>[Signature of Employee]</h3><br>
  ----------------------------------------<br>
`,
  template_hk: `
Employment Agreement (Sample)<br>
<br>
THIS AGREEMENT made as of the day of <h3>@@startDate@@</h3>, between <h3>@@employerName@@</h3> a corporation incorporated under the laws of the legal jurisdiction of Hong Kong with business registration of <h3>@@businessRegNumber@@</h3> and having its principal place of business at <h3>@@organisationAddress@@</h3> (the "Employer") and <h3>[Name of Employee]</h3> (the "Employee").<br>
<br>
WHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.
IN CONSIDERATION of the promises and other good and valuable consideration (the
sufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>
1. Employment<br>
The Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.
<br><br>
2. Position Title<br>
As a <h3>@@title@@</h3>, the Employee is required to perform the following duties and undertake
the following responsibilities in a professional manner.<br>
(a) As full compensation for all services provided the employee shall be paid at the
rate of <h3>@@renumerationValue@@</h3>. Such payments shall be subject to such normal statutory deductions
by the Employer.<br>
(b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>
(c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>
(d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br>
<br>3. Working Arrangements<br>
@@workingArrangements@@<br><br>
Sick Leave Days: <b>@@sickLeaveDays@@</b><br>
Annual Leave Days: <b>@@personalLeaveDays@@</b><br>
Other Leaves Days: <b>@@specialLeaveDays@@</b><br>
<br>
SIGNED, SEALED AND DELIVERED in the presence of:<br>
<br>
<h3>@@employerName@@</h3><br>
----------------------------------------<br>
<br>
<h3>[Signature of Employee]</h3><br>
----------------------------------------<br>
`,
  template_vn: `
Employment Agreement (Sample)<br>
<br>
THIS AGREEMENT made as of the day of <h3>@@startDate@@</h3>, between <h3>@@employerName@@</h3> a corporation incorporated under the laws of the legal jurisdiction of Vietnam with business registration of <h3>@@businessRegNumber@@</h3> and having its principal place of business at <h3>@@organisationAddress@@</h3> (the "Employer") and <h3>[Name of Employee]</h3> (the "Employee").<br>
<br>
WHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.
IN CONSIDERATION of the promises and other good and valuable consideration (the
sufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>
1. Employment<br>
The Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.
<br><br>
2. Position Title<br>
As a <h3>@@title@@</h3>, the Employee is required to perform the following duties and undertake
the following responsibilities in a professional manner.<br>
(a) As full compensation for all services provided the employee shall be paid at the
rate of <h3>@@renumerationValue@@</h3>. Such payments shall be subject to such normal statutory deductions
by the Employer.<br>
(b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>
(c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>
(d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br>
<br>3. Working Arrangements<br>
@@workingArrangements@@<br><br>
Sick Leave Days: <b>@@sickLeaveDays@@</b><br>
Annual Leave Days: <b>@@personalLeaveDays@@</b><br>
Other Leaves Days: <b>@@specialLeaveDays@@</b><br>
<br>
SIGNED, SEALED AND DELIVERED in the presence of:<br>
<br>
<h3>@@employerName@@</h3><br>
----------------------------------------<br>
<br>
<h3>[Signature of Employee]</h3><br>
----------------------------------------<br>
`,

  working_arrangements_temp_full_time: `
  Working Days: <b>@@workingDays@@</b><br>
  Working Hours: <b>@@workingHours@@</b><br>
  Lunch Hour: <b>@@lunchHour@@</b>`,
};


export default CONTRACTS;
