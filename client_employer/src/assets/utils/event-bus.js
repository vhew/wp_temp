import Vue from 'vue';

const EventHub = new Vue();

Vue.prototype.$eventHub = EventHub;

export default EventHub;
