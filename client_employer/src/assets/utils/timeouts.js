import eventHub from './event-bus';

export default {
  setNetworkTimeouts: () => {
    const badConnectionTimeout = setTimeout(() => {
      eventHub.$emit('show-alert-bad-connection');
    }, 20 * 1000); // timeout 5s
    const networkTimeout = setTimeout(() => {
      eventHub.$emit('show-alert-network-timeout');
    }, 2 * 60 * 1000); // timeout 2m

    return [networkTimeout, badConnectionTimeout];
  },
  /**
   * @param {[*]} timeouts
   */
  clearTimeouts: (timeouts) => {
    if (Array.isArray(timeouts)) {
      timeouts.forEach((timeout) => {
        clearTimeout(timeout);
      });
    }
  },
};
