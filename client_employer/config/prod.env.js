module.exports = {
  NODE_ENV: '"production"',
  API_URL: '"https://api.jobdoh.com/v1"',
  VUE_APP_VERSION: '"9981112878248ccbe7385813d654802c7d079fd5"',
  GMAP_API: '"AIzaSyCOh2V6qgvNLSdEHUR0ei7upR_usL9RhYc"',
  GEOCODE_API: '"AIzaSyDsnu1bWrxP1Jv6qxjtWULX8negbALv3mE"',
  LOG_LEVEL: '"warn"'
}
