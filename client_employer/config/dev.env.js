'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL: '"http://localhost:8000/v1"',
  VUE_APP_VERSION: '"9981112878248ccbe7385813d654802c7d079fd5"',
  GMAP_API: '"AIzaSyCOh2V6qgvNLSdEHUR0ei7upR_usL9RhYc"',
  GEOCODE_API: '"AIzaSyDsnu1bWrxP1Jv6qxjtWULX8negbALv3mE"',
  LOG_LEVEL: '"debug"'
})
