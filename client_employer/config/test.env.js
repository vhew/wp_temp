'use strict'

const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  API_URL: '"https://staging-api.jobdoh.com/v1"',
  VUE_APP_VERSION: '"9981112878248ccbe7385813d654802c7d079fd5"',
  GMAP_API: '"AIzaSyCOh2V6qgvNLSdEHUR0ei7upR_usL9RhYc"',
  GEOCODE_API: '"AIzaSyDsnu1bWrxP1Jv6qxjtWULX8negbALv3mE"'
})
