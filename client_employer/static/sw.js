/* eslint-disable no-console */
// eslint-disable-next-line no-undef
importScripts('https://www.gstatic.com/firebasejs/7.8.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.8.0/firebase-messaging.js');

const config = {
  messagingSenderId: '996263494846',
  projectId: 'jobdoh2employer',
  apiKey: 'AIzaSyA5m_XMaeAHs3nars-eKmF5CLuYdYDVsyQ',
  authDomain: 'jobdoh2employer.firebaseapp.com',
  databaseURL: 'https://jobdoh2employer.firebaseio.com',
  storageBucket: 'jobdoh2employer.appspot.com',
  appId: '1:996263494846:web:9d65cd2bcd09ca862b4aef',
  measurementId: 'G-GWLX7HBZXS',
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

const services = {
  async getDb() {
    return new Promise((resolve, reject) => {
      const request = self.indexedDB.open('jobdoh', 1);

      request.onerror = (e) => {
        console.log('Error opening db', e);
        // eslint-disable-next-line prefer-promise-reject-errors
        reject('Error');
      };

      request.onsuccess = (e) => {
        resolve(e.target.result);
      };

      request.onupgradeneeded = (e) => {
        console.log('onupgradeneeded');
        const db = e.target.result;
        db.createObjectStore('notifications', { autoIncrement: true, keyPath: 'id' });
      };
    });
  },
  async getNotificationsFromDb() {
    const db = await services.getDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['notifications'], 'readonly');
      trans.oncomplete = () => {
        // eslint-disable-next-line no-use-before-define
        resolve(notifications);
      };

      const store = trans.objectStore('notifications');
      let notifications = [];

      store.openCursor().onsuccess = (e) => {
        const cursor = e.target.result;
        if (cursor) {
          notifications.push(cursor.value);
          cursor.continue();
        }
      };
    });
  },
  async addNotificaitonsToDb(notification) {
    console.log('addNotificaitonsToDb', notification);
    const db = await services.getDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['notifications'], 'readwrite');
      trans.oncomplete = () => {
        resolve();
      };

      const store = trans.objectStore('notifications');
      store.add(notification);
    });
  },
  async editNotificaitonsToDb(notification) {
    const db = await services.getDb();
    return new Promise((resolve) => {
      const trans = db.transaction(['notifications'], 'readwrite');
      trans.oncomplete = () => {
        resolve();
      };

      const store = trans.objectStore('notifications');
      store.put(notification);
    });
  },

};
// const { services } = require('../src/assets/js/services/IdbService');

messaging.setBackgroundMessageHandler((payload) => {
  console.log('self', this);
  console.log('[firebase-messaging-sw.js] Received background message ', payload);

  // const background_notifications = localStorage.getItem('background_notifications');
  // background_notifications.push(payload.data);
  // localStorage.setItem('background_notifications', background_notifications);
  const notifications = services.getNotificationsFromDb();
  let filteredNotifications = [];
  if (notifications != null && notifications.length > 0) {
    filteredNotifications = notifications
      .filter(e => e.title === payload.data.title && e.body === payload.data.body);
  }
  const data = {
    ...payload.data,
    receiveDate: new Date(),
    isRead: false,
  };
  if (filteredNotifications.length === 0) {
    services.addNotificaitonsToDb(data);
  }
  // storex.dispatch('refreshNotifications');
  // Customize notification here
  // const notificationTitle = payload.data.title;
  // const notificationOptions = {
  //   body: payload.data.body,
  //   icon: '/img/icons/icon-72x72.png',
  // };

  // return self.registration.showNotification(notificationTitle,
  //   notificationOptions);
});
