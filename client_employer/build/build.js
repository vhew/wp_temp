'use strict'

require('./check-versions')()

process.env.NODE_ENV = process.argv[2];
// eslint-disable-next-line no-console
console.log(process.env.NODE_ENV);

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const config = require('../config')
let webpackConfig;
let spinner;
if (process.env.NODE_ENV === 'staging') {
  webpackConfig = require('./webpack.staging.conf')
  spinner = ora('building for staging...')
} else {
  webpackConfig = require('./webpack.prod.conf')
  spinner = ora('building for production...')
}
spinner.start()

rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')
    // eslint-disable-next-line no-console
    console.log(chalk.cyan('  Build complete.\n'))
    // eslint-disable-next-line no-console
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
  })
})
