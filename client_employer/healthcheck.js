const debug = require("debug")("INFO");
const http = require("http");

const options = {
  host: "localhost",
  port: "5001",
  path: "/health",
  timeout: 2000
};

const request = http.request(options, res => {
  debug("STATUS: %s", res.statusCode);
  if (res.statusCode === 400) {
    process.exit(0);
  } else {
    process.exit(1);
  }
});

request.on("error", err => {
  debug("ERROR: %O", err);
  process.exit(1);
});

request.end();
