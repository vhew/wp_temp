#--------- GitLab Agent CI/CD Tunnel configuration
# Set KUBE_CONTEXT to use with the GitLab Kubernetes Agent's CI/CD tunnel. See
# https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html.
#-----------------------------------
# variables:
# Set the variable $KUBE_CONTEXT through the GitLab UI, or set it here by
# uncommenting the following two lines and replacing the Agent's path with your own:
# variables:
#   KUBE_CONTEXT: .gitlab/agents/gitlab-kubernetes-agent:gitlab-kubernetes-agent



# -------- Helm v2 release detection
# The detect-helm2-release will try to identify if you have any app installed with Helm v2.
# If so, it will fail the pipeline and ask you to migrate those apps to Helm v3, since
# the gl-helmfile utility will be using Helm v3 to manage your apps.
#
# If this job succeeds, it means you don't have any Helm v2 apps, so you can completely remove
# the detect-helm2-releases job from your pipeline.
#
# By default, the gl-fail-if-helm2-releases-exist utility script is checking for apps possibly
# installed in the gitlab-managed-apps namespace. If you were using a different namespace, feel
# free to replace gitlab-managed-apps by whatever that is in the script below.
#-----------------------------------


stages:
  - test
  - build
  - staging
  - production

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
.kube-context:
  before_script:
    - if [ -n "$KUBE_CONTEXT" ]; then kubectl config use-context "$KUBE_CONTEXT"; fi

before_script:
#  - npm install npm@latest -g
#  - node -v
#  - which node
#  - pwd
#  - ls
#  - env

detect-helm2-releases:
  extends: [.kube-context]
  stage: test
  image: "registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:v1.3.2"
  environment:
    name: staging/${CI_COMMIT_REF_NAME}
  script:
    - gl-fail-if-helm2-releases-exist gitlab-managed-apps
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

apply:
  extends: [.kube-context]
  stage: staging
  image: "registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:v1.3.2"
  environment:
    name: staging/${CI_COMMIT_REF_NAME}
  script:
    - gl-ensure-namespace gitlab-managed-apps
    - gl-helmfile --file $CI_PROJECT_DIR/helmfile.yaml apply --suppress-secrets
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

#####################################
#       TESTS - ESLINT & UNIT       #
#-----------------------------------#

test_server:
  image: node:10.15-alpine
  stage: test
  variables:
    NODE_ENV: "testing"
    PROCESS_TYPE: "api"
    LOGGER_LEVEL: "error"
    LOGGER_ENABLED: "FALSE"
    PORT: 80
    DB_TYPE: "mongodb+srv"
    DB_HOST: "cluster0-qagfz.gcp.mongodb.net"
    DB_NAME: "jobdoh_testing"
    DB_USER: "jobdoh_testing"
    DB_PWD: "passw0rd"
    AMQP_URL: "amqp://jobdoh:${CI_STAGING_RABBITMQ_PASS}@staging-rabbitmq-svc:5672"
    FIREBASE_PRIVATE_KEY: ${CI_FIREBASE_PRIVATE_KEY}
  script:
    - cd server
    - npm install
    - node scripts/db_clear.js
    - npm test
  only:
    - master_leave_system
    - master_interview
    - master_remote_job_updates    
    - master_employer_readonly
    - master_rating_sys
    - master_job_engagement
    - master_user_journey
    - master_job_filter
    - master_rankings
    - master_ces
    - master_employer_ui_changes
    - master_backend_refactor
    - master_user_permission
    - master_query_fixes
    - master_job_filter
    - master_job_create_improvements
    - master_bug_fixes_phoenix
    - master_bug_fixes_akp
    - master_multiple_shift
    - version
    - master
    - production

test_client_employer:
  image: liboxuanhk/docker-node-python-chrome
  stage: test
  variables:
    NODE_ENV: "testing"
    NODE_OPTIONS: "--max-old-space-size=8192"
  script:
    - cd client_employer
    - npm install
    - npm test
  only:
    - master_job_create_improvements
    - master_bug_fixes_phoenix
    - master_multiple_shift
    - employer
    - version
    - master
    - production

# create server container on the gitlab registry
build_staging_server:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - cat /kaniko/.docker/config.json
    - echo ${CI_PROJECT_DIR} ${CI_REGISTRY} ${CI_REGISTRY_IMAGE} ${CI_COMMIT_REF_NAME}
    - /kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/server/Dockerfile.staging --destination ${CI_REGISTRY}/${CI_REGISTRY_IMAGE}/server:${CI_COMMIT_REF_NAME}
  only:
    - master
build_production_server:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - cat /kaniko/.docker/config.json
    - echo ${CI_PROJECT_DIR} ${CI_REGISTRY} ${CI_REGISTRY_IMAGE} ${CI_COMMIT_REF_NAME}
    - /kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/server/Dockerfile.production --destination ${CI_REGISTRY}/${CI_REGISTRY_IMAGE}/server:${CI_COMMIT_REF_NAME}
  only:
    - production

# BUILD EMPLOYER
build_staging_client_employer:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/client_employer/Dockerfile.staging --destination ${CI_REGISTRY}/${CI_REGISTRY_IMAGE}/client_employer:${CI_COMMIT_REF_NAME}
  only:
    - master
build_production_client_employer:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/client_employer/Dockerfile.production --destination ${CI_REGISTRY}/${CI_REGISTRY_IMAGE}/client_employer:${CI_COMMIT_REF_NAME}
  only:
    - production
build_staging_starport:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - cat /kaniko/.docker/config.json
    - echo ${CI_PROJECT_DIR} ${CI_REGISTRY} ${CI_REGISTRY_IMAGE} ${CI_COMMIT_REF_NAME}
    - /kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/Dockerfile --destination ${CI_REGISTRY}/${CI_REGISTRY_IMAGE}/starport_app:${CI_COMMIT_REF_NAME}
  only:
    - master
#####################################
#              DEPLOY               #
#-----------------------------------#

# DEPLOY SERVER
deploy_staging_server:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: staging
  only:
    - master
  variables:
    NODE_ENV: "staging"
    SUBDOMAIN: "staging-server"
    PORT: 8000
    DB_TYPE: "mongodb+srv"
    DB_HOST: "cluster0-qagfz.gcp.mongodb.net"
    DB_PORT: 27017
    DB_NAME: "worknetwork_staging"
    DB_USER: "worknetwork_staging"
    DB_PWD: "${CI_DB_PWD}"
    AMQP_URL: "amqp://jobdoh:${CI_STAGING_RABBITMQ_PASS}@staging-rabbitmq-svc:5672"
    FIREBASE_PRIVATE_KEY: ${CI_FIREBASE_PRIVATE_KEY}
    DEBUG: "ERROR,WARN,INFO"
    NAMESPACE: ${KUBE_NAMESPACE}
    DEPLOYMENT_SLUG: "server-${CI_ENVIRONMENT_SLUG}"
  environment:
    name: staging/${CI_COMMIT_REF_NAME}
    url: http://${SUBDOMAIN}.worknetwork.io
    on_stop: stop_staging_server
  script:
    - kubectl version
    - echo ${NAMESPACE}
    - echo ${CI_ENVIRONMENT_SLUG}
    - echo ${CI_COMMIT_REF_NAME}
    - echo ${HOSTNAME}
    - echo ${AMQP_URL}
    - kubectl create configmap nodeenv-staging --from-literal=NODE_ENV=${NODE_ENV} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap serverport-staging --from-literal=PORT=${PORT} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbtype-staging --from-literal=DB_TYPE=${DB_TYPE} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbhost-staging --from-literal=DB_HOST=${DB_HOST} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbport-staging --from-literal=DB_PORT=${DB_PORT} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbname-staging --from-literal=DB_NAME=${DB_NAME} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbuser-staging --from-literal=DB_USER=${DB_USER} -o yaml --dry-run | kubectl apply -f -
    - kubectl create secret generic dbpwd-staging --from-literal=DB_PWD=${DB_PWD} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap amqpurl-staging --from-literal=AMQP_URL=${AMQP_URL} -o yaml --dry-run | kubectl apply -f -
    - kubectl create secret generic firebaseprivatekey-staging --from-literal=FIREBASE_PRIVATE_KEY="${FIREBASE_PRIVATE_KEY}" -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap debug-staging --from-literal=DEBUG=${DEBUG} -o yaml --dry-run | kubectl apply -f -
    - cd server/manifests/
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${DEPLOYMENT_SLUG}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__SUBDOMAIN__/${SUBDOMAIN}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__NAMESPACE__/${NAMESPACE}/" ingress.yaml service.yaml
    - |
      if kubectl apply -f deployment_staging.yaml | grep -q unchanged; then
          echo "=> Patching deployment to force image update."
          kubectl patch -f deployment_staging.yaml -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"ci-last-updated\":\"$(date +'%s')\"}}}}}"
      else
          echo "=> Deployment apply has changed the object, no need to force image update."
      fi
    - kubectl apply -f service.yaml || true
    - kubectl apply -f ingress.yaml
    - kubectl rollout status -f deployment_staging.yaml
    - kubectl get all,ing -l ref=${DEPLOYMENT_SLUG}
deploy_production_server:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: production
  only:
    - production
  variables:
    NODE_ENV: "production"
    SUBDOMAIN: "server"
    PORT: 8000
    DB_TYPE: "mongodb+srv"
    DB_HOST: "cluster0-qagfz.gcp.mongodb.net"
    DB_PORT: 27017
    DB_NAME: "worknetwork_production"
    DB_USER: "worknetwork_production"
    DB_PWD: ${CI_PRODUCTION_DB_PWD}
    AMQP_URL: "amqp://jobdoh:${CI_PRODUCTION_RABBITMQ_PASS}@production2-rabbitmq-svc:5672"
    FIREBASE_PRIVATE_KEY: ${CI_FIREBASE_PRIVATE_KEY}
    DEBUG: "ERROR,WARN"
    NAMESPACE: ${KUBE_NAMESPACE}
    DEPLOYMENT_SLUG: "server-${CI_ENVIRONMENT_SLUG}"
  environment:
    name: production/${CI_COMMIT_REF_NAME}
    url: http://${SUBDOMAIN}.worknetwork.io
    on_stop: stop_production_server
  script:
    - kubectl version
    - echo ${DEPLOYMENT_SLUG}
    - echo ${CI_COMMIT_REF_NAME}
    - echo ${HOSTNAME}
    - echo ${AMQP_URL}
    - kubectl create configmap nodeenv-production --from-literal=NODE_ENV=${NODE_ENV} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap serverport-production --from-literal=PORT=${PORT} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbtype-production --from-literal=DB_TYPE=${DB_TYPE} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbhost-production --from-literal=DB_HOST=${DB_HOST} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbport-production --from-literal=DB_PORT=${DB_PORT} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbname-production --from-literal=DB_NAME=${DB_NAME} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap dbuser-production --from-literal=DB_USER=${DB_USER} -o yaml --dry-run | kubectl apply -f -
    - kubectl create secret generic dbpwd-production --from-literal=DB_PWD=${DB_PWD} -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap amqpurl-production --from-literal=AMQP_URL=${AMQP_URL} -o yaml --dry-run | kubectl apply -f -
    - kubectl create secret generic firebaseprivatekey-production --from-literal=FIREBASE_PRIVATE_KEY="${FIREBASE_PRIVATE_KEY}" -o yaml --dry-run | kubectl apply -f -
    - kubectl create configmap debug-production --from-literal=DEBUG=${DEBUG} -o yaml --dry-run | kubectl apply -f -
    - cd server/manifests/
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${DEPLOYMENT_SLUG}/" deployment_production.yaml ingress.yaml service.yaml
    - sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" deployment_production.yaml ingress.yaml service.yaml
    - sed -i "s/__SUBDOMAIN__/${SUBDOMAIN}/" deployment_production.yaml ingress.yaml service.yaml
    - sed -i "s/__NAMESPACE__/${NAMESPACE}/" ingress.yaml service.yaml
    - |
      if kubectl apply -f deployment_production.yaml | grep -q unchanged; then
          echo "=> Patching deployment to force image update."
          kubectl patch -f deployment_production.yaml -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"ci-last-updated\":\"$(date +'%s')\"}}}}}"
      else
          echo "=> Deployment apply has changed the object, no need to force image update."
      fi
    - kubectl apply -f service.yaml || true
    - kubectl apply -f ingress.yaml
    - kubectl rollout status -f deployment_production.yaml
    - kubectl get all,ing -l ref=${DEPLOYMENT_SLUG}

deploy_staging_starport:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: staging
  only:
    - master
  variables:
    NODE_ENV: "staging"
    SUBDOMAIN: "staging-ledger"
    PORT: 1317
    NAMESPACE: ${KUBE_NAMESPACE}
    DEPLOYMENT_SLUG: "starport-${CI_ENVIRONMENT_SLUG}"
  environment:
    name: staging/${CI_COMMIT_REF_NAME}
    url: http://${SUBDOMAIN}.worknetwork.io
    on_stop: stop_staging_server
  script:
    - kubectl version
    - echo ${NAMESPACE}
    - echo ${CI_ENVIRONMENT_SLUG}
    - echo ${CI_COMMIT_REF_NAME}
    - echo ${HOSTNAME}
    - echo ${AMQP_URL}
    - kubectl create configmap serverport-staging --from-literal=PORT=${PORT} -o yaml --dry-run | kubectl apply -f -
    - cd manifests/
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${DEPLOYMENT_SLUG}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__SUBDOMAIN__/${SUBDOMAIN}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__NAMESPACE__/${NAMESPACE}/" ingress.yaml service.yaml
    - |
      if kubectl apply -f deployment_staging.yaml | grep -q unchanged; then
          echo "=> Patching deployment to force image update."
          kubectl patch -f deployment_staging.yaml -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"ci-last-updated\":\"$(date +'%s')\"}}}}}"
      else
          echo "=> Deployment apply has changed the object, no need to force image update."
      fi
    - kubectl apply -f service.yaml || true
    - kubectl apply -f ingress.yaml
    - kubectl rollout status -f deployment_staging.yaml
    - kubectl get all,ing -l ref=${DEPLOYMENT_SLUG}
# DEPLOY WORKER
# deploy_staging_client_worker:
#   image:
#     name: lachlanevenson/k8s-kubectl:latest
#     entrypoint: ["/bin/sh", "-c"]
#   stage: staging
#   only:
#     - master
#   variables:
#     NODE_ENV: "staging"
#     SUBDOMAIN: "staging-nomarketplace-worker"
#     PORT: 5004
#     DEBUG: "ERROR,WARN,INFO"
#     NAMESPACE: ${KUBE_NAMESPACE}
#     DEPLOYMENT_SLUG: "worker-${CI_ENVIRONMENT_SLUG}"
#   environment:
#     name: staging/${CI_COMMIT_REF_NAME}
#     url: http://${SUBDOMAIN}.jobdoh.com
#     on_stop: stop_staging_client_worker
#   script:
#     - kubectl version
#     - echo ${CI_ENVIRONMENT_SLUG}
#     - echo ${CI_COMMIT_REF_NAME}
#     - echo ${DEPLOYMENT_SLUG}
#     - echo ${HOSTNAME}
#     - kubectl create configmap nodeenv-staging --from-literal=NODE_ENV=${NODE_ENV}  -o yaml --dry-run | kubectl apply -f -
#     - kubectl create configmap clientworkerport-staging --from-literal=PORT=${PORT}  -o yaml --dry-run | kubectl apply -f -
#     - kubectl create configmap debug-staging --from-literal=DEBUG=${DEBUG} -o yaml --dry-run | kubectl apply -f -
#     - cd client_worker/manifests/
#     - sed -i "s/__CI_ENVIRONMENT_SLUG__/${DEPLOYMENT_SLUG}/" deployment_staging.yaml ingress.yaml service.yaml
#     - sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" deployment_staging.yaml ingress.yaml service.yaml
#     - sed -i "s/__SUBDOMAIN__/${SUBDOMAIN}/" deployment_staging.yaml ingress.yaml service.yaml
#     - sed -i "s/__PORT__/${PORT}/" deployment_staging.yaml ingress.yaml service.yaml
#     - sed -i "s/__NAMESPACE__/${NAMESPACE}/" ingress.yaml service.yaml
#     - |
#       if kubectl apply -f deployment_staging.yaml | grep -q unchanged; then
#           echo "=> Patching deployment to force image update."
#           kubectl patch -f deployment_staging.yaml -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"ci-last-updated\":\"$(date +'%s')\"}}}}}"
#       else
#           echo "=> Deployment apply has changed the object, no need to force image update."
#       fi
#     - kubectl apply -f service.yaml || true
#     - kubectl apply -f ingress.yaml
#     - kubectl rollout status -f deployment_staging.yaml
#     - kubectl get all,ing -l ref=${DEPLOYMENT_SLUG}

# deploy_production_client_worker:
#   image:
#     name: lachlanevenson/k8s-kubectl:latest
#     entrypoint: ["/bin/sh", "-c"]
#   stage: production
#   only:
#     - production
#   variables:
#     NODE_ENV: "production"
#     SUBDOMAIN: "nomarketplace-worker"
#     PORT: 5004
#     DEBUG: "ERROR,WARN"
#     NAMESPACE: ${KUBE_NAMESPACE}
#     DEPLOYMENT_SLUG: "worker-${CI_ENVIRONMENT_SLUG}"
#   environment:
#     name: production/${CI_COMMIT_REF_NAME}
#     url: http://${SUBDOMAIN}.jobdoh.com
#     on_stop: stop_production_client_worker
#   script:
#     - kubectl version
#     - echo ${CI_ENVIRONMENT_SLUG}
#     - echo ${CI_COMMIT_REF_NAME}
#     - echo ${DEPLOYMENT_SLUG}
#     - echo ${HOSTNAME}
#     - kubectl create configmap nodeenv-production --from-literal=NODE_ENV=${NODE_ENV}  -o yaml --dry-run | kubectl apply -f -
#     - kubectl create configmap clientworkerport-production --from-literal=PORT=${PORT}  -o yaml --dry-run | kubectl apply -f -
#     - kubectl create configmap debug-production --from-literal=DEBUG=${DEBUG} -o yaml --dry-run | kubectl apply -f -
#     - cd client_worker/manifests/
#     - sed -i "s/__CI_ENVIRONMENT_SLUG__/${DEPLOYMENT_SLUG}/" deployment_production.yaml ingress.yaml service.yaml
#     - sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" deployment_production.yaml ingress.yaml service.yaml
#     - sed -i "s/__SUBDOMAIN__/${SUBDOMAIN}/" deployment_production.yaml ingress.yaml service.yaml
#     - sed -i "s/__PORT__/${PORT}/" deployment_production.yaml ingress.yaml service.yaml
#     - sed -i "s/__NAMESPACE__/${NAMESPACE}/" ingress.yaml service.yaml
#     - |
#       if kubectl apply -f deployment_production.yaml | grep -q unchanged; then
#           echo "=> Patching deployment to force image update."
#           kubectl patch -f deployment_production.yaml -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"ci-last-updated\":\"$(date +'%s')\"}}}}}"
#       else
#           echo "=> Deployment apply has changed the object, no need to force image update."
#       fi
#     - kubectl apply -f service.yaml || true
#     - kubectl apply -f ingress.yaml
#     - kubectl rollout status -f deployment_production.yaml
#     - kubectl get all,ing -l ref=${DEPLOYMENT_SLUG}



# DEPLOY EMPLOYER
deploy_staging_client_employer:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: staging
  only:
    - master
  variables:
    NODE_ENV: "staging"
    SUBDOMAIN: "staging-employer"
    PORT: 5001
    FIREBASE_PRIVATE_KEY: ${CI_FIREBASE_PRIVATE_KEY}
    DEBUG: "ERROR,WARN,INFO"
    NAMESPACE: ${KUBE_NAMESPACE}
    DEPLOYMENT_SLUG: "employer-${CI_ENVIRONMENT_SLUG}"
  environment:
    name: staging/${CI_COMMIT_REF_NAME}
    url: http://${SUBDOMAIN}.worknetwork.io
    on_stop: stop_staging_client_employer
  script:
    - kubectl version
    - echo ${CI_ENVIRONMENT_SLUG}
    - echo ${CI_COMMIT_REF_NAME}
    - echo ${HOSTNAME}
    - kubectl create configmap nodeenv-staging --from-literal=NODE_ENV=${NODE_ENV}  -o yaml --dry-run=client | kubectl apply -f -
    - kubectl create configmap clientemployerport-staging --from-literal=PORT=${PORT}  -o yaml --dry-run=client | kubectl apply -f -
    - kubectl create secret generic firebaseprivatekey-staging --from-literal=FIREBASE_PRIVATE_KEY="${FIREBASE_PRIVATE_KEY}" -o yaml --dry-run=client | kubectl apply -f -
    - kubectl create configmap debug-staging --from-literal=DEBUG=${DEBUG} -o yaml --dry-run=client | kubectl apply -f -
    - cd client_employer/manifests/
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${DEPLOYMENT_SLUG}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__SUBDOMAIN__/${SUBDOMAIN}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__PORT__/${PORT}/" deployment_staging.yaml ingress.yaml service.yaml
    - sed -i "s/__NAMESPACE__/${NAMESPACE}/" ingress.yaml service.yaml
    - |
      if kubectl apply -f deployment_staging.yaml | grep -q unchanged; then
          echo "=> Patching deployment to force image update."
          kubectl patch -f deployment_staging.yaml -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"ci-last-updated\":\"$(date +'%s')\"}}}}}"
      else
          echo "=> Deployment apply has changed the object, no need to force image update."
      fi
    - kubectl apply -f service.yaml || true
    - kubectl apply -f ingress.yaml
    - kubectl rollout status -f deployment_staging.yaml
    - kubectl get all,ing -l ref=${DEPLOYMENT_SLUG}
deploy_production_client_employer:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: production
  only:
    - production
  variables:
    NODE_ENV: "production"
    SUBDOMAIN: "employer"
    PORT: 5001
    FIREBASE_PRIVATE_KEY: ${CI_FIREBASE_PRIVATE_KEY}
    DEBUG: "ERROR,WARN"
    NAMESPACE: ${KUBE_NAMESPACE}
    DEPLOYMENT_SLUG: "employer-${CI_ENVIRONMENT_SLUG}"
  environment:
    name: production/${CI_COMMIT_REF_NAME}
    url: http://${SUBDOMAIN}.worknetwork.io
    on_stop: stop_production_client_employer
  script:
    - kubectl version
    - echo ${CI_ENVIRONMENT_SLUG}
    - echo ${CI_COMMIT_REF_NAME}
    - echo ${HOSTNAME}
    - kubectl create configmap nodeenv-production --from-literal=NODE_ENV=${NODE_ENV}  -o yaml --dry-run=client | kubectl apply -f -
    - kubectl create configmap clientemployerport-production --from-literal=PORT=${PORT}  -o yaml --dry-run=client | kubectl apply -f -
    - kubectl create secret generic firebaseprivatekey-staging --from-literal=FIREBASE_PRIVATE_KEY="${FIREBASE_PRIVATE_KEY}" -o yaml --dry-run=client | kubectl apply -f -
    - kubectl create configmap debug-production --from-literal=DEBUG=${DEBUG} -o yaml --dry-run=client | kubectl apply -f -
    - cd client_employer/manifests/
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${DEPLOYMENT_SLUG}/" deployment_production.yaml ingress.yaml service.yaml
    - sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" deployment_production.yaml ingress.yaml service.yaml
    - sed -i "s/__SUBDOMAIN__/${SUBDOMAIN}/" deployment_production.yaml ingress.yaml service.yaml
    - sed -i "s/__PORT__/${PORT}/" deployment_production.yaml ingress.yaml service.yaml
    - sed -i "s/__NAMESPACE__/${NAMESPACE}/" ingress.yaml service.yaml
    - |
      if kubectl apply -f deployment_production.yaml | grep -q unchanged; then
          echo "=> Patching deployment to force image update."
          kubectl patch -f deployment_production.yaml -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"ci-last-updated\":\"$(date +'%s')\"}}}}}"
      else
          echo "=> Deployment apply has changed the object, no need to force image update."
      fi
    - kubectl apply -f service.yaml || true
    - kubectl apply -f ingress.yaml
    - kubectl rollout status -f deployment_production.yaml
    - kubectl get all,ing -l ref=${DEPLOYMENT_SLUG}

#####################################
#               STOP                #
#-----------------------------------#

# STOP SERVER
stop_staging_server:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: staging
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_SLUG: "server-${CI_ENVIRONMENT_SLUG}"
  when: manual
  only:
    - master
  environment:
    name: staging/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - kubectl version
    - kubectl delete ing -l ref=${DEPLOYMENT_SLUG}
    - kubectl delete all -l ref=${DEPLOYMENT_SLUG}
stop_production_server:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: production
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_SLUG: "server-${CI_ENVIRONMENT_SLUG}"
  when: manual
  only:
    - production
  environment:
    name: production/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - kubectl version
    - kubectl delete ing -l ref=${DEPLOYMENT_SLUG}
    - kubectl delete all -l ref=${DEPLOYMENT_SLUG}

# STOP EMPLOYER
stop_staging_client_employer:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: staging
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_SLUG: "employer-${CI_ENVIRONMENT_SLUG}"
  when: manual
  only:
    - master
    - dev
  environment:
    name: staging/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - kubectl version
    - kubectl delete ing -l ref=${DEPLOYMENT_SLUG}
    - kubectl delete all -l ref=${DEPLOYMENT_SLUG}
stop_production_client_employer:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: production
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_SLUG: "employer-${CI_ENVIRONMENT_SLUG}"
  when: manual
  only:
    - production
  environment:
    name: production/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - kubectl version
    - kubectl delete ing -l ref=${DEPLOYMENT_SLUG}
    - kubectl delete all -l ref=${DEPLOYMENT_SLUG}
