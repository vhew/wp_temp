package keeper_test

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	keepertest "gitlab.com/vhew/wp_temp/testutil/keeper"
	"gitlab.com/vhew/wp_temp/x/wptemp/keeper"
	"gitlab.com/vhew/wp_temp/x/wptemp/types"
)

func setupMsgServer(t testing.TB) (types.MsgServer, context.Context) {
	k, ctx := keepertest.WptempKeeper(t)
	return keeper.NewMsgServerImpl(*k), sdk.WrapSDKContext(ctx)
}
