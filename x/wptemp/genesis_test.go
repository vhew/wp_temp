package wptemp_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	keepertest "gitlab.com/vhew/wp_temp/testutil/keeper"
	"gitlab.com/vhew/wp_temp/x/wptemp"
	"gitlab.com/vhew/wp_temp/x/wptemp/types"
)

func TestGenesis(t *testing.T) {
	genesisState := types.GenesisState{
		// this line is used by starport scaffolding # genesis/test/state
	}

	k, ctx := keepertest.WptempKeeper(t)
	wptemp.InitGenesis(ctx, *k, genesisState)
	got := wptemp.ExportGenesis(ctx, *k)
	require.NotNil(t, got)

	// this line is used by starport scaffolding # genesis/test/assert
}
