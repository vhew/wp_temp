FROM ubuntu

ARG APP_PATH=/usr/src/app
ARG DEBIAN_FRONTEND=noninteractive
# RUN apt update && apt install -y golang@latest
# RUN apk update && apk upgrade && apk add curl && apt install curl
RUN apt update && apt install -y curl
# RUN apk add --no-cache bash
RUN curl -OL https://golang.org/dl/go1.16.7.linux-amd64.tar.gz
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.7.linux-amd64.tar.gz

ENV PATH="/usr/local/go/bin:${PATH}"
RUN curl https://get.starport.network/starport! | bash

# RUN mv starport /usr/local/bin/

COPY ./ ${APP_PATH}/
WORKDIR ${APP_PATH}


EXPOSE 26657
EXPOSE 4500 
EXPOSE 1317
CMD ["starport", "chain", "serve"]