const {
  Worker,
} = require('../../models/mongodb');

const {
  PaymentMethod,
  WorkerPaymentMethod,
} = require('../../src/modules/payments/model');

const {
  Contract,
} = require('../../src/modules/campaigns/model');

const payments = {
  initState: async () => {
    const worker = new Worker({
      _id: 'worker_11',
      name: 'worker eleven',
      phone: '+959972645544',
      identification: 'NSA007',
      verified: true,
    });
    const createdWorker = await worker.save();

    const contract = new Contract({
      rewards: [
        {
          rewardType: 'Discount',
          rewardId: '5e79a589d701eb001c3aac58',
          workerId: createdWorker._id,
        },
      ],
      status: 'COMPLETE',
      contractDetails: {
        type: 'GOAL1_LEVEL2',
      },
    });
    const createdContract = await contract.save();

    const paymentMethod = new PaymentMethod({
      name: 'WAVE',
      code: 'WAVEMM',
      region: 'mm',
      currency: 'MMK',
      transactionFeeRule: [],
    });
    const createdPaymentMethod = await paymentMethod.save();

    const paymentMethod2 = new PaymentMethod({
      name: 'KBZ Bank',
      code: 'KBZXMM',
      region: 'mm',
      currency: 'MMK',
      transactionFeeRule: [],
    });
    const createdPaymentMethod2 = await paymentMethod2.save();


    const workerPaymentMethod = new WorkerPaymentMethod({
      workerId: createdWorker._id,
      paymentMethodId: createdPaymentMethod2._id,
      coordinates: '9991000345992',
      verified: true,
    });
    const createdWorkerPaymentMethod = await workerPaymentMethod.save();

    await Worker.findOneAndUpdate({
      _id: createdWorker._id,
    }, {
      $addToSet: {
        workerPaymentMethodsIds: createdWorkerPaymentMethod._id,
      },
    });

    return {
      contractId: createdContract._id.toString(),
      workerId: createdWorker._id.toString(),
      paymentMethodId: createdPaymentMethod._id.toString(),
      workerPaymentMethodId: createdWorkerPaymentMethod._id.toString(),
    };
  },
};

module.exports = payments;
