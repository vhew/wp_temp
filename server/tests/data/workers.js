const moment = require('moment');
const {
  Employer,
  Job,
  Shift,
  Worker,
  AlienWorker,
  Organisation,
} = require('../../models/mongodb');

// make it run alone to check in the database
const workers = {
  initState: async () => {
    const organisation = new Organisation({
      name: 'Test Company',
    });
    const createdOrganisation = await organisation.save();

    const employer = new Employer({
      _id: 'lFvCWJ5DZgNN52uYVE3yQjb1kya2',
      organisationsIds: [createdOrganisation._id],
      name: 'Jax',
      userLevel: 6,
      email: 'jax@gmail.com',
    });
    const createdEmployer = await employer.save();

    const unverifiedWorker = new Worker({
      _id: `unverified_${createdOrganisation._id}_+959972649988`,
      name: 'Unverified worker',
      phone: '+95933255522',
      verified: false,
    });
    const createdUnverifiedWorker = await unverifiedWorker.save();

    const worker = new Worker({
      _id: 'worker_11',
      name: 'worker eleven',
      phone: '+959972645544',
      identification: 'NSA007',
      verified: true,
      paymentCoordinates: '+9592056570',
      paymentMethod: 'ONGO',
      paymentCurrency: 'MMK',
    });
    const createdWorker = await worker.save();

    const alienWorker = new AlienWorker({
      name: 'Alien Worker',
      identification: '007004',
      phone: '+959999999991',
      verified: false,
      paymentCoordinates: '9991110093345',
      paymentMethod: 'BANK_KBZ',
      paymentCurrency: 'MMK',
      createOrganisationId: createdOrganisation._id,
    });
    const createdAlienWorker = await alienWorker.save();

    const job = new Job({
      organisationId: createdOrganisation._id,
      title: 'Test Job',
      private: true,
      type: 'FULL_TIME',
      renumerationValue: 1500000,
      fulltimeStandardHoursStart: '09:00+06:30',
      fulltimeStandardHoursEnd: '18:00+06:30',
    });
    const createdJob = await job.save();

    await Organisation.findOneAndUpdate({
      _id: createdOrganisation._id,
    }, {
      $addToSet: {
        jobsIds: createdJob._id,
      },
    });

    const shift = new Shift({
      jobId: createdJob._id,
      startTime: moment.utc('2019-07-01').set({ hour: 9, minute: 0 }),
      endTime: moment.utc('2022-07-01').set({ hour: 18, minute: 0 }),
      workerAccepted: true,
      workerId: createdAlienWorker._id,
      organisationId: createdOrganisation._id,
    });
    await shift.save();

    return {
      workerId: createdWorker._id.toString(),
      organisationId: createdOrganisation._id.toString(),
      alienWorkerId: createdAlienWorker._id.toString(),
      unverifiedWorkerId: createdUnverifiedWorker._id.toString(),
      employerId: createdEmployer._id.toString(),
    };
  },
};

module.exports = workers;
