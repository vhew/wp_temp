const {
  Employer,
  Notification,
  Worker,
} = require('../../src/modules/models');


const notifications = {
  initState: async () => {
    const employer = new Employer({
      _id: 'notification_employer_id',
      name: 'Jax',
      userLevel: 6,
      email: 'jax@gmail.com',
    });
    const createdEmployer = await employer.save();

    const worker = new Worker({
      _id: 'notification_worker_id',
      name: 'worker eleven',
      phone: '+959972645544',
      identification: 'NSA007',
      verified: true,
      paymentCoordinates: '+9592056570',
      paymentMethod: 'ONGO',
      paymentCurrency: 'MMK',
    });
    const createdWorker = await worker.save();

    const update = new Notification({
      senderId: createdEmployer._id,
      receiverId: createdWorker._id,
      title: 'Test Notification Title',
      body: 'Test Notification Message',
      isRead: true,
    });

    const update2 = new Notification({
      senderId: createdEmployer._id,
      receiverId: createdWorker._id,
      title: 'Test Unread Notification Title',
      body: 'Test Unread Notification Message',
      isRead: false,
    });

    await update.save();
    const notification = await update2.save();

    return {
      senderId: createdEmployer._id.toString(),
      receiverId: createdWorker._id.toString(),
      notificationId: notification._id.toString(),
    };
  },
};

module.exports = notifications;
