const utils = require('../../src/utils');

const getIncomeTaxPerMonthTestCase = {
  id: 'Get Income Tax Per Month Test Case',
  params: {
    taxConfigurationReference: {
      parentRelief: 1000000,
      childRelief: 500000,
      personalRelief: '20%',
      spouseRelief: 1000000,
      taxPercentages: [
        {
          min: 0,
          max: 2000000,
          percent: 0,
        },
        {
          min: 2000001,
          max: 5000000,
          percent: 5,
        },
        {
          min: 5000001,
          max: 10000000,
          percent: 10,
        },
        {
          min: 10000001,
          max: 20000000,
          percent: 15,
        },
        {
          min: 20000001,
          max: 30000000,
          percent: 20,
        },
        {
          min: 30000000,
          max: 100000000,
          percent: 25,
        },
      ],
    },
    taxConfigurations: {
      parents: 2,
      children: 1,
      spouse: 1,
      socialSecurityFee: 72000,
    },
    renumerationValue: 1500000,
  },
  func: utils._getIncomeTaxPerMonth,
  expected: 64516.67,
};

const getIncomeTaxPerMonthTestCase2 = {
  id: 'Get Income Tax Per Month Test Case 2',
  params: {
    taxConfigurationReference: {
      parentRelief: 1000000,
      childRelief: 500000,
      personalRelief: '20%',
      spouseRelief: 1000000,
      taxPercentages: [
        {
          min: 0,
          max: 2000000,
          percent: 0,
        },
        {
          min: 2000001,
          max: 5000000,
          percent: 5,
        },
        {
          min: 5000001,
          max: 10000000,
          percent: 10,
        },
        {
          min: 10000001,
          max: 20000000,
          percent: 15,
        },
        {
          min: 20000001,
          max: 30000000,
          percent: 20,
        },
        {
          min: 30000000,
          max: 100000000,
          percent: 25,
        },
      ],
    },
    taxConfigurations: {
      parents: 2,
      children: 0,
      spouse: 0,
      socialSecurityFee: 72000,
    },
    renumerationValue: 800000,
  },
  func: utils._getIncomeTaxPerMonth,
  expected: 17566.67,
};

describe('Testing utility functions 2', () => {
  const cases = [
    getIncomeTaxPerMonthTestCase,
    getIncomeTaxPerMonthTestCase2,
  ];

  cases.forEach((obj) => {
    const {
      id,
      params,
      func,
      expected,
    } = obj;

    test.concurrent(`testing ${id}`, async () => {
      const result = await func(params);
      return expect(result).toEqual(expected);
    });
  });
});
