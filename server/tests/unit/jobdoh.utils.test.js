const moment = require('moment');
const utils = require('../../src/utils');

const normalPayCycleTestCase = {
  id: 'Current PayCycle [1, 1] and Today is 5th, should return 1st of current month & 1st of next moneth',
  params: {
    payCycleMonthDays: [1, 1],
    arbitaryDate: moment.utc().set('date', 5),
  },
  func: utils._getPayCycle,
  expected: {
    payCycleStartTime: moment.utc().set('date', 1).startOf('day'),
    payCycleEndTime: moment.utc().add(1, 'M').set('date', 1).startOf('day'),
  },
};

const currentPayCyclePeriodsTestCase1 = {
  id: 'Current PayCycle [1, 15, 1] and arbitaryDate is 5th, should return 1st & 15 of current month',
  params: {
    payCycleMonthDays: [1, 15, 1],
    arbitaryDate: moment.utc().set('date', 5),
  },
  func: utils._getPayCycle,
  expected: {
    payCycleStartTime: moment.utc().set('date', 1).startOf('day'),
    payCycleEndTime: moment.utc().set('date', 15).startOf('day'),
  },
};

const currentPayCyclePeriodsTestCase2 = {
  id: 'Current PayCycle [1, 15, 1] and arbitaryDate is 18th, should return 15th of current month & 1st of next month',
  params: {
    payCycleMonthDays: [1, 15, 1],
    arbitaryDate: moment.utc().set('date', 18),
  },
  func: utils._getPayCycle,
  expected: {
    payCycleStartTime: moment.utc().set('date', 15).startOf('day'),
    payCycleEndTime: moment.utc().add(1, 'M').set('date', 1).startOf('day'),
  },
};

const currentPayCyclePeriodsTestCase3 = {
  id: 'Current PayCycle [1, 8, 15, 23, 1] and Today is 18th, should return 15th of current month & 23rd of current month',
  params: {
    payCycleMonthDays: [1, 8, 15, 23, 1],
    arbitaryDate: moment.utc().set('date', 18),
  },
  func: utils._getPayCycle,
  expected: {
    payCycleStartTime: moment.utc().set('date', 15).startOf('day'),
    payCycleEndTime: moment.utc().set('date', 23).startOf('day'),
  },
};

const currentPayCyclePeriodsTestCase4 = {
  id: 'Current PayCycle [5, 5] and Today is 18th, should return 5th of current month & 5th of next month',
  params: {
    payCycleMonthDays: [5, 5],
    arbitaryDate: moment.utc().set('date', 18),
  },
  func: utils._getPayCycle,
  expected: {
    payCycleStartTime: moment.utc().set('date', 5).startOf('day'),
    payCycleEndTime: moment.utc().add(1, 'M').set('date', 5).startOf('day'),
  },
};

const currentPayCyclePeriodsTestCase5 = {
  id: 'Current PayCycle [10, 10] and Today is 5th, should return 10th of previous month & 10th of current month',
  params: {
    payCycleMonthDays: [10, 10],
    arbitaryDate: moment.utc().set('date', 5),
  },
  func: utils._getPayCycle,
  expected: {
    payCycleStartTime: moment.utc().subtract(1, 'M').set('date', 10).startOf('day'),
    payCycleEndTime: moment.utc().set('date', 10).startOf('day'),
  },
};

const isTodayPayCycleAdvancementTestCase = {
  id: 'isTodayPayCycleAdvancementTestCase',
  params: {
    payCycle: {
      payCycleStartTime: moment.utc('2019-08-01').startOf('day'),
      payCycleEndTime: moment.utc('2019-08-31').startOf('day'),
    },
    payCycleAdvancements: [
      {
        payCycleIdentifier: '2019-07-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-08-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-09-27T00:00:00.000Z',
        advancedDays: 5,
      },
    ],
    dateToBeChecked: moment.utc('2019-08-27').startOf('day'),
  },
  func: utils._isTodayPayCycleAdvancement,
  expected: true,
};

const isTodayPayCycleAdvancementTestCase2 = {
  id: 'isTodayPayCycleAdvancementTestCase2',
  params: {
    payCycle: {
      payCycleStartTime: moment.utc('2019-08-01').startOf('day'),
      payCycleEndTime: moment.utc('2019-08-31').startOf('day'),
    },
    payCycleAdvancements: [
      {
        payCycleIdentifier: '2019-07-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-08-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-09-27T00:00:00.000Z',
        advancedDays: 5,
      },
    ],
    dateToBeChecked: moment.utc('2019-08-30').startOf('day'),
  },
  func: utils._isTodayPayCycleAdvancement,
  expected: true,
};

const isTodayPayCycleAdvancementTestCase3 = {
  id: 'isTodayPayCycleAdvancementTestCase3',
  params: {
    payCycle: {
      payCycleStartTime: moment.utc('2019-08-01').startOf('day'),
      payCycleEndTime: moment.utc('2019-08-31').startOf('day'),
    },
    payCycleAdvancements: [
    ],
    dateToBeChecked: moment.utc('2019-08-26').startOf('day'),
  },
  func: utils._isTodayPayCycleAdvancement,
  expected: false,
};

const isTodayPayCycleAdvancementTestCase4 = {
  id: 'isTodayPayCycleAdvancementTestCase4',
  params: {
    payCycle: {
      payCycleStartTime: moment.utc('2019-08-01').startOf('day'),
      payCycleEndTime: moment.utc('2019-08-31').startOf('day'),
    },
    payCycleAdvancements: [
      {
        payCycleIdentifier: '2019-07-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-08-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-09-27T00:00:00.000Z',
        advancedDays: 5,
      },
    ],
    dateToBeChecked: moment.utc('2019-08-26').startOf('day'),
  },
  func: utils._isTodayPayCycleAdvancement,
  expected: false,
};

const isTodayPayCycleAdvancementTestCase5 = {
  id: 'isTodayPayCycleAdvancementTestCase5',
  params: {
    payCycle: {
      payCycleStartTime: moment.utc('2019-08-01').startOf('day'),
      payCycleEndTime: moment.utc('2019-08-31').startOf('day'),
    },
    payCycleAdvancements: [
      {
        payCycleIdentifier: '2019-07-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-08-27T00:00:00.000Z',
        advancedDays: 5,
      },
      {
        payCycleIdentifier: '2019-09-27T00:00:00.000Z',
        advancedDays: 5,
      },
    ],
    dateToBeChecked: moment.utc('2019-09-01').startOf('day'),
  },
  func: utils._isTodayPayCycleAdvancement,
  expected: false,
};

describe('Testing utility functions', () => {
  const cases = [
    normalPayCycleTestCase,
    currentPayCyclePeriodsTestCase1,
    currentPayCyclePeriodsTestCase2,
    currentPayCyclePeriodsTestCase3,
    currentPayCyclePeriodsTestCase4,
    currentPayCyclePeriodsTestCase5,
    isTodayPayCycleAdvancementTestCase,
    isTodayPayCycleAdvancementTestCase2,
    isTodayPayCycleAdvancementTestCase3,
    isTodayPayCycleAdvancementTestCase4,
    isTodayPayCycleAdvancementTestCase5,
  ];

  cases.forEach((obj) => {
    const {
      id,
      params,
      func,
      expected,
    } = obj;

    test.concurrent(`testing ${id}`, async () => {
      const result = await func(params);
      return expect(result).toEqual(expected);
    });
  });
});
