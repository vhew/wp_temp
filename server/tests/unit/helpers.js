const moment = require('moment');
const { ObjectId } = require('mongoose').Types;
const {
  Worker,
  AlienWorker,
  Employer,
  Organisation,
  Job,
  Shift,
  WorkTokenValue,
  WorkToken,
  FiatToken,
} = require('../../models/mongodb');

const { Marketplace } = require('../../src/modules/marketplaces/model');

const {
  JobCategory,
  JobRole,
  JobSkill,
  SkillCategory,
  SkillSubCategory,
} = require('../../src/modules/jobRequirements/model');

// make it run alone to check in the database
const helpers = {
  initState: async () => {
    const publicMarketplace = new Marketplace({
      _id: '5e1d54053998d418ddc644ba',
      name: 'Public',
      organisationId: 'public-marketplace',
    });
    const createdPublicMarketplace = await publicMarketplace.save();

    const organisation = new Organisation({
      name: 'Test Company',
      payCycleMonthDays: [
        1,
        1,
      ],
      leaveSickAccuralRateDaysPerYear: 10,
      leavePersonalAccrualRateDaysPerYear: 10,
      leaveSpecialAccuralRateDaysPerYear: 10,
    });
    const createdOrganisation = await organisation.save();

    const employer6 = await new Employer({
      _id: 'lFvCWJ5DZgNN52uYVE3yQjb1kya2',
      organisationsIds: [createdOrganisation._id],
      name: 'Jax',
      userLevel: 6,
      email: 'jax@gmail.com',
    }).save();

    const employer5 = await new Employer({
      _id: 'employer_lvl_5',
      organisationsIds: [createdOrganisation._id],
      name: 'Level 5',
      userLevel: 5,
      email: 'employerlvl5@gmail.com',
    }).save();

    const employer4 = await new Employer({
      _id: 'employer_lvl_4',
      organisationsIds: [createdOrganisation._id],
      name: 'Level 4',
      userLevel: 4,
      email: 'employerlvl4@gmail.com',
    }).save();

    const employer3 = await new Employer({
      _id: 'employer_lvl_3',
      organisationsIds: [createdOrganisation._id],
      name: 'Level 3',
      userLevel: 3,
      email: 'employerlvl3@gmail.com',
    }).save();

    const employer2 = await new Employer({
      _id: 'employer_lvl_2',
      organisationsIds: [createdOrganisation._id],
      name: 'Level 2',
      userLevel: 2,
      email: 'employerlvl2@gmail.com',
    }).save();

    const employer1 = await new Employer({
      _id: 'employer_lvl_1',
      organisationsIds: [createdOrganisation._id],
      name: 'Level 1',
      userLevel: 1,
      email: 'employerlvl1@gmail.com',
    }).save();

    const unverifiedWorker = new Worker({
      _id: `unverified_${createdOrganisation._id}_+959972649988`,
      name: 'Unverified worker',
      phone: '+95933255522',
      verified: false,
    });
    const createdUnverifiedWorker = await unverifiedWorker.save();

    // JOB SKILLS
    const jobCategory = await new JobCategory({
      name: 'Test Category',
    }).save();

    const skillCategory = await new SkillCategory({
      name: 'Test Skill Category',
    }).save();

    const skillSubCategory = await new SkillSubCategory({
      name: 'Test Skill Sub Category',
    }).save();

    const jobRole = await new JobRole({
      name: 'Test JobRole',
    }).save();

    const jobSkill = await new JobSkill({
      name: 'Test JobSkill',
    }).save();

    const jobSkillKitchenAdministration = await new JobSkill({
      name: 'Kitchen Adminstration',
    }).save();

    const jobKitchenAdministration = new Job({
      organisationId: createdOrganisation._id,
      title: 'Kitchen Adminstration',
      // private: true,
      type: 'FULL_TIME',
      renumerationValue: 1500000,
      fulltimeStandardHoursStart: '09:00+06:30',
      fulltimeStandardHoursEnd: '18:00+06:30',
      listingStartDate: moment().add(-1, 'days').toISOString(),
      listingEndDate: moment().add(3, 'years').toISOString(),
      jobSkillRequirements: [{
        skillId: jobSkillKitchenAdministration._id.toString(),
        level: 0,
        notes: '',
      }],
      marketplaceId: '5e1d54053998d418ddc644ba',
    });
    const createdJobKitchenAdministration = await jobKitchenAdministration.save();


    const job = new Job({
      organisationId: createdOrganisation._id,
      title: 'Test Job',
      // private: true,
      type: 'FULL_TIME',
      renumerationValue: 1500000,
      fulltimeStandardHoursStart: '09:00+06:30',
      fulltimeStandardHoursEnd: '18:00+06:30',
      listingStartDate: moment().add(-1, 'days').toISOString(),
      listingEndDate: moment().add(3, 'years').toISOString(),
    });
    const createdJob = await job.save();

    const job2 = new Job({
      organisationId: createdOrganisation._id,
      title: 'Test Job 2',
      // private: true,
      type: 'FULL_TIME',
      renumerationValue: 1500000,
      fulltimeStandardHoursStart: '09:00+06:30',
      fulltimeStandardHoursEnd: '18:00+06:30',
      listingStartDate: moment().add(-1, 'days').toISOString(),
      listingEndDate: moment().add(3, 'years').toISOString(),
    });
    const createdJob2 = await job2.save();


    await Organisation.findOneAndUpdate({
      _id: createdOrganisation._id,
    }, {
      $addToSet: {
        jobsIds: createdJob._id,
      },
    });

    const worker = new Worker({
      _id: 'worker_11',
      name: 'worker eleven',
      phone: '+959972645544',
      identification: 'NSA007',
      verified: true,
      paymentCoordinates: '+9592056570',
      paymentMethod: 'ONGO',
      paymentCurrency: 'MMK',
      userLevel: 6,
      jobSkills: [
        {
          jobSkillId: jobSkillKitchenAdministration._id.toString(),
          jobSkillName: 'Kitchen Administration',
          level: 1,
          definition: '1 - 3 years',
          uploads: [],
          skillNote: '',
        },
      ],
      description: 'This is a test worker',
      workerPaymentMethodsIds: [
        ObjectId('5ece5074319d9c001bc4444b'),
      ],
      jobsAppliedIds: [
        ObjectId('5fec7cd72a6dd193d00a8272'),
      ],
      personalityType: 'ABCD',
      digitalLiteracyScore: '25',
      marketplacesIds: [ObjectId('5e1d54053998d418ddc644ba')],
    });

    const createdWorker = await worker.save();

    const shift = new Shift({
      _id: ObjectId('5d1ae143d51026001b8b8e01'),
      jobId: createdJob._id,
      startTime: moment.utc('2019-07-01').set({ hour: 9, minute: 0 }),
      endTime: moment.utc('2022-07-01').set({ hour: 18, minute: 0 }),
      workerAccepted: true,
      workerId: createdWorker._id,
      organisationId: createdOrganisation._id,
      leaveSickDays: 10,
      leavePersonalDays: 10,
      leaveSpecialDays: 10,
      leaveSickUpdateDate: moment().toISOString(),
      leavePersonalUpdateDate: moment().toISOString(),
      leaveSpecialUpdateDate: moment().toISOString(),
      employerId: employer6.id,
    });
    const createdShift = await shift.save();

    const shift2 = new Shift({
      jobId: createdJob._id,
      startTime: moment.utc('2019-07-01').set({ hour: 10, minute: 0 }),
      endTime: moment.utc('2022-07-01').set({ hour: 19, minute: 0 }),
      workerAccepted: false,
      candidatesIds: [createdWorker._id],
      organisationId: createdOrganisation._id,
      leaveSickDays: 10,
      leavePersonalDays: 10,
      leaveSpecialDays: 10,
      leaveSickUpdateDate: moment().add(-1, 'year').toISOString(),
      leavePersonalUpdateDate: moment().add(-1, 'year').toISOString(),
      leaveSpecialUpdateDate: moment().add(-1, 'year').toISOString(),
      employerId: employer6.id,
    });
    const createdShift2 = await shift2.save();

    const shift3 = new Shift({
      jobId: createdJob._id,
      startTime: moment.utc('2019-07-01').set({ hour: 10, minute: 0 }),
      endTime: moment.utc('2022-07-01').set({ hour: 19, minute: 0 }),
      workerAccepted: false,
      candidatesIds: [createdWorker._id],
      organisationId: createdOrganisation._id,
    });
    const createdShift3 = await shift3.save();
    await Shift.findOneAndUpdate({
      _id: createdShift3._id,
    }, {
      $push: {
        siblingShiftsIds: createdShift3._id,
      },
    });

    await Job.findOneAndUpdate({
      _id: createdJob._id,
    }, {
      $push: {
        shiftsIds: createdShift._id,
      },
    });

    await Job.findOneAndUpdate({
      _id: createdJob._id,
    }, {
      $push: {
        shiftsIds: createdShift3._id,
      },
    });

    const workToken = new WorkToken({
      shiftId: createdShift._id,
      workerId: createdWorker._id,
      checkinTime: moment.utc('2019-07-01').set({ hour: 9, minute: 0 }),
      checkoutTime: moment.utc('2019-07-01').set({ hour: 18, minute: 0 }),
      type: 'STANDARD',
      confirmed: null,
    });
    const createdWorkToken = await workToken.save();

    const workToken2 = new WorkToken({
      shiftId: createdShift._id,
      workerId: createdWorker._id,
      checkinTime: moment.utc('2019-07-02').set({ hour: 9, minute: 0 }),
      checkoutTime: moment.utc('2019-07-02').set({ hour: 18, minute: 0 }),
      type: 'STANDARD',
      confirmed: true,
    });
    const createdWorkToken2 = await workToken2.save();

    const workToken3 = new WorkToken({
      shiftId: createdShift._id,
      workerId: createdWorker._id,
      checkinTime: moment.utc('2019-07-03').set({ hour: 9, minute: 0 }),
      checkoutTime: moment.utc('2019-07-03').set({ hour: 18, minute: 0 }),
      type: 'STANDARD',
      confirmed: false,
      payNow: true,
    });
    const createdWorkToken3 = await workToken3.save();

    const workToken4 = new WorkToken({
      shiftId: createdShift._id,
      workerId: createdWorker._id,
      checkinTime: moment.utc('2019-07-04').set({ hour: 9, minute: 0 }),
      checkoutTime: moment.utc('2019-07-04').set({ hour: 18, minute: 0 }),
      type: 'STANDARD',
      confirmed: false,
      payNow: true,
    });
    const createdWorkToken4 = await workToken4.save();


    const fiatToken = new FiatToken({
      amount: 10000,
      coordinates: '+959972645544',
      currency: 'MMK',
      method: 'ONGO',
      workTokenId: createdWorkToken._id,
      transactionRef: '879758963214578956',
    });
    const createdFiatToken = await fiatToken.save();

    const fiatToken2 = new FiatToken({
      amount: 20000,
      coordinates: '+959972645544',
      currency: 'MMK',
      method: 'ONGO',
      workTokenId: createdWorkToken2._id,
      transactionRef: '879758963214578956',
    });
    const createdFiatToken2 = await fiatToken2.save();

    // const fiatToken3 = new FiatToken({
    //   amount: 30000,
    //   coordinates: '+959972645544',
    //   currency: 'MMK',
    //   method: 'ONGO',
    //   workTokenId: createdWorkToken3._id,
    //   transactionRef: '879758963214578956',
    // });
    // const createdFiatToken3 = await fiatToken3.save();


    const workTokenValue = new WorkTokenValue({
      expectedWorkTokenShiftId: createdShift._id,
      expectedWorkTokenWorkerId: createdWorker._id,
      expectedWorkTokenType: 'STANDARD',
      expectedWorkTokenCheckinTime: moment.utc('2019-07-01').set({ hour: 9, minute: 0 }),
      expectedWorkTokenCheckoutTime: moment.utc('2019-07-01').set({ hour: 18, minute: 0 }),
      workTokenId: createdWorkToken._id,
      confirmRequest: true,
      amount: 10000,
      fee: 1000,
      currency: 'MMK',
      method: 'ONGO',
      coordinates: '+9599726545540',
    });
    const createdWorkTokenValue = await workTokenValue.save();

    const workTokenValue2 = new WorkTokenValue({
      expectedWorkTokenShiftId: createdShift._id,
      expectedWorkTokenWorkerId: createdWorker._id,
      expectedWorkTokenType: 'STANDARD',
      expectedWorkTokenCheckinTime: moment.utc('2019-07-02').set({ hour: 9, minute: 0 }),
      expectedWorkTokenCheckoutTime: moment.utc('2019-07-02').set({ hour: 18, minute: 0 }),
      workTokenId: createdWorkToken2._id,
      confirmRequest: true,
      amount: 20000,
      fee: 2000,
      currency: 'MMK',
      method: 'ONGO',
      coordinates: '+9599726545540',
    });
    const createdWorkTokenValue2 = await workTokenValue2.save();

    const workTokenValue3 = new WorkTokenValue({
      expectedWorkTokenShiftId: createdShift._id,
      expectedWorkTokenWorkerId: createdWorker._id,
      expectedWorkTokenType: 'STANDARD',
      expectedWorkTokenCheckinTime: moment.utc('2019-07-03').set({ hour: 9, minute: 0 }),
      expectedWorkTokenCheckoutTime: moment.utc('2019-07-03').set({ hour: 18, minute: 0 }),
      workTokenId: createdWorkToken3._id,
      confirmRequest: true,
      amount: 30000,
      fee: 3000,
      currency: 'MMK',
      method: 'ONGO',
      coordinates: '+9599726545540',
    });
    const createdWorkTokenValue3 = await workTokenValue3.save();

    const workTokenValue4 = new WorkTokenValue({
      expectedWorkTokenShiftId: createdShift._id,
      expectedWorkTokenWorkerId: createdWorker._id,
      expectedWorkTokenType: 'STANDARD',
      expectedWorkTokenCheckinTime: moment.utc('2019-07-04').set({ hour: 9, minute: 0 }),
      expectedWorkTokenCheckoutTime: moment.utc('2019-07-04').set({ hour: 18, minute: 0 }),
      workTokenId: createdWorkToken4._id,
      confirmRequest: true,
      amount: 30000,
      fee: 3000,
      currency: 'MMK',
      method: 'ONGO',
      coordinates: '+9599726545540',
    });
    const createdWorkTokenValue4 = await workTokenValue4.save();

    const leaveToken = new WorkToken({
      shiftId: createdShift.id,
      checkinTime: moment().toISOString(),
      type: 'LEAVE_SICK',
      workerId: createdWorker.id,
      references: [{
        leaveStart: moment().add(1, 'day').toISOString(),
        leaveEnd: moment().add(2, 'day').toISOString(),
        halfDay: false,
      }],
    });
    const createdLeaveToken = await leaveToken.save();

    const alienWorker = new AlienWorker({
      name: 'Alien Worker',
      identification: '007004',
      phone: '+959999999991',
      verified: false,
      paymentCoordinates: '9991110093345',
      paymentMethod: 'BANK_KBZ',
      paymentCurrency: 'MMK',
      createOrganisationId: createdOrganisation._id,
    });
    const createdAlienWorker = await alienWorker.save();

    await Shift.findOneAndUpdate({
      _id: createdShift2._id,
    }, {
      $set: {
        workerId: createdAlienWorker._id,
      },
    });


    return {
      publicMarketplace: createdPublicMarketplace,
      // ORGANISATIONS
      organisationId: createdOrganisation._id.toString(),
      // JOBS
      jobId: createdJob._id.toString(),
      job2Id: createdJob2._id.toString(),
      jobKitchenAdministration: createdJobKitchenAdministration._id.toString(),
      // SHIFTS
      shiftId: createdShift._id.toString(),
      offeredShiftId: createdShift3._id.toString(),
      shiftIdMultipleVacancy: createdShift2._id.toString(),
      // WORK TOKENS
      workTokenValueId: createdWorkTokenValue._id.toString(),
      workTokenId: createdWorkToken._id.toString(),
      leaveToken: createdLeaveToken,
      // WORK TOKEN VALUES
      createdWorkTokenValue2,
      createdWorkTokenValue3,
      createdWorkTokenValue4,
      // FIAT TOKENS
      createdFiatToken,
      createdFiatToken2,
      // WORKERS
      workerId: createdWorker._id.toString(),
      alienWorkerId: createdAlienWorker._id.toString(),
      unverifiedWorkerId: createdUnverifiedWorker._id.toString(),
      // EMPLOYERS
      employer6Id: employer6._id.toString(),
      employer5Id: employer5._id.toString(),
      employer4Id: employer4._id.toString(),
      employer3Id: employer3._id.toString(),
      employer2Id: employer2._id.toString(),
      employer1Id: employer1._id.toString(),
      // JOB SKILLS
      jobCategoryId: jobCategory._id.toString(),
      skillCategoryId: skillCategory._id.toString(),
      skillSubCategoryId: skillSubCategory._id.toString(),
      jobRoleId: jobRole._id.toString(),
      jobSkillId: jobSkill._id.toString(),
      jobSkillKitchenAdministration,
    };
  },
};

module.exports = helpers;
