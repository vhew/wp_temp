

const joi = require('joi');

const envVarsSchema = joi.object({
  PORT: joi.number()
    .default(8000),
  FIREBASE_PRIVATE_KEY: joi.string(),
  // HTTPS_KEY: joi.string(),
  // HTTPS_CERT: joi.string(),
  // HTTPS_CA: joi.string(),
}).unknown()
  .required();

const { error, value: envVars } = joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  server: {
    port: envVars.PORT,
    firebasePrivateKey: envVars.FIREBASE_PRIVATE_KEY,
    // https: {
    //   key: envVars.HTTPS_KEY,
    //   cert: envVars.HTTPS_CERT,
    //   ca: envVars.HTTPS_CA,
    // },
  },
};

module.exports = config;
