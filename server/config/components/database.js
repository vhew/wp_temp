const joi = require('joi');

const envVarsSchema = joi.object({
  DB_TYPE: joi.string()
    .default('mongodb'),
  DB_HOST: joi.string()
    .default('127.0.0.1'),
  DB_PORT: joi.number()
    .default(27017),
  DB_NAME: joi.string()
    .required(),
  DB_USER: joi.string()
    .required(),
  DB_PWD: joi.string()
    .required(),
}).unknown()
  .required();

const { error, value: envVars } = joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  database: {
    type: envVars.DB_TYPE,
    host: envVars.DB_HOST,
    port: envVars.DB_PORT,
    name: envVars.DB_NAME,
    user: envVars.DB_USER,
    pwd: envVars.DB_PWD,
  },
};

module.exports = config;
