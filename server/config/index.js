const processType = process.env.PROCESS_TYPE;
const config = (processType === 'worker1') ? require('./worker1') : require('./api');

module.exports = config;
