const common = require('./components/common');
const logger = require('./components/logger');
const server = require('./components/server');
const database = require('./components/database');

/*
const joi = require('joi');
const envVarsSchema = joi.object({

}).unknown()
  .required();

const { error, value: envVars } = joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error('Config validation error: ${error.message}');
}
*/

const processConfig = {
  process_type: 'api',
};

module.exports = Object.assign({},
  common,
  logger,
  server,
  database,
  processConfig);
