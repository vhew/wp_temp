const discounts = [
  {
    type: 'GOAL1_LEVEL1',
    discountPercent: 100,
    discountFixed: 0,
  },
  {
    type: 'GOAL1_LEVEL2',
    discountPercent: 1.0,
    discountFixed: 0,
  },
  {
    type: 'STAYHOME_CAMPAIGN_1',
    discountPercent: 73.68,
    discountFixed: 0,
  },
  {
    type: '11_11_PROMOTION',
    discountPercent: 70.79,
    discountFixed: 0,
  },
  {
    type: 'WELCOME_CAMPAIGN',
    discountPercent: 100,
    discountFixed: 0,
  },
  {
    type: 'CHRISTMAS_CAMPAIGN',
    discountPercent: 66,
    discountFixed: 0,
  },
  {
    type: 'NEWYEAR_CAMPAIGN',
    discountPercent: 55,
    discountFixed: 0,
  },
  {
    type: 'KAREN_NEWYEAR_CAMPAIGN',
    discountPercent: 34,
    discountFixed: 0,
  },
  {
    type: 'VALENTINEDAY_CAMPAIGN',
    discountPercent: 77,
    discountFixed: 0,
  },
];
module.exports = discounts;
