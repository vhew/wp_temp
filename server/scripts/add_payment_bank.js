require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const { getDbUrl, connectDatabase } = require('../src/utils/db');

// eslint-disable-next-line max-len
// export DB_TYPE=mongodb+srv DB_USER=jobdoh_staging DB_PWD=passw0rd DB_HOST=cluster0-qagfz.gcp.mongodb.net DB_NAME=jobdoh_staging

const BANKS = [
  { name: 'HSBC', code: 'HSBC' },
  { name: 'Bank of China', code: 'BKCH' },
  { name: 'Dah Sing', code: 'DSBA' },
  { name: 'DBS', code: 'DBSS' },
  { name: 'Alipay', code: 'ALNY' },
  { name: 'Wechat Pay', code: 'WCPY' },
  { name: 'Octopus', code: 'OCCA' },
  { name: 'OCBC', code: 'OCBC' },
  { name: 'Bank of East Asia', code: 'BEAS' },
  { name: 'ICBC', code: 'ICBK' },
  { name: 'Citibank', code: 'CITI' },
];

const TEMPLATE = {
  code: '__code__HK',
  region: 'hk',
  currency: 'HKD',
  transactionFeeRule: [{
    min: 0,
    max: 1000000000,
    fixed: 0,
    percent: 0,
  }],
};


// eslint-disable-next-line func-names
(async function () {
  try {
    info('__Start__: ');
    const DB_URL = process.env.DB_URL || getDbUrl();
    await connectDatabase(DB_URL)
      .catch((err) => {
        info('Error while connecting DB');
        error('%O', err);
        process.exit(0);
      });

    const db = mongoose.connection.db;
    const paymentMethodDb = db.collection('paymentmethods');
    const paymentMethodsToAdd = BANKS.map(bank => ({
      ...TEMPLATE,
      name: bank.name,
      code: TEMPLATE.code.replace('__code__', bank.code),
    }));

    await paymentMethodDb.insertMany(paymentMethodsToAdd).then(info);
    info('__Finished__: ');
  } catch (err) {
    error('%O', err);
  }
  process.exit(0);
}());
