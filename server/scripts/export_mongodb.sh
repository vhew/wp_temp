#!/bin/sh

PASSWD=${PRODUCTION_MONGODB_PWD}
DATE=`date +%Y%m%d`
ENVIRONMENT='production'
COLLECTIONS='agendaJobs alienworkers contracts discounttokens employers expectedmarketplacesubscribers feedbacktokens fiattokens jobcategories jobcontracts jobroles jobs jobskills ledgerentries marketplaces notifications organisations paymentmethods products receipts serverconfigs shifts skillcategories skillsubcategories userpermissions workerpaymentmethods workers worktokens worktokenvalues jobmatchings'

for COLLECTION in ${COLLECTIONS}; do
    mongoexport --uri "mongodb+srv://jobdoh_ro:${PASSWD}@cluster0-qagfz.gcp.mongodb.net/jobdoh_production" --forceTableScan --out jobdoh_db_${DATE}/${DATE}_${ENVIRONMENT}_${COLLECTION}.json --collection ${COLLECTION}
done
