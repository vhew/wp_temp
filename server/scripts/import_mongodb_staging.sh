#!/bin/sh

PASSWD=${STAGING_MONGODB_PWD}
# COLLECTIONS='organisations employers workers jobs shifts worktokens worktokenvalues fiattokens'
COLLECTIONS='townships townshipdistancematrixes paymentmethods marketplaces'

for COLLECTION in ${COLLECTIONS}; do
    mongoimport --uri "mongodb+srv://jobdoh_staging:${PASSWD}@cluster0-qagfz.gcp.mongodb.net/jobdoh_staging" --file ${COLLECTION}.json --collection ${COLLECTION}
done
