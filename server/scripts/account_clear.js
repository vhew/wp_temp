require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const joi = require('joi');


const getEnvVars = () => {
  const envVarsSchema = joi.object({
    DB_URL: joi.string()
      .default('mongodb+srv://jobdoh_staging:passw0rd@cluster0-qagfz.gcp.mongodb.net/jobdoh_staging'),
  }).unknown()
    .required();
  const { err, value: envVars } = joi.validate(process.env, envVarsSchema);
  if (err) {
    throw new Error(`Config validation error: ${err.message}`);
  }
  return envVars;
};

const envVars = getEnvVars();

const WORKER_PHONES_TO_DELETE = [
  '+959123456789',
  '+959691831270',
  '+85233333333',
  '+84912345678',
  '+852211111111',
  '+85211111111',
];

const EMPLOYER_MAILS_TO_DELETE = [
  'myanmar@jobdoh.com',
  'jobdoh.employer1@gmail.com',
  'bd.myanmar@gmail.com',
  'lightcode2018@gmail.com',
  'maythukhaing1197@gmail.com',
  'phoenixjames05@gmail.com',
  'jobdoh.employer2@gmail.com',
  'zayarthu.dev.99@gmail.com',
  'thonnhsu.n.t@gmail.com',
  'khine.tz.soe.345@gmail.com',
  'zayarthu4@gmail.com',
  'roondanpyan@gmail.com',
  'jobdoh.employer.demo@gmail.com',
  'bd.myanmar@jobdoh.com',
];

const ORGS_ID_TO_DELETE = [
  '5ddbb5b7702563002d59e551', // JOBDOH
  '5f2933f8d688c1001b2cc609', // JOBDOH
  '5f8ead9aef337c0026aba96c', // s
  '5f48c95bdb9917001c7a2649', // Test Org
  '5dd5f1292422ab001b87d452', // TestOrg
  '5f2b58ecf8e120001bab7fd2', // Ho Chi Minh
  '5f07aa205a3d10001b98249b', // Chan Thar
  '5ef4178abc8c49001bc18be5', // sample
  '5eba6f5edea114001bba7da6', // Kira
  '5e78718e04aef3001b5019a4', // Ezvenue
  '5e1d54a73a44e9001bd19afd', // Cyberport
  '5face490fb7723002d2dc1ae', // GGG
  '5df753a4702563002d59fde9', // MarvelTesting
  '5d4870e586a0de001e6e0f4a', // ProdTest
].map(id => mongoose.Types.ObjectId(id));

const JOBS_ID_TO_DELETE = [
  '5e283807f3a36d001b39de8b',
].map(id => mongoose.Types.ObjectId(id));

async function connectDatabase(url) {
  info('DB Opening: %s', url);
  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  info('DB Connected');
  return ret;
}

// eslint-disable-next-line func-names
(async function () {
  await connectDatabase(envVars.DB_URL)
    .catch((err) => {
      info('Error while connecting DB');
      error('%O', err);
      process.exit(0);
    });

  try {
    const db = mongoose.connection.db;
    const workerDB = await db.collection('workers');
    const employerDB = await db.collection('employers');
    const orgDB = await db.collection('organisations');
    const jobDB = await db.collection('jobs');
    const shiftDB = await db.collection('shifts');
    const workTokenDB = await db.collection('worktokens');
    const workTokenValueDB = await db.collection('worktokenvalues');
    const fiatTokenDB = await db.collection('fiattokens');

    info('deleting workers whose phone no. are: ', WORKER_PHONES_TO_DELETE);
    const workers = await workerDB.find({ phone: { $in: WORKER_PHONES_TO_DELETE } }).toArray();
    const workerIds = workers.map(e => e._id);
    const workTokens = await workTokenDB.find({ workerId: { $in: workerIds } }).toArray();
    const workTokenIds = workTokens.map(e => e._id);
    await workTokenDB.deleteMany({
      workerId: { $in: workerIds },
    });
    await workTokenValueDB.deleteMany({
      expectedWorkTokenWorkerId: { $in: workerIds },
    });
    await fiatTokenDB.deleteMany({
      workTokenId: { $in: workTokenIds },
    });
    await workerDB.deleteMany({
      phone: { $in: WORKER_PHONES_TO_DELETE },
    });

    info('deleting employers whose emails are: ', EMPLOYER_MAILS_TO_DELETE);
    await employerDB.deleteMany({
      email: { $in: EMPLOYER_MAILS_TO_DELETE },
    });

    info('deleting organisations whose ids are: ', JOBS_ID_TO_DELETE);

    info('deleting organisations whose ids are: ', ORGS_ID_TO_DELETE);
    await orgDB.deleteMany({
      _id: { $in: ORGS_ID_TO_DELETE },
    });
    await jobDB.deleteMany({
      organisationId: { $in: ORGS_ID_TO_DELETE.map(e => e.toString()) },
    });
    await shiftDB.deleteMany({
      organisationId: { $in: ORGS_ID_TO_DELETE },
    });
    await employerDB.updateMany(
      {},
      { $pull: { organisationsIds: { $in: ORGS_ID_TO_DELETE } } },
    );
  } catch (err) {
    error('%O', err);
  }
  process.exit(0);
}());
