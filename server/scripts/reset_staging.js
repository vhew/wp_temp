require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
// const firebaseAdmin = require('firebase-admin');

async function connectDatabase(url) {
  info('DB Opening: %s', url);
  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  info('DB Connected');
  return ret;
}

async function dropCollections() {
  const dbUrl = 'mongodb+srv://jobdoh_staging:passw0rd@cluster0-qagfz.gcp.mongodb.net/jobdoh_staging';
  await connectDatabase(dbUrl)
    .catch((err) => {
      info('Error while connecting DB');
      error('%O', err);
      process.exit(0);
    });

  const preventedCollections = ['townships', 'townshipdistancematrixes', 'paymentmethods', 'marketplaces', 'system.views'];
  const collections = await mongoose.connection.db.listCollections().toArray();
  const collectionsToDrop = collections.filter(e => !preventedCollections.includes(e.name));
  info('Collections', collections.map(c => c.name));
  info('Prevented Collections', preventedCollections);
  const dropPromises = collectionsToDrop.map(async (collection) => {
    info('dropping collection %s', collection.name);
    return mongoose.connection.db.collection(collection.name).remove({});
  });
  try {
    await Promise.all(dropPromises);
  } catch (err) {
    error('%O', err);
  }
  process.exit(0);
}

/*
async function deleteFirebaseUsers() {
  const emails = ['vh.spam@gmail.com'];
  const phones = ['+959429863448'];
}


// initialise Firebase Admin
firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert({
    projectId: 'dotted-howl-158112',
    clientEmail: 'firebase-adminsdk-w9gi0@dotted-howl-158112.iam.gserviceaccount.com',
    clientId: '100062061334108703140',
    privateKeyId: 'a8d8fc156d06e73397b1afce3ace2d207e78c524',
    privateKey: process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
    authUri: 'https://accounts.google.com/o/oauth2/auth',
    tokenUri: 'https://oauth2.googleapis.com/token',
    authProviderX509CertUrl: 'https://www.googleapis.com/oauth2/v1/certs',
    clientX509CertUrl: 'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-w9gi0%40dotted-howl-158112.iam.gserviceaccount.com',
  }),
  databaseURL: 'https://dotted-howl-158112.firebaseio.com',
});
*/

dropCollections();
