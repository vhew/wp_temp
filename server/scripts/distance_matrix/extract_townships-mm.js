const fs = require('fs');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const xlsx = require('node-xlsx').default;
const googleMaps = require('@google/maps');
const { locationDataFromGeodata } = require('../../src/services/GeocodeService');

const townshipsFileName = `${__dirname}/../../data/townships-mm.json`;

// source: http://themimu.info/place-codes
// Myanmar PCodes Release-VIII.i_Sep2017_Countrywide.zip
const myanmarLocationsFileName = `${__dirname}/../../data/myanmar_pcodes_countrywide.xlsx`;

const getJSONFileFrom = fileName => JSON.parse(
  fs.exists(fileName) ? fs.readFileSync(fileName) : {},
);

// API key for Places and Distance Matrix
const googleMapsClient = googleMaps.createClient({
  key: process.env.GMAPS_API_KEY,
  Promise,
});

// const geolocate = (township, district, region) => {
//   const location = new Promise((resolve, reject) => {
//     googleMapsClient.geocode({
//       address: `${township} township, ${district} district, ${region} region, myanmar`,
//     })
//       .asPromise()
//       .then((response) => {
//         if (response.json.results[0]) {
//           const results = response.json.results[0];
//           if (response.json.results[1]) {
//             warn('more than 1 result: %s %s %s', township, district, region);
//             response.json.results.forEach((result) => {
//               result.address_components.forEach((component) => {
//                 warn(component.short_name);
//               });
//               warn(result.geometry.location);
//               warn('');
//             });
//           }
//           resolve(results.geometry.location);
//         }
//       })
//       .catch((err) => {
//         error('error: \n%O', err);
//         reject(err);
//       });
//   });
//   return location;
// };

const geocode = address => new Promise((resolve) => {
  googleMapsClient.geocode({ address }).asPromise().then(response => resolve(response.json));
});

const locationDataOf = async (address) => {
  const geoData = await geocode(address);
  info(`extracting locationdata from ${address}`);
  return locationDataFromGeodata(geoData);
};
const getId = t => [t.township, t.district, t.country]
  .map(e => e.toLowerCase().replace(/ /g, ''))
  .reduce((p, c) => `${p}-${c}`);

const run = async () => {
  const myArgs = process.argv.slice(2);

  // const regionsToExtract = ['Yangon', 'Bago (East)', 'Bago (West)'];
  info('Parsing .xlsx file...');
  const sheets = await xlsx.parse(myanmarLocationsFileName);
  const regionSheet = sheets[0].data;
  const districtSheet = sheets[2].data;
  const townshipRows = sheets[3].data // _03_ Township Sheet
    .slice(1); // remove headers
    // .filter(row => regionsToExtract.includes(row[1]));

  const getRegionMM = (name) => {
    const result = regionSheet.find(row => row[1] === name)[2];
    return result;
  };

  const getDistrictMM = (name) => {
    const result = districtSheet.find(row => row[5] === name);
    return result && result[6];
  };

  info('Getting township data...');
  let townships = await Promise.all(townshipRows.map(async (row) => {
    const region = row[1];
    const regionMM = getRegionMM(region);
    const district = row[3];
    const districtMM = getDistrictMM(district);
    const township = row[5];
    const townshipMM = row[6];
    const address = `${township} township, ${region} region, myanmar`;
    const locationData = await locationDataOf(address);
    const thisTownship = {
      township: locationData.township,
      townshipMM,
      townshipLat: locationData.coordinates.lat,
      townshipLng: locationData.coordinates.lng,
      district,
      districtMM,
      state: region,
      stateMM: regionMM,
      country: locationData.country,
    };
    return thisTownship;
  }));
  info(`total: ${townships.length}`);
  townships = townships.filter(t => !!t.township);
  info(`valid items: ${townships.length}`);
  townships = [...new Map(townships
    .reverse()
    .map((t) => {
      const _id = getId(t);
      return [_id, { _id, ...t }];
    })).values()].reverse();
  info(`after filtered: ${townships.length}`);

  const newData = townships;
  const oldData = getJSONFileFrom(townshipsFileName);
  const mergedData = [
    ...oldData,
    ...newData,
  ];
  const dataToWrite = myArgs[0] && myArgs[0] === 'ow' ? newData : mergedData;
  info('writing output');
  fs.writeFile(townshipsFileName, JSON.stringify(dataToWrite), 'utf8', (err) => {
    error(err);
  });
};
run();
