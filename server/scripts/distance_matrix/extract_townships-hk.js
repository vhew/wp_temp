const fs = require('fs');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const googleMaps = require('@google/maps');
const { locationDataFromGeodata } = require('../../src/services/GeocodeService');

const townshipsFileName = `${__dirname}/../../data/townships-hk.json`;

const hongkongLocationsFileName = `${__dirname}/../../data/hongkong_suburbs.json`;

const getJSONFileFrom = fileName => (fs.existsSync(fileName)
  ? JSON.parse(fs.readFileSync(fileName))
  : []);

// API key for Places and Distance Matrix
const googleMapsClient = googleMaps.createClient({
  key: process.env.GMAPS_API_KEY,
  Promise,
});

// const geolocate = (township, district, region) => {
//   const location = new Promise((resolve, reject) => {
//     googleMapsClient.geocode({
//       address: `${township} township, ${district} district, ${region} region, myanmar`,
//     })
//       .asPromise()
//       .then((response) => {
//         if (response.json.results[0]) {
//           const results = response.json.results[0];
//           if (response.json.results[1]) {
//             warn('more than 1 result: %s %s %s', township, district, region);
//             response.json.results.forEach((result) => {
//               result.address_components.forEach((component) => {
//                 warn(component.short_name);
//               });
//               warn(result.geometry.location);
//               warn('');
//             });
//           }
//           resolve(results.geometry.location);
//         }
//       })
//       .catch((err) => {
//         error('error: \n%O', err);
//         reject(err);
//       });
//   });
//   return location;
// };

const geocode = address => new Promise((resolve) => {
  googleMapsClient.geocode({ address }).asPromise().then(response => resolve(response.json));
});

const locationDataOf = async (address) => {
  const geoData = await geocode(address);
  info(`extracting locationdata from ${address}`);
  return locationDataFromGeodata(geoData);
};

const getId = t => [t.township, t.state, t.country]
  .map(e => e && e.toLowerCase().replace(/ /g, ''))
  .reduce((p, c) => `${p}-${c}`);

const run = async () => {
  const myArgs = process.argv.slice(2);

  info('Getting township data...');
  let townships = await Promise.all(
    getJSONFileFrom(hongkongLocationsFileName).map(async (address) => {
      const locationData = await locationDataOf(address);
      info(locationData.township, 'State:', locationData.stae);
      const thisTownship = {
        township: locationData.township,
        townshipMM: null,
        townshipLat: locationData.coordinates.lat,
        townshipLng: locationData.coordinates.lng,
        district: null,
        districtMM: null,
        state: locationData.state || null,
        stateMM: null,
        country: locationData.country,
      };
      return { _id: getId(thisTownship), ...thisTownship };
    }),
  );
  info(`total: ${townships.length}`);
  townships = townships.filter(t => !!t.township);
  info(`valid items: ${townships.length}`);
  const duplicatedItems = townships.filter(a => townships.filter(b => a._id === b._id).length > 1);
  info(`duplicated items: ${duplicatedItems.length}/${townships.length}`);
  info(duplicatedItems.map(e => e.id));
  townships = [...new Map(townships
    .reverse()
    .map(t => [t._id, t])).values(),
  ].reverse();
  info(`after filtered: ${townships.length}`);

  const newData = townships;
  const oldData = getJSONFileFrom(townshipsFileName);
  const mergedData = [
    ...oldData,
    ...newData,
  ];
  const dataToWrite = myArgs[0] && myArgs[0] === 'ow' ? newData : mergedData;
  info('writing output');
  fs.writeFile(townshipsFileName, JSON.stringify(dataToWrite), 'utf8', (err) => {
    error(err);
  });
};
run();
