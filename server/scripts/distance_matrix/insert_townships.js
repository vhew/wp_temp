const fs = require('fs');
const debug = require('debug')('DEBUG');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const {
  TownshipDistanceMatrix,
} = require('../../models/mongodb');
const { Township } = require('../../src/modules/Locations/model');

const config = require('../../config/api');

const townshipsFileName = `${__dirname}/../../data/townships.json`;
const townshipsDistanceMatrixFileName = `${__dirname}/../../data/townships_distance_matrix.json`;

// const getId = t => [t.township, t.district, t.country]
//   .map(e => e.toLowerCase().replace(/ /g, ''))
//   .reduce((p, c) => `${p}-${c}`);

const connectDB = async (url) => {
  info('DB Opening: %s', url);

  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  if (!(ret === mongoose)) {
    process.exit(0);
  }
  return ret;
};

const getPercent = (nume, deno) => `${Math.round((nume / deno) * 100)}%`;

const configDB = async () => {
  mongoose.set('useFindAndModify', false);
  let dbUrl;
  if (config.database.type === 'mongodb+srv') {
    dbUrl = `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}/${config.database.name}`;
  } else {
    dbUrl = `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}:${config.database.port}/${config.database.name}`;
  }
  await connectDB(dbUrl).catch((err) => {
    error('%O', err);
  });
};

const saveTownships = async (townships) => {
  for (let i = 0; i < townships.length; i += 1) {
    const township = townships[i];
    const newTownship = new Township(township);
    await newTownship.save().catch(e => error(e));
    debug(`[${getPercent(i, townships.length)}] inserted township ${township.township}`);
  }
};

const saveDistanceMatrix = async (distanceMatrices) => {
  for (let i = 0; i < distanceMatrices.length; i += 1) {
    const distanceMatrix = distanceMatrices[i];
    const newDistanceMatrix = new TownshipDistanceMatrix(distanceMatrix);
    await newDistanceMatrix.save().catch(e => error(e));
    debug('[%s] inserted townshipDistance: %s to %s', getPercent(i, distanceMatrices.length), distanceMatrix.origin, distanceMatrix.destination);
  }
};

const getJSONFileFrom = fileName => JSON.parse(fs.readFileSync(fileName));

const run = async () => {
  await configDB();

  const townships = getJSONFileFrom(townshipsFileName);
  const townshipsDistanceMatrix = getJSONFileFrom(townshipsDistanceMatrixFileName);

  await saveTownships(townships);
  info('finished inserting townships');
  await saveDistanceMatrix(townshipsDistanceMatrix);
  info('finished inserting townshipsDistanceMatrix');

  info('insert finished. ctrl-c to exit');
};
run();
