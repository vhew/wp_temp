/* eslint-disable camelcase */
const fs = require('fs');
const debug = require('debug')('DEBUG');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const googleMaps = require('@google/maps');
const { log } = require('console');
const { setTimeout, setInterval } = require('timers');


const townshipsFileName = `${__dirname}/../../data/townships-hk.json`;
const townshipsDistanceMatrixFileName = `${__dirname}/../../data/townships_distance_matrix-hk.json`;

const getJSONFileFrom = fileName => (fs.existsSync(fileName)
  ? JSON.parse(fs.readFileSync(fileName))
  : []);
// API key for Places and Distance Matrix
const googleMapsClient = googleMaps.createClient({
  key: process.env.GMAPS_API_KEY,
  Promise,
});

const getPercent = (nume, deno) => `${Math.round((nume / deno) * 100)}%`;

const sleep = ms => new Promise((resolve) => {
  debug('Continuing in...');
  let c = Math.round(ms / 1000);
  const interval = setInterval(() => { c -= 1; debug(c); }, 1000);
  setTimeout(() => {
    clearInterval(interval);
    resolve();
  }, ms);
});

const getDepatureTime = (hours, minutes, localOffset) => {
  const today = new Date();
  const universalEpoch = Date.UTC(
    today.getFullYear(), today.getMonth(), today.getDate() + 1, hours || 0, minutes || 0,
  ) / 1000;
  const localOffetInSecond = (localOffset || 0) * 3600;
  return universalEpoch - localOffetInSecond;
};

const divideArray = (a, n) => Array(Math.floor(a.length / n) + Math.ceil((a.length % n) / n))
  .fill()
  .map((_, i) => i)
  .map(v => a.slice(v * n, v * n + n));

const getDistanceMatrix = (origins, destinations, departure_time) => {
  const matrix = new Promise((resolve, reject) => {
    const query = {
      origins,
      destinations,
      mode: 'driving',
      language: 'en',
      units: 'metric',
      region: 'mm',
      avoid: 'tolls',
      departure_time,
    };
    googleMapsClient.distanceMatrix(query)
      .asPromise()
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        error('error: \n%O', err);
        reject(err);
      });
  });
  return matrix;
};

let counter = 0;
const departure_time = getDepatureTime(8, 30, 8); // 8:30 AM in HongKong timezone

const extractDistanceMatrix = (townshipSets, total) => {
  const promises = [];
  townshipSets.forEach(({ townshipX, townshipY }) => {
    promises.push(
      getDistanceMatrix(townshipX.subsetQuery, townshipY.subsetQuery, departure_time)
        .then(
          (e) => {
            const { distance, duration, duration_in_traffic } = e.json.rows[0].elements[0];
            const result = {
              origin: townshipX._id,
              originChecks: e.json && e.json.origin_addresses[0],
              destination: townshipY._id,
              destinationCheck: e.json && e.json.destination_addresses[0],
              distance: distance && distance.value,
              duration: (duration && duration.value)
                + (duration_in_traffic && duration_in_traffic.value),
            };
            counter += 1;

            log(`[${getPercent(counter, total)}] ${result.origin} to ${result.destination} : ${result.distance}km, ${result.duration / 3600}h`);
            return result;
          },
        ),
    );
  });
  return promises;
};

const run = async () => {
  const myArgs = process.argv.slice(2);
  const overWrite = myArgs[0] && myArgs[0] === 'ow';

  const townshipsHK = JSON.parse(fs.readFileSync(townshipsFileName)).map(t => ({
    ...t,
    // create subset of townships to create matrix from
    subsetQuery: `${t.township} township, ${t.state || ''}, Hong Kong`,
  }));

  try {
    const oldData = getJSONFileFrom(townshipsDistanceMatrixFileName);

    info('Getting distance data...');
    info(`Departure Time: ${departure_time}`);
    // const statesToExtract = ['Yangon', 'Nay Pyi Taw'];
    // const townshipsByState = statesToExtract.map(s => townshipsHK.filter(t => t.state === s));
    // statesToExtract.forEach((e, i) => {
    //   info(`${e}: ${townshipsByState[i].length} townships`);
    // });
    const total = townshipsHK.length ** 2;
    info('Total: ', townshipsHK.length, ' townships');
    // const dividedTownships = divideArray(townshipsHK, 50);

    const mappedTownships = townshipsHK.map(x => townshipsHK.map(y => ({
      townshipX: x,
      townshipY: y,
    }))).flat();

    const dividedTownshipSets = divideArray(mappedTownships, 2500);

    info(`Divided into ${dividedTownshipSets.length} group(s)`);
    const actualTownships = dividedTownshipSets.map(e => e.length).reduce((p, c) => p + c);

    if (total !== actualTownships) {
      throw new Error(`Error in dividing townships. (expected: ${total}, actual: ${actualTownships})`);
    }

    const responses = [];
    for (let i = 0; i < dividedTownshipSets.length; i += 1) {
      await sleep(6000);
      const townshipSets = dividedTownshipSets[i];
      const results = await Promise.all(extractDistanceMatrix(townshipSets, total));
      responses.push(results);
    }

    const townships = Array.from(responses.flat());
    const success = townships.filter(e => !!e).length;
    info('Finished requesting');
    info(`SUCCESS: ${success}/${total}`);

    const newData = townships;
    const mergedData = [
      ...oldData,
      ...newData,
    ];

    const dataToWrite = overWrite ? newData : mergedData;

    fs.writeFile(townshipsDistanceMatrixFileName,
      JSON.stringify(dataToWrite),
      'utf8',
      (err) => {
        error(err);
      });
  } catch (e) {
    error(e);
  }
};
run();
