require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const config = require('../config/components/database');

async function connectDatabase(url) {
  info('DB Opening: %s', url);
  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  return ret;
}

async function dropCollections() {
  let dbUrl;
  if (config.database.type === 'mongodb+srv') {
    dbUrl = `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}/${config.database.name}`;
  } else {
    dbUrl = `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}:${config.database.port}/${config.database.name}`;
  }
  await connectDatabase(dbUrl).catch((err) => {
    error('%O', err);
    process.exit(0);
  });

  const collections = await mongoose.connection.db.listCollections().toArray();
  const dropPromises = collections.map((collection) => {
    info('dropping collection %s', collection.name);
    return mongoose.connection.db.dropCollection(collection.name);
  });
  try {
    await Promise.all(dropPromises);
  } catch (err) {
    error('%O', err);
  }
  process.exit(0);
}
dropCollections();
