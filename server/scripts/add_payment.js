require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const { getDbUrl, connectDatabase } = require('../src/utils/db');

// eslint-disable-next-line max-len
// export DB_TYPE=mongodb+srv DB_USER=jobdoh_staging DB_PWD=passw0rd DB_HOST=cluster0-qagfz.gcp.mongodb.net DB_NAME=jobdoh_staging
const TOWNSHIPS = [
  { name: 'Bago', code: 'BGO' },
  { name: 'Dawei', code: 'DWI' },
  { name: 'Hpa-An', code: 'HPA' },
  { name: 'Lashio', code: 'LSH' },
  { name: 'Loikaw', code: 'LIW' },
  { name: 'Magway', code: 'MGQ' },
  { name: 'Mandalay', code: 'MDL' },
  { name: 'Maung Daw', code: 'MDW' },
  { name: 'Mawlamyine', code: 'MLM' },
  { name: 'Minbya', code: 'MBA' },
  { name: 'Myitkyina', code: 'MYT' },
  { name: 'Mrauk Oo', code: 'MRO' },
  { name: 'Muse', code: 'MSE' },
  { name: 'Myebon', code: 'MBN' },
  { name: 'Napitaw', code: 'NPT' },
  { name: 'Ngapali', code: 'NPL' },
  { name: 'Sittwe', code: 'STW' },
  { name: 'Yangon', code: 'YGN' },
];

const TEMPLATE = {
  name: 'Cash Transfer (__cityname__)',
  code: 'X__citycode__MM',
  region: 'mm',
  currency: 'MMK',
  paymentType: 'CASH_TRANSFER',
  transactionFeeRule: [{
    min: 0,
    max: 0,
    fixed: 0,
    percent: 0,
  }],
};


// eslint-disable-next-line func-names
(async function () {
  try {
    info('__Start__: ');
    const DB_URL = process.env.DB_URL || getDbUrl();
    await connectDatabase(DB_URL)
      .catch((err) => {
        info('Error while connecting DB');
        error('%O', err);
        process.exit(0);
      });

    const db = mongoose.connection.db;
    const paymentMethodDb = db.collection('paymentmethods');

    const paymentMethodsToAdd = TOWNSHIPS.map(township => ({
      ...TEMPLATE,
      name: TEMPLATE.name.replace('__cityname__', township.name),
      code: TEMPLATE.code.replace('__citycode__', township.code),
    }));

    await paymentMethodDb.insertMany(paymentMethodsToAdd).then(info);
    info('__Finished__: ');
  } catch (err) {
    error('%O', err);
  }
  process.exit(0);
}());
