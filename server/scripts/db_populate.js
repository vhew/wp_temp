require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const casual = require('casual');
const {
  Employer,
  Organisation,
  Job,
  Shift,
} = require('../models/mongodb/');
const config = require('../config/components/database');

async function connectDatabase(url) {
  info('DB Opening: %s', url);
  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  return ret;
}

casual.seed(123);

async function createEmployer() {
  const newOrganisation = new Organisation({
    name: casual.company_name,
  });
  const organisation = await newOrganisation.save();

  const newEmployer = new Employer({
    _id: casual.uuid,
    name: casual.full_name,
    email: casual.email,
    organisationsIds: [organisation._id],
  });
  const employer = await newEmployer.save();
  return employer;
}

async function createJob(organisationId) {
  const newShift = new Shift({
    startTime: '2019-12-12T09:00:00+06:30',
    endTime: '2019-12-12T09:010:00+06:30',
  });
  const shift = await newShift.save();

  const newJob = new Job({
    organisationId,
    title: casual.title,
    description: casual.description,
    location: casual.city,
    locationCoordinates: `${casual.longitude},${casual.latitude}`,
    type: 'PART_TIME',
    role: 'DATA',
    renumerationValue: 10900,
    shifts: [shift._id],
  });
  const job = await newJob.save();

  return job;
}

// create books using casual, but do it in order
async function populate() {
  let dbUrl;
  if (config.database.type === 'mongodb+srv') {
    dbUrl = `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}/${config.database.name}`;
  } else {
    dbUrl = `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}:${config.database.port}/${config.database.name}`;
  }
  await connectDatabase(dbUrl).catch((err) => {
    error('%O', err);
    process.exit(0);
  });

  const employer = await createEmployer();
  info('createEmployer: %O', employer);

  const [organisationId] = employer.organisationsIds;
  const createJobs = new Array(20).fill(organisationId);
  const createJobsPromises = createJobs.map(async (id) => {
    const job = await createJob(id);
    info('created Job: %O', job);
  });
  try {
    await Promise.all(createJobsPromises);
  } catch (err) {
    error('%O', err);
  }
  process.exit();
}
populate();
