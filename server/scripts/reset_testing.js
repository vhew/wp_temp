require('dotenv').config();
const info = require('debug')('INFO');
const logError = require('debug')('ERROR');
const mongoose = require('mongoose');
const joi = require('joi');

const envVarsSchema = joi.object({
  DB_URL: joi.string()
    .default('mongodb+srv://jobdoh_staging:passw0rd@cluster0-qagfz.gcp.mongodb.net/jobdoh_staging'),
}).unknown()
  .required();
const { error, value: envVars } = joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const WORKER_PHONES_TO_DELETE = [
  '+85255555555',
  '+85277777777',
];

const ORGS_NAME_TO_DELETE = [
  'MTR Corporation',
];

async function connectDatabase(url) {
  info('DB Opening: %s', url);
  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  info('DB Connected');
  return ret;
}

// eslint-disable-next-line func-names
(async function () {
  await connectDatabase(envVars.DB_URL)
    .catch((err) => {
      info('Error while connecting DB');
      logError('%O', err);
      process.exit(0);
    });

  try {
    const db = mongoose.connection.db;
    const workerDB = await db.collection('workers');
    const employerDB = await db.collection('employers');
    const orgDB = await db.collection('organisations');
    const jobDB = await db.collection('jobs');
    const shiftDB = await db.collection('shifts');

    info('deleting workers whose phone no. are: ', WORKER_PHONES_TO_DELETE);
    await workerDB.deleteMany({
      phone: { $in: WORKER_PHONES_TO_DELETE },
    });


    info('deleting organisations whose names are: ', ORGS_NAME_TO_DELETE);
    const orgs = await Promise.all(ORGS_NAME_TO_DELETE.map(e => orgDB.findOne({ name: e })));
    const ORGS_ID_TO_DELETE = Array.from(new Set(
      orgs.map(org => (
        mongoose.Types.ObjectId(org._id))),
    ));
    info({ ORGS_ID_TO_DELETE });
    await orgDB.deleteMany({
      _id: { $in: ORGS_ID_TO_DELETE },
    });
    await jobDB.deleteMany({
      organisationId: { $in: ORGS_ID_TO_DELETE.map(id => `${id}`) },
    });
    await shiftDB.deleteMany({
      organisationId: { $in: ORGS_ID_TO_DELETE },
    });
    await employerDB.updateMany(
      {},
      { $pull: { organisationsIds: { $in: ORGS_ID_TO_DELETE } } },
    );
  } catch (err) {
    logError('%O', err);
  }
  process.exit(0);
}());
