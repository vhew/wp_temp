/* eslint-disable quote-props */
require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const moment = require('moment');
const joi = require('joi');
// const firebaseAdmin = require('firebase-admin');

// const DB_URL = 'mongodb+srv://jobdoh_staging:passw0rd@cluster0-qagfz.gcp.mongodb.net/jobdoh_staging';
// const DB_URL = 'mongodb://jobdoh_development:passw0rd@127.0.0.1:27017/jobdoh_development2';
const getEnvVars = () => {
  const envVarsSchema = joi.object({
    DB_URL: joi.string()
      .default('mongodb+srv://jobdoh_staging:passw0rd@cluster0-qagfz.gcp.mongodb.net/jobdoh_staging'),
  }).unknown()
    .required();
  const { err, value: envVars } = joi.validate(process.env, envVarsSchema);
  if (err) {
    throw new Error(`Config validation error: ${err.message}`);
  }
  return envVars;
};

const envVars = getEnvVars();

async function connectDatabase(url) {
  info('DB Opening: %s', url);
  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  info('DB Connected');
  return ret;
}

// async function getRandomInt(max) {
//   return Math.floor(Math.random() * max);
// }
async function getTransactionRef() {
  const minm = 100000000;
  const maxm = 999990000;
  return Math.floor(Math.random() * (maxm - minm + 1)) + minm;
}

function generateCode(length) {
  let result = '';
  const characters = 'abcdefghijklmnopqrstuvwxyz';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
// async function getRandomInt(max) {
//   return Math.floor(Math.random() * max);
// }

const WORKER_PHONES = [
  '+959895628510',
  '+959444444444',
  '+85266666666',
  '+85211111111',
];
const TRANSACTION_REFS = [
  {
    phone: '+959895628510',
    transactionRef: '8418684164261863816',
    feeTransactionRef: '3981794179791279983',
    khupayTransactionRef: '19471973981732979847',
  },
  {
    phone: '+959444444444',
    transactionRef: '1873091739173921973',
    feeTransactionRef: '81732987193749173294',
    khupayTransactionRef: '13982741989821798723',
  },
  {
    phone: '+85266666666',
    transactionRef: '128917941739797329874',
    feeTransactionRef: '21983749817329472398',
    khupayTransactionRef: '193274917497193471273',
  },
  {
    phone: '+85211111111',
    transactionRef: '13247198374912709471993',
    feeTransactionRef: '1298749817491732947137',
    khupayTransactionRef: '19740932749173249273497',
  },
];
const ALIEN_JOBS = [
  mongoose.Types.ObjectId('6001311b294dee001b849710'),
  mongoose.Types.ObjectId('600138c6d2bd8b001b9e189d'),
  mongoose.Types.ObjectId('60012eac294dee001b84970b'),
  mongoose.Types.ObjectId('60013258d2bd8b001b9e1894'),
  mongoose.Types.ObjectId('600132006c3c6f001bc7c18b'),
  mongoose.Types.ObjectId('60012fdc833f8a001b591147'),
  mongoose.Types.ObjectId('600135e8294dee001b84972e'),
  mongoose.Types.ObjectId('6001372e4a6b56001b80e7b4'),
  mongoose.Types.ObjectId('600136d4294dee001b84972f'),
  mongoose.Types.ObjectId('6001354b4a6b56001b80e7b1'),
  mongoose.Types.ObjectId('600131ba294dee001b849712'),
  mongoose.Types.ObjectId('6001387d6c3c6f001bc7c197'),
  mongoose.Types.ObjectId('600133f7294dee001b84972c'),
  mongoose.Types.ObjectId('600132a1294dee001b849729'),
  mongoose.Types.ObjectId('6001345a6c3c6f001bc7c190'),
  mongoose.Types.ObjectId('6001338ad2bd8b001b9e1895'),
  mongoose.Types.ObjectId('600136506c3c6f001bc7c193'),
  mongoose.Types.ObjectId('600132f26c3c6f001bc7c18e'),
  mongoose.Types.ObjectId('6001377fd2bd8b001b9e189c'),
  mongoose.Types.ObjectId('5ffd8df2bff30b001b686ff5'),
  mongoose.Types.ObjectId('600130cf294dee001b84970e'),
  mongoose.Types.ObjectId('600146d24a6b56001b80e7c0'),
  mongoose.Types.ObjectId('600147676c3c6f001bc7c1a2'),
  mongoose.Types.ObjectId('600147c94a6b56001b80e7c6'),
  mongoose.Types.ObjectId('600147166c3c6f001bc7c1a1'),
];
// eslint-disable-next-line func-names
(async function () {
  await connectDatabase(envVars.DB_URL)
    .catch((err) => {
      info('Error while connecting DB');
      error('%O', err);
      process.exit(0);
    });

  try {
    info('__Start__: ');
    const db = mongoose.connection.db;
    const WorkerDB = await db.collection('workers');
    const JobDB = await db.collection('jobs');
    const JobContractDB = await db.collection('jobcontracts');
    const ShiftDB = await db.collection('shifts');
    const WorkerPaymentDB = await db.collection('workerpaymentmethods');
    const WorkTokenDB = await db.collection('worktokens');
    const WorkTokenValueDB = await db.collection('worktokenvalues');
    const FiatTokenDB = await db.collection('fiattokens');
    const alienworkerDB = await db.collection('alienworkers');
    const MarketplaceDB = await db.collection('marketplaces');
    const ReceiptDB = await db.collection('receipts');
    const OrganisationDB = await db.collection('organisations');
    const SubscriberDB = await db.collection('expectedmarketplacesubscribers');

    // marketplace job 1
    await JobDB.insertOne({
      'paymentMethodsIds': [
        mongoose.Types.ObjectId('5e1b1b23e5fe3c035c78c21f'),
      ],
      'renumerationCurrency': 'HKD',
      'renumerationValue': 1000,
      'score': 0,
      'fulltimeStandardHoursStart': '09:00+06:30',
      'fulltimeStandardHoursEnd': '18:00+06:30',
      'fulltimeOvertimeRenumerationPerHour': 0,
      'fulltimeLeavePersonalAccruePerDay': 0,
      'fulltimeLeaveSickAccruePerDay': 0,
      'fulltimePublicHolidays': [],
      'fulltimeNonWorkWeekDays': [
        0,
        6,
      ],
      'payCycleGraceDays': 0,
      'benefits': [
        'Over-time Pay',
      ],
      'jobTypeConversion': [],
      'shiftsFilled': false,
      'minShiftsBalance': 0,
      'requirementSpokenLanguages': [],
      'requirementWrittenLanguages': [],
      'requirementDocuments': [],
      'requirementScreeningChannels': [],
      'requirementRequiredSkills': [],
      'jobSkillRequirements': [
        {
          'skillId': '5f487f96152c73001cef7c81',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f96152c73001cef7c85',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f970821e5001b893713',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f9a152c73001cef7c96',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f9b0821e5001b89372b',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f9c152c73001cef7c9e',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f87152c73001cef7c4d',
          'level': 1,
          'notes': '',
        },
      ],
      'requirementVenueWorked': [],
      'requirementPreferredSkills': [],
      'requirementDressCodes': [],
      'requirementDemographicAgeGroup': [],
      'requirementDemographicGender': [],
      'affiliateIds': null,
      'marketplaceId': mongoose.Types.ObjectId('5e1d54053998d418ddc644ba'),
      'title': 'Digital Marketing Specialist (Do not apply)',
      'description': 'To design, create, and deliver marketing programs to support expansion and growth of the company services and products. Marketing specialists rely on judgment in planning and experience to accomplish identified goals.',
      'address': 'Causeway Bay, Hong Kong',
      'location': {
        'township': 'Causeway Bay',
        '__typename': 'Location',
      },
      'locationCoordinates': {
        'type': 'Point',
        'coordinates': [
          114.1914919,
          22.2859787,
        ],
      },
      'type': 'FULL_TIME',
      'jobRoleId': mongoose.Types.ObjectId('5f48a252152c73001cef7d50'),
      'paymentOption': 'MONTHLY',
      'requirementDescription': '',
      'requirementMinExperienceLevel': null,
      'hoursPerWeek': 0,
      'isOpenEnded': true,
      'allowFlexibleTime': false,
      'listingStartDate': moment('2021-04-21T17:30:00.000Z').toDate(),
      'listingEndDate': moment('2030-04-29T17:30:00.000Z').toDate(),
      'allowAdvancedPay': true,
      'isPortfolioRequired': false,
      'notes': '',
      'proxyCompanyName': 'Demo Pty Ltd',
      'proxyCompanyDescription': 'This is an IT Company.',
      'proxyEmployerName': 'U Kyaw Kyaw',
      'proxyCompanyContact': '+959790685503',
      'organisationId': '5ffd24f4558749001cc1a039',
      'createdAt': moment('2021-04-22T04:03:12.700Z').toDate(),
      'updatedAt': moment('2021-04-22T05:24:07.135Z').toDate(),
      '__v': 0,
    });
    const job1 = await JobDB.findOne({ title: 'Digital Marketing Specialist (Do not apply)' });
    info('job 1: ', job1);
    await JobContractDB.insertOne({
      'jobId': job1._id,
      'jobStartDate': moment('2021-05-01T04:03:15.937Z').toDate(),
      'renumerationValue': '1000',
      'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
      'signingDateEmployer': moment('2021-05-01T04:03:15.937Z').toDate(),
      'contract': '\nEmployment Agreement (Sample)<br>\n<br>\nTHIS AGREEMENT made as of the day of <h3>2021-04-22</h3>, between <h3>Demo Pty Ltd</h3> a corporation incorporated under the laws of the legal jurisdiction of Hong Kong and having its principal place of business at <h3>U Kyaw Kyaw</h3> (the "Employer") and <h3>[Name of Employee]</h3> (the "Employee").<br>\n<br>\nWHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.\nIN CONSIDERATION of the promises and other good and valuable consideration (the\nsufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>\n1. Employment<br>\nThe Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.\n<br><br>\n2. Position Title<br>\nAs a <h3>Digital Marketing Specialist</h3>, the Employee is required to perform the following duties and undertake\nthe following responsibilities in a professional manner.<br>\n(a) As full compensation for all services provided the employee shall be paid at the\nrate of <h3>1000/MONTHLY</h3>. Such payments shall be subject to such normal statutory deductions\nby the Employer.<br>\n(b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>\n(c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>\n(d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br>\n<br>\nSIGNED, SEALED AND DELIVERED in the presence of:<br>\n<br>\n<h3>U Kyaw Kyaw</h3><br>\n----------------------------------------<br>\n<br>\n<h3>[Signature of Employee]</h3><br>\n----------------------------------------<br>\n',
      '__v': 0,
    });
    const contract1 = await JobContractDB.findOne({ jobId: job1._id });
    info('contract 1: ', contract1);
    await ShiftDB.insertOne({
      'candidatesIds': [],
      'rejectedCandidatesIds': [],
      'interviews': [],
      'disabledWorkersIds': [],
      'workerAccepted': false,
      'workComplete': false,
      'divisible': true,
      'deductions': [],
      'leaveSickDays': 0,
      'leavePersonalDays': 0,
      'leaveSpecialDays': 0,
      'jobId': job1._id,
      'startTime': moment('2021-05-30T17:30:00.000Z').toDate(),
      'endTime': moment('2030-04-29T17:30:00.000Z').toDate(),
      'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
      'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
      'createdAt': moment('2021-04-22T04:03:15.350Z').toDate(),
      'updatedAt': moment('2021-04-22T04:03:15.932Z').toDate(),
      '__v': 0,
    });
    const shift1 = await ShiftDB.findOne({ jobId: job1._id });
    info('shift 1: ', shift1);
    await JobDB.findOneAndUpdate(
      {
        _id: job1._id,
      },
      {
        $set: {
          'shiftsIds': [
            shift1._id,
          ],
          'contractId': contract1._id.toString(),
        },
      },
    );
    await ShiftDB.findOneAndUpdate(
      {
        jobId: job1._id,
      },
      {
        $set: {
          'siblingShiftsIds': [
            shift1._id,
          ],
        },
      },
    );

    // marketplace job 2
    await JobDB.insertOne({
      'paymentMethodsIds': [
        mongoose.Types.ObjectId('5e1b1b23e5fe3c035c78c21f'),
      ],
      'renumerationCurrency': 'HKD',
      'renumerationValue': 300000,
      'score': 0,
      'fulltimeStandardHoursStart': '09:00+06:30',
      'fulltimeStandardHoursEnd': '18:00+06:30',
      'fulltimeOvertimeRenumerationPerHour': 0,
      'fulltimeLeavePersonalAccruePerDay': 0,
      'fulltimeLeaveSickAccruePerDay': 0,
      'fulltimePublicHolidays': [],
      'fulltimeNonWorkWeekDays': [
        0,
        6,
      ],
      'payCycleGraceDays': 0,
      'benefits': [
        'Attendance Bonus',
        'Performance Bonus',
      ],
      'jobTypeConversion': [],
      'shiftsFilled': false,
      'minShiftsBalance': 0,
      'requirementSpokenLanguages': [],
      'requirementWrittenLanguages': [],
      'requirementDocuments': [],
      'requirementScreeningChannels': [],
      'requirementRequiredSkills': [],
      'jobSkillRequirements': [
        {
          'skillId': '5f487fad152c73001cef7ce6',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487fadc9273e001bdcdd55',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f76152c73001cef7c09',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f9a0821e5001b893723',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f9ec9273e001bdcdd11',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f9cc9273e001bdcdd09',
          'level': 1,
          'notes': '',
        },
        {
          'skillId': '5f487f870821e5001b8936db',
          'level': 1,
          'notes': '',
        },
      ],
      'requirementVenueWorked': [],
      'requirementPreferredSkills': [],
      'requirementDressCodes': [],
      'requirementDemographicAgeGroup': [],
      'requirementDemographicGender': [],
      'affiliateIds': null,
      'marketplaceId': mongoose.Types.ObjectId('5e1d54053998d418ddc644ba'),
      'title': 'Administrative Assistant (Do not apply)',
      'description': 'Making travel and meeting arrangements, preparing reports and maintaining appropriate filing systems. The ideal candidate should have excellent oral and written communication skills and be able to organize their work using tools.',
      'address': 'Ward 7, Beteen Pyay Road and Kyun Taw Rod, Karmaryut Tsp Near Seik Pyo Yay Bus Stop, Yangon, Myanmar (Burma)',
      'location': {
        'township': 'Kamayut',
        '__typename': 'Location',
      },
      'locationCoordinates': {
        'type': 'Point',
        'coordinates': [
          96.130712,
          16.8171212,
        ],
      },
      'type': 'FULL_TIME',
      'jobRoleId': mongoose.Types.ObjectId('5f48a23ec9273e001bdcddc1'),
      'paymentOption': 'MONTHLY',
      'requirementDescription': '',
      'requirementMinExperienceLevel': null,
      'hoursPerWeek': 0,
      'isOpenEnded': true,
      'allowFlexibleTime': false,
      'listingStartDate': moment('2021-04-21T17:30:00.000Z').toDate(),
      'listingEndDate': moment('2030-04-29T17:30:00.000Z').toDate(),
      'allowAdvancedPay': true,
      'isPortfolioRequired': false,
      'notes': '',
      'proxyCompanyName': 'Demo Pty Ltd',
      'proxyCompanyDescription': 'This is an IT Company.',
      'proxyEmployerName': 'U Kyaw Kyaw',
      'proxyCompanyContact': '+959790685503',
      'proxyCompanyAddress': null,
      'organisationId': '5ffd24f4558749001cc1a039',
      'createdAt': moment('2021-04-22T03:57:33.298Z').toDate(),
      'updatedAt': moment('2021-04-22T03:58:51.195Z').toDate(),
      '__v': 0,
      'contractId': '6080f4302b851500271911c8',
      'contractAcceptanceTest': null,
      'requirementCustomDressCode': null,
      'requirementNumberOfContracts': null,
      'role': null,
    });
    const job2 = await JobDB.findOne({ title: 'Administrative Assistant (Do not apply)' });
    info('job2 : ', job2);
    await JobContractDB.insertOne({
      'jobId': job2._id,
      'jobStartDate': moment('2021-05-01T04:03:15.937Z').toDate(),
      'renumerationValue': '300000',
      'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
      'signingDateEmployer': moment('2021-05-01T04:03:15.937Z').toDate(),
      'contract': '\nEmployment Agreement (Sample)<br>\n<br>\nTHIS AGREEMENT made as of the day of <h3>2021-04-22</h3>, between <h3>Demo Pty Ltd</h3> a corporation incorporated under the laws of the legal jurisdiction of Hong Kong and having its principal place of business at <h3>U Kyaw Kyaw</h3> (the "Employer") and <h3>[Name of Employee]</h3> (the "Employee").<br>\n<br>\nWHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.\nIN CONSIDERATION of the promises and other good and valuable consideration (the\nsufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>\n1. Employment<br>\nThe Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.\n<br><br>\n2. Position Title<br>\nAs a <h3>Digital Marketing Specialist</h3>, the Employee is required to perform the following duties and undertake\nthe following responsibilities in a professional manner.<br>\n(a) As full compensation for all services provided the employee shall be paid at the\nrate of <h3>1000/MONTHLY</h3>. Such payments shall be subject to such normal statutory deductions\nby the Employer.<br>\n(b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>\n(c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>\n(d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br>\n<br>\nSIGNED, SEALED AND DELIVERED in the presence of:<br>\n<br>\n<h3>U Kyaw Kyaw</h3><br>\n----------------------------------------<br>\n<br>\n<h3>[Signature of Employee]</h3><br>\n----------------------------------------<br>\n',
      '__v': 0,
    });
    const contract2 = await JobContractDB.findOne({ jobId: job2._id });
    info('contract2 : ', contract2);
    await ShiftDB.insertOne({
      'candidatesIds': [],
      'rejectedCandidatesIds': [],
      'interviews': [],
      'disabledWorkersIds': [],
      'workerAccepted': false,
      'workComplete': false,
      'divisible': true,
      'deductions': [],
      'leaveSickDays': 0,
      'leavePersonalDays': 0,
      'leaveSpecialDays': 0,
      'jobId': job2._id,
      'startTime': moment('2021-05-30T17:30:00.000Z').toDate(),
      'endTime': moment('2030-04-29T17:30:00.000Z').toDate(),
      'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
      'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
      'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
      'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
    });
    const shift2 = await ShiftDB.findOne({ jobId: job2._id });
    info('shift2 : ', shift2);
    await JobDB.findOneAndUpdate(
      {
        _id: job2._id,
      },
      {
        $set: {
          'shiftsIds': [
            shift2._id,
          ],
          'contractId': contract2._id.toString(),
        },
      },
    );
    await ShiftDB.findOneAndUpdate(
      {
        jobId: job2._id,
      },
      {
        $set: {
          'siblingShiftsIds': [
            shift2._id,
          ],
        },
      },
    );

    // marketplace job 3 with multiple shift
    await JobDB.insertOne({
      'paymentMethodsIds': [
        mongoose.Types.ObjectId('5e1b1b23e5fe3c035c78c21f'),
      ],
      'renumerationCurrency': 'MMK',
      'renumerationValue': 1200,
      'score': 0,
      'fulltimeStandardHoursStart': '09:00+06:30',
      'fulltimeStandardHoursEnd': '18:00+06:30',
      'fulltimeOvertimeRenumerationPerHour': 0,
      'fulltimeLeavePersonalAccruePerDay': 0,
      'fulltimeLeaveSickAccruePerDay': 0,
      'fulltimePublicHolidays': [],
      'fulltimeNonWorkWeekDays': [
        0,
        6,
      ],
      'payCycleGraceDays': 0,
      'benefits': [
        'Attendance Bonus',
        'Performance Bonus',
      ],
      'jobTypeConversion': [],
      'shiftsFilled': true,
      'minShiftsBalance': 0,
      'requirementSpokenLanguages': [],
      'requirementWrittenLanguages': [],
      'requirementDocuments': [],
      'requirementScreeningChannels': [],
      'requirementRequiredSkills': [],
      'jobSkillRequirements': [
        {
          'skillId': '5f487fa80821e5001b893767',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487fa83c5102001b6cbf6f',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487f9c152c73001cef7ca2',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487f9c152c73001cef7c9e',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487f870821e5001b8936db',
          'level': 0,
          'notes': '',
        },
      ],
      'requirementVenueWorked': [],
      'requirementPreferredSkills': [],
      'requirementDressCodes': [],
      'requirementDemographicAgeGroup': [],
      'requirementDemographicGender': [],
      'affiliateIds': null,
      'marketplaceId': mongoose.Types.ObjectId('5e1d54053998d418ddc644ba'),
      'title': 'Promoter (Do not apply)',
      'description': 'Promoters demonstrate and provide information on products or services for various brands and/or companies. Their duties include showing potential customers how a product or service works. They also answer questions and aim to convince customers to buy a product or service.',
      'address': 'Ward 7, Beteen Pyay Road and Kyun Taw Rod, Karmaryut Tsp Near Seik Pyo Yay Bus Stop, Yangon, Myanmar (Burma)',
      'location': {
        'township': 'Kamayut',
        '__typename': 'Location',
      },
      'locationCoordinates': {
        'type': 'Point',
        'coordinates': [
          96.130712,
          16.8171212,
        ],
      },
      'type': 'PART_TIME',
      'jobRoleId': mongoose.Types.ObjectId('5f48a255c9273e001bdcddc8'),
      'paymentOption': 'HOURLY',
      'requirementDescription': '',
      'requirementMinExperienceLevel': null,
      'hoursPerWeek': 0,
      'isOpenEnded': true,
      'allowFlexibleTime': false,
      'listingStartDate': moment('2021-04-21T17:30:00.000Z').toDate(),
      'listingEndDate': moment('2030-04-29T17:30:00.000Z').toDate(),
      'allowAdvancedPay': true,
      'isPortfolioRequired': false,
      'notes': '',
      'proxyCompanyName': 'Demo Pty Ltd',
      'proxyCompanyDescription': 'This is an IT Company.',
      'proxyEmployerName': 'U Kyaw Kyaw',
      'proxyCompanyContact': '+959790685503',
      'proxyCompanyAddress': null,
      'organisationId': '5ffd24f4558749001cc1a039',
      'createdAt': moment('2021-04-22T03:57:33.298Z').toDate(),
      'updatedAt': moment('2021-04-22T03:58:51.195Z').toDate(),
      '__v': 0,
      'contractId': '6080f4302b851500271911c8',
      'contractAcceptanceTest': null,
      'requirementCustomDressCode': null,
      'requirementNumberOfContracts': null,
      'role': null,
    });
    const job3 = await JobDB.findOne({ title: 'Promoter (Do not apply)' });
    info('job2 : ', job3);
    await JobContractDB.insertOne({
      'jobId': job3._id,
      'jobStartDate': moment('2021-05-01T04:03:15.937Z').toDate(),
      'renumerationValue': '1200',
      'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
      'signingDateEmployer': moment('2021-05-01T04:03:15.937Z').toDate(),
      'contract': '\nEmployment Agreement (Sample)<br>\n<br>\nTHIS AGREEMENT made as of the day of <h3>2021-04-22</h3>, between <h3>Demo Pty Ltd</h3> a corporation incorporated under the laws of the legal jurisdiction of Hong Kong and having its principal place of business at <h3>U Kyaw Kyaw</h3> (the "Employer") and <h3>[Name of Employee]</h3> (the "Employee").<br>\n<br>\nWHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.\nIN CONSIDERATION of the promises and other good and valuable consideration (the\nsufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>\n1. Employment<br>\nThe Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.\n<br><br>\n2. Position Title<br>\nAs a <h3>Digital Marketing Specialist</h3>, the Employee is required to perform the following duties and undertake\nthe following responsibilities in a professional manner.<br>\n(a) As full compensation for all services provided the employee shall be paid at the\nrate of <h3>1000/MONTHLY</h3>. Such payments shall be subject to such normal statutory deductions\nby the Employer.<br>\n(b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>\n(c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>\n(d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br>\n<br>\nSIGNED, SEALED AND DELIVERED in the presence of:<br>\n<br>\n<h3>U Kyaw Kyaw</h3><br>\n----------------------------------------<br>\n<br>\n<h3>[Signature of Employee]</h3><br>\n----------------------------------------<br>\n',
      '__v': 0,
    });
    const contract3 = await JobContractDB.findOne({ jobId: job3._id });
    info('contract2 : ', contract3);
    for (let i = 0; i < 4; i += 1) {
      if (i === 0) {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': true,
          'workerId': 'klkVj0JnaYejVYzoRlHuOjrnBOK2',
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job3._id,
          'startTime': moment('2021-08-01T02:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T05:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      } else {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': false,
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job3._id,
          'startTime': moment('2021-08-01T02:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T05:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      }
    }
    for (let i = 0; i < 4; i += 1) {
      if (i === 0) {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': true,
          'workerId': '82JjiO0gqOWIiHCd0GAsU951pJ32',
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job3._id,
          'startTime': moment('2021-08-01T06:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T11:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      } else {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': false,
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job3._id,
          'startTime': moment('2021-08-01T06:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T11:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      }
    }
    const shifts3 = await ShiftDB.find({ jobId: job3._id }).toArray();
    info('shift3 : ', shifts3);
    await JobDB.findOneAndUpdate(
      {
        _id: job3._id,
      },
      {
        $set: {
          'shiftsIds': shifts3.map(e => e._id),
          'contractId': contract3._id.toString(),
        },
      },
    );
    await Promise.all(shifts3.map(async (shift) => {
      await ShiftDB.findOneAndUpdate(
        {
          _id: shift._id,
        },
        {
          $set: {
            'siblingShiftsIds': [
              shift._id,
            ],
          },
        },
      );
    }));

    // marketplace job 4 with multiple shift
    await JobDB.insertOne({
      'paymentMethodsIds': [
        mongoose.Types.ObjectId('5e1b1b23e5fe3c035c78c21f'),
      ],
      'renumerationCurrency': 'HKD',
      'renumerationValue': 40,
      'score': 0,
      'fulltimeStandardHoursStart': '09:00+06:30',
      'fulltimeStandardHoursEnd': '18:00+06:30',
      'fulltimeOvertimeRenumerationPerHour': 0,
      'fulltimeLeavePersonalAccruePerDay': 0,
      'fulltimeLeaveSickAccruePerDay': 0,
      'fulltimePublicHolidays': [],
      'fulltimeNonWorkWeekDays': [
        0,
        6,
      ],
      'payCycleGraceDays': 0,
      'benefits': [
        'Attendance Bonus',
        'Performance Bonus',
      ],
      'jobTypeConversion': [],
      'shiftsFilled': true,
      'minShiftsBalance': 0,
      'requirementSpokenLanguages': [],
      'requirementWrittenLanguages': [],
      'requirementDocuments': [],
      'requirementScreeningChannels': [],
      'requirementRequiredSkills': [],
      'jobSkillRequirements': [
        {
          'skillId': '5f487fa80821e5001b893767',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487fa83c5102001b6cbf6f',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487f9c152c73001cef7ca2',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487f9c152c73001cef7c9e',
          'level': 0,
          'notes': '',
        },
        {
          'skillId': '5f487f870821e5001b8936db',
          'level': 0,
          'notes': '',
        },
      ],
      'requirementVenueWorked': [],
      'requirementPreferredSkills': [],
      'requirementDressCodes': [],
      'requirementDemographicAgeGroup': [],
      'requirementDemographicGender': [],
      'affiliateIds': null,
      'marketplaceId': mongoose.Types.ObjectId('5e1d54053998d418ddc644ba'),
      'title': 'Promoter (Do not apply.)',
      'description': 'Promoters demonstrate and provide information on products or services for various brands and/or companies. Their duties include showing potential customers how a product or service works. They also answer questions and aim to convince customers to buy a product or service.',
      'address': 'Causeway Bay, Hong Kong',
      'location': {
        'township': 'Causeway Bay',
        '__typename': 'Location',
      },
      'locationCoordinates': {
        'type': 'Point',
        'coordinates': [
          '114.1914919',
          '22.2859787',
        ],
      },
      'type': 'PART_TIME',
      'jobRoleId': mongoose.Types.ObjectId('5f48a255c9273e001bdcddc8'),
      'paymentOption': 'HOURLY',
      'requirementDescription': '',
      'requirementMinExperienceLevel': null,
      'hoursPerWeek': 0,
      'isOpenEnded': true,
      'allowFlexibleTime': false,
      'listingStartDate': moment('2021-04-21T17:30:00.000Z').toDate(),
      'listingEndDate': moment('2030-04-29T17:30:00.000Z').toDate(),
      'allowAdvancedPay': true,
      'isPortfolioRequired': false,
      'notes': '',
      'proxyCompanyName': 'Demo Pty Ltd',
      'proxyCompanyDescription': 'This is an IT Company.',
      'proxyEmployerName': 'U Kyaw Kyaw',
      'proxyCompanyContact': '+959790685503',
      'proxyCompanyAddress': null,
      'organisationId': '5ffd24f4558749001cc1a039',
      'createdAt': moment('2021-04-22T03:57:33.298Z').toDate(),
      'updatedAt': moment('2021-04-22T03:58:51.195Z').toDate(),
      '__v': 0,
      'contractId': '6080f4302b851500271911c8',
      'contractAcceptanceTest': null,
      'requirementCustomDressCode': null,
      'requirementNumberOfContracts': null,
      'role': null,
    });
    const job4 = await JobDB.findOne({ title: 'Promoter (Do not apply.)' });
    info('job4 : ', job4);
    await JobContractDB.insertOne({
      'jobId': job4._id,
      'jobStartDate': moment('2021-05-01T04:03:15.937Z').toDate(),
      'renumerationValue': '40',
      'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
      'signingDateEmployer': moment('2021-05-01T04:03:15.937Z').toDate(),
      'contract': '\nEmployment Agreement (Sample)<br>\n<br>\nTHIS AGREEMENT made as of the day of <h3>2021-04-22</h3>, between <h3>Demo Pty Ltd</h3> a corporation incorporated under the laws of the legal jurisdiction of Hong Kong and having its principal place of business at <h3>U Kyaw Kyaw</h3> (the "Employer") and <h3>[Name of Employee]</h3> (the "Employee").<br>\n<br>\nWHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.\nIN CONSIDERATION of the promises and other good and valuable consideration (the\nsufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>\n1. Employment<br>\nThe Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.\n<br><br>\n2. Position Title<br>\nAs a <h3>Digital Marketing Specialist</h3>, the Employee is required to perform the following duties and undertake\nthe following responsibilities in a professional manner.<br>\n(a) As full compensation for all services provided the employee shall be paid at the\nrate of <h3>1000/MONTHLY</h3>. Such payments shall be subject to such normal statutory deductions\nby the Employer.<br>\n(b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>\n(c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>\n(d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br>\n<br>\nSIGNED, SEALED AND DELIVERED in the presence of:<br>\n<br>\n<h3>U Kyaw Kyaw</h3><br>\n----------------------------------------<br>\n<br>\n<h3>[Signature of Employee]</h3><br>\n----------------------------------------<br>\n',
      '__v': 0,
    });
    const contract4 = await JobContractDB.findOne({ jobId: job4._id });
    info('contract4 : ', contract4);
    for (let i = 0; i < 4; i += 1) {
      if (i === 0) {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': true,
          'workerId': 'zO8gTbidqgP80RNdT6IytUSqrBB2',
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job4._id,
          'startTime': moment('2021-08-01T02:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T05:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      } else {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': false,
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job4._id,
          'startTime': moment('2021-08-01T02:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T05:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      }
    }
    for (let i = 0; i < 4; i += 1) {
      if (i === 0) {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': true,
          'workerId': '7VQ1OxaS8bfdzVol94peVNBh88X2',
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job4._id,
          'startTime': moment('2021-08-01T06:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T11:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      } else {
        await ShiftDB.insertOne({
          'candidatesIds': [],
          'rejectedCandidatesIds': [],
          'interviews': [],
          'disabledWorkersIds': [],
          'workerAccepted': false,
          'workComplete': false,
          'divisible': true,
          'deductions': [],
          'leaveSickDays': 0,
          'leavePersonalDays': 0,
          'leaveSpecialDays': 0,
          'jobId': job4._id,
          'startTime': moment('2021-08-01T06:30:00.000Z').toDate(),
          'endTime': moment('2030-08-29T11:30:00.000Z').toDate(),
          'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
          'organisationId': mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
          'createdAt': moment('2021-04-22T03:57:35.517Z').toDate(),
          'updatedAt': moment('2021-04-22T03:57:36.435Z').toDate(),
        });
      }
    }
    const shifts4 = await ShiftDB.find({ jobId: job4._id }).toArray();
    info('shift3 : ', shifts4);
    await JobDB.findOneAndUpdate(
      {
        _id: job4._id,
      },
      {
        $set: {
          'shiftsIds': shifts4.map(e => e._id),
          'contractId': contract4._id.toString(),
        },
      },
    );
    await Promise.all(shifts4.map(async (shift) => {
      await ShiftDB.findOneAndUpdate(
        {
          _id: shift._id,
        },
        {
          $set: {
            'siblingShiftsIds': [
              shift._id,
            ],
          },
        },
      );
    }));

    await OrganisationDB.findOneAndUpdate(
      {
        _id: mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
      },
      {
        $addToSet: {
          jobsIds: { $each: [job1._id, job2._id, job3._id, job4._id] },
        },
      },
    );

    await MarketplaceDB.insertOne({
      'name': 'Demo Jobs',
      'organisationId': '5ffd24f4558749001cc1a039',
      'imageUrl': 'https://firebasestorage.googleapis.com/v0/b/jobdoh2employer.appspot.com/o/marketplaces%2Ftesting%2Fproduct%2Fimages%2Ffake%20logo.jpg?alt=media&token=f77db129-6156-4025-9356-42bf2d903aad',
      'description': 'These are for demo jobs.',
    });


    // adding data for citizens
    info('adding data for citizens');
    await Promise.all(WORKER_PHONES.map(async (phone) => {
      const worker = await WorkerDB.findOne({
        phone,
        verified: true,
      });
      const shift = await ShiftDB.findOne({
        workerId: worker._id,
      });
      const job = await JobDB.findOne({
        _id: shift.jobId,
      });
      await JobDB.findOneAndUpdate(
        {
          _id: shift.jobId,
        },
        {
          $set: {
            'renumerationCurrency': 'HKD',
          },
        },
      );
      await JobContractDB.insertOne({
        'jobId': job._id,
        'jobStartDate': moment('2021-03-22T11:48:25.977Z').toDate(),
        'renumerationValue': job.renumerationValue,
        'employerId': '5bgTYnCqydPBoGnyZ1BeNEVrTxp2',
        'signingDateEmployer': moment('2021-03-22T11:48:25.977Z').toDate(),
        'contract': `\nEmployment Agreement<br>\n<br>\nTHIS AGREEMENT made as of the day of <h3>2021-04-01</h3>, between <h3>Demo Pty Ltd</h3> a corporation incorporated under the laws of the legal jurisdiction of Hong Kong and having its principal place of business at <h3>U Kyaw Kyaw</h3> (the "Employer") and <h3>${worker.name}</h3> (the "Employee").<br>\n<br>\nWHEREAS the Employer desires to obtain the benefit of the services of the Employee, and the Employee desires to render such services on the terms and conditions set forth.\nIN CONSIDERATION of the promises and other good and valuable consideration (the\nsufficiency and receipt of which are hereby acknowledged) the parties agree as follows:<br>\n1. Employment<br>\nThe Employee agrees that he will at all times faithfully, industriously, and to the best of his skill, ability, experience and talents, perform all of the duties required of his position. In carrying out these duties and responsibilities, the Employee shall comply with all Employer policies, procedures, rules and regulations, both written and oral, as are announced by the Employer from time to time. It is also understood and agreed to by the Employee that his assignment, duties and responsibilities and reporting arrangements may be changed by the Employer in its sole discretion without causing termination of this agreement.\n<br><br>\n2. Position Title<br>\nAs a <h3>${job.title}</h3>, the Employee is required to perform the following duties and undertake\nthe following responsibilities in a professional manner.<br>\n(a) As full compensation for all services provided the employee shall be paid at the\nrate of <h3>${job.renumerationValue}/MONTHLY</h3>. Such payments shall be subject to such normal statutory deductions\nby the Employer.<br>\n(b) (may wish to include bonus calculations or omit in order to exercise discretion).<br>\n(c) The salary mentioned in paragraph (l)(a) shall be reviewed on an annual basis.<br>\n(d) All reasonable expenses arising out of employment shall be reimbursed assuming same have been authorized prior to being incurred and with the provision of appropriate receipts.<br>\n<br>\nSIGNED, SEALED AND DELIVERED in the presence of:<br>\n<br>\n<h3>U Kyaw Kyaw</h3><br>\n----------------------------------------<br>\n<br>\n<h3>[Signature of Employee]</h3><br>\n----------------------------------------<br>\n`,
        '__v': 0,
        'signingDateWorker': moment('2021-03-22T12:17:55.853Z').toDate(),
        'workerId': worker._id,
      });
      const contract = await JobContractDB.findOne({ jobId: job._id });
      await JobDB.findOneAndUpdate(
        {
          _id: job._id,
        },
        {
          $set: {
            'contractId': contract._id.toString(),
          },
        },
      );
      await WorkerPaymentDB.insertOne({
        'workerId': worker._id,
        'paymentMethodId': mongoose.Types.ObjectId('5e1b1b23e5fe3c035c78c21f'),
        'coordinates': worker.phone,
        'verified': true,
        'invalidate': null,
      });
      const workerPayments = await WorkerPaymentDB.findOne({ workerId: worker._id });
      const marketplace = await MarketplaceDB.findOne({ name: 'Demo Jobs' });
      await SubscriberDB.insertOne({
        'name': worker.name,
        'phone': worker.phone,
        'identification': worker.identification,
        'email': 'jobdoh.demo@gmail.com',
        'marketplaceId': marketplace._id,
        'workerId': worker._id,
      });
      await WorkerDB.findOneAndUpdate(
        {
          phone,
          verified: true,
        },
        {
          $addToSet: {
            marketplacesIds: { $each: [marketplace._id, mongoose.Types.ObjectId('5e1d54053998d418ddc644ba')] },
          },
        },
      );
      await WorkerDB.findOneAndUpdate(
        {
          phone,
          verified: true,
        },
        {
          $set: {
            'defaultWorkerPaymentMethodId': workerPayments._id,
            'workerPaymentMethodsIds': [workerPayments._id],
            'jobSkills': [
              {
                'jobSkillId': '5f487f96152c73001cef7c81',
                'jobSkillName': 'Content Management',
                'level': 1,
                'definition': '6 months - 3 years',
                'uploads': [],
                'skillNote': '',
              },
              {
                'jobSkillId': '5f487f96152c73001cef7c85',
                'jobSkillName': 'Facebook Management',
                'level': 1,
                'definition': '6 months - 3 years',
                'uploads': [],
                'skillNote': null,
              },
              {
                'jobSkillId': '5f487f9a152c73001cef7c96',
                'jobSkillName': 'Creativity attribute',
                'level': 1,
                'definition': 'Important to have',
                'uploads': [],
                'skillNote': null,
              },
              {
                'jobSkillId': '5f487f9c152c73001cef7c9e',
                'jobSkillName': 'Time Management attribute',
                'level': 1,
                'definition': 'Important to have',
                'uploads': [],
                'skillNote': null,
              },
              {
                'jobSkillId': '5f487f87152c73001cef7c4d',
                'jobSkillName': 'Spoken Cantonese',
                'level': 1,
                'definition': 'Can communicate with foreigners',
                'uploads': [],
                'skillNote': null,
              },
              {
                'jobSkillId': '5f487fad152c73001cef7ce6',
                'jobSkillName': 'Google Docs',
                'level': 1,
                'definition': '6 months - 3 years',
                'uploads': [],
                'skillNote': null,
              },
              {
                'jobSkillId': '5f487f9a0821e5001b893723',
                'jobSkillName': 'Communication attribute',
                'level': 1,
                'definition': 'Important to have',
                'uploads': [],
                'skillNote': null,
              },
              {
                'jobSkillId': '5f487f870821e5001b8936db',
                'jobSkillName': 'Spoken English',
                'skillNote': null,
                'level': 1,
                'definition': 'Can communicate with foreigners',
                'uploads': [],
              },
            ],
            'jobHistories': [
              {
                'position': 'Junior Digital Marketing',
                'companyName': 'MMM',
                'startDate': '2020-06-25',
                'endDate': '2021-01-18',
                'currentlyWorking': null,
                'description': 'I have 8 months of work experience with as Junior Digital Marketing.',
              },
            ],
            'description': 'A 28 years old digital marketing specialist want to work at your company.\nI have 8 months experience in digital marketing field. Thanks.',
          },
        },
      );

      const months = [moment().add(-1, 'month').format('YYYY-MM'), moment().format('YYYY-MM')];
      const enddays = moment(months[0], 'YYYY-MM').daysInMonth();
      // adding bonus
      await WorkTokenDB.insertOne({
        'confirmed': true,
        'payNow': true,
        'references': [],
        'shiftId': shift._id,
        'workerId': worker._id,
        'checkinTime': moment(`${months[0]}-01T03:00:00.498Z`).toDate(),
        'checkoutTime': moment(`${months[0]}-${enddays}T12:00:00.645Z`).toDate(),
        'type': 'BONUS',
        createdAt: moment().toDate(),
      }, async (err, result) => {
        if (err) error(err);
        await WorkTokenValueDB.insertOne({
          'confirmRequest': true,
          'expectedWorkTokenShiftId': shift._id,
          'expectedWorkTokenWorkerId': worker._id,
          'expectedWorkTokenType': 'BONUS',
          'expectedWorkTokenCheckinTime': moment(`${months[0]}-01T03:00:00.498Z`).toDate(),
          'expectedWorkTokenCheckoutTime': moment(`${months[0]}-${enddays}T12:00:00.645Z`).toDate(),
          'workTokenId': result.ops[0]._id,
          'amount': 1000,
          'fee': 0,
          'currency': 'HKD',
          'method': 'Tap & Go',
          'coordinates': worker.phone,
          'notes': 'For phone bill',
          'createdAt': moment().toDate(),
        });
        await FiatTokenDB.insertOne({
          'amount': 1000,
          'coordinates': worker.phone,
          'currency': 'HKD',
          'method': 'Tap & Go',
          'workTokenId': result.ops[0]._id,
          'transactionRef': TRANSACTION_REFS.find(e => e.phone === worker.phone).transactionRef,
          'createdAt': moment().toDate(),
        });
      });

      // adding deductions
      await WorkTokenDB.insertOne({
        'confirmed': true,
        'payNow': true,
        'references': [],
        'shiftId': shift._id,
        'workerId': worker._id,
        'checkinTime': moment(`${months[0]}-01T04:00:00.498Z`).toDate(),
        'checkoutTime': moment(`${months[0]}-${enddays}T11:00:00.645Z`).toDate(),
        'type': 'DEDUCTION',
        createdAt: moment().toDate(),
      }, async (err, result) => {
        if (err) error(err);
        await WorkTokenValueDB.insertOne({
          'confirmRequest': true,
          'expectedWorkTokenShiftId': shift._id,
          'expectedWorkTokenWorkerId': worker._id,
          'expectedWorkTokenType': 'DEDUCTION',
          'expectedWorkTokenCheckinTime': moment(`${months[0]}-01T03:00:00.498Z`).toDate(),
          'expectedWorkTokenCheckoutTime': moment(`${months[0]}-${enddays}T12:00:00.645Z`).toDate(),
          'workTokenId': result.ops[0]._id,
          'amount': 1000,
          'fee': 0,
          'currency': 'HKD',
          'method': 'Tap & Go',
          'coordinates': worker.phone,
          'notes': 'unpaid leave',
          'createdAt': moment().toDate(),
        });
        await FiatTokenDB.insertOne({
          'amount': 1000,
          'coordinates': worker.phone,
          'currency': 'HKD',
          'method': 'Tap & Go',
          'workTokenId': result.ops[0]._id,
          'transactionRef': TRANSACTION_REFS.find(e => e.phone === worker.phone).transactionRef,
          'createdAt': moment().toDate(),
        });
      });
      await WorkTokenDB.insertOne({
        'confirmed': true,
        'payNow': true,
        'references': [],
        'shiftId': shift._id,
        'workerId': worker._id,
        'checkinTime': moment(`${months[0]}-01T03:00:00.498Z`).toDate(),
        'checkoutTime': moment(`${months[0]}-${enddays}T12:00:00.645Z`).toDate(),
        'type': 'DEDUCTION',
        createdAt: moment().toDate(),
      }, async (err, result) => {
        if (err) error(err);
        await WorkTokenValueDB.insertOne({
          'confirmRequest': true,
          'expectedWorkTokenShiftId': shift._id,
          'expectedWorkTokenWorkerId': worker._id,
          'expectedWorkTokenType': 'DEDUCTION',
          'expectedWorkTokenCheckinTime': moment(`${months[0]}-01T03:00:00.498Z`).toDate(),
          'expectedWorkTokenCheckoutTime': moment(`${months[0]}-${enddays}T12:00:00.645Z`).toDate(),
          'workTokenId': result.ops[0]._id,
          'amount': 1000,
          'fee': 0,
          'currency': 'HKD',
          'method': 'Tap & Go',
          'coordinates': worker.phone,
          'notes': 'late',
          'createdAt': moment().toDate(),
        });
        await FiatTokenDB.insertOne({
          'amount': 1000,
          'coordinates': worker.phone,
          'currency': 'HKD',
          'method': 'Tap & Go',
          'workTokenId': result.ops[0]._id,
          'transactionRef': TRANSACTION_REFS.find(e => e.phone === worker.phone).transactionRef,
          'createdAt': moment().toDate(),
        });
      });
      await Promise.all(months.map(async (month) => {
        let days = moment(month, 'YYYY-MM').daysInMonth();
        const oneDayAmount = job.renumerationValue / days;
        const khupayFee = 3 / 100 * (oneDayAmount * 3);

        if (month === moment().format('YYYY-MM')) {
          days = moment().add(-1, 'day').format('DD');
        }

        const lateDays = ['11', '16', '17'];
        const wrongLocationDays = ['05', '19', '24'];
        const allWrongDays = ['28'];
        const leaveDay = ['13', '22'];
        const khupayDays = ['01', '02', '03'];
        const code = generateCode(6);
        const password = generateCode(6);
        if (month !== moment().format('YYYY-MM')) {
          await ReceiptDB.insertOne({
            'workerId': worker._id,
            'shiftId': shift._id.toString(),
            'transactionRef': TRANSACTION_REFS.find(e => e.phone === worker.phone).transactionRef,
            'password': password,
            'jobId': job._id.toString(),
            'organisationId': '5ffd24f4558749001cc1a039',
            'code': code,
            'complete': true,
            'payCycleStart': `${month}-01`,
            'payCycleEnd': `${month}-${days.toString()}`,
            'createdAt': moment().toDate(),
            '__v': 0,
            'receivedAt': moment().toDate(),
          });
        }
        for (let i = 1; i <= days; i += 1) {
          let day = i.toString();
          if (day.length === 1) day = `0${day}`;
          if (lateDays.includes(day)) {
            await WorkTokenDB.insertOne({
              confirmed: true,
              payNow: false,
              references: [
                {
                  'key': 'CHECKIN_IMAGE',
                  'value': 'https://images.mubicdn.net/images/cast_member/34094/cache-532-1478101707/image-w856.jpg?size=800x',
                },
                {
                  'key': 'CHECKIN_COORDINATES',
                  'value': {
                    'lat': 16.7689853,
                    'lng': 96.1602374,
                  },
                },
                {
                  'key': 'CHECKOUT_IMAGE',
                  'value': 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/bruce-lee-once-upon-a-time-in-hollywood-1563903489.jpg?crop=0.495xw:0.990xh;0,0&resize=480:*',
                },
                {
                  'key': 'CHECKOUT_COORDINATES',
                  'value': {
                    'lat': 16.7689853,
                    'lng': 96.1602374,
                  },
                },
              ],
              shiftId: shift._id,
              checkinTime: moment(`${month}-${day}T05:00:00.498Z`).toDate(),
              type: 'HYBRID_TIMESHEET',
              workerId: worker._id,
              checkoutTime: moment(`${month}-${day}T15:00:00.645Z`).toDate(),
              createdAt: moment().toDate(),
            });
          } else if (wrongLocationDays.includes(day)) {
            await WorkTokenDB.insertOne({
              confirmed: true,
              payNow: false,
              references: [
                {
                  'key': 'CHECKIN_IMAGE',
                  'value': 'https://images.mubicdn.net/images/cast_member/34094/cache-532-1478101707/image-w856.jpg?size=800x',
                },
                {
                  'key': 'CHECKIN_COORDINATES',
                  'value': {
                    'lat': 16.7798674,
                    'lng': 96.1769126,
                  },
                },
                {
                  'key': 'CHECKOUT_IMAGE',
                  'value': 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/bruce-lee-once-upon-a-time-in-hollywood-1563903489.jpg?crop=0.495xw:0.990xh;0,0&resize=480:*',
                },
                {
                  'key': 'CHECKOUT_COORDINATES',
                  'value': {
                    'lat': 16.7793173,
                    'lng': 96.1756357,
                  },
                },
              ],
              shiftId: shift._id,
              checkinTime: moment(`${month}-${day}T03:00:00.498Z`).toDate(),
              type: 'HYBRID_TIMESHEET',
              workerId: worker._id,
              checkoutTime: moment(`${month}-${day}T12:00:00.645Z`).toDate(),
              createdAt: moment().toDate(),
            });
          } else if (allWrongDays.includes(day)) {
            await WorkTokenDB.insertOne({
              confirmed: true,
              payNow: false,
              references: [
                {
                  'key': 'CHECKIN_IMAGE',
                  'value': 'https://images.mubicdn.net/images/cast_member/34094/cache-532-1478101707/image-w856.jpg?size=800x',
                },
                {
                  'key': 'CHECKIN_COORDINATES',
                  'value': {
                    'lat': 16.7798674,
                    'lng': 96.1769126,
                  },
                },
                {
                  'key': 'CHECKOUT_IMAGE',
                  'value': 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/bruce-lee-once-upon-a-time-in-hollywood-1563903489.jpg?crop=0.495xw:0.990xh;0,0&resize=480:*',
                },
                {
                  'key': 'CHECKOUT_COORDINATES',
                  'value': {
                    'lat': 16.7793173,
                    'lng': 96.1756357,
                  },
                },
              ],
              shiftId: shift._id,
              checkinTime: moment(`${month}-${day}T06:00:00.498Z`).toDate(),
              type: 'HYBRID_TIMESHEET',
              workerId: worker._id,
              checkoutTime: moment(`${month}-${day}T15:00:00.645Z`).toDate(),
              createdAt: moment().toDate(),
            });
          } else if (leaveDay.includes(day)) {
            let start = parseInt(day, 10) - 1;
            if (start.length === 1) start = `0${start}`;
            await WorkTokenDB.insertOne({
              'confirmed': true,
              'payNow': true,
              'references': [
                {
                  'leaveStart': `${month}-${start}T17:30:00.000Z`,
                  'leaveEnd': `${month}-${day}T17:30:00.000Z`,
                  'halfDay': false,
                  'utcOffset': 390,
                },
              ],
              'shiftId': shift._id,
              'checkinTime': moment(`${month}-${day}T06:00:00.498Z`).toDate(),
              'type': 'LEAVE_SICK',
              'workerId': worker._id,
              'createdAt': moment('2020-11-05T01:01:12.881Z').toDate(),
              'updatedAt': moment('2020-11-06T11:22:41.047Z').toDate(),
            });
          } else if (month !== moment().format('YYYY-MM') && khupayDays.includes(day)) {
            await WorkTokenDB.insertOne({
              confirmed: true,
              payNow: false,
              references: [
                {
                  'key': 'CHECKIN_IMAGE',
                  'value': 'https://images.mubicdn.net/images/cast_member/34094/cache-532-1478101707/image-w856.jpg?size=800x',
                },
                {
                  'key': 'CHECKIN_COORDINATES',
                  'value': {
                    'lat': 16.7689853,
                    'lng': 96.1602374,
                  },
                },
                {
                  'key': 'CHECKOUT_IMAGE',
                  'value': 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/bruce-lee-once-upon-a-time-in-hollywood-1563903489.jpg?crop=0.495xw:0.990xh;0,0&resize=480:*',
                },
                {
                  'key': 'CHECKOUT_COORDINATES',
                  'value': {
                    'lat': 16.7689853,
                    'lng': 96.1602374,
                  },
                },
              ],
              shiftId: shift._id,
              checkinTime: moment(`${month}-${day}T03:00:00.498Z`).toDate(),
              type: 'HYBRID_TIMESHEET',
              workerId: worker._id,
              checkoutTime: moment(`${month}-${day}T12:00:00.645Z`).toDate(),
              createdAt: moment().toDate(),
            });
            let workTokenId = '';
            // let workTokenValueId = '';
            await WorkTokenDB.insertOne({
              'confirmed': true,
              'payNow': true,
              'references': [],
              'shiftId': shift._id,
              'workerId': worker._id,
              'checkinTime': moment(`${month}-${day}T03:00:00.498Z`).toDate(),
              'checkoutTime': moment(`${month}-${day}T12:00:00.645Z`).toDate(),
              'type': 'STANDARD',
              createdAt: moment().toDate(),
              '__v': 0,
            }, async (err, result) => {
              if (err) error(err);
              else workTokenId = result.ops[0]._id;
              await WorkTokenValueDB.insertOne({
                'confirmRequest': true,
                'expectedWorkTokenShiftId': shift._id,
                'expectedWorkTokenWorkerId': worker._id,
                'expectedWorkTokenType': 'STANDARD',
                'expectedWorkTokenCheckinTime': moment(`${month}-${day}T03:00:00.498Z`).toDate(),
                'expectedWorkTokenCheckoutTime': moment(`${month}-${day}T12:00:00.645Z`).toDate(),
                'workTokenId': workTokenId,
                'amount': oneDayAmount,
                'fee': khupayFee,
                'currency': 'HKD',
                'method': 'Tap & Go',
                'coordinates': worker.phone,
                createdAt: moment().toDate(),
                '__v': 0,
              });
              const feeCoordinates = '+959966617840';
              await FiatTokenDB.insertOne({
                'amount': khupayFee / 3,
                'coordinates': feeCoordinates,
                'currency': 'HKD',
                'method': 'Tap & Go',
                'workTokenId': workTokenId,
                'transactionRef': TRANSACTION_REFS.find(e => e.phone === worker.phone).feeTransactionRef,
                'createdAt': moment().toDate(),
                'updatedAt': moment().toDate(),
              });
              await FiatTokenDB.insertOne({
                'amount': oneDayAmount,
                'coordinates': worker.phone,
                'currency': 'HKD',
                'method': 'Tap & Go',
                'workTokenId': workTokenId,
                'transactionRef': TRANSACTION_REFS.find(e => e.phone === worker.phone).khupayTransactionRef,
                'createdAt': moment().toDate(),
                'updatedAt': moment().toDate(),
              });
            });
          } else {
            await WorkTokenDB.insertOne({
              confirmed: true,
              payNow: false,
              references: [
                {
                  'key': 'CHECKIN_IMAGE',
                  'value': 'https://images.mubicdn.net/images/cast_member/34094/cache-532-1478101707/image-w856.jpg?size=800x',
                },
                {
                  'key': 'CHECKIN_COORDINATES',
                  'value': {
                    'lat': 16.7689853,
                    'lng': 96.1602374,
                  },
                },
                {
                  'key': 'CHECKOUT_IMAGE',
                  'value': 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/bruce-lee-once-upon-a-time-in-hollywood-1563903489.jpg?crop=0.495xw:0.990xh;0,0&resize=480:*',
                },
                {
                  'key': 'CHECKOUT_COORDINATES',
                  'value': {
                    'lat': 16.7689853,
                    'lng': 96.1602374,
                  },
                },
              ],
              shiftId: shift._id,
              checkinTime: moment(`${month}-${day}T03:00:00.498Z`).toDate(),
              type: 'HYBRID_TIMESHEET',
              workerId: worker._id,
              checkoutTime: moment(`${month}-${day}T12:00:00.645Z`).toDate(),
              createdAt: moment().toDate(),
            });
          }
          // add payroll data
          if (month !== moment().format('YYYY-MM') && !khupayDays.includes(day)) {
            let workTokenId = '';
            // let workTokenValueId = '';
            await WorkTokenDB.insertOne({
              'confirmed': true,
              'payNow': true,
              'references': [],
              'shiftId': shift._id,
              'workerId': worker._id,
              'checkinTime': moment(`${month}-${day}T03:00:00.498Z`).toDate(),
              'checkoutTime': moment(`${month}-${day}T12:00:00.645Z`).toDate(),
              'type': 'STANDARD',
              createdAt: moment().toDate(),
              '__v': 0,
            }, async (err, result) => {
              if (err) error(err);
              else workTokenId = result.ops[0]._id;
              await WorkTokenValueDB.insertOne({
                'confirmRequest': true,
                'expectedWorkTokenShiftId': shift._id,
                'expectedWorkTokenWorkerId': worker._id,
                'expectedWorkTokenType': 'STANDARD',
                'expectedWorkTokenCheckinTime': moment(`${month}-${day}T03:00:00.498Z`).toDate(),
                'expectedWorkTokenCheckoutTime': moment(`${month}-${day}T12:00:00.645Z`).toDate(),
                'workTokenId': workTokenId,
                'amount': oneDayAmount,
                'fee': 0,
                'currency': 'HKD',
                'method': 'Tap & Go',
                'coordinates': worker.phone,
                createdAt: moment().toDate(),
                '__v': 0,
              });
              await FiatTokenDB.insertOne({
                'amount': oneDayAmount,
                'coordinates': worker.phone,
                'currency': 'HKD',
                'method': 'Tap & Go',
                'workTokenId': workTokenId,
                'transactionRef': TRANSACTION_REFS.find(e => e.phone === worker.phone).transactionRef,
                'createdAt': moment().toDate(),
                'updatedAt': moment().toDate(),
              });
            });
          }
        }
      }));
    }));
    // Adding data for alien workers
    info('Adding data for alien workers');
    const alienworkers = await alienworkerDB.find({ createOrganisationId: mongoose.Types.ObjectId('5ffd24f4558749001cc1a039') }).toArray();
    await Promise.all(alienworkers.map(async (worker) => {
      await WorkerPaymentDB.insertOne({
        'workerId': worker._id.toString(),
        'paymentMethodId': mongoose.Types.ObjectId('5e1b1b23e5fe3c035c78c21f'),
        'coordinates': worker.phone,
      });
      const workerPayments = await WorkerPaymentDB.findOne({ workerId: worker._id.toString() });

      await alienworkerDB.findOneAndUpdate(
        {
          phone: worker.phone,
          verified: true,
        },
        {
          $set: {
            'defaultWorkerPaymentMethodId': workerPayments._id,
            'workerPaymentMethodsIds': [workerPayments._id],
          },
        },
      );
      const month = moment().add(-1, 'month').format('YYYY-MM');
      const days = moment(month, 'YYYY-MM').daysInMonth();
      await ShiftDB.findOneAndUpdate(
        {
          workerId: worker._id.toString(),
        },
        {
          $set: {
            'workComplete': false,
            'endTime': moment(`2025-01-${days}T12:00:00.645Z`).toDate(),
          },
        },
      );
      await ShiftDB.findOneAndUpdate(
        {
          workerId: '5ffd7decbff30b001b686ff3',
        },
        {
          $set: {
            'workComplete': true,
            'endTime': moment(`${month}-${days - 1}T12:00:00.645Z`).toDate(),
          },
        },
      );

      async function addDataForAliens() {
        const shift = await ShiftDB.findOne({
          workerId: worker._id.toString(),
        });
        const job = await JobDB.findOne({
          _id: shift.jobId,
        });
        await JobDB.findOneAndUpdate(
          {
            _id: shift.jobId,
          },
          {
            $set: {
              'renumerationCurrency': 'HKD',
            },
          },
        );
        const normalTransactionRef = await getTransactionRef();
        for (let i = 1; i <= days; i += 1) {
          let day = i.toString();
          if (day.length === 1) day = `0${day}`;
          // add payroll data
          await WorkTokenDB.insertOne({
            'confirmed': true,
            'payNow': true,
            'references': [],
            'shiftId': shift._id,
            'workerId': worker._id.toString(),
            'checkinTime': moment(`${month}-${day}T03:00:00.498Z`).toDate(),
            'checkoutTime': moment(`${month}-${day}T12:00:00.645Z`).toDate(),
            'type': 'STANDARD',
            createdAt: moment().toDate(),
            '__v': 0,
          });
        }
        const currentdays = moment(month, 'YYYY-MM').daysInMonth();
        const oneDayAmount = job.renumerationValue / currentdays;
        const workTokens = await WorkTokenDB.find({ shiftId: shift._id }).toArray();
        await Promise.all(workTokens.map(async (workToken) => {
          await WorkTokenValueDB.insertOne({
            'confirmRequest': true,
            'expectedWorkTokenShiftId': shift._id,
            'expectedWorkTokenWorkerId': worker._id.toString(),
            'expectedWorkTokenType': 'STANDARD',
            'expectedWorkTokenCheckinTime': workToken.checkinTime,
            'expectedWorkTokenCheckoutTime': workToken.checkoutTime,
            'workTokenId': workToken._id,
            'amount': oneDayAmount,
            'fee': 0,
            'currency': 'HKD',
            'method': 'Tap & Go',
            'coordinates': worker.phone,
            createdAt: moment().toDate(),
            '__v': 0,
          });
          await FiatTokenDB.insertOne({
            'amount': oneDayAmount,
            'coordinates': worker.phone,
            'currency': 'HKD',
            'method': 'Tap & Go',
            'workTokenId': workToken._id,
            'transactionRef': normalTransactionRef.toString(),
            'createdAt': moment().toDate(),
            'updatedAt': moment().toDate(),
          });
        }));
      }
      await addDataForAliens();
    }));
    await JobDB.findOneAndUpdate(
      {
        paymentOption: 'DALIY',
      },
      {
        $set: {
          paymentOption: 'DAILY',
        },
      },
    );
    info('__Finished__: ');
  } catch (err) {
    error('%O', err);
  }
  process.exit(0);
}());
