#!/bin/sh

COLLECTIONS='organisations employers workers jobs shifts worktokens worktokenvalues fiattokens'

for COLLECTION in ${COLLECTIONS}; do
  mongoimport --db jobdoh_development --collection ${COLLECTION} --file 20190828_production_${COLLECTION}.json
done