require('dotenv').config();
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const mongoose = require('mongoose');
const joi = require('joi');


const getEnvVars = () => {
  const envVarsSchema = joi.object({
    DB_URL: joi.string()
      .default('mongodb+srv://jobdoh_staging:passw0rd@cluster0-qagfz.gcp.mongodb.net/jobdoh_staging'),
  }).unknown()
    .required();
  const { err, value: envVars } = joi.validate(process.env, envVarsSchema);
  if (err) {
    throw new Error(`Config validation error: ${err.message}`);
  }
  return envVars;
};

const envVars = getEnvVars();

const WORKER_PHONES = [
  // '+959691831271',
  // '+959404770584',
  '+959895628510',
  '+959444444444',
  '+85266666666',
  '+85211111111',
];
const UNDELETE_PRODUCTS = [
  mongoose.Types.ObjectId('60810814aecf03002ef54489'),
  mongoose.Types.ObjectId('60812f792b851500271911c9'),
];
const UNDELETE_JOBS = [
  mongoose.Types.ObjectId('6001311b294dee001b849710'),
  mongoose.Types.ObjectId('600138c6d2bd8b001b9e189d'),
  mongoose.Types.ObjectId('60012eac294dee001b84970b'),
  mongoose.Types.ObjectId('60013258d2bd8b001b9e1894'),
  mongoose.Types.ObjectId('600132006c3c6f001bc7c18b'),
  mongoose.Types.ObjectId('60012fdc833f8a001b591147'),
  mongoose.Types.ObjectId('600135e8294dee001b84972e'),
  mongoose.Types.ObjectId('6001372e4a6b56001b80e7b4'),
  mongoose.Types.ObjectId('600136d4294dee001b84972f'),
  mongoose.Types.ObjectId('6001354b4a6b56001b80e7b1'),
  mongoose.Types.ObjectId('600131ba294dee001b849712'),
  mongoose.Types.ObjectId('6001387d6c3c6f001bc7c197'),
  mongoose.Types.ObjectId('600133f7294dee001b84972c'),
  mongoose.Types.ObjectId('600132a1294dee001b849729'),
  mongoose.Types.ObjectId('6001345a6c3c6f001bc7c190'),
  mongoose.Types.ObjectId('6001338ad2bd8b001b9e1895'),
  mongoose.Types.ObjectId('600136506c3c6f001bc7c193'),
  mongoose.Types.ObjectId('600132f26c3c6f001bc7c18e'),
  mongoose.Types.ObjectId('6001377fd2bd8b001b9e189c'),
  mongoose.Types.ObjectId('5ffd8df2bff30b001b686ff5'),
  mongoose.Types.ObjectId('600130cf294dee001b84970e'),
  mongoose.Types.ObjectId('600146d24a6b56001b80e7c0'),
  mongoose.Types.ObjectId('600147676c3c6f001bc7c1a2'),
  mongoose.Types.ObjectId('600147c94a6b56001b80e7c6'),
  mongoose.Types.ObjectId('600147166c3c6f001bc7c1a1'),
];

async function connectDatabase(url) {
  info('DB Opening: %s', url);
  const ret = await mongoose.connect(url, { useNewUrlParser: true });
  info('DB Connected');
  return ret;
}

// eslint-disable-next-line func-names
(async function () {
  await connectDatabase(envVars.DB_URL)
    .catch((err) => {
      info('Error while connecting DB');
      error('%O', err);
      process.exit(0);
    });

  try {
    info('__Start__: ');
    const db = mongoose.connection.db;
    const workerDB = await db.collection('workers');
    const shiftDB = await db.collection('shifts');
    const jobDB = await db.collection('jobs');
    const alienworkerDB = await db.collection('alienworkers');
    const workTokenDB = await db.collection('worktokens');
    const workTokenValueDB = await db.collection('worktokenvalues');
    const fiatTokenDB = await db.collection('fiattokens');
    const receiptDB = await db.collection('receipts');
    const discountTokenDB = await db.collection('discounttokens');
    const workerPaymentMethodDB = await db.collection('workerpaymentmethods');
    const jobContractDB = await db.collection('jobcontracts');
    const marketplaceDB = await db.collection('marketplaces');
    const productDB = await db.collection('products');
    const SubscriberDB = await db.collection('expectedmarketplacesubscribers');

    const workers = await workerDB.find({ phone: { $in: WORKER_PHONES } }).toArray();
    const alienworkers = await alienworkerDB.find({ createOrganisationId: mongoose.Types.ObjectId('5ffd24f4558749001cc1a039') }).toArray();
    const alienworkerIds = alienworkers.map(e => e._id.toString());
    info('alienworker ids: ', alienworkerIds);
    const workerIds = workers.map(e => e._id);
    info('worker ids: ', workerIds);
    // const shifts = await shiftDB
    //   .find({ workerId: { $in: [...workerIds, ...alienworkerIds] } }).toArray();
    // const jobIds = shifts.map(e => e.jobId.toString());
    const workTokens = await workTokenDB
      .find({ workerId: { $in: [...workerIds, ...alienworkerIds] } }).toArray();
    const workTokenIds = workTokens.map(e => e._id);
    info('worktoken ids: ', workTokenIds);
    await shiftDB.updateMany(
      {
        workerId: { $in: [...workerIds, ...alienworkerIds] },
      },
      {
        $set: {
          deductions: [],
        },
      },
    );
    await workTokenDB.deleteMany({
      workerId: { $in: [...workerIds, ...alienworkerIds] },
    });
    await workTokenValueDB.deleteMany({
      expectedWorkTokenWorkerId: { $in: [...workerIds, ...alienworkerIds] },
    });
    await fiatTokenDB.deleteMany({
      workTokenId: { $in: workTokenIds },
    });
    await receiptDB.deleteMany({
      workerId: { $in: [...workerIds, ...alienworkerIds] },
    });
    await discountTokenDB.deleteMany({
      workerId: { $in: [...workerIds, ...alienworkerIds] },
    });
    await workerPaymentMethodDB.deleteMany({
      workerId: { $in: [...workerIds, ...alienworkerIds] },
    });
    await jobContractDB.deleteMany({
      employerId: '5bgTYnCqydPBoGnyZ1BeNEVrTxp2', // delete all from jobdoh.demo@gmail.com
    });
    await shiftDB.deleteMany({
      organisationId: mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
      jobId: { $nin: UNDELETE_JOBS },
    });
    await jobDB.deleteMany({
      organisationId: '5ffd24f4558749001cc1a039',
      _id: { $nin: UNDELETE_JOBS },
    });
    await productDB.deleteMany({
      organisationId: mongoose.Types.ObjectId('5ffd24f4558749001cc1a039'),
      _id: { $nin: UNDELETE_PRODUCTS },
    });
    await marketplaceDB.deleteMany({
      organisationId: '5ffd24f4558749001cc1a039',
    });
    await alienworkerDB.deleteOne({
      phone: '+959987654321',
      name: 'Maung Maung',
    });
    await Promise.all(WORKER_PHONES.map(async (phone) => {
      await workerDB.findOneAndUpdate(
        {
          phone,
          verified: true,
        },
        {
          $set: {
            jobsBookmarkIds: [],
            addToCartProductsIds: [],
            jobsAppliedIds: [],
            jobsWithdrewIds: [],
            jobsNotHiredIds: [],
            jobSkills: [],
            jobHistories: [],
            description: '',
            marketplacesIds: [],
          },
        },
      );
      await SubscriberDB.deleteMany({
        phone,
      });
    }));
    info('__Finished__: ');
  } catch (err) {
    error('%O', err);
  }
  process.exit(0);
}());
