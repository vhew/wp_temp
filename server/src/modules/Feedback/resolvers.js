const error = require('debug')('ERROR');
const {
  ApolloError,
} = require('apollo-server-koa');
const {
  Job,
} = require('../../../models/mongodb');

const ErrorHandlingService = require('../../services/ErrorHandlingService');
const { FeedbackToken } = require('../Feedback/model');
const { JobContract } = require('../JobContracts/model');
const RatingService = require('../../services/RatingService');
const agenda = require('../../scheduler/agenda');
const agendaHelper = require('../../scheduler/helper');

const getCombinedFeedbacktoken = (fTokens = []) => {
  const [fToken] = fTokens;
  return Object.assign(fToken || {}, {
    feedbackValue: JSON.stringify(fTokens.map(f => JSON.parse(f.feedbackValue))),
  });
};

const resolvers = {
  Query: {
    feedbackTokens: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.id,
      //   type: 'Worker',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // debug('query worker as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const filter = {};
        const ignoreKeys = [];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            filter[key] = data[key];
          }
        });

        const feedbackTokens = await FeedbackToken.find(filter);

        if (feedbackTokens == null) { throw new ApolloError('Feedback Tokens not found', 404); }

        ret = feedbackTokens;
      } catch (e) {
        error('query:feedbackTokens:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
  },
  Mutation: {
    async createFeedbackToken(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createUnverifiedWorker as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const { fromId, targetId } = data;
        const update = {};
        update.fromType = data.fromType;
        update.fromId = fromId;
        update.targetType = data.targetType;
        update.targetId = targetId;
        update.feedbackType = data.feedbackType;
        update.feedbackValue = data.feedbackValue;
        update.reference = data.reference;
        ret = await FeedbackToken.updateOne(
          { fromId, targetId },
          update,
          { upsert: true },
        );
      } catch (err) {
        error('mutation:createFeedbackToken:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async rateEmployer(_, data, ctx) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let createdFeedbackToken;
      try {
        const {
          targetId: jobContractId, fromId: workerId, value, reference,
        } = data;

        createdFeedbackToken = await RatingService.rateJob(
          workerId, jobContractId, value, reference,
        );
        if (createdFeedbackToken) {
          await agendaHelper.cancelAutoRatingForJob({ workerId, jobContractId });
        }
      } catch (err) {
        error('mutation:rateEmployer:', err);
        throw ErrorHandlingService.getError(err);
      }
      return createdFeedbackToken;
    },
    async rateWorker(_, data, ctx) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let createdFeedbackToken;
      try {
        const {
          jobContractId, workerId, value, reference,
        } = data;

        createdFeedbackToken = await RatingService.rateWorker(
          jobContractId, workerId, value, reference,
        );
        if (createdFeedbackToken) {
          await agendaHelper.cancelAutoRatingForWorker({ jobContractId, workerId });
        }
      } catch (err) {
        error('mutation:rateWorker:', err);
        throw ErrorHandlingService.getError(err);
      }
      return createdFeedbackToken;
    },
  },
  Job: {
    feedback: async (job, { workerId }) => {
      let feedbackToken;
      try {
        if (workerId) {
          const jobContract = await JobContract.findOne({
            jobId: job.id,
            workerId,
          }, '_id');
          feedbackToken = await FeedbackToken.findOne({
            fromId: workerId,
            targetId: jobContract && jobContract._id.toString(),
          });
        } else {
          const jobContracts = await JobContract.find({ jobId: job.id }, '_id');
          const feedbackTokens = await FeedbackToken.find({
            targetId: { $in: jobContracts.map(e => e._id.toString()) },
          });
          feedbackToken = getCombinedFeedbacktoken(feedbackTokens);
        }
      } catch (err) {
        error('resolver:Job:feedback:', err);
        throw ErrorHandlingService.getError(err);
      }
      return feedbackToken;
    },
  },
  Organisation: {
    organisationRating: async (organisation) => {
      let result;
      try {
        const organisationId = organisation.id;
        const organisationJobs = await Job.find({ organisationId });
        let contracts = await Promise.all(organisationJobs.map(async (orgJob) => {
          const contract = await JobContract.findOne({ jobId: orgJob.id });
          if (contract == null) return null;
          return contract;
        }));
        contracts = contracts.filter(e => e != null);
        let ratings = await Promise.all(contracts.map(async (contract) => {
          const feedback = await FeedbackToken.findOne({ targetId: contract.id });
          if (feedback == null) return null;
          return parseInt(feedback.feedbackValue, 10);
        }));
        ratings = ratings.filter(e => e != null);
        const averageRating = ratings.reduce((a, b) => a + b, 0) / ratings.length;
        result = parseInt((averageRating * 3) / 5, 10);
      } catch (err) {
        error('resolver:Organisation:organisationRating:', err);
      }
      return result;
    },
  },
  Worker: {
    // feedback: async (worker, { employerId }) => {
    //   let feedbackToken;
    //   try {
    //     feedbackToken = await FeedbackToken.findOne({
    //       fromId: employerId, // todo: replace with jobcontractId
    //       targetId: worker.id,
    //     });
    //   } catch (err) {
    //     throw ErrorHandlingService.getError(err);
    //   }
    //   return feedbackToken;
    // },
  },
  Shift: {
    feedbackToWorker: async ({ jobId, workerId }) => {
      let result;
      try {
        if (!workerId || !jobId) return null;
        const jobContract = await JobContract.findOne({
          jobId,
          workerId,
        }, '_id');
        const feedbackToken = await FeedbackToken.findOne({
          fromId: jobContract && jobContract._id.toString(),
          targetId: workerId,
        });
        result = feedbackToken;
      } catch (err) {
        error('resolver:Shift:feedbackToWorker:', err);
      }
      return result;
    },
    feedbackToEmployer: async ({ jobId, workerId }) => {
      let result;
      try {
        if (!workerId || !jobId) return null;
        const jobContract = await JobContract.findOne({
          jobId,
          workerId,
        }, '_id');
        const feedbackToken = await FeedbackToken.findOne({
          fromId: workerId,
          targetId: jobContract && jobContract._id.toString(),
        });
        result = feedbackToken;
      } catch (err) {
        error('resolver:Shift:feedbackToEmployer:', err);
      }
      return result;
    },
  },

};

module.exports = resolvers;
