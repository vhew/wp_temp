const mongoose = require('mongoose');

const FeedbackSchema = new mongoose.Schema(
  {
    fromType: {
      type: String,
    },
    fromId: {
      type: String,
    },
    targetType: {
      type: String,
    },
    targetId: {
      type: String,
    },
    feedbackType: {
      type: String,
    },
    feedbackValue: {
      type: String,
    },
    reference: {
      type: String,
    },
  },
  { timestamps: true },
);

FeedbackSchema.index({ fromId: 1, targetId: 1 });

const FeedbackToken = mongoose.model('FeedbackToken', FeedbackSchema);

module.exports = {
  FeedbackToken,
};
