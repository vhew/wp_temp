const { GraphQLModule } = require('@graphql-modules/core');
const CoreModule = require('./../cores/index');
const PaymentModule = require('./payments/index');
const CampaignModule = require('./campaigns/index');
const MarketplaceModule = require('./marketplaces/index');
const JobRequirementModule = require('./jobRequirements/index');
const LedgerEntryModule = require('./LedgerEntries/index');
const ShiftModule = require('./Shifts/index');
const NotificationModule = require('./Notifications/index');
const TransactionModule = require('./transactions/index');
const BatchUploadModule = require('./batchuploads/index');
const PayrollModule = require('./Payrolls/index');
const TimesheetModule = require('./Timesheets/index');
const DisbursementModule = require('./disbursement/index');
const UserPermissionModule = require('./UserPermissions/index');
const AdministratorModule = require('./Administrators/index');
const JobContractModule = require('./JobContracts/index');
const LocationModule = require('./Locations/index');
const FeedbackModule = require('./Feedback/index');
const ProductModule = require('./Products/index');
const AffiliateModule = require('./Affiliates/index');
const InterviewModule = require('./interview/index');


const AppModule = new GraphQLModule({
  imports: [
    CoreModule,
    PaymentModule,
    CampaignModule,
    MarketplaceModule,
    JobRequirementModule,
    LedgerEntryModule,
    ShiftModule,
    NotificationModule,
    TransactionModule,
    PayrollModule,
    BatchUploadModule,
    TimesheetModule,
    DisbursementModule,
    UserPermissionModule,
    AdministratorModule,
    JobContractModule,
    LocationModule,
    FeedbackModule,
    ProductModule,
    AffiliateModule,
    InterviewModule,
  ],
});

module.exports = AppModule;
