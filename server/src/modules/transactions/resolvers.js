const { ApolloError } = require('apollo-server-koa');
const error = require('debug')('ERROR');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const {
  Worker,
  Job,
  WorkTokenValue,
  Organisation,
} = require('../../../models/mongodb/');
const AccessControlService = require('../../services/AccessControlService');
const PayslipPaidService = require('../../services/PayslipPaidService');

const resolvers = {
  Query: {
    transactions: async (root, data) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.shiftId,
      //   type: 'Shift',
      // };
      // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('querying fiatTokens as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        const organisation = await Organisation.findById(data.organisationId);
        if (organisation == null) throw new ApolloError('Cannot find job', 404);
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);
        ret = WorkTokenValue.aggregate([
          {
            $match: {
              expectedWorkTokenCheckinTime: { $gte: new Date('2019-12-15') },
              expectedWorkTokenWorkerId: '0qGJKUACMiPfeHaEcRwVkFAskJ32',
            },
          },
          {
            $group: {
              _id: '$expectedWorkTokenWorkerId',
              amount: { $sum: '$amount' },
            },
          },
        ]);
      } catch (err) {
        error('query:transactions:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
      // return [{
      //   salary: 100000,
      //   amount: 30000,
      //   fee: 1000,
      //   discount: 1000,
      // }];
    },
    disbursementSummary: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      let ret;
      try {
        const { amount } = await PayslipPaidService.getTotalPaid({
          workerId: data.workerId,
          jobId: data.jobId,
          from: data.timeRange.from,
          to: data.timeRange.to,
        });
        const worker = await Worker.findById(data.workerId);
        ret = {
          worker,
          amount,
        };
      } catch (e) {
        error('query:disbursementSummary:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    disbursementDetail: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      let ret;
      try {
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);
        const job = await Job.findById(data.jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        const { totalKhuPay, totalBonusDeduction } = await PayslipPaidService
          .getDisbursementDetail({
            workerId: data.workerId,
            jobId: data.jobId,
            from: data.timeRange.from,
            to: data.timeRange.to,
          });

        const {
          amount,
          fee,
        } = totalKhuPay[0];
        const {
          bonus,
          deductions,
        } = totalBonusDeduction[0];
        ret = {
          amount,
          fee,
          bonus,
          deductions,
        };
      } catch (err) {
        error('query:disbursementDetail:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },

  },
};

module.exports = resolvers;
