
const error = require('debug')('ERROR');
const sha256 = require('sha256');
const {
  LedgerEntry,
} = require('./model');

const LedgerUtils = {
  createLedgerEntry: async (data) => {
    const lastEntry = await LedgerEntry.find()
      .limit(1).sort({ $natural: -1 })
      .catch(err => error(err));

    const prevHash = (lastEntry.length > 0) ? lastEntry[0].hash : '0';
    let dataToHash = '';
    const update = {};
    Object.keys(data).forEach((key) => {
      update[key] = data[key];
      dataToHash += data[key];
    });
    update.prevHash = prevHash;

    dataToHash += prevHash;
    update.hash = sha256(dataToHash);
    const ledgerEntry = new LedgerEntry(update);

    const ret = await ledgerEntry.save()
      .catch(err => error(err));
    return ret;
  },
};

module.exports = LedgerUtils;
