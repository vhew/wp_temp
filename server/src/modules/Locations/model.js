const mongoose = require('mongoose');

const TownshipSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      required: true,
    },
    township: { type: String },
    townshipLocal: { type: String },
    townshipLat: { type: String },
    townshipLng: { type: String },
    district: { type: String },
    districtLocal: { type: String },
    state: { type: String },
    stateLocal: { type: String },
    country: { type: String },
  },
  { timestamps: true },
);

TownshipSchema.index({ township: 1 });

const Township = mongoose.model('Township', TownshipSchema);

module.exports = {
  Township,
};
