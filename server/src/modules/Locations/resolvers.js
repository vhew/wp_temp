const error = require('debug')('ERROR');
const {
  Township,
} = require('./model');

const resolvers = {
  Query: {
    townships: async (root, data, ctx) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let result;
      try {
        const { state, country } = data;
        const townships = await Township.find({ country }).sort({ township: 1 });
        const townshipsByState = townships.filter(t => t.state === state);
        result = townshipsByState.length ? townshipsByState : townships;
      } catch (err) {
        error('query:townships:', err);
        ctx.ctx.throw(err);
      }
      return result;
    },
  },
};

module.exports = resolvers;
