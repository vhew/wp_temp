const error = require('debug')('ERROR');
const {
  Shift,
} = require('../../../models/mongodb/index');

const utils = {
  _getSameTimesShiftsIds: async ({ shiftId }) => {
    let ret;
    try {
      const appliedShift = await Shift.findById(shiftId);
      const shifts = await Shift.find({
        jobId: appliedShift.jobId,
      });

      ret = shifts
        .filter(shift => shift.startTime.toString() === appliedShift.startTime.toString()
        && shift.endTime.toString() === appliedShift.endTime.toString());
    } catch (err) {
      error(err);
    }
    const shiftsIds = ret.map(e => e._id);
    return shiftsIds;
  },
};

module.exports = utils;
