const { ObjectId } = require('mongoose').Types;
const error = require('debug')('ERROR');
const info = require('debug')('INFO');
const moment = require('moment');
const R = require('ramda');
const {
  ApolloError,
} = require('apollo-server-koa');
const {
  Job,
  Shift,
  Worker,
  Organisation,
  WorkToken,
} = require('./../../../models/mongodb/index');
const utils = require('../../utils');
const locales = require('../../locales/index');
const notifications = require('../../services/NotificationService');
const LeaveService = require('../../services/LeaveService');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const agenda = require('../../scheduler/agenda');

const LEAVE_TYPES = [
  'LEAVE_SICK',
  'LEAVE_PERSONAL',
  'LEAVE_SPECIAL',
  'LEAVE_UNPAID',
];

const resolvers = {
  Mutation: {
    calculateSiblingShifts: async (root, data) => {
      let ret;
      try {
        const job = await Job.findById(data.jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        const aggregatedResult = await Shift.aggregate([
          {
            $match: {
              jobId: ObjectId(data.jobId),
            },
          },
          {
            $group: {
              _id: {
                startTime: '$startTime',
                endTime: '$endTime',
              },
              shiftsIds: {
                $push: '$_id',
              },
            },
          },
        ]);

        const groupedShiftsIds = aggregatedResult.map(r => r.shiftsIds);

        ret = await Promise.all(
          groupedShiftsIds.map(siblingShiftsIds => Shift.updateMany(
            {
              _id: {
                $in: siblingShiftsIds,
              },
            },
            {
              $set: {
                siblingShiftsIds,
              },
            },
          )),
        );
      } catch (err) {
        error('mutation:calculateSiblingShifts:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret ? 1 : 0;
    },
    offerShift: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Shift',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation offerShift as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const shift = await Shift.findById(data.id);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);
        if (shift.siblingShiftsIds.length === 0) {
          ret = await Shift.findOneAndUpdate(
            {
              _id: shift._id,
            },
            {
              $set: {
                workerId: data.workerId,
              },
              $pull: {
                candidatesIds: data.workerId,
              },
            },
          );
        } else {
          const unfilledShifts = await Shift.find({
            $and: [
              {
                _id: {
                  $in: shift.siblingShiftsIds,
                },
              },
              {
                workerId: {
                  $exists: false,
                },
              },
            ],
          });
          if (unfilledShifts.length > 0) {
            const [offeredShift] = unfilledShifts;
            ret = await Shift.findOneAndUpdate(
              {
                _id: offeredShift._id,
              },
              {
                $set: {
                  workerId: data.workerId,
                },
              },
              {
                new: true,
              },
            );
            if (ret) {
              await Shift.updateMany(
                {
                  _id: {
                    $in: shift.siblingShiftsIds,
                  },
                },
                {
                  $pull: {
                    candidatesIds: data.workerId,
                  },
                },
              );
            }
          }
        }


        await Worker.findOneAndUpdate(
          {
            _id: data.workerId,
          },
          {
            $pull: {
              jobsBookmarkIds: shift.jobId,
            },
          },
        );

        // notify worker
        // const shift = await Shift.findById(data.id);
        const job = await Job.findById(shift.jobId);
        const organisation = await Organisation.findById(job.organisationId);

        const language = worker.languageSetting || 'en';
        const title = locales.translate('offerShiftTitle', language);
        let body = locales.translate('offerShiftBody', language);
        body = body.replace('@@@', organisation.name);
        body = body.replace('###', job.title);
        const message = {
          title,
          body,
          data: {
            senderId: shift.employerId,
            receiverId: data.workerId,
            title,
            body,
            jobId: shift.jobId,
            shiftId: data.id,
          },
          fcm_options: {
            analyticsLabel: 'notification_individual_hired',
          },
        };
        notifications.send({
          userType: 'worker',
          id: data.workerId,
          messageType: 'push',
          message,
        });
        // withdraw job in x hours (if not accepted yet)
        const x = 48;
        const nextXHour = moment().add(x, 'hours').toDate();
        const args = JSON.stringify({
          root,
          data: {
            shiftId: shift._id,
            workerId: data.workerId,
            jobId: shift.jobId,
          },
          ctx,
        });
        await agenda.schedule(nextXHour, 'withdraw job', args);
      } catch (err) {
        error('mutation:offerShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      const update = await Shift.findById(data.id);
      return update;
    },
    tradeShiftRequest: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.id,
      //   type: 'Shift',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation offerShift as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);

        // notify worker
        // const shift = await Shift.findById(data.id);
        const job = await Job.findById(data.jobId);
        await Shift.findOneAndUpdate(
          { _id: ObjectId(data.shiftId) },
          {
            $set: {
              tradeShiftStatus: 'pending',
            },
          },
          { new: true },
        ).catch(err => error(err));
        const language = worker.languageSetting || 'en';
        const title = locales.translate('tradeShiftTitle', language);
        let body = locales.translate('tradeShiftBody', language);
        body = body.replace('@@@', worker.name);
        body = body.replace('###', job.title);
        const workers = [];
        await Promise.all(
          job.shiftsIds.map(async (shiftId) => {
            const workerShift = await Shift.findById(shiftId);
            const workerId = await workerShift.workerId;
            if (shiftId !== data.shiftId && workerId != null) {
              const shiftWorker = await Worker.findById(workerId);
              workers.push(shiftWorker);
              const message = {
                title,
                body,
                data: {
                  senderId: worker.id,
                  receiverId: workerShift.workerId,
                  title,
                  body,
                  jobId: data.jobId,
                  shiftId: data.shiftId,
                  click_action: 'trade_job',
                },
                fcm_options: {
                  analyticsLabel: 'notification_individual_hired',
                },
              };
              notifications.send({
                userType: 'worker',
                id: workerId,
                messageType: 'push',
                message,
              });
              ret = workers;
            }
          }),
        );
      } catch (err) {
        error('mutation:tradeShiftRequest:', err);
        throw ErrorHandlingService.getError(err);
      }
      // const update = await Shift.findById(data.shiftId);
      return ret;
    },
    tradeShiftConfirm: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.id,
      //   type: 'Shift',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation offerShift as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);

        const shiftToCreate = JSON.parse(JSON.stringify(shift));
        delete shiftToCreate._id;
        const newShift = new Shift(shiftToCreate);
        const confirmShift = await newShift.save();
        await Job.findOneAndUpdate(
          {
            _id: data.jobId,
          },
          {
            $addToSet: {
              shiftsIds: confirmShift._id,
            },
          },
        );

        // update in old shift
        const job = await Job.findById(data.jobId);
        ret = await Shift.findOneAndUpdate(
          { _id: ObjectId(data.shiftId) },
          {
            $set: {
              workComplete: true,
              endTime: new Date(),
            },
          },
          { new: true },
        ).catch(err => error(err));

        // update in new shift
        ret = await Shift.findOneAndUpdate(
          { _id: confirmShift._id },
          {
            $set: {
              workerId: data.workerId,
              workComplete: false,
            },
          },
          { new: true },
        ).catch(err => error(err));

        const language = worker.languageSetting || 'en';
        const title = locales.translate('tradeShiftConfirmTitle', language);
        let body = locales.translate('tradeShiftConfirmBody', language);
        body = body.replace('@@@', worker.name);
        body = body.replace('###', job.title);
        const message = {
          title,
          body,
          data: {
            senderId: worker.id,
            receiverId: shift.workerId,
            title,
            body,
            jobId: data.jobId,
            shiftId: data.shiftId,
          },
          fcm_options: {
            analyticsLabel: 'notification_individual_hired',
          },
        };
        notifications.send({
          userType: 'worker',
          id: shift.workerId,
          messageType: 'push',
          message,
        });
      } catch (err) {
        error('mutation:tradeShiftConfirm:', err);
        throw ErrorHandlingService.getError(err);
      }
      const update = await Shift.findById(data.shiftId);
      return update;
    },
    tradeShiftReject: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.id,
      //   type: 'Shift',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation offerShift as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);

        // notify worker
        // const shift = await Shift.findById(data.id);
        const job = await Job.findById(data.jobId);

        ret = await Shift.findOneAndUpdate(
          { _id: ObjectId(data.shiftId) },
          {
            $set: {
              tradeShiftStatus: 'rejected',
            },
          },
          { new: true },
        ).catch(err => error(err));

        const language = worker.languageSetting || 'en';
        const title = locales.translate('tradeShiftRejectTitle', language);
        let body = locales.translate('tradeShiftRejectBody', language);
        body = body.replace('@@@', worker.name);
        body = body.replace('###', job.title);
        const message = {
          title,
          body,
          data: {
            senderId: worker.id,
            receiverId: shift.workerId,
            title,
            body,
            jobId: data.jobId,
            shiftId: data.shiftId,
          },
          fcm_options: {
            analyticsLabel: 'notification_individual_hired',
          },
        };
        notifications.send({
          userType: 'worker',
          id: shift.workerId,
          messageType: 'push',
          message,
        });
      } catch (err) {
        error('mutation:tradeShiftReject:', err);
        throw ErrorHandlingService.getError(err);
      }
      const update = await Shift.findById(data.shiftId);
      return update;
    },
    updateLeave: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        // construct data
        const update = {};
        const ignoreKeys = ['id'];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Shift.findOneAndUpdate({ _id: ObjectId(data.id) }, update, {
          new: true,
        });
      } catch (err) {
        error('mutation:updateLeave:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    acceptLeave: async (root, data, ctx) => {
      let ret;
      try {
        utils.checkUser(ctx);

        const leaveToken = await WorkToken.findById(data.leaveToken.id)
          .then(LeaveService.checkLeaveTokenType);

        const shiftId = ObjectId(data.id);
        const shift = await Shift.findById(shiftId);
        if (!shift) throw new ApolloError('Cannot find shift', 404);
        const organisation = await Organisation.findById(shift.organisationId);
        if (!organisation) throw new ApolloError(`Organization not found. id: ${shift.organisationId}`, 404);

        const nRemaindingLeaves = R.pipe(
          LeaveService.calNRemaindingLeaves,
          LeaveService.checkRemainingLeaves,
        )({ shift, organisation, leaveToken });

        const updatedDate = moment(leaveToken.createdAt).toISOString();

        const args = {};
        if (leaveToken.type === 'LEAVE_SICK') {
          args.leaveSickDays = nRemaindingLeaves;
          args.leaveSickUpdateDate = updatedDate;
        } else if (leaveToken.type === 'LEAVE_PERSONAL') {
          args.leavePersonalDays = nRemaindingLeaves;
          args.leavePersonalUpdateDate = updatedDate;
        } else if (leaveToken.type === 'LEAVE_SPECIAL') {
          args.leaveSpecialDays = nRemaindingLeaves;
          args.leaveSpecialUpdateDate = updatedDate;
        }

        // 1. update worktoken
        const updatedLeaveToken = await WorkToken.findOneAndUpdate(
          { _id: leaveToken.id },
          { confirmed: true, payNow: true },
          { new: true },
        );
        if (updatedLeaveToken && updatedLeaveToken.confirmed) {
          // 2. update shift's leave value
          ret = await Shift.findOneAndUpdate({ _id: shiftId }, args, {
            new: true,
          });

          // 3. send notification
          const [reference] = updatedLeaveToken.references;
          const leaveStart = moment(reference.leaveStart).format('DD-MM-YYYY');
          const leaveEnd = moment(reference.leaveEnd).format('DD-MM-YYYY');
          const leaveDuration = leaveStart + (leaveStart === leaveEnd ? '' : ` to ${leaveEnd}`);
          const title = 'Leave request has been approved';
          const body = `Your leave request for ${leaveDuration} has been approved by the employer`;
          const message = {
            senderId: shift.employerId,
            receiverId: shift.workerId,
            title,
            body,
            data: {
              title,
              body,
              jobId: shift.jobId,
              shiftId: shift.id,
            },
            fcm_options: {
              analyticsLabel: 'notification_individual_leave',
            },
          };
          notifications
            .send({
              userType: 'worker',
              id: shift.workerId,
              messageType: 'push',
              message,
            })
            .catch(e => Error('Error sending notification to worker', e));
        }
      } catch (err) {
        error('mutation:acceptLeave:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    rejectLeave: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const { leaveToken } = data;
        if (!LEAVE_TYPES.includes(leaveToken.type)) ctx.ctx.throw(400, 'Error - Invalid Leave Token');

        // 1. update worktoken
        const updatedLeaveToken = await WorkToken.findOneAndUpdate(
          { _id: leaveToken.id },
          { confirmed: false, payNow: false },
          { new: true },
        ).catch(e => error('Error updating worktoken', e));
        if (updatedLeaveToken && updatedLeaveToken.confirmed === false) {
          ret = await Shift.findById(ObjectId(data.id));
          const [reference] = updatedLeaveToken.references;
          const leaveStart = moment(reference.leaveStart).format('DD-MM-YYYY');
          const leaveEnd = moment(reference.leaveEnd).format('DD-MM-YYYY');
          const leaveDuration = leaveStart + (leaveStart === leaveEnd ? '' : ` to ${leaveEnd}`);
          const title = 'Leave request has been rejected';
          const body = `Your leave request for ${leaveDuration} has been rejected by the employer`;
          const shift = ret;
          const message = {
            senderId: shift.employerId,
            receiverId: shift.workerId,
            title,
            body,
            data: {
              title,
              body,
              jobId: shift.jobId,
              shiftId: shift.id,
            },
            fcm_options: {
              analyticsLabel: 'notification_individual_leave',
            },
          };
          notifications
            .send({
              userType: 'worker',
              id: shift.workerId,
              messageType: 'push',
              message,
            })
            .catch(e => Error('Error sending notification to worker', e));
        } else {
          error('Error updating worktoken');
        }
      } catch (err) {
        error('mutation:rejectLeave:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
  },

  Shift: {
    siblingShifts: async (shift) => {
      const results = await Shift.find({
        _id: {
          $in: shift.siblingShiftsIds,
        },
      }).catch(err => error('resolver:Shift:siblingShifts:', err));
      return results;
    },
    vacanciesLeft: async (shift) => {
      const vancantShifts = await Shift.find({
        $and: [
          {
            _id: {
              $in: shift.siblingShiftsIds,
            },
          },
          {
            workerAccepted: false,
          },
        ],
      });
      return vancantShifts.length;
    },
  },
};

module.exports = resolvers;
