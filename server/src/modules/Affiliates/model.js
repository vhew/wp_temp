const mongoose = require('mongoose');

const AffiliateSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    imageUrl: {
      type: String,
    },
    info: {
      type: String,
    },
  },
);

const Affiliate = mongoose.model('Affiliate', AffiliateSchema);

module.exports = {
  Affiliate,
};
