const error = require('debug')('ERROR');
const { ApolloError } = require('apollo-server-koa');
const { Employer } = require('../../../models/mongodb');
const utils = require('../../utils');
const { Affiliate } = require('./model');

const resolvers = {
  Query: {
    affiliates: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        ret = await Affiliate.find().catch(e => error('Error querying affiliates', e));
      } catch (e) {
        error('query:affiliates:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    affiliate: async (root, data, ctx) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      const { name } = data;

      let ret;
      try {
        ret = await Affiliate
          .findOne({ name: new RegExp(name, 'i') })
          .catch(e => error(`Error finding affiliate with name: ${name}`, e));
      } catch (e) {
        error('query:affiliate:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
  },

  Mutation: {
    createAffiliate: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (!(await utils._isAdmin(ctx.user))) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        const newAffiliate = new Affiliate(data);
        ret = await newAffiliate
          .save()
          .catch(e => error('Error creating affiliate', 'args', data, e));
      } catch (e) {
        error('mutation:createAffiliate:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    updateAffiliate: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (!(await utils._isAdmin(ctx.user))) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      //   construct data
      const { id } = data;
      const ignoreKeys = ['id'];
      const update = {};

      data.keys.forEach((key) => {
        if (!ignoreKeys.includes(key)) {
          update[key] = data[key];
        }
      });


      let ret;
      try {
        ret = await Affiliate
          .findByIdAndUpdate(id, update)
          .catch(e => error(`Error updating affiliate with id: ${id}`, 'args:', update, e));
      } catch (e) {
        error('mutation:updateAffiliate:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    addEmployerAffiliate: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (!(await utils._isAdmin(ctx.user))) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      const {
        affiliateId,
        employerId,
      } = data;

      let ret;
      try {
        ret = await Employer
          .findByIdAndUpdate(employerId, { $push: { affiliateIds: affiliateId } })
          .catch(e => error(`Error adding affiliate (id: ${affiliateId}) to employer (id: ${employerId})`, e));
      } catch (e) {
        error('mutation:addEmployerAffiliate:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    removeEmployerAffiliate: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (!(await utils._isAdmin(ctx.user))) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      const {
        affiliateId,
        employerId,
      } = data;

      let ret;
      try {
        ret = await Employer
          .findByIdAndUpdate(employerId, { $pull: { affiliateIds: affiliateId } })
          .catch(e => error(`Error removing affiliate (id: ${affiliateId}) from employer (id: ${employerId})`, e));
      } catch (e) {
        error('mutation:removeEmployerAffiliate:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
  },

  Job: {
    affiliates: async (job) => {
      let ret;
      try {
        if (Array.isArray(job.affiliateIds)) {
          const affiliates = job.affiliateIds && job.affiliateIds.map(
            id => Affiliate
              .findById(id)
              .catch(e => error(`Error finding affiliate with id: ${id}`, e)),
          );
          ret = await Promise.all(affiliates);
        } else {
          ret = [];
        }
      } catch (e) {
        error('resolver:Job:affiliates:', e);
        throw new ApolloError(e, 500);
      }
      return ret;
    },
  },

  Employer: {
    affiliates: async (employer) => {
      let ret;
      try {
        if (Array.isArray(employer.affiliateIds)) {
          const affiliates = employer.affiliateIds && employer.affiliateIds.map(
            id => Affiliate
              .findById(id)
              .catch(e => error(`Error finding affiliate with id: ${id}`, e)),
          );
          ret = await Promise.all(affiliates);
        } else {
          ret = [];
        }
      } catch (e) {
        error('resolver:Employer:affiliates:', e);
        throw new ApolloError(e, 500);
      }
      return ret;
    },
  },
};

module.exports = resolvers;
