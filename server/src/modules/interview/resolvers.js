const moment = require('moment');
const { ObjectId } = require('mongoose').Types;
const { ApolloError } = require('apollo-server-koa');
const error = require('debug')('ERROR');
const notiService = require('../../services/NotificationService');
const {
  Job,
  Shift,
  Worker,
  Employer,
} = require('./../../../models/mongodb/index');
const { CONFLICT_INTERVIEW_TIME } = require('../../utils/errorCodes');
const utils = require('../../utils');
const constants = require('../../utils/constants');
const Mail = require('../../../models/Mail');
const agenda = require('../../scheduler/agenda');

const resolvers = {
  Mutation: {
    async requestInterview(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const job = await Job.findById(data.jobId);
        const shift = await Shift.findById(data.shiftId);

        await Shift.findOneAndUpdate({
          _id: data.shiftId,
        }, {
          $push: {
            interviews: {
              workerId: data.workerId,
              name: data.name,
              requirements: data.requirements,
              times: data.times,
              confirmed: false,
              interviewLength: data.interviewLength,
              type: data.interviewType,
              address: data.address,
            },
          },
        });

        const notification = notiService.generateNotification({
          from: shift.employerId,
          to: data.workerId,
          userType: notiService.userType.WORKER,
          messageType: notiService.messageType.PUSH,
          title: 'Request',
          body: `Interview request from ${job.title}`,
          data,
        });
        notiService.send(notification);
        ret = 1;
      } catch (err) {
        error('mutation:requestInterview:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async confirmInterview(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const {
          startTime, endTime, shiftId, workerId, jobId,
        } = data;
        const job = await Job.findById(jobId, 'title');
        const shift = await Shift.findById(shiftId, 'employerId interviews');
        const worker = await Worker.findById(workerId, 'name');
        const employer = await Employer.findById(shift.employerId, 'zoomAuthCode zoomAccessToken zoomRefreshToken');
        const interviewToConfirm = shift.interviews.find(e => e.workerId === workerId);

        const interviewData = { startTime, endTime };

        const isBetweenConfirmedTimes = shift.interviews
          .filter(e => e.confirmed)
          .some(
            c => moment(startTime).isBetween(c.startTime, c.endTime, undefined, '[)')
              || moment(endTime).isBetween(c.startTime, c.endTime, undefined, '(]'),
          );

        if (isBetweenConfirmedTimes) {
          throw new ApolloError(CONFLICT_INTERVIEW_TIME.message, CONFLICT_INTERVIEW_TIME.code);
        }

        const zoomUrl = interviewToConfirm.type === 'ZOOM' ? await utils.getZoomInterviewUrl(employer, interviewData) : null;

        const updatedShift = await Shift.findOneAndUpdate(
          {
            _id: shiftId,
            interviews: { $elemMatch: { workerId, confirmed: false } },
          },
          {
            $set: {
              'interviews.$.startTime': startTime,
              'interviews.$.endTime': endTime,
              'interviews.$.confirmed': true,
              'interviews.$.link': zoomUrl,
            },
          },
          { new: true },
        );
        const confirmedInterview = updatedShift.interviews.find(e => e.workerId === workerId);

        // 🔔 notify
        const notification = {
          from: constants.authorizedAdminIds[0],
          to: shift.employerId,
          userType: notiService.userType.EMPLOYER,
          messageType: notiService.messageType.PUSH_EMAIL,
          title: 'Request Accepted',
          body: `${worker.name} accepted the interview request for ${job.title}.`,
          data: {
            jobId,
            shiftId,
          },
          mail: { to: employer, action: 'CONFIRM' },
        };
        const reminderNotificationToEmployer = {
          ...notification,
          title: 'Interview Reminder',
          body: `Interview for ${job.title} job will be on today`,
        };
        const reminderNotificationToWorker = {
          ...notification,
          to: ctx.user.id,
          title: 'Interview Reminder',
          body: `Interview for ${job.title} job will be on today`,
          messageType: notiService.messageType.PUSH,
        };

        notiService.send(notiService.generateNotification(notification));

        const reminderDate = moment(startTime).set('hours', 0).toDate();
        await agenda.schedule(reminderDate, 'notify', { notification: JSON.stringify(notiService.generateNotification(reminderNotificationToWorker)) });
        await agenda.schedule(reminderDate, 'notify', { notification: JSON.stringify(notiService.generateNotification(reminderNotificationToEmployer)) });

        ret = confirmedInterview;
      } catch (err) {
        error('mutation:confirmInterview:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async updateInterview(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const shift = await Shift.findById(data.shiftId);

        // construct data
        await Shift.updateOne({
          _id: data.shiftId,
          interviews: { $elemMatch: { workerId: data.workerId, confirmed: false } },
        }, {
          $set: {
            'interviews.$.requirements': data.requirements,
            'interviews.$.times': data.times,
            'interviews.$.address': data.address,
            'interviews.$.interviewLength': data.interviewLength,
          },
        });

        const notification = notiService.generateNotification({
          from: shift.employerId,
          to: data.workerId,
          userType: notiService.userType.WORKER,
          messageType: notiService.messageType.PUSH,
          title: 'Interview Edited',
          body: 'Interview schedule is edited by employer please check.',
          data: {
            jobId: data.jobId,
            shiftId: data.shiftId,
          },
        });
        notiService.send(notification);
        ret = 1;
      } catch (err) {
        error('mutation:updateInterview:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async withdrawInterview(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const job = await Job.findById(data.jobId);
        const shift = await Shift.findById(data.shiftId);
        const worker = await Worker.findById(data.workerId);

        // construct data
        await Shift.updateOne({
          _id: data.shiftId,
        }, {
          $pull: { interviews: { workerId: data.workerId } },
          // $set: {
          //   'interviews.$.requirements': data.requirements,
          //   'interviews.$.times': data.times,
          // },
        }, { new: true });
        if (data.from === 'worker') {
          const title = 'Interview Withdraw';
          const body = `${job.title} interview is withdraw from ${worker.name}`;
          notiService.sendToEmployersInOrganisation(
            new ObjectId(job.organisationId),
            {
              from: worker._id,
              title,
              body,
              userType: notiService.userType.EMPLOYER,
              messageType: notiService.messageType.PUSH_EMAIL,
              data: { jobId: shift.jobId, shiftId: data.shiftId },
              mail: { action: Mail.getActions().CONFIRM },
              analyticsLabel: 'notification_individual_interview',
            },
          );
        } else {
          const title = 'Interview Edited';
          const body = `${job.title} interview is withdraw by employer please check`;
          const message = {
            title,
            body,
            jobId: data.jobId,
            data: {
              senderId: shift.employerId,
              receiverId: data.workerId,
              title,
              body,
              jobId: data.jobId,
              shiftId: data.shiftId,
            },
          };
          notiService.send(
            {
              userType: 'worker',
              id: data.workerId,
              messageType: 'push',
              message,
            },
          );
        }
        ret = 1;
      } catch (err) {
        error('mutation:withdrawInterview:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
  },
};

module.exports = resolvers;
