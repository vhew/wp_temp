const mongoose = require('mongoose');

const UserPermissionSchema = new mongoose.Schema({
  resourceName: {
    type: String,
    required: true,
  },
  resourceId: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
  },
  permissions: {
    type: Array,
  },
}, {
  timestamps: true,
});

const UserPermission = mongoose.model('UserPermission', UserPermissionSchema);

module.exports = {
  UserPermission,
};
