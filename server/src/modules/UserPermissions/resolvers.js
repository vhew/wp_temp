const error = require('debug')('ERROR');
const {
  Employer,
} = require('../../../models/mongodb/index');

const {
  UserPermission,
} = require('./model');

const resolvers = {
  Mutation: {
    updateUserPermissions: async (root, data, ctx) => {
      const {
        resourceName,
        resourceId,
        userId,
        permissions,
      } = data;

      let ret;
      try {
        if (resourceName === 'Organisation') {
          await Employer.updateOne({
            _id: userId,
          }, {
            $addToSet: {
              organisationsIds: resourceId,
            },
          });
        }
        ret = await UserPermission.findOneAndUpdate({
          resourceName,
          resourceId,
          userId,
        }, {
          $set: {
            permissions,
          },
        }, {
          upsert: true,
          new: true,
        }).catch(err => error(err));
      } catch (e) {
        error('mutation:updateUserPermissions:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
  },

  Organisation: {
    permissions: async (organisation, { employerId }) => {
      const userPermission = await UserPermission.findOne({
        resourceName: 'Organisation',
        resourceId: organisation._id,
        userId: employerId,
      }).catch(err => error('resolver:Organisation:permissions:', err));

      if (userPermission == null) {
        return [];
      }

      const { permissions } = userPermission;
      return permissions;
    },
  },
};

module.exports = resolvers;
