const index = require('./../../models/mongodb/index');
const notification = require('./Notifications/model');

module.exports = {
  ...index,
  ...notification,
};
