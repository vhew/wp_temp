const mongoose = require('mongoose');

const MarketplaceSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    organisationId: {
      type: String,
      required: true,
    },
    imageUrl: {
      type: String,
    },
    description: {
      type: String,
    },
  },
);

const ExpectedMarketplaceSubscriberSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
  },
  email: {
    type: String,
  },
  identification: {
    type: String,
    required: true,
  },
  marketplaceId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Marketplace',
    required: true,
  },
  hasSubscribed: {
    type: Boolean,
  },
  workerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Worker',
  },
});

const Marketplace = mongoose.model('Marketplace', MarketplaceSchema);
const ExpectedMarketplaceSubscriber = mongoose.model('ExpectedMarketplaceSubscriber', ExpectedMarketplaceSubscriberSchema);

module.exports = {
  Marketplace,
  ExpectedMarketplaceSubscriber,
};
