const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const _ = require('lodash');
const {
  ApolloError, UserInputError,
} = require('apollo-server-koa');
const { ObjectId } = require('mongoose').Types;
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const {
  Job,
  Worker,
  Shift,
  Organisation,
} = require('../../../models/mongodb/index');
const {
  Marketplace,
  ExpectedMarketplaceSubscriber,
} = require('./model');
const utils = require('../../utils');

const notifications = require('../../services/NotificationService');


const resolvers = {
  Query: {
    marketplaces: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.organisationId,
        type: 'Organisation',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      let result;
      let publicMarketplace;
      try {
        result = await Marketplace.find({
          organisationId: data.organisationId,
        })
          .catch(err => error('query:marketplaces:', err));

        publicMarketplace = await Marketplace.find({
          organisationId: 'public-marketplace',
        }).catch(err => error('query:marketplaces:', err));
      } catch (e) {
        error('query:marketplaces:', e);
        ctx.ctx.throw(500, e);
      }

      return [...result, ...publicMarketplace];
    },
    marketplace: async (root, data, ctx) => {
      let result;
      try {
        result = await Marketplace.findById(data.id)
          .catch(err => error('query:marketplace:', err));
      } catch (e) {
        error('query:marketplace:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },
    expectedMarketplaceSubscribers: async (root, data, ctx) => {
      let result;
      try {
        result = await ExpectedMarketplaceSubscriber.find({
          marketplaceId: ObjectId(data.marketplaceId),
        }).catch(err => error('query:expectedMarketplaceSubscribers:', err));
      } catch (e) {
        error('query:expectedMarketplaceSubscribers:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },
  },

  Mutation: {
    createMarketplace: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.organisationId,
        type: 'Organisation',
      };
      if (data.organisationId != null && await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation createMarketplace as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        const organisation = await Organisation.findById(data.organisationId);
        if (!organisation) throw new ApolloError('Cannot find organisation', 404);
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const marketplace = new Marketplace(update);
        ret = await marketplace.save()
          .catch(err => error('mutation:createMarketplace:', err));
      } catch (err) {
        error('mutation:createMarketplace:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    createExpectedMarketplaceSubscriber: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const expectedMarketplaceSubscriber = new ExpectedMarketplaceSubscriber(update);
        ret = await expectedMarketplaceSubscriber.save()
          .catch(err => error(err));

        if (ret == null) {
          throw new Error();
        }
        const ret2 = await Worker.findOneAndUpdate({
          identification: data.identification,
        }, {
          $addToSet: {
            marketplacesIds: data.marketplaceId,
          },
        }).catch(err => error('mutation:createExpectedMarketplaceSubscriber:', err));

        if (ret2 == null) {
          notifications.sendToUnregisteredPerson({
            notificationType: 'EMAIL',
            message: {
              subject: 'Marketplace Invite',
              html: 'You have been invited to apply jobs in the private marketplace.',
            },
            coordinates: data.email,
          });
        }
      } catch (e) {
        error('mutation:createExpectedMarketplaceSubscriber:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    updateMarketplaceSubscriberList: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        info(data);
        if (utils.hasDuplicates(data.subscriberList, 'phone')) {
          throw new UserInputError('Duplicated entries');
        }
        const marketplaceId = new ObjectId(data.marketplaceId);

        const subList = Array.from(data.subscriberList);
        const existingSubList = await ExpectedMarketplaceSubscriber
          .find({ marketplaceId })
          .then(arr => arr.map(e => ({
            name: e.name, identification: e.identification, phone: e.phone, email: e.email,
          })));
        const isEqualByPhone = (a, b) => a.phone === b.phone;

        const newSubs = _.differenceWith(subList, existingSubList, isEqualByPhone);
        const deleteSubs = _.differenceWith(existingSubList, subList, isEqualByPhone);
        const oldSubs = _.intersectionWith(subList, existingSubList, isEqualByPhone);
        const updateSubs = _.differenceWith(oldSubs, existingSubList, _.isEqual);

        const [createResult, deleteResult, updateResult] = await Promise.all([
          utils.createMarketplaceSubscribers(marketplaceId, newSubs),
          utils.deleteMarketplaceSubscribers(marketplaceId, deleteSubs),
          utils.updateMarketplaceSubscribers(marketplaceId, updateSubs),
        ]);

        ret = {
          createResult,
          updateResult,
          deleteResult,
        };
      } catch (e) {
        error('mutation:updateMarketplaceSubscriberList:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },
    marketplaceQuickHack: async (root, data) => {
      const ret = await Shift.updateMany({
        workerId: data.workerId,
      }, {
        $unset: {
          workerId: 1,
        },
        $set: {
          workerAccepted: false,
        },
      }, {
        multi: true,
      });
      const ret2 = await Shift.updateMany({
        candidatesIds: data.workerId,
      }, {
        $set: {
          candidatesIds: [],
        },
      });
      const ret3 = await Worker.updateMany({
        _id: data.workerId,
      }, {
        $set: {
          jobsAppliedIds: [],
        },
        $unset: {
          profileUrl: 1,
          youtubeUrl: 1,
          description: 1,
        },
      }, {
        multi: true,
      });
      return (!!ret && !!ret2 && !!ret3);
    },

  },

  Organisation: {
    marketplaces: async (organisation) => {
      const ret = await Marketplace.find({
        organisationId: organisation._id,
      }).catch(err => error('resolver:Organisation:marketplaces:', err));
      return ret;
    },
  },
  Marketplace: {
    jobs: async (marketplace) => {
      const jobs = await Job.find({
        marketplaceId: marketplace._id,
        shiftsFilled: false,
      }).catch(err => error('resolver:Marketplace:jobs:', err));
      return jobs;
    },
  },
  Job: {
    marketplace: async (job) => {
      const result = await Marketplace.findById(job.marketplaceId)
        .catch(err => error('resolver:Job:marketplace:', err));
      return result;
    },
  },
  Worker: {
    marketplaces: async (worker) => {
      const results = await Marketplace.find({
        _id: {
          $in: worker.marketplacesIds,
        },
      }).catch(err => error('resolver:Worker:marketplaces:', err));
      return results;
    },
  },
  ExpectedMarketplaceSubscriber: {
    marketplace: async (expectedMarketplaceSubscriber) => {
      const marketplace = await Marketplace.find({
        _id: expectedMarketplaceSubscriber.marketplaceId,
      }).catch(err => error('resolver:ExpectedMarketplaceSubscriber:marketplace:', err));
      return marketplace;
    },
    worker: async (expectedMarketplaceSubscriber) => {
      const worker = await Worker.find({
        _id: expectedMarketplaceSubscriber.workerId,
      }).catch(err => error('resolver:ExpectedMarketplaceSubscriber:worker:', err));
      return worker;
    },
  },
};

module.exports = resolvers;
