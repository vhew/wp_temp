const fs = require('fs');
const { GraphQLModule } = require('@graphql-modules/core');
const CoreModule = require('../../cores/index');

const typeDefs = fs.readFileSync('./src/modules/JobContracts/types.gql', 'utf8');
const resolvers = require('./resolvers');

const JobContractModule = new GraphQLModule({
  imports: [
    CoreModule,
  ],
  typeDefs,
  resolvers,
});

module.exports = JobContractModule;
