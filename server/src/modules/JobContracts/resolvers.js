const error = require('debug')('ERROR');
const { ApolloError } = require('apollo-server-koa');
const {
  Job,
} = require('../../../models/mongodb/index');
const {
  JobContract,
} = require('./model');

const resolvers = {
  Query: {
  },

  Mutation: {
    createJobContract: async (root, data, ctx) => {
      let ret;
      try {
        const job = await Job.findById(data.jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const jobContract = await new JobContract(update)
          .save()
          .catch(err => error(err));

        if (jobContract) {
          await Job
            .findOneAndUpdate({
              _id: data.jobId,
            }, {
              $set: {
                contractId: jobContract._id,
              },
            }).catch(e => error(e));

          ret = jobContract;
        } else {
          throw new ApolloError('Error creating job contract', 500, { data });
        }
      } catch (e) {
        error('mutation:createJobContract:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    // updateJobContract: async (root, data, ctx) => {
    //   const { id, signingDateWorker, contract } = data;

    //   const ret = await JobContract.findOneAndUpdate(
    //     { _id: id },
    //     { $set: { signingDateWorker, contract } },
    //     { new: true },
    //   ).catch(err => console.err(err));

    //   return ret;
    // },
  },

  Job: {
    // TODO: create index
    // 1. jobId, workerId
    // 2. jobId
    jobContract: async (job, { workerId }) => {
      let result;
      try {
        const signedContract = await JobContract.findOne({
          jobId: job.id,
          workerId,
        });
        if (signedContract) return signedContract;

        const unsignedContract = await JobContract.findOne({
          jobId: job.id,
          workerId: { $exists: false },
        });
        result = unsignedContract;
      } catch (err) {
        error('resolver:Job:jobContract:', err);
      }
      return result;
    },
    jobContracts: async (job) => {
      let contracts;
      try {
        contracts = await JobContract.find({ jobId: job.id });
      } catch (err) {
        error('resolver:Job:jobContracts:', err);
      }
      return contracts;
    },
  },

  Shift: {
    jobContract: async ({ workerId, jobId }) => {
      let result;
      try {
        if (!workerId || !jobId) return null;
        const jobContract = await JobContract.findOne({
          jobId,
          workerId,
        });
        result = jobContract;
      } catch (err) {
        error('resolver:Shift:jobContract:', err);
      }
      return result;
    },
  },
};

module.exports = resolvers;
