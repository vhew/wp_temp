const mongoose = require('mongoose');

const JobContractSchema = new mongoose.Schema(
  {
    jobId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Job',
    },
    jobStartDate: {
      type: Date,
    },
    renumerationValue: {
      type: String,
    },
    employerId: {
      type: String,
    },
    workerId: {
      type: String,
    },
    signingDateEmployer: {
      type: Date,
    },
    signingDateWorker: {
      type: Date,
    },
    contract: {
      type: String,
    },
  },
);

JobContractSchema.index({ jobId: 1, workerId: 1 });

const JobContract = mongoose.model('JobContract', JobContractSchema);


module.exports = {
  JobContract,
};
