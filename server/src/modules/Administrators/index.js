const fs = require('fs');
const { GraphQLModule } = require('@graphql-modules/core');
const CoreModule = require('../../cores/index');

const typeDefs = fs.readFileSync('./src/modules/Administrators/types.gql', 'utf8');
const resolvers = require('./resolvers');

const AdministratorModule = new GraphQLModule({
  imports: [
    CoreModule,
  ],
  typeDefs,
  resolvers,
});

module.exports = AdministratorModule;
