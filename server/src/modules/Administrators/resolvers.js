const { AuthenticationError } = require('apollo-server-koa');
const {
  Employer,
} = require('../../../models/mongodb/index');

const resolvers = {
  Query: {
    employersAdmin: async (root, data, ctx) => {
      if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
      const employers = await Employer.find();
      return employers;
    },
  },
};

module.exports = resolvers;
