const error = require('debug')('ERROR');
const {
  JobApplication,
} = require('./model');

const resolvers = {
  Query: {
  },

  Worker: {
    jobApplications: async (worker) => {
      const ret = await JobApplication.find({
        workerId: worker.id,
      }).catch(err => error('mutation:jobApplications:', err));

      return ret;
    },
  },
};

module.exports = resolvers;
