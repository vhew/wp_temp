const mongoose = require('mongoose');

const JobApplicationSchema = new mongoose.Schema(
  {
    shiftId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Shift',
      required: true,
    },
    workerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Worker',
      required: true,
    },
    appliedDate: {
      type: Date,
    },
    status: {
      type: String,
    },
  },
);

const JobApplication = mongoose.model('JobRole', JobApplicationSchema);

module.exports = {
  JobApplication,
};
