const moment = require('moment');
const {
  ApolloError, AuthenticationError,
} = require('apollo-server-koa');
const error = require('debug')('ERROR');
const WorkerDetailService = require('../../services/WorkerDetailService');
const AccessControlService = require('../../services/AccessControlService');
const ERROR_CODES = require('../../utils/errorCodes');
const helpers = require('./helpers');
const microservices = require('./microservices');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const {
  Worker,
  AlienWorker,
  Job,
  Shift,
  WorkToken,
  WorkTokenValue,
} = require('../../../models/mongodb/index');
const {
  PaymentMethod,
  WorkerPaymentMethod,
} = require('../payments/model');
const utils = require('../../utils');
const { Receipt } = require('../Products/model');

const resolvers = {
  Query: {
    estimatedPayBalance: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      let ret;
      try {
        const payCycle = await helpers.getPayCycle({
          jobId: data.jobId,
        });

        const virtualWorkTokens = await helpers.createVirtualWorkTokens({
          jobId: data.jobId,
          workerId: data.workerId,
          payCycle: {
            payCycleStartTime: payCycle.payCycleStartTime,
            payCycleEndTime: moment.utc(),
          },
        });

        if (virtualWorkTokens.length === 0) {
          return {
            totalAmount: 0,
            workDays: -1,
            feePerDay: 0,
          };
        }

        const totalAmount = await helpers.calculateTotalAmountOfWorkTokens({
          jobId: data.jobId,
          payCycle,
          deductionAmount: 0,
          workTokens: virtualWorkTokens,
        });

        // const feePerDay = await helpers.getFee({
        //   jobId: data.jobId,
        //   amountPerWorkToken: (totalAmount / virtualWorkTokens.length),
        // });

        const worker = await Worker.findById(data.workerId);
        let { workerPaymentMethodId } = data;
        if (workerPaymentMethodId == null) {
          workerPaymentMethodId = worker.defaultWorkerPaymentMethodId;
        }
        const workerPaymentMethod = await WorkerPaymentMethod
          .findById(workerPaymentMethodId);

        let feePerDay = await microservices.feeService({
          jobId: data.jobId,
          amount: totalAmount / virtualWorkTokens.length,
          numberRequested: virtualWorkTokens.length,
          workerPaymentMethodId,
        });

        const paymentMethod = await PaymentMethod
          .findById(workerPaymentMethod.paymentMethodId);
        const { transactionFeeRule } = paymentMethod;
        let fixedFee = 0;
        if (transactionFeeRule != null) {
          const result = transactionFeeRule
            .find(e => e.min <= totalAmount && totalAmount < e.max);

          if (result && result.fixed > 0) {
            // eslint-disable-next-line max-len
            feePerDay = ((feePerDay * virtualWorkTokens.length) - result.fixed) / virtualWorkTokens.length;
            fixedFee = result.fixed;
          }
        }
        const lastShiftFinished = await helpers.getLastShiftFinished({
          workTokens: virtualWorkTokens,
        });
        ret = {
          totalAmount,
          workDays: virtualWorkTokens.length,
          lastShiftFinished,
          feePerDay,
          fixedFee,
        };
      } catch (e) {
        error('query:estimatedPayBalance:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    payBalance: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      let ret;
      try {
        const { workerId, jobId } = data;
        const payCycle = await helpers.getPayCycle({
          jobId,
        });
        const { payCycleStartTime } = payCycle;

        const shifts = await Shift.find({
          jobId,
          workerId,
        });

        const workTokens = await WorkToken.aggregate([
          {
            $match: {
              createdAt: {
                $gte: new Date(payCycleStartTime),
              },
              shiftId: {
                $in: shifts.map(shift => shift._id),
              },
              payNow: false,
              confirmed: true,
              workerId,
              checkoutTime: {
                $exists: true,
              },
            },
          },
          {
            $lookup: {
              from: 'fiattokens',
              localField: '_id',
              foreignField: 'workTokenId',
              as: 'fiatTokens',
            },
          },
          {
            $match: {
              $expr: {
                $eq: [
                  {
                    $size: '$fiatTokens',
                  },
                  0,
                ],
              },
            },
          },
        ]).catch(err => error(err));

        if (workTokens.length === 0) {
          const [lastWorkToken] = await WorkToken.find({
            shiftId: {
              $in: shifts.map(shift => shift._id),
            },
            workerId,
          }).sort({
            checkoutTime: -1,
          }).limit(1);

          const lastShiftFinished = lastWorkToken != null
            ? new Date(lastWorkToken.checkoutTime) : null;
          ret = {
            totalAmount: 0,
            workDays: -1,
            feePerDay: 0,
            lastShiftFinished,
          };
        }

        const totalAmount = await helpers.calculateTotalAmountOfWorkTokens({
          jobId,
          payCycle,
          deductionAmount: 0,
          workTokens,
        });

        const worker = await Worker.findById(data.workerId);
        let { workerPaymentMethodId } = data;
        if (workerPaymentMethodId == null) {
          workerPaymentMethodId = worker.defaultWorkerPaymentMethodId;
        }
        const workerPaymentMethod = await WorkerPaymentMethod
          .findById(workerPaymentMethodId);

        let feePerDay = await microservices.feeService({
          jobId: data.jobId,
          amount: totalAmount / workTokens.length,
          numberRequested: workTokens.length,
          workerPaymentMethodId,
        });

        const paymentMethod = await PaymentMethod
          .findById(workerPaymentMethod.paymentMethodId);
        const { transactionFeeRule } = paymentMethod;
        let fixedFee = 0;
        if (transactionFeeRule) {
          const { fixed } = transactionFeeRule
            .find(e => e.min <= totalAmount && totalAmount < e.max);
          if (fixed > 0) {
            feePerDay = ((feePerDay * workTokens.length) - fixed) / workTokens.length;
            fixedFee = fixed;
          }
        }
        const lastShiftFinished = await helpers.getLastShiftFinished({
          workTokens,
        });
        ret = {
          totalAmount,
          workDays: workTokens.length,
          lastShiftFinished,
          feePerDay,
          fixedFee,
        };
      } catch (e) {
        error('query:payBalance:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    pendingTransaction: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      let ret;
      try {
        const payCycle = await helpers.getPayCycle({
          jobId: data.jobId,
        });
        const { payCycleStartTime } = payCycle;
        const job = await Job.findById(data.jobId);
        let transactions = await WorkToken.aggregate([
          {
            $match: {
              createdAt: {
                $gte: new Date(payCycleStartTime),
              },
              shiftId: {
                $in: job.shiftsIds,
              },
              workerId: data.workerId,
              type: 'STANDARD',
            },
          },
          {
            $lookup: {
              from: 'fiattokens',
              localField: '_id',
              foreignField: 'workTokenId',
              as: 'fiatTokens',
            },
          },
          {
            $lookup: {
              from: 'discounttokens',
              let: { workerId: data.workerId },
              pipeline: [
                {
                  $match:
                    {
                      $expr:
                       {
                         $and:
                          [
                            { $eq: ['$workerId', '$$workerId'] },
                            { $eq: ['$status', 'REQUESTED'] },
                          ],
                       },
                    },
                },
              ],
              as: 'discountTokens',
            },
          },
          {
            $match: {
              $expr: {
                $eq: [
                  {
                    $size: '$fiatTokens',
                  },
                  0,
                ],
              },
            },
          },
          {
            $lookup: {
              from: 'worktokenvalues',
              localField: '_id',
              foreignField: 'workTokenId',
              as: 'workTokenValue',
            },
          },
          {
            $project: {
              amount: {
                $sum: '$workTokenValue.amount',
              },
              fee: {
                $sum: '$workTokenValue.fee',
              },
              requestedAt: '$createdAt',
              currency: {
                $arrayElemAt: ['$workTokenValue.currency', 0],
              },
              method: {
                $arrayElemAt: ['$workTokenValue.method', 0],
              },
              coordinates: {
                $arrayElemAt: ['$workTokenValue.coordinates', 0],
              },
              discountTokens: '$discountTokens',
            },
          },
          {
            $group: {
              _id: '$coordinates',
              amount: {
                $sum: '$amount',
              },
              fee: {
                $sum: '$fee',
              },
              requestedAt: {
                $last: '$requestedAt',
              },
              currency: {
                $first: '$currency',
              },
              method: {
                $first: '$method',
              },
              coordinates: {
                $first: '$coordinates',
              },
              discountTokens: {
                $first: '$discountTokens',
              },
            },
          },
          {
            $addFields: {
              isPayroll: {
                $cond: [
                  {
                    $and: [
                      {
                        $eq: [
                          '$fee',
                          0,
                        ],
                      },
                      {
                        $eq: [
                          {
                            $size: '$discountTokens',
                          },
                          0,
                        ],
                      },
                    ],
                  },
                  true,
                  false,
                ],
              },
            },
          },
        ]);
        transactions = transactions.filter(x => x.coordinates !== '+454213209153');
        ret = transactions[0];
      } catch (e) {
        error('query:pendingTransaction:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },


  },

  Mutation: {
    runTimeSheetService: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      await utils
        .checkAccess(ctx, null, 'WRITE_TIMESHEET')
        .catch((err) => { throw err; });

      const {
        jobId,
        workerId,
      } = data;
      let ret;
      try {
        const job = await Job.findById(data.jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        let worker = await Worker.findById(data.workerId);
        if (worker == null) {
          worker = await AlienWorker.findById(data.workerId);
          if (worker == null) throw new ApolloError('Cannot find worker', 404);
        }
        const payCycle = await helpers.getPayCycle({
          jobId,
          forPreviousPayCycle: data.forPreviousPayCycle,
        });
        const usingTimesheetingFeatureLevel = await helpers
          .getOrganisationUsingTimesheetingFeatureLevel({ jobId });

        let requestedWorkTokens = [];
        if (usingTimesheetingFeatureLevel === 'FULL') {
          requestedWorkTokens = await helpers.getTimesheetingWorkTokens({
            jobId,
            workerId,
            payCycle,
          });
          await helpers.setWorkTokensConfirmedNull({
            workTokensIds: requestedWorkTokens.map(e => e._id),
          });
        } else {
          const virtualWorkTokens = await helpers.createVirtualWorkTokens({
            jobId,
            workerId,
            payCycle,
          });

          requestedWorkTokens = await helpers.createWorkTokens({
            virtualWorkTokens,
            payCycle,
          });
        }

        if (requestedWorkTokens.length === 0) return [];

        const deductionAmount = await helpers.calculateDeductionAmount({
          jobId,
          workerId,
          payCycle,
        });

        const {
          coordinates,
          currency,
          paymentMethod,
        } = await WorkerDetailService.getDefaultWorkerPaymentMethod({
          workerId,
        }).catch(async (err) => {
          error(err);
          await helpers.deleteWorkTokens({
            workTokens: requestedWorkTokens,
          });
        });

        const createdWorkTokenValues = await helpers.createWorkTokenValues({
          jobId,
          payCycle,
          currency,
          coordinates,
          method: paymentMethod,
          workTokens: requestedWorkTokens,
          deductionAmount,
          usingTimesheetingFeatureLevel,
        });

        await helpers.createDeductionWorkTokensAndWorkTokenValues({
          jobId,
          workerId,
          payCycle,
          currency,
          paymentMethod,
        });

        if (createdWorkTokenValues == null
          || requestedWorkTokens.length !== createdWorkTokenValues.length) {
          await helpers.deleteWorkTokens({
            workTokens: requestedWorkTokens,
          });
          throw new Error('WorkTokenValues are not created');
        }
        ret = createdWorkTokenValues;
      } catch (err) {
        error('mutation:runTimeSheetService:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },

    requestPay: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      const { jobId, numShiftsPay } = data;
      const workerId = ctx.user.id;

      const payCycle = await helpers.getPayCycle({
        jobId,
        forPreviousPayCycle: data.forPreviousPayCycle,
      });

      const usingTimesheetingFeatureLevel = await helpers
        .getOrganisationUsingTimesheetingFeatureLevel({ jobId });

      let requestedWorkTokens;
      if (usingTimesheetingFeatureLevel === 'FULL') {
        const workTokens = await helpers.getTimesheetingWorkTokens({
          jobId,
          workerId,
          payCycle,
        });
        // return workTokens;
        requestedWorkTokens = workTokens.slice(0, numShiftsPay);
        await helpers.setWorkTokensConfirmedNull({
          workTokensIds: requestedWorkTokens.map(e => e._id),
        });
      } else {
        const virtualWorkTokens = await helpers.createVirtualWorkTokens({
          jobId,
          workerId,
          payCycle,
        });

        if (numShiftsPay > virtualWorkTokens.length) {
          const { NOT_ENOUGH_SHIFT_FOR_REQUEST_PAY } = ERROR_CODES;
          const { message, code } = NOT_ENOUGH_SHIFT_FOR_REQUEST_PAY;
          throw new ApolloError(message, code);
        }

        const requestedVirtualWorkTokens = virtualWorkTokens.slice(0, numShiftsPay);
        requestedWorkTokens = await helpers.createWorkTokens({
          virtualWorkTokens: requestedVirtualWorkTokens,
          payCycle,
        });
      }

      const deductionAmount = await helpers.calculateDeductionAmount({
        jobId,
        workerId: ctx.user.id,
        payCycle,
      });

      const {
        coordinates,
        currency,
        paymentMethod,
      } = await WorkerDetailService
        .getPaymentMethod({ workerPaymentMethodId: data.workerPaymentMethodId });

      const createdWorkTokenValues = await helpers.createWorkTokenValues({
        jobId,
        payCycle,
        currency,
        coordinates,
        method: paymentMethod,
        workTokens: requestedWorkTokens,
        deductionAmount,
        isKhuPay: true,
      });

      if (createdWorkTokenValues == null
        || requestedWorkTokens.length !== createdWorkTokenValues.length) {
        await helpers.deleteWorkTokens({
          workTokens: requestedWorkTokens,
        });
        throw new Error('WorkTokenValues are not created');
      }
      return createdWorkTokenValues;
    },

    requestPay2point0: async (root, data) => {
      /*
        High level algorithm
        1. Identify WorkTokens
        2. Get Amount Service
        3. Get Fee Service
        4. WRITE relevant docs into database
      */

      const {
        jobId,
        workerId,
        numberRequested,
        workerPaymentMethodId,
        discountTokenId,
      } = data;
      const payCycle = await helpers.getPayCycle({
        jobId,
        forPreviousPayCycle: false,
      });
      let ret;
      try {
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);
        const job = await Job.findById(data.jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        // 1. Identify WorkTokens START
        let workTokens = [];
        const unpaidWorkTokens = await microservices
          .unpaidWorkTokenService({ jobId, payCycle });

        const usingTimesheetingFeatureLevel = await helpers
          .getOrganisationUsingTimesheetingFeatureLevel({ jobId });

        if (unpaidWorkTokens.length === 0) {
          if (usingTimesheetingFeatureLevel !== 'FULL') {
            workTokens = await microservices.createWorkTokensService({
              jobId,
              workerId,
              numberRequested,
              payCycle,
            });
          }
          if (usingTimesheetingFeatureLevel === 'FULL') {
            throw new Error('Not enough shifts to satisfy request');
          }
        } else {
        // eslint-disable-next-line no-lonely-if
          if (usingTimesheetingFeatureLevel !== 'FULL') {
            workTokens = unpaidWorkTokens.slice(0, numberRequested);

            await WorkToken.updateMany({
              _id: {
                $in: workTokens.map(workToken => workToken._id),
              },
            }, {
              $set: {
                payNow: true,
              },
            });
            if (numberRequested > unpaidWorkTokens.length) {
              const additionalWorkTokensNeedToCreated = numberRequested - workTokens.length;
              const newWorkTokens = await microservices.createWorkTokensService({
                jobId,
                workerId,
                shiftId: data.shiftId,
                numberRequested: additionalWorkTokensNeedToCreated,
                payCycle,
              });
              workTokens = [...workTokens, ...newWorkTokens];
            }
          }

          if (usingTimesheetingFeatureLevel === 'FULL') {
            workTokens = unpaidWorkTokens.slice(0, numberRequested);
            await WorkToken.updateMany({
              _id: {
                $in: workTokens.map(workToken => workToken._id),
              },
            }, {
              $set: {
                confirmed: null,
              },
            });
          }
        }
        // 1. Identify WorkTokens END

        // 2. Get Amount Service START
        const amount = await microservices.workTokenAmountService({
          jobId,
          payCycle,
        });
        // 2. Get Amount Service END

        // 3. Get Fee Service START
        const fee = await microservices.feeService({
          jobId,
          amount,
          workerPaymentMethodId,
          discountTokenId,
          numberRequested,
        });
        // 3. Get Fee Service END

        // 4. WRITE relevant docs into database START
        const {
          coordinates,
          currency,
          paymentMethod,
        } = await WorkerDetailService.getPaymentMethod({ workerPaymentMethodId });

        const workTokenValues = await helpers.createWorkTokenValues2point0({
          workTokens,
          amount,
          fee,
          currency,
          method: paymentMethod,
          coordinates,
        });
        ret = workTokenValues;
      // 4. WRITE relevant docs into database END
      } catch (err) {
        error('mutation:requestPay2point0:', err);
        throw ErrorHandlingService.getError(err);
      }

      return ret;
    },

    addExtraWorkingDay: async (root, data, ctx) => {
      if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
      /*
        High level algorithm
        1. Identify WorkTokens
        2. Get Amount Service
        3. Get Fee Service
        4. WRITE relevant docs into database
      */
      let ret;
      try {
        const {
          jobId,
          shiftId,
          workerId,
          requestedDay,
        } = data;
        const payCycle = await helpers.getPayCycle({
          jobId,
          forPreviousPayCycle: true,
        });
        const job = await Job.findById(data.jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);
        const workerPaymentMethodId = worker.defaultWorkerPaymentMethodId;
        const usingTimesheetingFeatureLevel = await helpers
          .getOrganisationUsingTimesheetingFeatureLevel({ jobId });
        const checkinTime = moment.utc(requestedDay)
          .set('hour', moment.utc(shift.startTime).get('hour'))
          .set('minute', moment.utc(shift.startTime).get('minute'));

        const checkoutTime = moment.utc(requestedDay)
          .set('hour', moment.utc(shift.endTime).get('hour'))
          .set('minute', moment.utc(shift.endTime).get('minute'));

        // 1. Identify WorkTokens START
        const workToken = new WorkToken({
          shiftId,
          workerId,
          checkinTime,
          checkoutTime,
          type: 'STANDARD',
          payNow: false,
          confirmed: null,
        });
        const workTokens = [];
        ret = await workToken.save();
        workTokens.push(ret);
        // 1. Identify WorkTokens END

        // 2. Get Amount Service START
        const amount = await microservices.workTokenAmountService({
          jobId,
          payCycle,
          usingTimesheetingFeatureLevel,
        });
        // if (moment(workToken.checkinTime).format('ddd') === 'Fri') {
        //   amount *= 3;
        // }
        // 2. Get Amount Service END

        // 3. WRITE relevant docs into database START
        const {
          coordinates,
          currency,
          paymentMethod,
        } = await WorkerDetailService.getPaymentMethod({ workerPaymentMethodId });

        await helpers.createWorkTokenValues2point0({
          workTokens,
          amount,
          fee: 0,
          currency,
          method: paymentMethod,
          coordinates,
        });
      // 3. WRITE relevant docs into database END
      } catch (err) {
        error('mutation:addExtraWorkingDay:', err);
        throw ErrorHandlingService.getError(err);
      }

      return ret;
    },

    cancelDisbursement: async (
      root,
      data,
      ctx,
    ) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      // delete WorkTokens & WorkTokenValues if employer has not taken any actions

      let ret;
      try {
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);
        const job = await Job.findById(data.jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        const usingTimesheetingFeatureLevel = await helpers
          .getOrganisationUsingTimesheetingFeatureLevel({ jobId: data.jobId });

        if (usingTimesheetingFeatureLevel !== 'FULL') {
          const workTokens = await WorkToken.find({
            $and: [
              {
                shiftId: {
                  $in: job.shiftsIds,
                },
              },
              {
                workerId: data.workerId,
              },
              {
                confirmed: null,
              },
            ],
          }).catch(err => error(err));

          await WorkToken.deleteMany({
            $and: [
              {
                shiftId: {
                  $in: job.shiftsIds,
                },
              },
              {
                workerId: data.workerId,
              },
              {
                confirmed: null,
              },
            ],
          }).catch(err => error(err));

          ret = await WorkTokenValue.deleteMany({
            workTokenId: {
              $in: workTokens.map(workToken => workToken._id),
            },
          }).catch(err => error(err));

          if (ret.n > 0) {
            return ret.n;
          }
        }

        // after employer confirmed or using automated timesheeting feature
        const payCycle = await helpers.getPayCycle({
          jobId: data.jobId,
        });
        const { payCycleStartTime } = payCycle;

        const confirmedWorkTokens = await WorkToken.aggregate([
          {
            $match: {
              createdAt: {
                $gte: new Date(payCycleStartTime),
              },
              shiftId: {
                $in: job.shiftsIds,
              },
              workerId: data.workerId,
            },
          },
          {
            $lookup: {
              from: 'fiattokens',
              localField: '_id',
              foreignField: 'workTokenId',
              as: 'fiatTokens',
            },
          },
          {
            $match: {
              $expr: {
                $eq: [
                  {
                    $size: '$fiatTokens',
                  },
                  0,
                ],
              },
            },
          },
          {
            $project: {
              _id: 1,
            },
          },
        ]);

        ret = await WorkToken.updateMany({
          _id: {
            $in: confirmedWorkTokens.map(e => e._id),
          },
        }, {
          $set: {
            payNow: false,
            confirmed: true,
          },
        }).catch(err => error(err));
        await WorkTokenValue.deleteMany({
          workTokenId: {
            $in: confirmedWorkTokens.map(workToken => workToken._id),
          },
        }).catch(err => error(err));
      } catch (err) {
        error('mutation:cancelDisbursement:', err);
        throw ErrorHandlingService.getError(err);
      }

      return ret.nModified;
    },


    async addBonus(
      root,
      data,
      ctx,
    ) {
      const shift = await Shift.findById(data.shiftId);
      const { jobId } = shift;
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: jobId,
        type: 'Job',
      };
      if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      const {
        shiftId,
        workerId,
        amount,
        forPreviousPayCycle,
        receiptId,
      } = data;
      let ret;
      try {
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        // const worker = await Worker.findById(data.workerId);
        // if (worker == null) throw new ApolloError('Cannot find worker', 404);

        const payCycle = await helpers.getPayCycle({
          jobId,
          forPreviousPayCycle,
        });
        const { payCycleEndTime, payCycleStartTime } = payCycle;
        payCycleEndTime.subtract(1, 'days');

        const {
          paymentMethod,
          coordinates,
          currency,
        } = await WorkerDetailService.getDefaultWorkerPaymentMethod({
          workerId,
        });

        const existingBonusWorkToken = await WorkToken.find({
          checkinTime: payCycleStartTime,
          checkoutTime: payCycleEndTime,
          type: 'BONUS',
          workerId,
        });
        if (existingBonusWorkToken.length > 0) {
          const { DUPLICATE_BONUS } = ERROR_CODES;
          const { message, code } = DUPLICATE_BONUS;
          throw new ApolloError(message, code);
        }

        const workToken = new WorkToken({
          shiftId,
          checkinTime: payCycleStartTime,
          checkoutTime: payCycleEndTime,
          type: 'BONUS',
          workerId: data.workerId,
          confirmed: true,
          payNow: true,
        });
        const newWorkToken = await workToken.save();
        const workTokenValue = new WorkTokenValue({
          expectedWorkTokenShiftId: data.shiftId,
          expectedWorkTokenWorkerId: data.workerId,
          expectedWorkTokenType: 'BONUS',
          expectedWorkTokenCheckinTime: payCycleStartTime,
          expectedWorkTokenCheckoutTime: payCycleEndTime,
          workTokenId: newWorkToken._id,
          confirmRequest: true,
          amount,
          fee: 0,
          currency,
          method: paymentMethod,
          coordinates,
          notes: data.notes,
        });
        await workTokenValue.save();
        if (receiptId != null) {
          const receipt = await Receipt.findById(receiptId);
          ret = await Receipt.findOneAndUpdate(
            {
              _id: receiptId,
            },
            {
              $set: {
                complete: true,
                paidLevel: receipt.paidLevel + 1,
              },
            },
          );
        }
        ret = newWorkToken;
      } catch (err) {
        error('mutation:addBonus:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },
};

module.exports = resolvers;
