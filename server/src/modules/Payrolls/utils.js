const moment = require('moment');
const error = require('debug')('ERROR');

const utils = {
  _getPayCycleForSpecifiedDate: ({
    specifiedDate,
    payCycleMonthDays,
    forPreviousPayCycle,
  }) => {
    let payCycleEndTime;
    let payCycleStartTime;
    // remove .utc here for currently
    const todayDateNumber = moment(specifiedDate).date();

    if (payCycleMonthDays.length === 2) {
      const [payDateNumber] = payCycleMonthDays;
      payCycleEndTime = moment.utc(specifiedDate)
        .set('date', payDateNumber)
        .startOf('day');
      payCycleStartTime = moment.utc(specifiedDate)
        .set('date', payDateNumber)
        .startOf('day');
      payCycleStartTime.subtract(1, 'months');
      // payCycleEndTime.add(1, 'months');

      if (moment.utc(specifiedDate).date() >= payDateNumber) {
        payCycleStartTime.add(1, 'months');
        payCycleEndTime.add(1, 'months');
      }
    } else {
      for (let i = 0; i < payCycleMonthDays.length - 1; i += 1) {
        if (payCycleMonthDays[i] < todayDateNumber
            && todayDateNumber <= payCycleMonthDays[i + 1]) {
          payCycleStartTime = moment.utc(specifiedDate).set('date', payCycleMonthDays[i]).startOf('day');
          payCycleEndTime = moment.utc(specifiedDate).set('date', payCycleMonthDays[i + 1]).startOf('day');
        }
      }
    }

    if (forPreviousPayCycle) {
      const payPeriodLength = payCycleMonthDays[1] - payCycleMonthDays[0];
      if (payPeriodLength === 0) {
        payCycleStartTime.subtract(1, 'months');
        payCycleEndTime.subtract(1, 'months');
      } else {
        payCycleStartTime.subtract(payPeriodLength, 'days');
        payCycleEndTime.subtract(payPeriodLength, 'days');
      }
    }

    return {
      payCycleEndTime,
      payCycleStartTime,
    };
  },

  _createVirtualShifts: ({
    shift,
    payCycle,
  }) => {
    const virtualShifts = [];
    let virtualShift;
    const { payCycleStartTime, payCycleEndTime } = payCycle;

    const startDay = (moment.utc(payCycleStartTime).isAfter(moment.utc(shift.startTime)))
      ? moment.utc(payCycleStartTime) : moment.utc(shift.startTime);

    for (
      let thisDay = moment.utc(startDay);
      thisDay.isBefore(moment.utc(payCycleEndTime))
        && thisDay.isSameOrBefore(moment.utc(shift.endTime));
      thisDay.add(1, 'days')
    ) {
      const startTime = moment.utc(thisDay)
        .set('hour', moment.utc(shift.startTime).get('hour'))
        .set('minute', moment.utc(shift.startTime).get('minute'));
      const endTime = moment.utc(thisDay)
        .set('hour', moment.utc(shift.endTime).get('hour'))
        .set('minute', moment.utc(shift.endTime).get('minute'));

      virtualShift = {
        _id: shift._id,
        workerId: shift.workerId,
        employerId: shift.employerId,
        startTime,
        endTime,
      };
      virtualShifts.push(virtualShift);

      if (thisDay.isSame(payCycleEndTime, 'day')) {
        const todayShiftFinishedTime = moment.utc()
          .set('hour', moment.utc(shift.endTime).get('hour'))
          .set('minute', moment.utc(shift.endTime).get('minute'));

        if (payCycleEndTime.isBefore(todayShiftFinishedTime)) {
          virtualShifts.pop();
        }
      }
    }
    return virtualShifts;
  },

  _createVirtualWorkTokens: ({
    shifts,
  }) => {
    const virtualWorkTokens = shifts.map((shift) => {
      const virtualWorkToken = {
        checkinTime: shift.startTime,
        checkoutTime: shift.endTime,
        type: 'VIRTUAL_STANDARD',
        shiftId: shift._id,
        workerId: shift.workerId,
        confirmed: null,
      };
      return virtualWorkToken;
    });
    return virtualWorkTokens;
  },

  _calculateTotalWorkDays: ({
    accountingStyle = 'simple',
    payCycle,
    fulltimeNonWorkWeekDays = [],
  }) => {
    const { payCycleStartTime, payCycleEndTime } = payCycle;

    if (accountingStyle === 'simple') {
      return payCycleEndTime.diff(payCycleStartTime, 'days');
    }

    let totalWorkDays = 0;
    for (let thisDay = moment.utc(payCycleStartTime);
      thisDay.isBefore(moment.utc(payCycleEndTime));
      thisDay.add(1, 'days')) {
      if (!fulltimeNonWorkWeekDays.includes(thisDay.day())) {
        totalWorkDays += 1;
      }
    }
    return totalWorkDays;
  },

  _isWorkTokenMatch: (workToken1, workToken2) => {
    const varianceSec = 60 * 60; // 1hr
    let isMatch = false;
    let checkinTimeMin = moment.utc(workToken2.checkinTime);
    let checkinTimeMax = moment.utc(workToken2.checkinTime);
    let checkoutTimeMin = moment.utc(workToken2.checkoutTime);
    let checkoutTimeMax = moment.utc(workToken2.checkoutTime);
    checkinTimeMin = checkinTimeMin.subtract(varianceSec, 'seconds');
    checkinTimeMax = checkinTimeMax.add(varianceSec, 'seconds');
    checkoutTimeMin = checkoutTimeMin.subtract(varianceSec, 'seconds');
    checkoutTimeMax = checkoutTimeMax.add(varianceSec, 'seconds');
    const workToken1CheckinTime = moment.utc(workToken1.checkinTime);
    const workToken1CheckoutTime = moment.utc(workToken1.checkoutTime);
    try {
      if (workToken1CheckinTime.isBetween(checkinTimeMin, checkinTimeMax)
          && workToken1CheckoutTime.isBetween(checkoutTimeMin, checkoutTimeMax)) {
        isMatch = true;
      }
    } catch (err) {
      error(err);
    }
    return isMatch;
  },
};

module.exports = utils;
