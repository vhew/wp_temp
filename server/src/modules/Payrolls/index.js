const fs = require('fs');
const { GraphQLModule } = require('@graphql-modules/core');
const CoreModule = require('../../cores/index');

const typeDefs = fs.readFileSync('./src/modules/Payrolls/types.gql', 'utf8');
const resolvers = require('./resolvers');

const PayrollModule = new GraphQLModule({
  imports: [
    CoreModule,
  ],
  typeDefs,
  resolvers,
});

module.exports = PayrollModule;
