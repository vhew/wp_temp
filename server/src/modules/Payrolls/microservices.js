const { ObjectId } = require('mongoose').Types;
const moment = require('moment');
const error = require('debug')('ERROR');
const {
  Organisation,
  Job,
  Shift,
  WorkToken,
  DiscountToken,
} = require('../../../models/mongodb/index');
const {
  PaymentMethod,
  WorkerPaymentMethod,
} = require('../payments/model');


const helpers = require('./helpers');

const microservices = {
  unpaidWorkTokenService: async ({ jobId, payCycle }) => {
    // WARNING: this service does not consider workTokens in disbursement page as unpaid - Ref1
    let unpaidWorkTokens = [];
    try {
      const {
        payCycleStartTime,
        payCycleEndTime,
      } = payCycle;

      const job = await Job.findById(jobId);
      unpaidWorkTokens = await WorkToken.aggregate([
        {
          $lookup: {
            from: 'fiattokens',
            localField: '_id',
            foreignField: 'workTokenId',
            as: 'fiattoken',
          },
        },
        {
          $match: {
            shiftId: {
              $in: job.shiftsIds,
            },
            checkinTime: {
              $gt: new Date(payCycleStartTime),
              $lt: new Date(payCycleEndTime),
            },
            type: 'STANDARD',
            payNow: false, // Ref1
            $expr: {
              $eq: [
                { $size: '$fiattoken' },
                0,
              ],
            },
          },
        },
        {
          $sort: {
            checkinTime: 1,
          },
        },
      ]);
    } catch (err) {
      throw new Error(err);
    }

    return unpaidWorkTokens;
  },
  intDivision(x, y) {
    const intArray = [];
    for (let i = 1; i <= y; i += 1) {
      const div = parseInt(x / y, 10);
      if (i === 1) {
        intArray.push(x - (div * (y - 1)));
      } else {
        intArray.push(div);
      }
    }
    return intArray;
  },
  createWorkTokensService: async ({
    jobId,
    workerId,
    numberRequested,
    payCycle,
    shiftId,
  }) => {
    let workTokens = [];
    const {
      payCycleStartTime,
      payCycleEndTime,
    } = payCycle;

    const job = await Job.findById(jobId);

    const { paymentOption } = job;

    if (paymentOption == null) {
      throw new Error('Job does not have payment option');
    }

    let shifts;
    if (paymentOption === 'HOURLY') {
      if (shiftId != null) {
        const shift = await Shift.findById(ObjectId(shiftId));
        shifts.push(shift);
      } else {
        shifts = await Shift.find({
          jobId,
          workerId,
          // workComplete: true,
          endTime: {
            $gt: new Date(payCycleStartTime),
          },
          // startTime: {
          //   $gt: new Date(payCycleStartTime),
          // },
          // endTime: {
          //   $lt: new Date(payCycleEndTime),
          // },
        })
          .sort({ startTime: 1 })
          .catch(err => error(err));
      }
      const noOfRequestArray = microservices.intDivision(numberRequested, shifts.length);
      // const [shift] = shifts;
      for (let j = 0; j < shifts.length; j += 1) {
        let tempWorkTokens = [];
        const shift = shifts[j];
        const noOfRequest = noOfRequestArray[j];
        if (shift.divisible === true) {
          const [lastWorkToken] = await WorkToken.find({
            shiftId: shift._id,
            workerId,
            checkinTime: {
              $gt: new Date(payCycleStartTime),
            },
            $or: [{ type: 'STANDARD' }, { type: 'PRODUCT' }],
            // type: 'STANDARD',
          }).sort({ _id: -1 }).limit(1);
          let startDay;
          if (lastWorkToken == null || lastWorkToken.length === 0) {
            startDay = (moment.utc(payCycleStartTime).isAfter(moment.utc(shift.startTime)))
              ? moment.utc(payCycleStartTime) : moment.utc(shift.startTime);
          } else {
            startDay = moment.utc(lastWorkToken.checkinTime).add(1, 'days');
          }
          for (
            let thisDay = moment.utc(startDay);
            thisDay.isBefore(moment.utc(payCycleEndTime))
              && thisDay.isSameOrBefore(moment.utc(shift.endTime));
            thisDay.add(1, 'days')
          ) {
            const checkinTime = moment.utc(thisDay)
              .set('hour', moment.utc(shift.startTime).get('hour'))
              .set('minute', moment.utc(shift.startTime).get('minute'));
            const checkoutTime = moment.utc(thisDay)
              .set('hour', moment.utc(shift.endTime).get('hour'))
              .set('minute', moment.utc(shift.endTime).get('minute'));
            tempWorkTokens.push({
              shiftId: shift._id,
              workerId,
              checkinTime,
              checkoutTime,
              type: 'STANDARD',
            });
            if (tempWorkTokens.length >= noOfRequest) {
              workTokens = [...tempWorkTokens, ...workTokens];
              break;
            }
          }
          if (tempWorkTokens.length < noOfRequest) {
            throw new Error('Not enough shifts to satisfy request');
          }
          tempWorkTokens = [];
        } else {
          for (let i = 0; i < shifts.length; i += 1) {
            const checkinTime = moment.utc(shifts[i].startTime)
              .set('hour', moment.utc(shifts[i].startTime).get('hour'))
              .set('minute', moment.utc(shifts[i].startTime).get('minute'));
            const checkoutTime = moment.utc(shifts[i].endTime)
              .set('hour', moment.utc(shifts[i].endTime).get('hour'))
              .set('minute', moment.utc(shifts[i].endTime).get('minute'));
            workTokens.push({
              shiftId: shifts[i]._id,
              workerId,
              checkinTime,
              checkoutTime,
              type: 'STANDARD',
            });
            if (workTokens.length >= noOfRequest) {
              break;
            }
          }
        }
      }
    }

    if (job.paymentOption === 'MONTHLY') {
      shifts = await Shift.find({
        jobId,
        workerId,
        workComplete: false, // deprecated
        endTime: {
          $gt: new Date(payCycleStartTime),
        },
      }).catch(err => error(err));
      const [shift] = shifts;

      const [lastWorkToken] = await WorkToken.find({
        shiftId: shift._id,
        workerId,
        checkinTime: {
          $gt: new Date(payCycleStartTime),
        },
        $or: [{ type: 'STANDARD' }, { type: 'PRODUCT' }],
        // type: 'STANDARD',
      }).sort({ _id: -1 }).limit(1);

      let startDay;

      if (lastWorkToken == null || lastWorkToken.length === 0) {
        startDay = (moment.utc(payCycleStartTime).isAfter(moment.utc(shift.startTime)))
          ? moment.utc(payCycleStartTime) : moment.utc(shift.startTime);
      } else {
        startDay = moment.utc(lastWorkToken.checkinTime).add(1, 'days');
      }

      for (
        let thisDay = moment.utc(startDay);
        thisDay.isBefore(moment.utc(payCycleEndTime))
          && thisDay.isSameOrBefore(moment.utc(shift.endTime));
        thisDay.add(1, 'days')
      ) {
        const checkinTime = moment.utc(thisDay)
          .set('hour', moment.utc(shift.startTime).get('hour'))
          .set('minute', moment.utc(shift.startTime).get('minute'));
        const checkoutTime = moment.utc(thisDay)
          .set('hour', moment.utc(shift.endTime).get('hour'))
          .set('minute', moment.utc(shift.endTime).get('minute'));

        workTokens.push({
          shiftId: shift._id,
          workerId,
          checkinTime,
          checkoutTime,
          type: 'STANDARD',
        });

        if (workTokens.length >= numberRequested) {
          break;
        }
      }
      if (workTokens.length < numberRequested) {
        throw new Error('Not enough shifts to satisfy request');
      }
    }

    if (job.paymentOption === 'FIXED') {
      shifts = await Shift.find({
        jobId,
        workerId,
        workComplete: false, // TO-DO after contract finished update // deprecated
        endTime: {
          $gt: new Date(payCycleStartTime),
          $lt: new Date(payCycleEndTime),
        },
      });
      const checkinTime = moment.utc(shifts[0].startTime)
        .set('hour', moment.utc(shifts[0].startTime).get('hour'))
        .set('minute', moment.utc(shifts[0].startTime).get('minute'));
      const checkoutTime = moment.utc(shifts[0].endTime)
        .set('hour', moment.utc(shifts[0].endTime).get('hour'))
        .set('minute', moment.utc(shifts[0].endTime).get('minute'));
      workTokens.push({
        shiftId: shifts[0]._id,
        workerId,
        checkinTime,
        checkoutTime,
        type: 'STANDARD',
      });
    }


    const createdWorkTokens = await Promise.all(workTokens.map((workToken) => {
      const wt = new WorkToken({
        shiftId: workToken.shiftId,
        workerId: workToken.workerId,
        checkinTime: workToken.checkinTime,
        checkoutTime: workToken.checkoutTime,
        type: workToken.type,
        payNow: false,
        confirmed: null,
      });
      return wt.save();
    }));
    return createdWorkTokens;
  },

  workTokenAmountService: async ({ jobId, payCycle, usingTimesheetingFeatureLevel }) => {
    const deductionAmount = await helpers.calculateDeductionAmount({
      jobId,
      payCycle,
    });
    const amount = await helpers.calculateAmountForEachWorkToken({
      jobId,
      payCycle,
      deductionAmount,
      usingTimesheetingFeatureLevel,
    });
    return amount;
  },

  feeService: async ({
    jobId,
    amount,
    workerPaymentMethodId,
    numberRequested,
    discountTokenId,
  }) => {
    const organisation = await Organisation.findOne({
      jobsIds: jobId,
    });
    let fee = amount * (organisation.fee / 100);

    const workerPaymentMethod = await WorkerPaymentMethod
      .findById(workerPaymentMethodId);

    const paymentMethod = await PaymentMethod
      .findById(workerPaymentMethod.paymentMethodId);

    const totalAmount = amount * numberRequested;

    const { transactionFeeRule } = paymentMethod;
    if (!transactionFeeRule || transactionFeeRule.length < 1) {
      return fee;
    }
    const { percent, fixed } = transactionFeeRule
      .find(e => e.min <= totalAmount && totalAmount < e.max) || {};

    const transactionFee = (totalAmount * (percent / 100)) + fixed;

    fee += transactionFee / numberRequested;
    let discount = 0;
    let discountToken;
    if (discountTokenId != null) {
      discountToken = await DiscountToken.findById(discountTokenId);
      if (discountToken != null) {
        discount = (parseFloat(discountToken.discountPercent)
        * fee) / 100;
        discount += discountToken.discountFixed;
      }
    }
    if (discount > 0) {
      fee -= discount;
    }
    return fee;
  },

  payCyclePaidService: async () => false,
};

module.exports = microservices;
