const { ObjectId } = require('mongoose').Types;
const moment = require('moment');
const error = require('debug')('ERROR');
const {
  Job,
  Organisation,
  Shift,
  WorkToken,
  WorkTokenValue,
} = require('../../../models/mongodb/index');
const utils = require('./utils');

const helpers = {
  getPayCycle: async ({
    jobId,
    forPreviousPayCycle,
  }) => {
    const organisation = await Organisation.findOne({
      jobsIds: jobId,
    });

    const payCycle = await utils._getPayCycleForSpecifiedDate({
      specifiedDate: new Date(),
      payCycleMonthDays: organisation.payCycleMonthDays,
      forPreviousPayCycle,
    });

    return payCycle;
  },

  createVirtualWorkTokens: async ({
    jobId,
    workerId,
    payCycle,
  }) => {
    const {
      payCycleStartTime,
      payCycleEndTime,
    } = payCycle;

    const job = await Job.findById(jobId);

    let shifts = [];
    if (job.paymentOption == null) {
      throw new Error('Job does not have payment option');
    }
    if (job.paymentOption === 'HOURLY') {
      let shifts2 = [];

      shifts2 = await Shift.find({
        jobId,
        workerId,
        // workComplete: true, // in the future checkin-out system will update it to TRUE
        endTime: {
          $gt: new Date(payCycleStartTime),
        },
      });
      await Promise.all(shifts2.map(async (shift) => {
        const vShifts = utils._createVirtualShifts({
          shift,
          payCycle,
        });
        shifts.push(...vShifts);
      }));
      // shifts = utils._createVirtualShifts({
      //   shift,
      //   payCycle,
      // });
    }

    if (job.paymentOption === 'MONTHLY') {
      const realShifts = await Shift.find({
        jobId,
        workerId,
        // workComplete: false,
        endTime: {
          $gt: new Date(payCycleStartTime),
        },
      }).catch(err => error(err));
      await Promise.all(realShifts.map(async (shift) => {
        const data = utils._createVirtualShifts({
          shift,
          payCycle,
        });
        shifts.push(...data);
      }));
      // shifts = utils._createVirtualShifts({
      //   shift,
      //   payCycle,
      // });
    }

    if (job.paymentOption === 'FIXED') {
      shifts = await Shift.find({
        jobId,
        workerId,
        workComplete: false, // TO-DO after contract finished update // deprecated
        endTime: {
          $gt: new Date(payCycleStartTime),
          $lt: new Date(payCycleEndTime),
        },
      });
    }

    if (shifts.length === 0) {
      return [];
    }
    let virtualWorkTokens = utils._createVirtualWorkTokens({
      shifts,
    });

    const [firstVirtualWorkToken] = virtualWorkTokens;
    const requestedWorkTokens = await WorkToken.aggregate([
      {
        $lookup: {
          from: 'worktokenvalues',
          localField: '_id',
          foreignField: 'workTokenId',
          as: 'workTokenValues',
        },
      },
      {
        $match: {
          shiftId: firstVirtualWorkToken.shiftId,
          workerId: firstVirtualWorkToken.workerId,
          checkinTime: {
            $gt: new Date(payCycleStartTime),
            $lt: new Date(payCycleEndTime),
          },
          $or: [{ type: 'STANDARD' }, { type: 'PRODUCT' }],
          // type: 'STANDARD',
          $expr: {
            $gt: [
              { $size: '$workTokenValues' },
              0,
            ],
          },
        },
      },
      {
        $sort: {
          checkinTime: 1,
        },
      },
    ]);
    // const workTokens = await WorkToken.find({
    //   // shiftId: firstVirtualWorkToken.shiftId,
    //   workerId: firstVirtualWorkToken.workerId,
    //   checkinTime: {
    //     $gte: payCycleStartTime,
    //     $lt: payCycleEndTime,
    //   },
    //   type: 'STANDARD',
    //   payNow: true,
    // }).catch(err => error(err));

    virtualWorkTokens = virtualWorkTokens.filter(virtualWorkToken => !requestedWorkTokens
      .some(realWorkToken => utils
        ._isWorkTokenMatch(realWorkToken, virtualWorkToken)));


    return virtualWorkTokens;
  },

  calculateAmountForEachWorkToken: async ({
    jobId,
    payCycle,
    deductionAmount,
    usingTimesheetingFeatureLevel,
  }) => {
    const job = await Job.findById(jobId);
    const organisation = await Organisation.findOne({
      jobsIds: jobId,
    });

    const {
      paymentOption,
      renumerationValue,
      fulltimeNonWorkWeekDays,
    } = job;
    let salary = 0;
    const shift = await Shift.findById(job.shiftsIds[0]);
    if (shift.salary != null && shift.salary !== 0) {
      salary = shift.salary;
    } else {
      salary = renumerationValue;
    }
    const {
      accountingStyle,
    } = organisation;

    if (paymentOption === 'FIXED') {
      return salary - deductionAmount;
    }

    if (paymentOption === 'MONTHLY') {
      let totalWorkDays = utils._calculateTotalWorkDays({
        accountingStyle,
        fulltimeNonWorkWeekDays,
        payCycle,
      });
      if (usingTimesheetingFeatureLevel === 'FULL') {
        totalWorkDays = utils._calculateTotalWorkDays({
          accountingStyle: 'simple',
          fulltimeNonWorkWeekDays: [0, 6],
          payCycle,
        });
      }

      return ((salary - deductionAmount) / totalWorkDays);
    }
    if (paymentOption === 'HOURLY') {
      // const shift = await Shift.findOne({ jobId });
      // const totalHours = moment.utc(shift.startTime)
      //   .diff(moment.utc(shift.endTime), 'hours');
      // const amount = renumerationValue * totalHours;
      return ((salary - deductionAmount) * 9);
    }

    return -1;
  },

  createWorkTokens: async ({
    virtualWorkTokens,
  }) => {
    const createdWorkTokens = await Promise.all(virtualWorkTokens
      .map((workToken) => {
        // eslint-disable-next-line no-param-reassign
        workToken.type = 'STANDARD';
        const newWorkToken = new WorkToken(workToken);
        return newWorkToken.save();
      })).catch(err => error(err));

    return createdWorkTokens;
  },

  calculateDeductionAmount: async ({
    payCycle,
    jobId,
    workerId,
  }) => {
    const shifts = await Shift.find({
      workComplete: false,
      jobId,
      workerId,
    });

    const job = await Job.findById(jobId);
    const { renumerationValue } = job;
    const { payCycleStartTime, payCycleEndTime } = payCycle;

    let deductions = shifts.map(shift => shift.deductions);
    deductions = deductions.flat();

    const isCurrentPayCycleDeductions = (deduction) => {
      const enabledDate = moment.utc(deduction.enabledDate);
      const disabledDate = moment.utc(deduction.disabledDate);
      if (enabledDate.isSame(disabledDate)) {
        if (payCycleStartTime.isBefore(enabledDate) && payCycleEndTime.isAfter(enabledDate)) {
          return true;
        }
      } else if (enabledDate.isBefore(payCycleEndTime) && disabledDate.isAfter(payCycleEndTime)) {
        return true;
      }
      return false;
    };
    const currentPayCycleDeductions = deductions.filter(isCurrentPayCycleDeductions);

    let deductionAmount = 0;
    currentPayCycleDeductions.forEach((currentPayCycleDeduction) => {
      const { amount } = currentPayCycleDeduction;
      if (currentPayCycleDeduction.type === 'PERCENTAGE') {
        const percentage = parseInt(amount, 10);
        deductionAmount += (renumerationValue * percentage / 100);
      } else {
        const fixedAmount = parseInt(amount, 10);
        deductionAmount += fixedAmount;
      }
    });
    return deductionAmount;
  },

  workdayCount: (start, end) => {
    const first = start.clone().endOf('week'); // end of first week
    const last = end.clone().startOf('week'); // start of last week
    const days = last.diff(first, 'days') * 5 / 7; // this will always multiply of 7
    let wfirst = first.day() - start.day(); // check first week
    if (start.day() === 0) wfirst -= 1; // -1 if start with sunday
    let wlast = end.day() - last.day(); // check last week
    if (end.day() === 6) wlast -= 1; // -1 if end with saturday
    return wfirst + Math.floor(days) + wlast; // get the total
  },
  createWorkTokenValues: async ({
    jobId,
    currency,
    method,
    coordinates,
    workTokens,
    payCycle,
    deductionAmount,
    isKhuPay,
    usingTimesheetingFeatureLevel,
  }) => {
    const job = await Job.findById(jobId);
    const { paymentOption, renumerationValue } = job;

    let workTokenValues = [];
    if (paymentOption === 'FIXED' || paymentOption === 'MONTHLY') {
      const amount = await helpers.calculateAmountForEachWorkToken({
        jobId,
        payCycle,
        deductionAmount,
        usingTimesheetingFeatureLevel,
      });
      let fee = 0;
      if (isKhuPay) {
        fee = await helpers.getFee({
          jobId,
          amountPerWorkToken: amount,
        });
      }
      let weekendCount = 0;
      const totalDays = payCycle.payCycleEndTime.diff(payCycle.payCycleStartTime, 'days');
      const workDays = helpers
        .workdayCount(payCycle.payCycleStartTime, payCycle.payCycleEndTime.add(-1, 'days'));
      const weekendDays = totalDays - workDays;
      workTokenValues = await Promise.all(workTokens.reverse().map(async (workToken) => {
        let currentAmount = amount;
        // if (usingTimesheetingFeatureLevel === 'FULL') {
        //   if (moment(workToken.checkinTime).format('ddd') === 'Fri'
        //   && workToken.id !== workTokens[0].id) {
        //     currentAmount = amount * 3;
        //   }
        // }
        if (usingTimesheetingFeatureLevel === 'FULL') {
          if (weekendCount < weekendDays) {
            currentAmount = amount * 2;
          }
          weekendCount += 1;
        }
        const workTokenValue = new WorkTokenValue({
          expectedWorkTokenShiftId: workToken.shiftId,
          expectedWorkTokenWorkerId: workToken.workerId,
          expectedWorkTokenType: 'STANDARD',
          expectedWorkTokenCheckinTime: workToken.checkinTime,
          expectedWorkTokenCheckoutTime: workToken.checkoutTime,
          workTokenId: workToken._id,
          confirmRequest: true,
          amount: currentAmount,
          fee,
          currency,
          method,
          coordinates,
        });
        const created = await workTokenValue.save()
          .catch((err) => {
            console.error(err);
          });
        return created;
        // return workTokenValue.save();
      }))
        .catch(err => error(err));
    }

    if (paymentOption === 'HOURLY') {
      let fee = 0;
      workTokenValues = await Promise.all(workTokens.map(async (workToken) => {
        const totalHours = moment.utc(workToken.checkoutTime)
          .diff(moment.utc(workToken.checkinTime), 'hours');
        const amount = renumerationValue * totalHours;

        if (isKhuPay) {
          fee = await helpers.getFee({
            jobId,
            amountPerWorkToken: amount,
          });
        }

        const workTokenValue = new WorkTokenValue({
          expectedWorkTokenShiftId: workToken.shiftId,
          expectedWorkTokenWorkerId: workToken.workerId,
          expectedWorkTokenType: 'STANDARD',
          expectedWorkTokenCheckinTime: workToken.checkinTime,
          expectedWorkTokenCheckoutTime: workToken.checkinTime,
          workTokenId: workToken._id,
          confirmRequest: true,
          amount,
          fee,
          currency,
          method,
          coordinates,
        });
        return workTokenValue.save();
      })).catch(err => error(err));
    }

    return workTokenValues;
  },

  createWorkTokenValues2point0: async ({
    workTokens,
    amount,
    fee,
    currency,
    method,
    coordinates,
  }) => {
    const workTokenValues = await Promise.all(workTokens.map((workToken) => {
      const workTokenValue = new WorkTokenValue({
        expectedWorkTokenShiftId: workToken.shiftId,
        expectedWorkTokenWorkerId: workToken.workerId,
        expectedWorkTokenType: 'STANDARD',
        expectedWorkTokenCheckinTime: workToken.checkinTime,
        expectedWorkTokenCheckoutTime: workToken.checkinTime,
        workTokenId: workToken._id,
        confirmRequest: true,
        amount,
        fee,
        currency,
        method,
        coordinates,
      });
      return workTokenValue.save();
    })).catch(err => error(err));
    return workTokenValues;
  },

  createDeductionWorkTokensAndWorkTokenValues: async ({
    jobId,
    workerId,
    payCycle,
    currency,
    paymentMethod,
  }) => {
    const shifts = await Shift.find({
      workComplete: false,
      jobId,
      workerId,
    });

    let deductions = shifts.map(shift => shift.deductions);
    deductions = deductions.flat();


    const job = await Job.findById(jobId);
    const { renumerationValue } = job;
    const DEDUCTION_COORDINATES = '+454213209153';
    const { payCycleStartTime, payCycleEndTime } = payCycle;

    const checkinTime = moment.utc(payCycleStartTime);
    const checkoutTime = moment.utc(payCycleEndTime)
      .subtract(1, 'days');

    const isCurrentPayCycleDeductions = (deduction) => {
      const enabledDate = moment.utc(deduction.enabledDate);
      const disabledDate = moment.utc(deduction.disabledDate);
      if (enabledDate.isSame(disabledDate)) {
        if (payCycleStartTime.isBefore(enabledDate) && payCycleEndTime.isAfter(enabledDate)) {
          return true;
        }
      } else if (enabledDate.isBefore(payCycleEndTime) && disabledDate.isAfter(payCycleEndTime)) {
        return true;
      }
      return false;
    };
    const currentPayCycleDeductions = deductions.filter(isCurrentPayCycleDeductions);

    const workTokenValues = await Promise.all(currentPayCycleDeductions
      .map(async (currentPayCycleDeduction) => {
        const workToken = new WorkToken({
          confirmed: true,
          payNow: true,
          checkinTime,
          checkoutTime,
          type: 'DEDUCTION',
          shiftId: shifts[0]._id,
          workerId,
        });
        const createdWorkToken = await workToken.save();

        let amount;
        if (currentPayCycleDeduction.type === 'PERCENTAGE') {
          const percentage = parseInt(currentPayCycleDeduction.amount, 10);
          amount = renumerationValue * percentage / 100;
        } else {
          amount = currentPayCycleDeduction.amount;
        }
        const workTokenValue = new WorkTokenValue({
          confirmRequest: false,
          expectedWorkTokenShiftId: shifts[0]._id,
          expectedWorkTokenWorkerId: workerId,
          expectedWorkTokenType: 'DEDUCTION',
          expectedWorkTokenCheckinTime: createdWorkToken.checkinTime,
          expectedWorkTokenCheckoutTime: createdWorkToken.checkoutTime,
          workTokenId: createdWorkToken._id,
          amount,
          fee: 0,
          currency,
          method: paymentMethod,
          coordinates: DEDUCTION_COORDINATES,
          notes: currentPayCycleDeduction.notes,
        });
        return workTokenValue.save();
      }));
    return workTokenValues;
  },

  calculateTotalAmountOfWorkTokens: async ({
    jobId,
    payCycle,
    deductionAmount,
    workTokens,
  }) => {
    let totalAmount = 0;
    const job = await Job.findById(jobId);
    const organisation = await Organisation.findOne({
      jobsIds: jobId,
    });

    const {
      paymentOption,
      renumerationValue,
      fulltimeNonWorkWeekDays,
    } = job;
    let salary = 0;
    const shift = await Shift.findById(job.shiftsIds[0]);
    if (shift.salary != null && shift.salary !== 0) {
      salary = shift.salary;
    } else {
      salary = renumerationValue;
    }
    const {
      accountingStyle,
    } = organisation;

    if (paymentOption === 'FIXED') {
      return salary - deductionAmount;
    }

    if (paymentOption === 'MONTHLY') {
      const totalWorkDays = utils._calculateTotalWorkDays({
        accountingStyle,
        fulltimeNonWorkWeekDays,
        payCycle,
      });

      const amountPerDay = ((salary - deductionAmount) / totalWorkDays);
      return amountPerDay * workTokens.length;
    }

    if (paymentOption === 'HOURLY') {
      totalAmount = workTokens.reduce(
        (accumulator, currentValue) => {
          const totalHours = moment.utc(currentValue.checkoutTime)
            .diff(moment.utc(currentValue.checkinTime), 'hours');
          const amount = salary * totalHours;
          // const amount = salary * 9;
          return accumulator + amount;
        },
        0,
      );
      return totalAmount;
    }
    return -1;
  },

  getLastShiftFinished: async ({
    workTokens,
  }) => {
    const dates = workTokens
      .map(workToken => new Date(workToken.checkoutTime));
    return moment(Math.max.apply(null, dates));
  },

  getFee: async ({
    jobId,
    amountPerWorkToken,
  }) => {
    const organisation = await Organisation.findOne({
      jobsIds: jobId,
    });
    const fee = amountPerWorkToken * organisation.fee / 100;
    return fee;
  },

  deleteWorkTokens: async ({
    workTokens,
  }) => {
    const ret = await WorkToken.deleteMany({
      _id: {
        $in: workTokens.map(e => e._id),
      },
    }).catch(err => error('error %O', err));
    return ret;
  },

  getOrganisationUsingTimesheetingFeatureLevel: async ({
    jobId,
  }) => {
    const organisation = await Organisation.findOne({
      jobsIds: jobId,
    }).catch(err => error('error %O', err));

    const { settings } = organisation;
    const obj = settings.find(e => e.key === 'TIMESHEETING_FEATURE_LEVEL');
    if (obj == null) return false;

    return obj.value;
  },

  getTimesheetingWorkTokens: async ({
    jobId,
    workerId,
    payCycle,
  }) => {
    const { payCycleStartTime, payCycleEndTime } = payCycle;

    const shifts = await Shift.find({
      jobId,
      workerId,
    });
    const workTokens = await WorkToken.aggregate([
      {
        $match: {
          createdAt: {
            $gte: new Date(payCycleStartTime),
            $lte: new Date(payCycleEndTime),
          },
          shiftId: {
            $in: shifts.map(shift => shift._id),
          },
          payNow: false,
          type: 'STANDARD',
          workerId,
        },
      },
      {
        $lookup: {
          from: 'fiattokens',
          localField: '_id',
          foreignField: 'workTokenId',
          as: 'fiatTokens',
        },
      },
      {
        $lookup: {
          from: 'worktokenvalues',
          localField: '_id',
          foreignField: 'workTokenId',
          as: 'workTokenValues',
        },
      },
      {
        $match: {
          $expr: {
            $eq: [
              {
                $size: '$fiatTokens',
              },
              0,
            ],
          },
        },
      },
      {
        $match: {
          $expr: {
            $eq: [
              {
                $size: '$workTokenValues',
              },
              0,
            ],
          },
        },
      },
    ]).catch(err => error(err));
    return workTokens;
  },

  setWorkTokensConfirmedNull: async ({
    workTokensIds,
  }) => {
    const ret = await WorkToken.updateMany({
      _id: {
        $in: workTokensIds,
      },
    }, {
      $set: {
        confirmed: null,
      },
    });
    return ret;
  },
};

module.exports = helpers;
