const mongoose = require('mongoose');

const PaymentMethodSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    region: {
      type: String,
      required: true,
    },
    code: {
      type: String,
    },
    transactionFeeRule: {
      type: Array,
    },
    currency: {
      type: String,
    },
    paymentType: {
      type: String,
    },
  },
);

const WorkerPaymentMethodSchema = new mongoose.Schema(
  {
    workerId: {
      type: String,
      required: true,
    },
    paymentMethodId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PaymentMethod',
    },
    coordinates: { type: String },
    verified: { type: Boolean },
    invalidate: { type: Date },
  },
);

const PaymentMethod = mongoose.model('PaymentMethod', PaymentMethodSchema);
const WorkerPaymentMethod = mongoose.model('WorkerPaymentMethod', WorkerPaymentMethodSchema);

const {
  WorkerSchema,
  JobSchema,
} = require('../../../models/mongodb/index');

WorkerSchema.add({
  workerPaymentMethodsIds: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'WorkerPaymentMethod',
    },
  ],
  defaultWorkerPaymentMethodId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'WorkerPaymentMethod',
  },
});
JobSchema.add({
  paymentMethodsIds: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PaymentMethod',
    },
  ],
  paymentOption: {
    type: String,
  },
});

module.exports = {
  PaymentMethod,
  WorkerPaymentMethod,
};
