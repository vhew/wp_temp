const typeDef = `
  extend type Query {
    paymentMethods(region: String): [ PaymentMethod! ]
  }
`;

module.exports = typeDef;
