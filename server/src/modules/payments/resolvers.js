const debug = require('debug')('DEBUG');
const error = require('debug')('ERROR');
const braintree = require('braintree');
const { ObjectId } = require('mongoose').Types;
const { AuthenticationError, ForbiddenError, ApolloError } = require('apollo-server-koa');
const {
  AlienWorker,
  Worker,
} = require('../../../models/mongodb/index');
const {
  PaymentMethod,
  WorkerPaymentMethod,
} = require('./model');
const utils = require('../../utils');
const ErrorHandlingService = require('../../services/ErrorHandlingService');

const resolvers = {
  Query: {
    paymentMethods: async (root, { region, currencies }, ctx) => {
      let ret;
      try {
        utils.checkUser(ctx);
        const regionFilter = region ? { region } : {};
        const currencyFilter = currencies ? { currency: { $in: currencies } } : {};
        const paymentTypeFilter = await utils.isWorker(ctx.user.id) ? { paymentType: { $ne: 'CASH_TRANSFER' } } : {};
        ret = await PaymentMethod.find({
          ...regionFilter,
          ...currencyFilter,
          ...paymentTypeFilter,
        }).sort({ currency: 1 });
      } catch (err) {
        error('query:paymentMethods:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },

  Mutation: {
    payWithBraintree: async (root, data) => {
      const gateway = new braintree.BraintreeGateway({
        environment: braintree.Environment.Sandbox,
        merchantId: '9t3qp28rszbbhd66',
        publicKey: 'h7d8jdnshbb5m6jr',
        privateKey: '12d8919285410c7afcf169dc360c2e7d',
      });
      const ret = await gateway.transaction.sale({
        amount: data.amount,
        paymentMethodNonce: data.payload.nonce,
        options: {
          submitForSettlement: true,
        },
      });
      return ret;
    },

    createPaymentMethod: async (root, data, ctx) => {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        debug('mutation createPaymentMethod as user(%s): \n%O', ctx.user.id, data);

        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const paymentMethod = new PaymentMethod(update);
        ret = await paymentMethod.save()
          .catch(err => error(err));
      } catch (err) {
        error('mutation:createPaymentMethod:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },

    addWorkerPaymentMethod: async (root, data, ctx) => {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        // const target = {
        //   id: data.workerId,
        //   type: 'Worker',
        // };
        // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
        debug('mutation addWorkerPaymentMethod as user(%s): \n%O', ctx.user.id, data);

        const workerPaymentMethod = new WorkerPaymentMethod({
          workerId: data.workerId,
          paymentMethodId: data.paymentMethodId,
          coordinates: data.coordinates,
          verified: data.verified,
        });
        const createdWorkerPaymentMethod = await workerPaymentMethod.save();

        if (ObjectId.isValid(data.workerId)) {
          const updatedWorker = await AlienWorker.findOneAndUpdate({
            _id: data.workerId,
          }, {
            $addToSet: {
              workerPaymentMethodsIds: createdWorkerPaymentMethod._id,
            },
          },
          { new: true });

          const paymentIsAdded = updatedWorker
            && updatedWorker.workerPaymentMethodsIds.includes(createdWorkerPaymentMethod._id);
          if (!paymentIsAdded) throw new ApolloError('adding worker payment method failed', 500);

          ret = updatedWorker;

          if (ret.workerPaymentMethodsIds.length === 1) {
            await AlienWorker.findOneAndUpdate({
              _id: data.workerId,
            }, {
              $set: {
                defaultWorkerPaymentMethodId: createdWorkerPaymentMethod._id,
              },
            });
          }
        } else {
          const updatedWorker = await Worker.findOneAndUpdate({
            _id: data.workerId,
          }, {
            $addToSet: {
              workerPaymentMethodsIds: createdWorkerPaymentMethod._id,
            },
          },
          { new: true });

          const paymentIsAdded = updatedWorker
            && updatedWorker.workerPaymentMethodsIds.includes(createdWorkerPaymentMethod._id);
          if (!paymentIsAdded) throw new ApolloError('adding worker payment method failed', 500);

          ret = updatedWorker;

          if (ret.workerPaymentMethodsIds.length === 1) {
            await Worker.findOneAndUpdate({
              _id: data.workerId,
            }, {
              $set: {
                defaultWorkerPaymentMethodId: createdWorkerPaymentMethod._id,
              },
            });
          }
        }
      } catch (err) {
        error('mutation:addWorkerPaymentMethod:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    editWorkerPaymentMethod: async (root, data, ctx) => {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        // const target = {
        //   id: data.workerId,
        //   type: 'Worker',
        // };
        // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
        // debug('mutation addWorkerPaymentMethod as user(%s): \n%O', ctx.user.id, data);
        ret = await WorkerPaymentMethod.findOneAndUpdate({
          _id: data.workerPaymentMethodId,
        }, {
          $set: {
            paymentMethodId: data.paymentMethodId,
            coordinates: data.coordinates,
          },
        },
        { new: true });
      } catch (err) {
        error('mutation:editWorkerPaymentMethod:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },

    setDefaultWorkerPaymentMethod: async (root, data, ctx) => {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.workerId,
          type: 'Worker',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        debug('mutation setDefaultWorkerPaymentMethod as user(%s): \n%O', ctx.user.id, data);

        const worker = await Worker.findById(data.workerId);
        const workerPaymentMethodsIds = worker.workerPaymentMethodsIds.map(e => e.toString());
        if (!workerPaymentMethodsIds.includes(`${data.workerPaymentMethodId}`)) {
          throw new ApolloError('Payment Method Id not found!', 404);
        }
        ret = await Worker.findOneAndUpdate({
          _id: data.workerId,
        }, {
          $set: {
            defaultWorkerPaymentMethodId: data.workerPaymentMethodId,
          },
        }).catch(err => error(err));
      } catch (e) {
        error('mutation:setDefaultWorkerPaymentMethod:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },

    deleteWorkerPaymentMethod: async (root, data, ctx) => {
      let updatedWorker;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

        const worker = await Worker.findById(data.workerId);
        const { defaultWorkerPaymentMethodId } = worker;

        if (defaultWorkerPaymentMethodId != null
            && defaultWorkerPaymentMethodId.toString() === data.workerPaymentMethodId) {
          throw new Error('Cannont delete default Worker Payment Method');
        }

        const ret = await WorkerPaymentMethod.deleteOne({
          _id: data.workerPaymentMethodId,
        }).catch(err => error(err));
        if (ret) {
          await Worker.findOneAndUpdate({
            _id: data.workerId,
          }, {
            $pull: {
              workerPaymentMethodsIds: data.workerPaymentMethodId,
            },
          });
        }
        updatedWorker = await Worker.findById(data.workerId);
      } catch (e) {
        error('mutation:deleteWorkerPaymentMethod:', e);
        ctx.ctx.throw(500, e);
      }
      return updatedWorker;
    },
  },

  Job: {
    paymentMethods: async (job) => {
      let ret;
      try {
        ret = await PaymentMethod.find({
          _id: {
            $in: job.paymentMethodsIds,
          },
        }).catch(err => error('resolver:Job:paymentMethods:', err));
      } catch (err) {
        error('resolver:Job:paymentMethods:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },

  Worker: {
    workerPaymentMethods: async (worker) => {
      const results = await WorkerPaymentMethod.find({
        _id: {
          $in: worker.workerPaymentMethodsIds,
        },
      }).catch(err => error('resolver:Worker:paymentMethods:', err));
      return results;
    },
    defaultWorkerPaymentMethod: async (worker) => {
      const result = await WorkerPaymentMethod
        .findById(worker.defaultWorkerPaymentMethodId)
        .catch(err => error('resolver:Worker:defaultWorkerPaymentMethod:', err));
      return result;
    },
  },

  WorkerPaymentMethod: {
    paymentMethod: async (workerPaymentMethod) => {
      const result = await PaymentMethod
        .findById(workerPaymentMethod.paymentMethodId)
        .catch(err => error('resolver:WorkerPaymentMethod:paymentMethod:', err));
      return result;
    },
  },
};

module.exports = resolvers;
