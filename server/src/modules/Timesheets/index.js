const fs = require('fs');
const { GraphQLModule } = require('@graphql-modules/core');
const CoreModule = require('../../cores/index');

const typeDefs = fs.readFileSync('./src/modules/Timesheets/types.gql', 'utf8');
const resolvers = require('./resolvers');

const TimesheetModule = new GraphQLModule({
  imports: [
    CoreModule,
  ],
  typeDefs,
  resolvers,
});

module.exports = TimesheetModule;
