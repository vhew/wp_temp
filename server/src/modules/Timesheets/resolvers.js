const R = require('ramda');
const moment = require('moment');
const { ObjectId } = require('mongoose').Types;
const error = require('debug')('ERROR');
const { ApolloError, AuthenticationError } = require('apollo-server-koa');
const {
  Organisation,
  Shift,
  Worker,
  WorkToken,
  Job,
  Employer,
} = require('./../../../models/mongodb/index');
const utils = require('../../utils');
const notiService = require('../../services/NotificationService');
const LeaveService = require('../../services/LeaveService');
const PayrollHelpers = require('../Payrolls/helpers');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const ERROR_CODES = require('../../utils/errorCodes');
const Mail = require('../../../models/Mail');


const LEAVE_TYPES = ['LEAVE_SICK', 'LEAVE_PERSONAL', 'LEAVE_SPECIAL', 'LEAVE_UNPAID'];


const resolvers = {
  Mutation: {
    async checkinShift(
      root,
      data,
      ctx,
    ) {
      /* Permissions */
      // const target = {
      //   id: data.shiftId,
      //   type: 'Shift',
      // };
      // if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');


      // Processing with error handling
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

        /* Validations */
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const { jobId } = shift;
        const job = await Job.findById(jobId);
        if (job == null) throw new ApolloError('Cannot find job', 404);
        const usingTimesheetingFeatureLevel = await PayrollHelpers
          .getOrganisationUsingTimesheetingFeatureLevel({ jobId });
        if (usingTimesheetingFeatureLevel == null) throw new Error('Timesheeting Feature Level is null.');
        let type = data.type;

        // validating for do not checkin twice a day for monthly salary type
        const startDate = new Date();
        startDate.setSeconds(0);
        startDate.setHours(0);
        startDate.setMinutes(0);
        const dateMidnight = new Date(startDate);
        dateMidnight.setHours(23);
        dateMidnight.setMinutes(59);
        dateMidnight.setSeconds(59);

        const todayWorkToken = await WorkToken.findOne({
          checkinTime: {
            $gt: startDate,
            $lt: dateMidnight,
          },
          workerId: data.workerId,
          type: { $nin: utils.leaveTypes },
        });
        if (job.paymentOption === 'MONTHLY' && !job.type.includes('PART_TIME')) {
          if (todayWorkToken != null) throw new Error('You already checkin for today');
        }

        // 🎫 create work token
        if (usingTimesheetingFeatureLevel === 'HYBRID') {
          type = 'HYBRID_TIMESHEET';
        }

        const workerId = data.workerId || ctx.user.id;

        const checkinData = {
          shiftId: data.shiftId,
          checkinTime: new Date().toISOString(),
          type,
          workerId,
          references: data.references,
          confirmed: true,
        };
        const workToken = new WorkToken(checkinData);
        const ret = await workToken.save();
        // const shift = await Shift.findOne({ workerId: data.workerId });

        // 🔔 send notification
        const worker = await Worker.findById(data.workerId);
        const title = `${worker.name} checkin`;
        const body = `${worker.name} checkin today.`;
        notiService.sendToEmployersInOrganisation(
          new ObjectId(job.organisationId),
          {
            from: worker._id,
            title,
            body,
            userType: notiService.userType.EMPLOYER,
            messageType: notiService.messageType.HIRED,
            data: { jobId: shift.jobId, shiftId: shift._id },
            mail: { action: Mail.getActions().CONFIRM },
            analyticsLabel: 'notification_individual_timesheet',
          },
        );
        return ret;
      } catch (err) {
        error('mutation:checkinShift:', err);
        throw ErrorHandlingService.getError(err);
      }
    },
    async checkoutShift(
      root,
      data,
      ctx,
    ) {
      // Processing with error handling
      try {
        /* permissions */
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        // const target = {
        //   id: data.shiftId,
        //   type: 'Shift',
        // };
        // if (await AccessControlService._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

        /* validations */
        const shift = await Shift.findOne({ workerId: data.workerId });
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find wprker', 404);
        const { references } = data;
        let checkoutTime;

        if (data.manualCheckoutTime == null) {
          checkoutTime = moment.utc();
        } else {
          checkoutTime = moment.utc(data.manualCheckoutTime);
          references.push({ key: 'MANUAL_CHECKOUT_TIME', value: true });
        }
        const ret = await WorkToken.findOneAndUpdate(
          { _id: data.workTokenId },
          {
            $set: {
              checkoutTime,
              confirmed: true,
            },
            $push: {
              references,
            },
          },
          { new: true },
        );
        const title = `${worker.name} checkout`;
        const body = `${worker.name} checkout today.`;
        notiService.sendToEmployersInOrganisation(
          shift.organisationId,
          {
            from: worker._id,
            title,
            body,
            userType: notiService.userType.EMPLOYER,
            messageType: notiService.messageType.HIRED,
            data: { jobId: shift.jobId, shiftId: data.id },
            mail: { action: Mail.getActions().CONFIRM },
            analyticsLabel: 'notification_individual_timesheet',
          },
        );
        return ret;
      } catch (err) {
        error('mutation:checkoutShift:', err);
        throw ErrorHandlingService.getError(err);
      }
    },
    async requestLeave(
      root,
      {
        shiftId,
        workerId,
        leaveType,
        checkinTime,
        references,
      },
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

        const shift = await Shift.findById(shiftId);
        if (!shift) throw new ApolloError('Shift not found', 404);
        const { organisationId, employerId, jobId } = shift;
        const organisation = await Organisation.findById(organisationId);
        if (!organisation) throw new ApolloError(`Organization not found. id: ${organisationId}`, 404);

        const leaveToken = {
          shiftId,
          checkinTime,
          type: leaveType,
          workerId,
          references,
        };

        R.pipe(
          LeaveService.calNRemaindingLeaves,
          LeaveService.checkRemainingLeaves,
        )({ shift, organisation, leaveToken });

        // 🎫 create work token (leaveType)
        const workToken = new WorkToken(leaveToken);
        ret = await workToken.save();

        // 🔔 send notification
        const [reference] = references;
        const leaveStart = moment(reference.leaveStart).format('DD-MM-YYYY');
        const leaveEnd = moment(reference.leaveEnd).format('DD-MM-YYYY');
        const leaveDuration = leaveStart + (leaveStart === leaveEnd ? '' : ` to ${leaveEnd}`);
        const worker = await Worker.findById(workerId);
        const title = `${worker.name} requested leave`;
        const body = `${worker.name} request leave for ${leaveDuration}.`;
        const employer = await Employer.findById(shift.employerId).catch(e => error(e));

        notiService.sendToEmployersInOrganisation(
          shift.organisationId, {
            from: worker.id,
            to: shift.employerId,
            title,
            body,
            userType: notiService.userType.EMPLOYER,
            messageType: notiService.messageType.PUSH_EMAIL,
            data: { jobId: shift.jobId, shiftId },
            mail: { to: employer, action: Mail.getActions().CONFIRM },
            analyticsLabel: 'notification_individual_timesheet',
          },
        );
        // ✉️ send mail to employer
        // if (mail) EmailService.send(to, title, mail);
      } catch (err) {
        error('mutation:requestLeave:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },

  Job: {
    timesheetingFeatureLevel: async (job) => {
      let ret;
      try {
        const organisation = await Organisation.findOne({
          jobsIds: job._id,
        });
        if (!organisation) throw new ApolloError('Organisation not found', 404);
        const found = organisation.settings.find(e => e.key === 'TIMESHEETING_FEATURE_LEVEL');
        ret = found && found.value;
      } catch (err) {
        error('resolver:Job:timesheetingFeatureLevel:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },

  Shift: {
    hasNotBeenCheckedOut: async (shift, { workerId }) => {
      let ret;
      try {
        const workToken = await WorkToken.findOne({
          shiftId: shift._id,
          workerId,
          type: { $nin: LEAVE_TYPES },
          checkoutTime: { $exists: false },
        });
        ret = workToken;
      } catch (err) {
        error('resolver:Shift:hasNotBeenCheckedOut:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },
};

module.exports = resolvers;
