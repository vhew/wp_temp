const error = require('debug')('ERROR');
const moment = require('moment');
const {
  Worker,
  Job,
  Organisation,
  Shift,
  AlienWorker,
} = require('../../../models/mongodb/');
const {
  PaymentMethod,
  WorkerPaymentMethod,
} = require('../payments/model');
const ErrorHandlingService = require('../../services/ErrorHandlingService');

const resolvers = {
  Mutation: {
    workerBatchUpload: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('querying as user(%s): \n%O', ctx.user.id, data);

      let ret;
      // Create Worker
      try {
        const newWorker = new Worker({
          _id: `unverified_${data.organisationId}_${data.batchData.phone}`,
          createOrganisationId: data.organisationId,
          phone: data.batchData.phone,
          name: data.batchData.name,
          identification: data.batchData.identification,
          verified: false,
        });
        const retCreateWorker = await newWorker.save().catch(e => error(e));
        const workerId = retCreateWorker._id;

        // Find payment method from database and add to the worker.
        const paymentMethod = await PaymentMethod.findOne({
          code: data.batchData.paymentMethod,
        }).catch(e => error(e));
        const workerPaymentMethod = new WorkerPaymentMethod({
          workerId,
          paymentMethodId: paymentMethod._id,
          coordinates: data.batchData.paymentCoordinates,
          verified: true,
        });
        const retWorkerPaymentMethod = await workerPaymentMethod.save().catch(e => error(e));

        const paymentMethodsIds = [];
        paymentMethodsIds.push(paymentMethod._id);

        // convert location format
        const locationCoordinates = {
          type: 'Point',
          // coordinates: batchData.locationCoordinates.split(','),
          coordinates: [
            parseFloat(data.batchData.locationCoordinates.split(',')[0]),
            parseFloat(data.batchData.locationCoordinates.split(',')[1]),
          ],
        };

        const fulltimeNonWorkWeekDays = data.batchData.fulltimeNonWorkWeekDays.toString().split(',')
          .map(x => parseInt(x, 10));

        // Create job
        const newJob = new Job({
          organisationId: data.organisationId,
          type: data.batchData.type.toString(),
          role: 'INTEL',
          renumerationValue: parseFloat(data.batchData.renumerationValue),
          renumerationCurrency: data.batchData.paymentCurrency.toString(),
          title: data.batchData.title.toString(),
          fulltimeStandardHoursStart: data.batchData.fulltimeStandardHoursStart.toString(),
          fulltimeStandardHoursEnd: data.batchData.fulltimeStandardHoursEnd.toString(),
          minShiftsBalance: parseInt(data.batchData.minShiftsBalance, 10),
          paymentOption: data.batchData.paymentOption,
          allowAdvancedPay: data.batchData.allowAdvancedPay,
          fulltimeNonWorkWeekDays,
          paymentMethodsIds,
          location: 'Yangon',
          locationCoordinates,
          private: true,
        });
        const retJob = await newJob.save().catch(e => error(e));
        const worker = await Worker.findById(workerId);
        const jobsAppliedIds = worker.jobsAppliedIds;
        jobsAppliedIds.push(retJob._id);
        await Worker.findOneAndUpdate(
          {
            _id: workerId,
          },
          {
            $addToSet: {
              workerPaymentMethodsIds: retWorkerPaymentMethod._id,
            },
            $set: {
              jobsAppliedIds,
              defaultWorkerPaymentMethodId: retWorkerPaymentMethod._id,
            },
          },
          { new: true },
        ).catch(e => error(e));

        // add job id in organisation
        const organisation = await Organisation.findById(data.organisationId);
        const jobsIds = organisation.jobsIds;
        jobsIds.push(retJob._id);
        await Organisation.findOneAndUpdate(
          { _id: data.organisationId },
          { $set: { jobsIds } },
          { new: true },
        ).catch(e => error(e));

        // create shift for job
        const startTime = moment(`${data.batchData.startDate.toString()} ${data.batchData.startTime.toString()}`,
          'YYYY-MM-DD hh:mmZ')
          .toDate();
        const endTime = moment(`${data.batchData.endDate.toString()} ${data.batchData.endTime.toString()}`,
          'YYYY-MM-DD hh:mmZ')
          .toDate();
        const newShift = new Shift({
          employerId: ctx.user.id,
          // employerId: 'hjicSQL7XBUkAeTVewcFBnU38483',
          organisationId: data.organisationId,
          jobId: retJob._id,
          startTime,
          endTime,
          workerId,
          workerAccepted: true,
        });
        const retShift = await newShift.save().catch(e => error(e));

        // add shift to the job
        const updateJobShift = await Job.findOneAndUpdate(
          { _id: retShift.jobId },
          {
            shiftsFilled: true,
            $addToSet: {
              shiftsIds: retShift._id,
            },
          },
          { new: true },
        ).catch(e => error(e));

        ret = updateJobShift ? 'success' : 'fail';
      } catch (e) {
        error('mutation:workerBatchUpload:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },

    alienBatchUpload: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('querying as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const newWorker = new AlienWorker({
          createOrganisationId: data.organisationId,
          phone: data.batchData.phone,
          name: data.batchData.name,
          identification: data.batchData.identification,
          verified: false,
        });
        const retCreateWorker = await newWorker.save();
        const workerId = retCreateWorker._id;
        const paymentMethod = await PaymentMethod.findOne({
          code: data.batchData.paymentMethod,
        }).catch(e => error(e));
        const workerPaymentMethod = new WorkerPaymentMethod({
          workerId,
          paymentMethodId: paymentMethod._id,
          coordinates: data.batchData.paymentCoordinates,
          verified: true,
        });
        const retWorkerPaymentMethod = await workerPaymentMethod.save().catch(e => error(e));

        const paymentMethodsIds = [];
        paymentMethodsIds.push(paymentMethod._id);

        const fulltimeNonWorkWeekDays = data.batchData.fulltimeNonWorkWeekDays.toString().split(',')
          .map(x => parseInt(x, 10));
        const newJob = new Job({
          organisationId: data.organisationId,
          type: data.batchData.type.toString(),
          role: 'INTEL',
          renumerationValue: parseFloat(data.batchData.renumerationValue),
          renumerationCurrency: data.batchData.paymentCurrency.toString(),
          title: data.batchData.title.toString(),
          fulltimeStandardHoursStart: data.batchData.fulltimeStandardHoursStart.toString(),
          fulltimeStandardHoursEnd: data.batchData.fulltimeStandardHoursEnd.toString(),
          minShiftsBalance: parseInt(data.batchData.minShiftsBalance, 10),
          paymentOption: data.batchData.paymentOption,
          fulltimeNonWorkWeekDays,
          paymentMethodsIds,
          location: 'Yangon',
          private: true,
        });
        const retJob = await newJob.save().catch(e => error(e));
        // const worker = await AlienWorker.findById(workerId);
        await AlienWorker.findOneAndUpdate(
          {
            _id: workerId,
          },
          {
            $addToSet: {
              workerPaymentMethodsIds: retWorkerPaymentMethod._id,
            },
            $set: {
              defaultWorkerPaymentMethodId: retWorkerPaymentMethod._id,
            },
          },
          { new: true },
        ).catch(e => error(e));

        const organisation = await Organisation.findById(data.organisationId).catch(e => error(e));
        const jobsIds = organisation.jobsIds;
        jobsIds.push(retJob._id);
        await Organisation.findOneAndUpdate(
          { _id: data.organisationId },
          { $set: { jobsIds } },
          { new: true },
        ).catch(e => error(e));
        const startTime = moment(`${data.batchData.startDate.toString()} ${data.batchData.startTime.toString()}`,
          'YYYY-MM-DD hh:mmZ')
          .toDate();
        const endTime = moment(`${data.batchData.endDate.toString()} ${data.batchData.endTime.toString()}`,
          'YYYY-MM-DD hh:mmZ')
          .toDate();
        const newShift = new Shift({
          employerId: ctx.user.id,
          // employerId: 'hjicSQL7XBUkAeTVewcFBnU38483',
          organisationId: data.organisationId,
          jobId: retJob._id,
          startTime,
          endTime,
          workerId,
          workerAccepted: true,
        });
        const retShift = await newShift.save().catch(e => error(e));
        const updateJobShift = await Job.findOneAndUpdate(
          { _id: retShift.jobId },
          {
            shiftsFilled: true,
            $addToSet: {
              shiftsIds: retShift._id,
            },
          },
          { new: true },
        ).catch(e => error(e));
        ret = updateJobShift ? 'success' : 'fail';
      } catch (e) {
        error('mutation:alienBatchUpload:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },
  },
};

module.exports = resolvers;
