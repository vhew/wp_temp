const { ObjectId } = require('mongoose').Types;
const error = require('debug')('ERROR');
const { ApolloError } = require('apollo-server-koa');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const {
  JobCategory,
  SkillCategory,
  SkillSubCategory,
  JobRole,
  JobSkill,
} = require('./model');
const utils = require('../../utils');


const resolvers = {
  Query: {
    jobCategories: async (root, data, ctx) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        ret = await JobCategory.find()
          .catch(e => error(e));
      } catch (e) {
        error('query:jobCategories:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    skillCategories: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        ret = await SkillCategory.find()
          .catch(err => error(err));
      } catch (e) {
        error('query:skillCategories:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    skillSubCategories: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');


      let ret;
      try {
        ret = await SkillSubCategory.find()
          .catch(err => error(err));
      } catch (e) {
        error('query:skillSubCategories:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    jobRoles: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        ret = await JobRole.find()
          .catch(err => error(err));
      } catch (e) {
        error('query:jobRoles:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    jobSkills: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let results;
      try {
        if (data.jobCategoryId != null) {
          let jobSkills;
          const { jobCategoryId } = data;
          try {
            const jobCategory = await JobCategory.findById(jobCategoryId);
            const jobRoles = await JobRole.find({
              _id: {
                $in: jobCategory.jobRolesIds,
              },
            }).catch(e => error(e));

            let allJobSkills = jobRoles.map(e => e.jobSkills);
            allJobSkills = allJobSkills.flat();

            const jobSkillsIds = allJobSkills.map(e => e.jobSkillId);

            jobSkills = await JobSkill.find({
              _id: {
                $in: jobSkillsIds,
              },
            }).catch(e => error(e));
          } catch (err) {
            throw new Error(err);
          }
          return jobSkills;
        }
        results = await JobSkill.find().catch(err => error(err));
      } catch (e) {
        error('query:jobSkills:', e);
      }
      return results;
    },
  },

  Mutation: {
    createJobCategory: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret;
      try {
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const jobCategory = new JobCategory(update);

        ret = await jobCategory.save()
          .catch(err => error(err));
      } catch (e) {
        error('mutation:createJobCategory:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    updateJobCategory: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret;
      try {
        const jobCategory = await JobCategory.findById(data.id);
        if (jobCategory == null) throw new ApolloError('Cannot find job category', 404);
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });

        ret = await JobCategory.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        ).catch(err => error(err));
      } catch (e) {
        error('mutation:updateJobCategory:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },
    createSkillCategory: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret;
      try {
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const skillCategory = new SkillCategory(update);

        ret = await skillCategory.save()
          .catch(err => error(err));
      } catch (e) {
        error('mutation:createSkillCategory:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    updateSkillCategory: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret;
      try {
        const skillCategory = await SkillCategory.findById(data.id);
        if (skillCategory == null) throw new ApolloError('Cannot find skill category', 404);
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });

        ret = await SkillCategory.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        ).catch(err => error(err));
      } catch (err) {
        error('mutation:updateSkillCategory:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    createSkillSubCategory: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret;
      try {
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const skillSubCategory = new SkillSubCategory(update);

        ret = await skillSubCategory.save()
          .catch(err => error(err));
      } catch (e) {
        error('mutation:createSkillSubCategory:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    updateSkillSubCategory: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';


      let ret;
      try {
        const skillSubCategory = await SkillSubCategory.findById(data.id);
        if (skillSubCategory == null) throw new ApolloError('Cannot find skill sub category', 404);

        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });

        ret = await SkillSubCategory.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        )
          .catch(err => error('mutation:updateSkillSubCategory:', err));
      } catch (err) {
        error('mutation:updateSkillSubCategory:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    createJobRole: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret;
      try {
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const jobRole = new JobRole(update);

        ret = await jobRole.save()
          .catch(err => error(err));
      } catch (e) {
        error('mutation:createJobRole:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    updateJobRole: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      let ret;
      try {
        const jobRole = await JobRole.findById(data.id);
        if (jobRole == null) throw new ApolloError('Cannot find job role', 404);

        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });

        ret = await JobRole.findOneAndUpdate({
          _id: data.id,
        }, update,
        { new: true })
          .catch(err => error('mutation:updateJobRole:', err));
      } catch (err) {
        error('mutation:updateJobRole:', err);
        throw ErrorHandlingService.getError(err);
      }

      return ret;
    },
    deleteJobRole: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret = 'success';
      try {
        const jobRole = await JobRole.findById(data.id);
        if (jobRole == null) throw new ApolloError('Cannot find job role', 404);

        await JobRole.deleteOne({
          _id: data.id,
        })
          .catch(() => { ret = 'error'; });
      } catch (err) {
        error('mutation:deleteJobRole:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    addJobRoleJobSkill: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      let ret;
      try {
        ret = await JobRole.findOneAndUpdate({
          _id: data.jobRoleId,
        }, {
          $addToSet: {
            jobSkills: { jobSkillId: data.jobSkillId, weight: data.weight },
          },
        }).catch(err => error(err));
      } catch (e) {
        error('mutation:addJobRoleJobSkill:', e);
        ctx.ctx.throw(500, e);
      }

      return ret;
    },
    createJobSkill: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';

      let ret;
      try {
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const jobSkill = new JobSkill(update);

        ret = await jobSkill.save()
          .catch(err => error(err));
      } catch (e) {
        error('mutation:createJobSkill:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    updateJobSkill: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      let ret;
      try {
        const jobSkill = await JobSkill.findById(data.id);
        if (jobSkill == null) throw new ApolloError('Cannot find job skill', 404);
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });

        ret = await JobSkill.findOneAndUpdate({
          _id: data.id,
        }, update,
        { new: true })
          .catch(err => error(err));
      } catch (err) {
        error('mutation:updateJobSkill:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },

  JobSkillRequirement: {
    jobSkill: async (e) => {
      const result = await JobSkill.findById(e.skillId)
        .catch(err => error('resolver:JobSkillRequirement:jobSkill:', err));
      return result;
    },
  },
  JobSkillLevel: {
    jobSkill: async (jobSkillLevel) => {
      const ret = await JobSkill.findOne({
        _id: jobSkillLevel.jobSkillId,
      }).catch(err => error('resolver:JobSkillLevel:jobSkill:', err));
      return ret;
    },
  },
  Worker: {
    jobSkills: async (worker) => {
      if (worker.jobSkills) {
        let ret;
        try {
          ret = await Promise.all(worker.jobSkills.map(async (js) => {
            const result = js;
            const jobSkill = await JobSkill
              .findOne({ _id: js.jobSkillId }).catch(err => error(err));
            if (worker.languageSetting === 'mm') {
              result.jobSkillName = jobSkill.nameMm;
            }
            if (worker.languageSetting === 'zg') {
              result.jobSkillName = jobSkill.nameZg;
            }
            return { ...result, jobSkill };
          }));
        } catch (err) {
          error('resolver:Worker:jobSkills:', err);
        }
        return ret;
      }
      return [];
    },
    // jobSkills: async (worker) => {
    //   const ret = await JobSkill.find({
    //     _id: {
    //       $in: worker.jobSkillsIds,
    //     },
    //   }).catch(err => error(err));
    //   return ret;
    // },
  },

  JobCategory: {
    jobRoles: async (jobCategory) => {
      const ret = await JobRole.find({
        _id: {
          $in: jobCategory.jobRolesIds,
        },
      }).catch(err => error('resolver:JobCategory:jobRoles:', err));
      return ret;
    },
    name: async (jobCategory, { lang }) => {
      if (lang == null) {
        return jobCategory.name;
      }
      if (lang === 'mm') {
        return jobCategory.nameMm;
      }
      if (lang === 'zg') {
        return jobCategory.nameZg;
      }
      return jobCategory.name;
    },
  },
  SkillCategory: {
    jobSkills: async (skillCategory) => {
      const ret = await JobSkill.find({
        _id: {
          $in: skillCategory.jobSkillsIds,
        },
      }).catch(err => error('resolver:SkillCategory:jobSkills:', err));
      return ret;
    },
    name: async (skillCategory, { lang }) => {
      if (lang == null) {
        return skillCategory.name;
      }
      if (lang === 'mm') {
        return skillCategory.nameMm;
      }
      if (lang === 'zg') {
        return skillCategory.nameZg;
      }
      return skillCategory.name;
    },
  },
  SkillSubCategory: {
    jobSkills: async (skillSubCategory) => {
      const ret = await JobSkill.find({
        _id: {
          $in: skillSubCategory.jobSkillsIds,
        },
      }).catch(err => error('resolver:SkillSubCategory:jobSkills:', err));
      return ret;
    },
  },
  JobSkillWeight: {
    jobSkill: async (jobSkillWeight) => {
      const ret = await JobSkill.findOne({
        _id: jobSkillWeight.jobSkillId,
      }).catch(err => error('resolver:JobSkillWeight:jobSkill:', err));
      return ret;
    },
  },
  JobRole: {
    jobSkills: async jobRole => jobRole.jobSkills,
    // const ret = await JobSkill.find({
    //   _id: {
    //     $in: jobRole.jobSkills.map(e => e.jobSkillId),
    //   },
    // }).catch(err => error(err));
    // return ret.map((e) => {
    //   const jobSkill = jobRole.jobSkills.find(skill => skill.jobSkillId === e.id);
    //   e.weight = jobSkill.weight;
    //   return e;
    // });
    name: async (jobRole, { lang }) => {
      if (lang == null) {
        return jobRole.name;
      }
      if (lang === 'mm') {
        return jobRole.nameMm;
      }
      if (lang === 'zg') {
        return jobRole.nameZg;
      }
      return jobRole.name;
    },
  },
  JobSkill: {
    name: async (jobSkill, { lang }) => {
      if (lang == null) {
        return jobSkill.name;
      }
      if (lang === 'mm') {
        return jobSkill.nameMm;
      }
      if (lang === 'zg') {
        return jobSkill.nameZg;
      }
      return jobSkill.name;
    },
  },
  Job: {
    jobCategory: async (job) => {
      const jobCategory = await JobCategory.findOne({
        jobRolesIds: { $in: [job.jobRoleId] },
      });
      return jobCategory;
    },
    jobRole: async (job) => {
      const jobRole = await JobRole.findById(job.jobRoleId);
      return jobRole;
    },
    industrySkill: async (job) => {
      // TODO: replace with findById()
      // ( need 'Industry' sub skill's id )
      const industryRole = await SkillSubCategory.findById(ObjectId('5f487f750821e5001b893693'));
      const industrySkillIds = industryRole
        ? industryRole.jobSkillsIds.map(e => e.toString())
        : [];
      const industrySkillId = job.jobSkillRequirements
        .map(e => e.skillId)
        .find(e => industrySkillIds.includes(e));
      const industrySkill = await JobSkill.findById(industrySkillId);
      return industrySkill;
    },
  },
};

module.exports = resolvers;
