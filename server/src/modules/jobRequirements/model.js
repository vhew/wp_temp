const mongoose = require('mongoose');

const JobCategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  nameMm: {
    type: String,
  },
  nameZg: {
    type: String,
  },
  nameZhHans: {
    type: String,
  },
  jobRolesIds: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'JobRole',
  }],
});
const SkillCategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  nameMm: {
    type: String,
  },
  nameZg: {
    type: String,
  },
  nameZhHans: {
    type: String,
  },
  jobSkillsIds: [{
    type: mongoose.Schema.Types.ObjectId,
  }],
});

const SkillSubCategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  jobSkillsIds: [{
    type: mongoose.Schema.Types.ObjectId,
  }],
  skillCategoryId: {
    type: mongoose.Schema.Types.ObjectId,
  },
});

const JobRoleSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    nameMm: {
      type: String,
    },
    nameZg: {
      type: String,
    },
    nameZhHans: {
      type: String,
    },
    description: {
      type: String,
    },
    titles: {
      type: Array,
    },
    jobSkills: [
      {
        type: Object,
      },
    ],
  },
  { timestamps: true },
);

const JobSkillSchema = new mongoose.Schema(
  {
    // jobRoleId: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: 'JobRole',
    //   required: true,
    // },
    name: {
      type: String,
      required: true,
    },
    nameMm: {
      type: String,
    },
    nameZg: {
      type: String,
    },
    nameZhHans: {
      type: String,
    },
    description: {
      type: String,
    },
    levels: [
      {
        level: Number,
        definition: String,
        definitionForWorker: String,
      },
    ],
    urls: [
      {
        type: String,
      },
    ],
    requiredDocuments: [
      {
        type: String,
      },
    ],
  },
  { timestamps: true },
);

const JobCategory = mongoose.model('JobCategory', JobCategorySchema);
const SkillCategory = mongoose.model('SkillCategory', SkillCategorySchema);
const SkillSubCategory = mongoose.model('SkillSubCategory', SkillSubCategorySchema);
const JobRole = mongoose.model('JobRole', JobRoleSchema);
const JobSkill = mongoose.model('JobSkill', JobSkillSchema);

module.exports = {
  JobCategory,
  JobRole,
  JobSkill,
  SkillCategory,
  SkillSubCategory,
};
