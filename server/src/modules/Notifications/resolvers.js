// const cron = require('node-cron');
const {
  ApolloError,
} = require('apollo-server-koa');
const error = require('debug')('ERROR');
const {
  Employer,
  Notification,
  Worker,
} = require('./../models');
const ERROR_CODES = require('../../../src/utils/errorCodes');

// cron.schedule('44 8 * * *', async () => {
//   console.log('running cron job3');
//   const shifts = await Shift.find(
//     {
//       workComplete: false,
//     },
//   );
//   debug('shifts', shifts);
//   const messages = await Promise.all(shifts.map(async (shift) => {
//     const organisation = await Organisation.findById(shift.organisationId);
//     debug('organisation', organisation);
//     if (organisation.settings.length === 0) {
//       return null;
//     }
//     const timesheetFeatureLevel = organisation.settings
//       .find(e => e.key === 'TIMESHEETING_FEATURE_LEVEL');
//     if (timesheetFeatureLevel.value !== 'NULL') {
//       const startTime = moment.utc(shift.startTime).local();
//       const endTime = moment.utc(shift.endTime).local();
//       const checkinScheduleHour = startTime.format('HH');
//       const checkoutScheduleHour = endTime.format('HH');
//       const checkinScheduleMinutes = startTime.format('mm');
//       const checkoutScheduleMinutes = endTime.format('mm');
//       return {
//         checkinData: {
//           userType: 'worker',
//           id: shift.workerId,
//           messageType: 'push',
//           message: {
//             senderId: shift.employerId,
//             receiverId: shift.workerId,
//             title: 'Alert!',
//             body: '15min till work. Don’t forget to checkin.',
//             fcm_options: {
//               analyticsLabel: 'notification_individual_hired',
//             },
//             data: {
//               title: 'Alert!',
//               body: '15min till work. Don’t forget to checkin.',
//               jobId: shift.jobId,
//               shiftId: shift.id,
//             },
//           },
//         },
//         checkinScheduleHour,
//         checkinScheduleMinutes,
//         checkoutData: {
//           userType: 'worker',
//           id: shift.workerId,
//           messageType: 'push',
//           message: {
//             senderId: shift.employerId,
//             receiverId: shift.workerId,
//             title: 'Alert!',
//             body: 'Don’t forget to checkout. Hope you had a good day',
//             fcm_options: {
//               analyticsLabel: 'notification_individual_hired',
//             },
//             data: {
//               title: 'Alert!',
//               body: 'Don’t forget to checkout. Hope you had a good day',
//               jobId: shift.jobId,
//               shiftId: shift.id,
//             },
//           },
//         },
//         checkoutScheduleHour,
//         checkoutScheduleMinutes,
//       };
//     }
//     return null;
//   }));
//   debug('messages', messages);
//   // cron.schedule('20 * * * * *', () => {
//   //   console.log('running cron job4');
//   // });

//   messages.forEach((message) => {
//     cron.schedule(`${message.checkinScheduleMinutes}
// ${message.checkinScheduleHour} * * *`, () => {
//       debug('running cron for checkin');
//       notifications.send(message.checkinData);
//     });
//     cron.schedule(`${message.checkoutScheduleMinutes}
// ${message.checkoutScheduleHour} * * *`, () => {
//       debug('running cron for checkout');
//       notifications.send(message.checkoutData);
//     });
//   });
//   // notifications.send(
//   //   {
//   //     userType: 'worker',
//   //     id: data.workerId,
//   //     messageType: 'hired',
//   //     message,
//   //   },
//   // );
// });
const resolvers = {
  Query: {
    notifications: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      if (ctx.user.id !== data.senderId && ctx.user.id !== data.receiverId) {
        const { UNAUTHORIZED_NOTIFICATION_QUERY } = ERROR_CODES;
        const { message, code } = UNAUTHORIZED_NOTIFICATION_QUERY;
        throw new ApolloError(message, code);
      }
      const params = {};
      Object.keys(data).forEach((key) => {
        params[key] = data[key];
      });
      const ret = await Notification.find(params)
        .catch(err => error('query:notifications:', err));
      return ret;
    },
  },

  Mutation: {
    updateNotification: async (root, data, ctx) => {
      let ret;
      try {
        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Notification.findOneAndUpdate(
          { _id: data.notificationId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateNotification:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
  },

  Person: {
    __resolveType(obj) {
      if (obj.organisationsIds) {
        return 'Employer';
      }
      if (obj.jobsIds) {
        return 'Organisation';
      }
      return 'Worker';
    },
  },

  Notification: {
    sender: async (notification) => {
      const employer = await Employer.findById(notification.senderId);
      const worker = await Worker.findById(notification.senderId);
      return employer || worker;
    },
    receiver: async (notification) => {
      const employer = await Employer.findById(notification.receiverId);
      const worker = await Worker.findById(notification.receiverId);
      return employer || worker;
    },
  },
};

module.exports = resolvers;
