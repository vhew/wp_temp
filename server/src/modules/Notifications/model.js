const mongoose = require('mongoose');

const NotificationSchema = new mongoose.Schema({
  senderId: {
    type: String,
    required: true,
  },
  receiverId: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  body: {
    type: String,
    required: true,
  },
  isRead: {
    type: Boolean,
  },
  notificationType: {
    type: String,
  },
}, { timestamps: true });

const Notification = mongoose.model('Notification', NotificationSchema);

module.exports = {
  Notification,
};
