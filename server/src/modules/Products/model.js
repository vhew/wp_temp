const mongoose = require('mongoose');

const ReceiptSchema = new mongoose.Schema({
  productId: {
    type: String,
  },
  workerId: {
    type: String,
  },
  shiftId: {
    type: String,
  },
  transactionRef: {
    type: String,
  },
  password: {
    type: String,
  },
  organisationId: {
    type: String,
  },
  jobId: {
    type: String,
  },
  paidLevel: {
    type: Number,
  },
  complete: {
    type: Boolean,
  },
  code: {
    type: String,
  },
  payCycleStart: {
    type: String,
  },
  payCycleEnd: {
    type: String,
  },
  receivedAt: {
    type: Date,
  },
  receivedLocation: {
    type: String,
  },
}, { timestamps: true });

const ProductSchema = new mongoose.Schema({
  organisationId: {
    type: String,
  },
  paymentMethodsIds: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PaymentMethod',
    },
  ],
  lumpSum: {
    type: Number,
  },
  instalment: {
    type: Number,
  },
  listingStartDate: {
    type: Date,
  },
  listingEndDate: {
    type: Date,
  },
  categoryHierarchies: {
    type: Array,
  },
  title: {
    type: String,
  },
  description: {
    type: String,
  },
  price: {
    type: Number,
  },
  priceRRP: {
    type: Number,
  },
  fee: {
    type: Number,
  },
  currency: {
    type: String,
  },
  tagLine: {
    type: String,
  },
  deposit: {
    type: Number,
  },
  tags: {
    type: Array,
  },
  productMetadata: {
    type: Object,
  },
  itemAttributes: {
    type: Array,
  },
  benefits: {
    type: Array,
  },
  pickupInstructions: {
    type: String,
  },
  pickupContact: {
    type: String,
  },
}, { timestamps: true });

const Product = mongoose.model('Product', ProductSchema);
const Receipt = mongoose.model('Receipt', ReceiptSchema);

module.exports = {
  Product,
  Receipt,
};
