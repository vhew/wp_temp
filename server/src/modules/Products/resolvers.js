// const cron = require('node-cron');
const debug = require('debug')('DEBUG');
const error = require('debug')('ERROR');
const moment = require('moment');
const { ApolloError, AuthenticationError, ForbiddenError } = require('apollo-server-koa');
const WorkerDetailService = require('../../services/WorkerDetailService');
const helpers = require('./helpers');
const utils = require('../../utils');
const microservices = require('./microservices');
const {
  Job,
  Worker,
  Organisation,
  WorkToken,
} = require('./../../../models/mongodb/index');
const {
  Product,
  Receipt,
} = require('./model');
const ErrorHandlingService = require('../../services/ErrorHandlingService');

const resolvers = {
  Query: {
    products: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let result;
      try {
        result = await Product.find();
        if (data.organisationId != null) {
          result = await Product.find({ organisationId: data.organisationId });
          return result;
        }
        if (result == null) {
          throw new ApolloError('Products not found', 404);
        }
      } catch (e) {
        error('query:products:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },

    products2: async (root, data) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // debug('query jobs as user(%s): \n%O', ctx.user.id, data);

      const products = [];
      let hasMore = false;
      // const {
      //   page,
      // } = data;

      let { lastProductRankIndex, pageSize } = data;
      try {
        const result = await Product.find();

        const productsIds = result.map(e => e._id);

        let index = lastProductRankIndex ? lastProductRankIndex + 1 : 0;

        if (!pageSize) {
          pageSize = productsIds.length;
        }

        while (products.length < pageSize && index < productsIds.length) {
          const product = await Product.findById(productsIds[index]);

          if (product) products.push(product);

          lastProductRankIndex = index;
          index += 1;
        }

        hasMore = index < productsIds.length;
        // jobs = jobs.slice((page - 1) * pageSize, page * pageSize);
      } catch (err) {
        error('query:products2:', err);
        throw ErrorHandlingService.getError(err);
      }
      return {
        products,
        hasMore,
        lastProductRankIndex,
      };
    },
    product: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let result;
      try {
        result = await Product.findById(data.id);

        if (result == null) {
          throw new ApolloError('Products not found', 404);
        }
      } catch (e) {
        error('query:product:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },
    receipts: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let result;
      try {
        result = await Receipt.find();

        if (data.organisationId != null) {
          result = await Receipt.find({ organisationId: data.organisationId });
          return result;
        }
        if (result == null) {
          throw new ApolloError('Receipts not found', 404);
        }
      } catch (e) {
        error('query:receipts:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },
    receipt: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let result;
      try {
        if (data.code != null) {
          result = await Receipt.findOne({
            code: data.code,
          });
          return result;
        }
        if (data.shiftId != null) {
          result = await Receipt.findOne({
            shiftId: data.shiftId,
            workerId: data.workerId,
          });
          return result;
        }
        result = await Receipt.findOne({
          productId: data.productId,
          workerId: data.workerId,
        });
      } catch (e) {
        error('query:receipt:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },
    payslip: async (root, data, ctx) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let result;
      try {
        if (data.code != null) {
          result = await Receipt.findOne({
            code: data.code,
          });
          return result;
        }
        if (data.password != null) {
          result = await Receipt.findOne({
            password: data.password,
          });
          return result;
        }
        if (data.transactionRef != null) {
          result = await Receipt.findOne({
            transactionRef: data.transactionRef,
          });
          return result;
        }
        if (data.complete != null) {
          result = await Receipt.findOne({
            workerId: data.workerId,
            complete: data.complete,
            productId: { $exists: false },
          });
          return result;
        }
        result = await Receipt.findOne({
          workerId: data.workerId,
        }).sort({ createdAt: -1 });
      } catch (e) {
        error('query:payslip:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },
    estimatedProductBalance: async (root, data, ctx) => {
      const ret = [];
      try {
        // API Permissions
        const target = {
          id: data.workerId,
          type: 'Worker',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        // if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

        // Prepare data
        const payCycle = await helpers.getPayCycle({
          jobId: data.jobId,
        });
        let { payCycleStartTime, payCycleEndTime } = payCycle;
        const job = await Job.findById(data.jobId);
        const product = await Product.findById(data.productId);
        const { fee } = product;
        let currentPayCycleDays = moment(payCycleEndTime).diff(payCycleStartTime, 'days');
        let salaryAmountForOneDay = job.renumerationValue / currentPayCycleDays;
        let amount = Math.ceil(fee / salaryAmountForOneDay) * salaryAmountForOneDay;


        ret.push({
          amount,
          currency: 'MMK',
          deductDate: null,
          completed: false,
        });
        for (let i = 1; i <= product.lumpSum; i += 1) {
          currentPayCycleDays = moment(payCycleEndTime).diff(payCycleStartTime, 'days');
          salaryAmountForOneDay = job.renumerationValue / currentPayCycleDays;
          const instalmentAmount = product.price / product.lumpSum;
          amount = Math.ceil(instalmentAmount / salaryAmountForOneDay) * salaryAmountForOneDay;
          ret.push({
            amount,
            currency: 'MMK',
            deductDate: moment(payCycleEndTime).add(-6, 'days'),
            completed: false,
          });
          payCycleStartTime = moment(payCycleStartTime).add(1, 'M');
          payCycleEndTime = moment(payCycleEndTime).add(1, 'M');
        }
        const totalPaidAmount = ret
          .map(e => e.amount)
          .reduce((a, b) => a + b, 0);
        const totalProductAmount = product.price + product.fee;
        const paybackAmount = totalPaidAmount - totalProductAmount;
        if (Math.round(paybackAmount) !== 0) {
          ret.push({
            hasPayBack: true,
            amount: paybackAmount,
            deductDate: moment(payCycleEndTime).add(-1, 'M').add(-1, 'day'),
          });
        }
      } catch (err) {
        error('query:estimatedProductBalance:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
  },

  Mutation: {
    /*
      High level algorithm
      1. Identify and Write WorkTokens
      2. Get Amount Service
      3. WRITE relevant docs into database
    */
    runProductTimeSheetService: async (root, data, ctx) => {
      // API Permissions
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      // Input validation
      const {
        jobId,
        productId,
        workerId,
      } = data;
      const worker = await Worker.findById(workerId);
      if (worker == null) throw new Error('worker not found');
      const job = await Job.findById(jobId);
      if (job == null) throw new Error('job not found');
      const product = await Product.findById(productId);
      if (product == null) throw new Error('product not found');
      // const receipt = await Receipt.findOne({ productId, workerId });

      // Prepare initial data
      const workerPaymentMethodId = worker.defaultWorkerPaymentMethodId;
      const payCycle = await helpers.getPayCycle({
        jobId,
        forPreviousPayCycle: false,
      });
      const {
        payCycleStartTime,
        payCycleEndTime,
      } = payCycle;
      const currentPayCycleDays = moment(payCycleEndTime).diff(payCycleStartTime, 'days');
      const salaryAmountForOneDay = job.renumerationValue / currentPayCycleDays;
      const instalmentAmount = product.price / product.lumpSum;
      const totalAmount = Math.ceil(instalmentAmount / salaryAmountForOneDay)
        * salaryAmountForOneDay;
      const numberRequested = totalAmount / salaryAmountForOneDay;
      const unpaidWorkTokens = await microservices
        .unpaidWorkTokenService({ jobId, payCycle });
      const usingTimesheetingFeatureLevel = await helpers
        .getOrganisationUsingTimesheetingFeatureLevel({ jobId });
      let workTokens = [];

      // Processing with error handling
      try {
        /* 1. Identify and Write WorkTokens START */
        if (unpaidWorkTokens.length === 0) {
          if (usingTimesheetingFeatureLevel !== 'FULL') {
            workTokens = await microservices.createWorkTokensService({
              jobId,
              workerId,
              productId,
              numberRequested,
              payCycle,
            });
          }
          if (usingTimesheetingFeatureLevel === 'FULL') {
            throw new Error('Not enough shifts to satisfy request');
          }
        } else {
        // eslint-disable-next-line no-lonely-if
          if (usingTimesheetingFeatureLevel !== 'FULL') {
            workTokens = unpaidWorkTokens.slice(0, numberRequested);

            await WorkToken.updateMany({
              _id: {
                $in: workTokens.map(workToken => workToken._id),
              },
            }, {
              $set: {
                payNow: true,
              },
            });
            if (numberRequested > unpaidWorkTokens.length) {
              const additionalWorkTokensNeedToCreated = numberRequested - workTokens.length;
              const newWorkTokens = await microservices.createWorkTokensService({
                jobId,
                workerId,
                productId,
                numberRequested: additionalWorkTokensNeedToCreated,
                payCycle,
              });
              workTokens = [...workTokens, ...newWorkTokens];
            }
          }

          if (usingTimesheetingFeatureLevel === 'FULL') {
            workTokens = unpaidWorkTokens.slice(0, numberRequested);
            await WorkToken.updateMany({
              _id: {
                $in: workTokens.map(workToken => workToken._id),
              },
            }, {
              $set: {
                confirmed: null,
              },
            });
          }
        }
        /* 1. Identify and Write WorkTokens END */

        /* 2. Get Amount Service START */
        const amount = await microservices.workTokenAmountService({
          jobId,
          payCycle,
        });
        /* 2. Get Amount Service END */

        /* 3. WRITE relevant docs into database START */
        const {
          coordinates,
          currency,
        } = await WorkerDetailService.getPaymentMethod({ workerPaymentMethodId });
        const workTokenValues = await helpers.createWorkTokenValues2point0({
          workTokens,
          amount,
          fee: 0,
          currency,
          method: 'PRODINST',
          coordinates,
        });
        /* 3. WRITE relevant docs into database END */

        return workTokenValues;
      } catch (err) {
        error('mutation:runProductTimeSheetService:', err);
        throw new Error(err);
      }
    },
    /*
      High level algorithm
      1. Identify and Write WorkTokens
      2. Get Amount Service
      3. WRITE relevant docs into database
    */
    requestProductPay: async (root, data, ctx) => {
      // Processing with error handling
      try {
        // API permissions
        const target = {
          id: data.workerId,
          type: 'Worker',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

        // Input validation
        const {
          jobId,
          productId,
          workerId,
          workerPaymentMethodId,
        } = data;
        const worker = await Worker.findById(workerId);
        if (worker == null) throw new Error('worker not found');
        const job = await Job.findById(jobId);
        if (job == null) throw new Error('job not found');
        const product = await Product.findById(productId);
        if (product == null) throw new Error('product not found');

        // Prepare initial data
        const payCycle = await helpers.getPayCycle({
          jobId,
          forPreviousPayCycle: false,
        });

        const { payCycleStartTime, payCycleEndTime } = payCycle;
        const productFee = product.fee;
        const currentPayCycleDays = moment(payCycleEndTime).diff(payCycleStartTime, 'days');
        const salaryAmountForOneDay = job.renumerationValue / currentPayCycleDays;
        const totalAmount = Math.ceil(productFee / salaryAmountForOneDay) * salaryAmountForOneDay;
        const numberRequested = totalAmount / salaryAmountForOneDay;
        let workTokens = [];
        const unpaidWorkTokens = await microservices
          .unpaidWorkTokenService({ jobId, payCycle });
        const usingTimesheetingFeatureLevel = await helpers
          .getOrganisationUsingTimesheetingFeatureLevel({ jobId });


        /* 1. Identify and Write WorkTokens START */
        if (unpaidWorkTokens.length === 0) {
          if (usingTimesheetingFeatureLevel !== 'FULL') {
            workTokens = await microservices.createWorkTokensService({
              jobId,
              workerId,
              productId,
              numberRequested,
              payCycle,
            });
          }
          if (usingTimesheetingFeatureLevel === 'FULL') {
            throw new ForbiddenError('Not enough shifts to satisfy request');
          }
        } else {
          // eslint-disable-next-line no-lonely-if
          if (usingTimesheetingFeatureLevel !== 'FULL') {
            workTokens = unpaidWorkTokens.slice(0, numberRequested);

            await WorkToken.updateMany({
              _id: {
                $in: workTokens.map(workToken => workToken._id),
              },
            }, {
              $set: {
                payNow: true,
              },
            });
            if (numberRequested > unpaidWorkTokens.length) {
              const additionalWorkTokensNeedToCreated = numberRequested - workTokens.length;
              const newWorkTokens = await microservices.createWorkTokensService({
                jobId,
                workerId,
                productId,
                numberRequested: additionalWorkTokensNeedToCreated,
                payCycle,
              });
              workTokens = [...workTokens, ...newWorkTokens];
            }
          }

          if (usingTimesheetingFeatureLevel === 'FULL') {
            workTokens = unpaidWorkTokens.slice(0, numberRequested);
            await WorkToken.updateMany({
              _id: {
                $in: workTokens.map(workToken => workToken._id),
              },
            }, {
              $set: {
                confirmed: null,
              },
            });
          }
        }
        /* 1. Identify and Write WorkTokens END */

        /* 2. Get Amount Service START */
        const amount = await microservices.workTokenAmountService({
          jobId,
          payCycle,
        });
        /* 2. Get Amount Service END */

        /* 3. WRITE relevant docs into database START */
        const {
          coordinates,
          currency,
        } = await WorkerDetailService.getPaymentMethod({ workerPaymentMethodId });

        const workTokenValues = await helpers.createWorkTokenValues2point0({
          workTokens,
          amount,
          fee: 0,
          currency,
          method: 'PRODFEE',
          coordinates,
        });
        /* 3. WRITE relevant docs into database END */

        return workTokenValues;
      } catch (err) {
        error('mutation:requestProductPay:', err);
        throw ErrorHandlingService.getError(err);
      }
    },
    async createProduct(
      root,
      data,
      ctx,
    ) {
      // Process with error handling
      let ret;
      try {
        // API Permissions
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        debug('mutation createProduct as user(%s): \n%O', ctx.user.id, data);

        // construct data
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const newProduct = new Product(update);
        ret = await newProduct.save();
      } catch (e) {
        error('mutation:createProduct:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },
    async updateProduct(
      root,
      data,
      ctx,
    ) {
      // Process with error handling
      let ret;
      try {
        // API Permissions
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');

        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Product.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateProduct:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async createReceipt(
      root,
      data,
      ctx,
    ) {
      // Process with error handling
      let ret;
      try {
        // API Permissions
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');

        // construct data
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        const newReceipt = new Receipt(update);
        ret = await newReceipt.save();
        // if (ret != null) {
        //   const worker = await Worker.findById(data.workerId);
        //   const job = await Job.findById(data.jobId);
        //   const product = await Product.findById(data.productId);
        //   const shiftId = job.shiftsIds[0];
        //   const shift = await Shift.findById(shiftId);
        //   const title = 'Product Purchased';
        //   const body = `${worker.name} purchase successful for ${product.title}`;
        //   // const body = `${worker.name} has applied to ${job.title}.`;
        //   const message = {
        //     title,
        //     body,
        //     data: {
        //       title,
        //       body,
        //       jobId: data.jobId,
        //       shiftId,
        //     },
        //     fcm_options: {
        //       analyticsLabel: '',
        //     },
        //   };
        //   notifications.send(
        //     {
        //       userType: 'employer',
        //       id: shift.employerId,
        //       messageType: 'push',
        //       message,
        //     },
        //   );
        // }
      } catch (e) {
        error('mutation:createReceipt:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    async updatePayslip(
      root,
      data,
      ctx,
    ) {
      // Process with error handling
      let ret;
      try {
        // // API Permissions
        // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        // if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');

        // construct data
        const update = {};
        const ignoreKeys = [
          'workerId',
          'transactionRef',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Receipt.findOneAndUpdate(
          { workerId: data.workerId, transactionRef: data.transactionRef },
          update,
          { new: true },
        );
      } catch (e) {
        error('mutation:updatePayslip:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    async updateReceipt(
      root,
      data,
      ctx,
    ) {
      // Process with error handling
      let ret;
      try {
        // API Permissions
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');

        // construct data
        const update = {};
        const ignoreKeys = [
          'productId',
          'workerId',
          'organisationId',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Receipt.findOneAndUpdate(
          { productId: data.productId, workerId: data.workerId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateReceipt:', err);
        throw new ErrorHandlingService(err);
      }
      return ret;
    },
    addToCartProduct: async (root, data, ctx) => {
      // Process with error handling
      let ret;
      try {
        // API Permissions
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

        ret = await Worker.findOneAndUpdate({
          _id: ctx.user.id,
        }, {
          $addToSet: {
            addToCartProductsIds: data.productId,
          },
        }).catch(err => error(err));
      } catch (e) {
        error('mutation:addToCartProduct:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },
    removeAddToCartProduct: async (root, data, ctx) => {
      let ret;
      try {
        // API Permissions
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

        // Process with error handling
        ret = await Worker.findOneAndUpdate({
          _id: ctx.user.id,
        }, {
          $pull: {
            addToCartProductsIds: data.productId,
          },
        }).catch(err => error('mutation:removeAddToCartProduct:', err));
      } catch (e) {
        error('mutation:removeAddToCartProduct:', e);
        throw new ErrorHandlingService(e);
      }
      return ret;
    },
  },

  Product: {
    organisation: async (product) => {
      const result = await Organisation.findById(product.organisationId);
      return result;
    },
    isPurchased: async (product, { workerId }) => {
      if (workerId == null) {
        debug('I am here.....');
        return false;
      }
      const workToken = await WorkToken.findOne({ productId: product.id, workerId });
      if (workToken == null) return false;
      debug('workToken.....', workToken);
      if (workToken.workerId === workerId) {
        debug('true.....');
        return true;
      }
      return false;
    },
    finishStatus: async (product, { workerId }) => {
      const result = await Receipt.findOne({
        productId: product.id,
        workerId,
      });
      if (result == null) {
        return '';
      }
      if (result.receivedAt != null) {
        return 'received';
      }
      return 'confirmed';
    },

  },

  Worker: {
    addToCartProducts: async (worker) => {
      if (worker.addToCartProductsIds == null) {
        return [];
      }
      const results = await Promise.all(worker.addToCartProductsIds.map(async (id) => {
        const object = await Product.findById(id);
        return object;
      }));
      return results;
    },
  },

  WorkToken: {
    product: async (worktoken) => {
      if (worktoken.type !== 'PRODUCT') {
        return {};
      }
      const result = await Product.findById(worktoken.productId);
      return result;
    },
  },

  Receipt: {
    product: async (receipt) => {
      const result = await Product.findById(receipt.productId);
      return result;
    },
    worker: async (receipt) => {
      const result = await Worker.findById(receipt.workerId);
      return result;
    },
    organisation: async (receipt) => {
      const result = await Organisation.findById(receipt.organisationId);
      return result;
    },
  },
};

module.exports = resolvers;
