const mongoose = require('mongoose');

const ContractSchema = new mongoose.Schema(
  {
    status: { type: String },
    rewards: {
      type: Array,
    },
    contractDetails: {
      type: Object,
    },
  },
  { timestamps: true },
);


const Contract = mongoose.model('Contract', ContractSchema);

module.exports = {
  Contract,
};
