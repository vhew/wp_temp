const debug = require('debug')('DEBUG');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const moment = require('moment');
const { ApolloError } = require('apollo-server-koa');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const { Contract } = require('./model');
const discounts = require('../../../discounts');
const {
  Worker,
  DiscountToken,
} = require('../../../models/mongodb/');

const resolvers = {
  Query: {
    contracts: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.shiftId,
      //   type: 'Shift',
      // };
      // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('querying fiatTokens as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const contracts = await Contract.find({
          'rewards.workerId': data.workerId,
          'contractDetails.type': data.contractType.toString(),
        })
          .catch(err => error(err));
        debug('discountTokens %O', contracts);
        ret = contracts;
      } catch (e) {
        error('query:contracts:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
  },
  Mutation: {
    async createContract(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        // construct data
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        // create Contract
        const newContract = new Contract(update);
        ret = await newContract.save().catch(e => error(e));
      } catch (err) {
        error('mutation:createContract:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async updateContract(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const contract = await Contract.findById(data.id);
        if (contract == null) throw new ApolloError('Cannot find contract', 404);
        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Contract.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        ).catch(e => error(e));
      } catch (err) {
        error('mutation:updateContract:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async runCampaign(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
          'contractId',
          'discount',
        ];
        Object.keys(data.campaignData).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data.campaignData[key];
          }
        });
        const retUpdateWorker = await Worker.findOneAndUpdate(
          { _id: data.campaignData.id },
          update,
          { new: true },
        ).catch(e => error(e));
        if (retUpdateWorker) {
          // Update contract
          info('Continue update contract');
          const retUpdateContract = await Contract.findOneAndUpdate(
            { _id: data.campaignData.contractId },
            {
              status: 'COMPLETE',
            },
            { new: true },
          ).catch(e => error(e));

          // Create discount token
          if (retUpdateContract) {
            info('Continue create discount token', retUpdateContract);
            if (retUpdateContract.status === 'COMPLETE') {
              const discount = discounts
                .find(
                  discountData => discountData.type === retUpdateContract.contractDetails.type,
                );
              const newDiscountToken = new DiscountToken({
                workerId: retUpdateWorker.id,
                discountPercent: discount.discountPercent,
                discountFixed: discount.discountFixed,
                status: 'AVAILABLE',
                used: false,
                expiryDate: moment.utc().add(2, 'days'),
              });
              const retCreateDiscountToken = await newDiscountToken.save();
              if (retCreateDiscountToken) {
                ret = await Contract.findOneAndUpdate(
                  {
                    _id: data.campaignData.contractId,
                    'rewards.workerId': data.campaignData.id,
                  },
                  {
                    $set: {
                      'rewards.$.rewardId': retCreateDiscountToken.id,
                      'rewards.$.rewardType': 'Discount',
                    },
                  },
                  { new: true },
                ).catch(e => error(e));
              }
            }
          }
        }
        // info('retUpdateWorker', retUpdateWorker);
      } catch (err) {
        error('mutation:runCampaign:', err);
        ctx.ctx.throw(500, err);
      }
      return ret ? 'success' : 'fail';
    },
    async checkContractComplete(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);

        info('contracts data', data);
        const contracts = await Contract.find({
          'rewards.workerId': data.workerId,
          'contractDetails.type': data.contractType.toString(),
          status: 'COMPLETE',
        }).catch(e => error(e));
        info('goal contracts', contracts);
        ret = (contracts.length > 0);
        info('contract ret', ret);
      } catch (err) {
        error('mutation:checkContractComplete:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async checkContractExist(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const worker = await Worker.findById(data.workerId);
        if (worker == null) throw new ApolloError('Cannot find worker', 404);

        info('contracts data', data);
        const contracts = await Contract.find({
          'rewards.workerId': data.workerId,
          'contractDetails.type': data.contractType.toString(),
        }).catch(e => error(e));

        info('goal contracts', contracts);
        ret = (contracts.length > 0);
        info('contract ret', ret);
      } catch (err) {
        error('mutation:checkContractExist:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async checkGoalComplete(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        if (data.goalId === 'GOAL1') {
          const contracts = await Contract.find({
            'rewards.workerId': data.workerId,
            'contractDetails.type': {
              $regex: data.goalId,
              $options: 'i',
            },
          }).catch(e => error(e));
          info('goal contracts', contracts);
          ret = contracts.every(contract => contract.status === 'COMPLETE');
          info('contract ret', ret);
        }
      } catch (err) {
        error('mutation:checkGoalComplete:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
  },
};

module.exports = resolvers;
