const {
  WorkTokenValue,
} = require('../../../models/mongodb/');

const resolvers = {
  Query: {
    disbursement: async () => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.shiftId,
      //   type: 'Shift',
      // };
      // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('querying fiatTokens as user(%s): \n%O', ctx.user.id, data);

      const ret = WorkTokenValue.aggregate([
        {
          $match: {
            expectedWorkTokenWorkerId: '0qGJKUACMiPfeHaEcRwVkFAskJ32',
          },
        },
        {
          $group: {
            _id: '$expectedWorkTokenWorkerId',
            amount: { $sum: '$amount' },
          },
        },

      ]);
      return ret;
      // return [{
      //   salary: 100000,
      //   amount: 30000,
      //   fee: 1000,
      //   discount: 1000,
      // }];
    },
  },
};

module.exports = resolvers;
