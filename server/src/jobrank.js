const info = require('debug')('INFO');
const error = require('debug')('ERROR');
// const warn = require('debug')('WARN');
const debug = require('debug')('DEBUG');
const amqp = require('amqplib');

const generateUuid = () => {
  const uuid = Math.random().toString()
        + Math.random().toString()
        + Math.random().toString();
  return uuid;
};

/**
 * @description connect to rabbitMQ and return connection data
 */
const connectToRabbitMq = async () => {
  // connect to rabbitmq
  const amqpUrl = process.env.AMQP_URL || 'amqp://localhost';
  let connection;
  let channel;
  let q;
  let correlationId;
  try {
    info('rpcJobrank connecting to rabbitmq at: %s', amqpUrl);
    connection = await amqp.connect(amqpUrl);
    channel = await connection.createChannel();
    q = await channel.assertQueue('', { exclusive: true });
    correlationId = generateUuid();
  } catch (err) {
    error('%O', err);
  }

  return {
    channel,
    correlationId,
    connection,
    q,
  };
};

const rpcJobrank = async (message) => {
  debug(message);
  const promise = new Promise(async (resolve, reject) => {
    const queue = 'rpc_jobrank';
    const {
      channel, connection, correlationId, q,
    } = await connectToRabbitMq();

    if (!q) {
      error('Unknown Queue. Make sure that RabbitMQ is running');
      return;
    }

    // create queue for reply
    try {
      const processResult = async (msg) => {
        if (msg.properties.correlationId === correlationId) {
          const data = JSON.parse(msg.content.toString());
          info('RPC reply');
          debug({ data });
          // discconect from rabitmq
          try {
            await channel.close();
            await connection.close();
          } catch (err) {
            error('RPC close: \n%O', err);
          }
          resolve(data);
        }
      };
      channel.consume(q.queue, processResult);
    } catch (err) {
      error('RPC response: \n%O', err);
      reject(err);
    }

    // send request
    const data = {
      message,
    };
    const content = new Buffer.from(JSON.stringify(data));
    const options = {
      correlationId,
      replyTo: q.queue,
    };
    info('RPC request');
    debug({ data, options });
    try {
      await channel.sendToQueue(
        queue,
        content,
        options,
      );
    } catch (err) {
      error('RPC request: \n%O', err);
    }
  }); // Promise
  return promise;
}; // rcpJobrank

const createJobMatchingScore = async (message) => {
  debug(message);
  const promise = new Promise(async (resolve, reject) => {
    const queue = 'job_matching';
    const {
      channel, connection, correlationId, q,
    } = await connectToRabbitMq();

    if (!q) {
      error('Unknown Queue. Make sure that RabbitMQ is running');
      return;
    }

    // create queue for reply
    try {
      const processResult = async (msg) => {
        if (msg.properties.correlationId === correlationId) {
          const data = JSON.parse(msg.content.toString());
          info('RPC reply');
          debug({ data });
          // discconect from rabitmq
          try {
            await channel.close();
            await connection.close();
          } catch (err) {
            error('RPC close: \n%O', err);
          }
          resolve(data);
        }
      };
      channel.consume(q.queue, processResult);
    } catch (err) {
      error('RPC response: \n%O', err);
      reject(err);
    }

    // send request
    const data = {
      message,
    };
    const content = new Buffer.from(JSON.stringify(data));
    const options = {
      correlationId,
      replyTo: q.queue,
    };
    info('RPC request');
    debug({ data, options });
    try {
      await channel.sendToQueue(
        queue,
        content,
        options,
      );
    } catch (err) {
      error('RPC request: \n%O', err);
    }
  }); // Promise
  return promise;
};

const jobrank = {
  getJobs: (params) => {
    debug('jobrank.getJobs(%O)', params);
    const jobIds = rpcJobrank(params);
    return jobIds;
  },
  /**
   * @param {{workerId: String | null, jobId: ObjectId | null}} params
   */
  createJobMatchingScore: (params) => {
    info('jobrank.createJobMatchingScore(%O)', params);
    return createJobMatchingScore(params).catch(e => error(e));
  },
};

module.exports = Object.assign({}, jobrank);
