const { ObjectId } = require('mongoose').Types;

module.exports = {
  authorizedAdminIds: [
    // '34KyJCLznHWHvvLTX6yID7clV6a2', // jobdoh.employer1@gmail.com
    // 'GniPsb6Vr0QLicFPKch8lE18hAw1', // lightcode2018@gmail.com
    // 'upK3BhyyuLPIdN204sauQn1QU5G3', // phoenixjames05@gmail.com
    '7APDOVEepLhBVEaZF5XYDLezfG43', // jobdoh.operations@gmail.com
    '34KyJCLznHWHvvLTX6yID7clV6a2', // jobdoh.employer1@gmail.com
    'lFvCWJ5DZgNN52uYVE3yQjb1kya2', // myanmar@jobdoh.com
    '2Aw1enPwipeyLiwQvbvOdN1vxDi2', // jobdoh.backend@gmail.com
  ],

  leaveTypes: ['LEAVE_SICK', 'LEAVE_PERSONAL', 'LEAVE_SPECIAL', 'LEAVE_UNPAID'],

  NOT_POSTED_MARKETPLACE_ID: ObjectId('5fdb677bb9aa853f791bab64'),

  PUBLIC_MARKETPLACE_ID: ObjectId('5e1d54053998d418ddc644ba'),

  kRegionToCurrency: {
    mm: 'MMK',
    hk: 'HKD',
    vn: 'VND',
    us: 'USD',
  },
};
