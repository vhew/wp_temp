const TAX_CONFIGUARTION_REFERENCES = {
  MYANMAR: {
    parentRelief: 1000000,
    childRelief: 500000,
    personalRelief: '20%',
    spouseRelief: 1000000,
    taxPercentages: [
      {
        min: 0,
        max: 2000000,
        percent: 0,
      },
      {
        min: 2000001,
        max: 5000000,
        percent: 5,
      },
      {
        min: 5000001,
        max: 10000000,
        percent: 10,
      },
      {
        min: 10000001,
        max: 20000000,
        percent: 15,
      },
      {
        min: 20000001,
        max: 30000000,
        percent: 20,
      },
      {
        min: 30000000,
        max: 100000000,
        percent: 25,
      },
    ],
    socialSecurityBoardFee: {
      amount: '2%',
      maxSalaryLimit: 3000000,
    },
  },
};

module.exports = TAX_CONFIGUARTION_REFERENCES;
