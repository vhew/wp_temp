const ERROR_CODES = {
  DUPLICATE_VERIFIED_PHONE: {
    message: 'Verified worker exists',
    code: 'DUPLICATE_VERIFIED_PHONE',
  },

  DUPLICATE_UNVERIFIED_PHONE_SAME_ORGANISATION: {
    message: 'Unverified worker from same organisation exists',
    code: 'DUPLICATE_UNVERIFIED_PHONE_SAME_ORGANISATION',
  },

  DUPLICATE_ALIEN_PHONE_SAME_ORGANISATION: {
    message: 'Unverified alien from same organisation exists',
    code: 'DUPLICATE_ALIEN_PHONE_SAME_ORGANISATION',
  },

  WORKER_DISABLED: {
    message: 'Request not completed. Worker is disabled!',
    code: 'WORKER_DISABLED',
  },

  DUPLICATE_BONUS: {
    message: 'Bonus is already added!',
    code: 'DUPLICATE_BONUS',
  },

  JOB_ID_NOT_IN_ARRAY: {
    message: 'Job Id not found in list',
    code: 'JOB_ID_NOT_IN_ARRAY',
  },

  DUPLICATE_PAY_CYCLE_IDENTIFIER: {
    message: 'Pay Cycle Identifier for the month is already added',
    code: 'DUPLICATE_PAY_CYCLE_IDENTIFIER',
  },

  UNAUTHORIZED_NOTIFICATION_QUERY: {
    message: 'Unauthorized Notification Query',
    code: 403,
  },

  NOT_ENOUGH_SHIFT_FOR_REQUEST_PAY: {
    message: 'Not enough shifts to satisfy requested KhuPay',
    code: 'NOT_ENOUGH_SHIFT_FOR_REQUEST_PAY',
  },

  UNAUTHORIZED_ACCESS: {
    message: 'Unauthorized Access - Permission Required',
    code: 'UNAUTHORIZED_ACCESS',
  },

  CONFLICT_INTERVIEW_TIME: {
    message: 'Interview time is unavailable. Choose different one.',
    code: 'CONFLICT_INTERVIEW_TIME',
  },

  APPLYING_INVALID_JOB: {
    message: 'Job is invalid to apply.',
    code: 'APPLYING_INVALID_JOB',
  },

  INSUFFICIENT_LEAVE: {
    message: 'Forbidden - insufficient leave',
    code: 'INSUFFICIENT_LEAVE',
  },
};

module.exports = ERROR_CODES;
