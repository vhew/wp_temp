const logInfo = require('debug')('INFO');
const logError = require('debug')('ERROR');
const mongoose = require('mongoose');
const config = require('../../config');

module.exports = {
  getDbUrl: () => (config.database.type === 'mongodb+srv'
    ? `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}/${config.database.name}`
    : `${config.database.type}://${config.database.user}:${config.database.pwd}@${config.database.host}:${config.database.port}/${config.database.name}`),

  connectDatabase: async (url) => {
    try {
      mongoose.set('useNewUrlParser', true);
      mongoose.set('useFindAndModify', false);
      mongoose.set('useCreateIndex', true);
      mongoose.set('useUnifiedTopology', true);

      logInfo('mongoose', mongoose.version);
      logInfo('DB Opening: %s', url);
      const ret = await mongoose.connect(url);
      if (!(ret === mongoose)) {
        process.exit(0);
      }
    } catch (e) {
      logError(e);
    }
  },
};
