const Agenda = require('agenda');
const debug = require('debug')('DEBUG');
const error = require('debug')('ERROR');
const { getDbUrl } = require('../utils/db');
const { Mutation: coreMutations } = require('../cores/resolvers');
const { Shift } = require('../../models/mongodb');
const RatingService = require('../services/RatingService');
const { Notification, NotificationToOrg } = require('../services/Notification');

const agenda = new Agenda({
  db: {
    address: getDbUrl(),
    collection: 'agendaJobs',
  },
});

agenda.define('withdraw job', async (agendaJob) => {
  const { root, data, ctx } = JSON.parse(agendaJob.attrs.data);
  const acceptedShift = await Shift.findOne({
    $and: [
      { _id: data.shiftId },
      { workerId: data.workerId },
      { workerAccepted: true },
    ],
  }, '_id');
  if (!acceptedShift) {
    await coreMutations.withdrawJob(root, data, ctx);
    debug('Job Withdrew');
  }
  agendaJob.remove(); // remove agendaJob from db
});

agenda.define('rate job', async (agendaJob) => {
  const {
    workerId, jobContractId, value, reference,
  } = agendaJob.attrs.data;
  await RatingService.rateJob(workerId, jobContractId, value, reference);
  agendaJob.remove();
});

agenda.define('rate worker', async (agendaJob) => {
  const {
    jobContractId, workerId, value, reference,
  } = agendaJob.attrs.data;
  await RatingService.rateWorker(jobContractId, workerId, value, reference);
  agendaJob.remove();
});

agenda.define('notify', async (agendaJob) => {
  const { data } = agendaJob.attrs;
  const notification = new Notification(JSON.parse(data.notification));
  await notification.send(data.messageType);
  agendaJob.remove();
});

agenda.define('notify organisation', async (agendaJob) => {
  const { data } = agendaJob.attrs;
  const notification = new NotificationToOrg({
    ...JSON.parse(data.notification),
    organisationId: data.organisationId,
  });
  await notification.send(data.messageType);
  agendaJob.remove();
});

agenda.start();

module.exports = agenda;
