const moment = require('moment');
const { Job } = require('../../models/mongodb');
const {
  NotificationToWorker, NotificationToOrg,
} = require('../services/Notification');
const RatingService = require('../services/RatingService');
const { authorizedAdminIds } = require('../utils/constants');
const { FeedbackToken } = require('../modules/Feedback/model');
const { JobContract } = require('../modules/JobContracts/model');
const agenda = require('./agenda');

const helper = {
  // TODO: stop using employerId as identifier for organisation or jobs since employer is volatile.
  // TODO: (eg: employer can be removed from organisation)
  scheduleAutoRating: async (shift) => {
    const { workerId, organisationId, jobId } = shift;
    const { _id: jobContractId } = await JobContract.findOne({
      workerId,
      jobId,
    }, '_id');
    const rateAt = moment(shift.completedAt).add(14, 'days').toDate();
    const remindAt = moment(rateAt).subtract(7, 'days').toDate();
    const args = {
      workerId, jobContractId, organisationId, jobId, rateAt, remindAt,
    };

    const feedbackAlreadyExists = await FeedbackToken.findOne({
      fromId: jobContractId,
      targetId: workerId,
    });
    if (!feedbackAlreadyExists) {
      await helper._scheduleToRateWorker(args);
    }
    await helper._scheduleToRateEmployer(args);
  },

  _scheduleToRateWorker: async ({
    workerId, jobContractId, organisationId, jobId, rateAt, remindAt,
  }) => {
    await agenda.schedule(rateAt, 'rate worker', {
      id: `auto_rating_${jobContractId}_${workerId}`,
      jobContractId,
      workerId,
      value: RatingService._getMockEmployerFeedback(),
    });
    const notiToOrg = new NotificationToOrg({
      organisationId,
      from: workerId,
      title: 'Reminder to rate your employee',
      body: '',
      data: { jobId },
      analyticsLabel: 'notification_reminder_employer_to_rate',
    });
    await agenda.schedule(remindAt, 'notify organisation', {
      id: `rating_reminder_${workerId}_${jobContractId}`,
      notification: JSON.stringify(notiToOrg.getData()),
      organisationId,
    });
  },
  _scheduleToRateEmployer: async ({
    workerId, jobContractId, jobId, rateAt, remindAt,
  }) => {
    await agenda.schedule(rateAt, 'rate job', {
      id: `auto_rating_${workerId}_${jobContractId}`,
      workerId,
      jobContractId,
      value: RatingService._getMockWorkerFeedback(),
    });
    const notiToWorker = new NotificationToWorker({
      from: authorizedAdminIds[0],
      to: workerId,
      title: 'Reminder to rate your job',
      body: '',
      data: { jobId },
      analyticsLabel: 'notification_reminder_worker_to_rate',
    });
    await agenda.schedule(remindAt, 'notify', {
      id: `rating_reminder_${jobContractId}_${workerId}`,
      notification: JSON.stringify(notiToWorker.getData()),
    });
  },

  cancelAutoRatingForJob: async ({ workerId, jobContractId }) => {
    await agenda.cancel({ name: 'rate job', 'data.id': `auto_rating_${workerId}_${jobContractId}` });
    await agenda.cancel({ name: 'notify', 'data.id': `rating_reminder_${jobContractId}_${workerId}` });
  },
  cancelAutoRatingForWorker: async ({ workerId, jobContractId }) => {
    await agenda.cancel({ name: 'rate worker', 'data.id': `auto_rating_${jobContractId}_${workerId}` });
    await agenda.cancel({ name: 'notify organisation', 'data.id': `rating_reminder_${workerId}_${jobContractId}` });
  },
};

module.exports = helper;
