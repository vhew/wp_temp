const {
  ApolloError,
} = require('apollo-server-koa');
const error = require('debug')('ERROR');
const info = require('debug')('INFO');
const moment = require('moment');
const { ObjectId } = require('mongoose').Types;
const {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
} = require('graphql-iso-date');
// const {
//   GraphQLJSON,
// } = require('graphql-type-json');
const {
  EmailAddress,
  URL,
  PhoneNumber,
  // PostalCode,
} = require('@okgrow/graphql-scalars');
// const {
//   USCurrency,
// } = require('graphql-currency-scalars');
// const firebaseAdmin = require('firebase-admin');
const utils = require('../../utils');
const helpers = require('../../helpers');
const {
  Worker,
  AlienWorker,
  Organisation,
  Job,
  DiscountToken,
} = require('../../../models/mongodb/');
const notiService = require('../../services/NotificationService');
const Mail = require('../../../models/Mail');
const TAX_CONFIGURATION_REFERENCES = require('../../utils/taxConfigurationReferences');

const resolvers = {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
  EmailAddress,
  URL,
  PhoneNumber,
  Mutation: {
    async requestJobPay(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.jobId,
        type: 'Job',
      };
      // if (await _isAdmin(ctx.user)) ctx.user.role = 'ADMIN';
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation requestJobPay as user(%s) %s: \n%O', ctx.user.id, ctx.user.role, data);

      const {
        jobId,
        dryRun,
        numShiftsPay,
        noFee,
      } = data;
      let { discountTokenId } = data;

      let workerId;
      if (data.workerId) {
        workerId = data.workerId;
      } else {
        workerId = ctx.user.id;
      }

      try {
        // const organisation = await Organisation.findOne({ jobsIds: jobId });
        const job = await Job.findById(jobId).populate({
          path: 'shiftsIds',
          model: 'Shift',
        });
        const organisation = await Organisation.findById(job.organisationId);

        let shiftRequested;
        if (job.type === 'FULL_TIME') {
          const isWorkerShift = shift => shift.workerId === workerId;
          shiftRequested = job.shiftsIds.find(isWorkerShift);
        }

        let worker;
        if (ObjectId.isValid(workerId)) {
          worker = await AlienWorker.findById(workerId);
        } else {
          worker = await Worker.findById(workerId);
        }
        // helpers.checkWorkerSettings({ workerId, jobId });

        // if (noFee) , disallow the worker, need to test, then implement

        let payCycle = utils._getPayCycle({
          payCycleMonthDays: organisation.payCycleMonthDays,
          arbitaryDate: moment.utc(),
        });
        info('payCycle %O', payCycle);
        const payCycleIdentifier = utils._getPayCycleIdentifier({
          payCycle,
          payCycleAdvancements: organisation.payCycleAdvancements,
        });
        info('payCycleIdentifier %O', payCycleIdentifier);

        let arbitaryDate = moment.utc();
        if (noFee && !utils._isTodayPayCycleAdvancement({
          payCycle,
          payCycleAdvancements: organisation.payCycleAdvancements,
          dateToBeChecked: moment.utc(),
        })) {
          if (organisation.payCycleMonthDays.length > 2) {
            const [start, end] = organisation.payCycleMonthDays;
            const timeRange = end - start;
            arbitaryDate = arbitaryDate.subtract(timeRange, 'days');
          } else {
            arbitaryDate = arbitaryDate.subtract(1, 'months');
          }
        }

        payCycle = utils._getPayCycle({
          payCycleMonthDays: organisation.payCycleMonthDays,
          arbitaryDate,
          // to Create Future WorkToken
          // predeterminedPayCycleEndTime: payCycleIdentifier,
        });
        info('payCycle %O', payCycle);

        let adjustedRenumerationValue = utils._getAdjustedRenumeration(
          payCycle,
          job.renumerationValue,
          shiftRequested.deductions,
        );
        info('adjustedRenumerationValue %O', adjustedRenumerationValue);

        let incomeTax = 0;
        const MM_TAX_CONFIG = TAX_CONFIGURATION_REFERENCES.MYANMAR;
        if (shiftRequested.taxConfigurations) {
          incomeTax = utils._getIncomeTaxPerMonth({
            taxConfigurationReference: MM_TAX_CONFIG,
            taxConfigurations: shiftRequested.taxConfigurations,
            renumerationValue: job.renumerationValue,
          });
          adjustedRenumerationValue -= incomeTax;
        }

        const { payCycleEndTime, payCycleStartTime } = payCycle;
        // to Create Future WorkToken
        let endDate;
        if (payCycleIdentifier == null || !noFee) {
          endDate = moment.utc();
        } else {
          endDate = payCycleEndTime;
        }

        const shifts = await helpers.createShiftsWithinPayCycle({
          payCycle,
          endDate,
          jobId,
        });

        info('lastShift: %O', shifts[shifts.length - 1]);
        if (shifts.length === 0) {
          return [];
        }

        const virtualWorkTokens = helpers.calculateVirtualWorkToken(
          shifts,
        );
        const finalVirtualWorkTokens = await helpers
          .compareVirtualWorkTokensWithRealWorkTokens(payCycle, virtualWorkTokens);

        const totalWorkDays = await utils._calculateTotalWorkDays({
          accountingStyle: organisation.accountingStyle,
          payCycle,
          job,
        });
        info('totalWorkDays %O', totalWorkDays);
        // let discount = 0;
        // let discountToken;
        // if (discountTokenId != null) {
        //   discountToken = await DiscountToken.findById(discountTokenId);
        //   if (discountToken.discount != null) {
        //     // discount = (parseFloat(discountToken.discountPercent)
        //     // * (this.amount - this.fee)) / 100;
        //     discount = discountToken.discountFixed;
        //   }
        // } else {
        //   discountTokenId = null;
        // }
        // info('discount %O', discount);
        // const amountForEachDay = (adjustedRenumerationValue + discount) / totalWorkDays;
        const amountForEachDay = (adjustedRenumerationValue) / totalWorkDays;
        // const discountForEachDay = (discount) / numShiftsPay;
        let fee = 0;
        if (!noFee) {
          fee = utils._getFeeSimple({
            amount: amountForEachDay,
            feePercent: organisation.fee,
          });
        }

        let discount = 0;
        let discountToken;
        if (discountTokenId != null) {
          discountToken = await DiscountToken.findById(discountTokenId);
          if (discountToken != null) {
            discount = (parseFloat(discountToken.discountPercent)
            * fee) / 100;
            discount += discountToken.discountFixed;
          }
        } else {
          discountTokenId = null;
        }
        if (discount > 0) {
          fee -= discount;
        }
        info('discount %O', discount);
        if (dryRun) {
          const workTokenValues = await helpers.getPayNowFalseWorkTokenValues(shiftRequested._id);
          const virtualWorkTokenValues = await helpers.createVirtualWorkTokenValues(
            fee,
            amountForEachDay,
            worker.paymentCurrency,
            worker.paymentMethod,
            worker.paymentCoordinates,
            payCycle,
            finalVirtualWorkTokens,
          );
          info('virtual WTV %O', virtualWorkTokenValues);
          return [...workTokenValues, ...virtualWorkTokenValues];
        }

        let paymentCoordinates = worker.paymentCoordinates;
        let paymentCurrency = worker.paymentCurrency;
        let paymentMethod = worker.paymentMethod;

        // if (!isUsingPrimaryPaymentMethod) {
        //   paymentCoordinates = worker.secondaryPaymentCoordinates;
        //   paymentCurrency = worker.secondaryPaymentCurrency;
        //   paymentMethod = worker.secondaryPaymentMethod;
        // }
        if (noFee && worker.paycycleUseAlternatePayment) {
          paymentCoordinates = worker.alternatePaymentCoordinates;
          paymentCurrency = worker.alternatePaymentCurrency;
          paymentMethod = worker.alternatePaymentMethod;
        }

        if (noFee) {
          await helpers.removeFeeAlreadyConfirmedPayNowFalse(shiftRequested._id);
        }

        const numberOfConfirmedPayNowFalseWorkTokens = await helpers
          .updateAlreadyConfirmedPayNowFalseWorkTokens(numShiftsPay, shiftRequested._id);
        const workTokens = await helpers
          .createWorkTokens(
            numShiftsPay - numberOfConfirmedPayNowFalseWorkTokens,
            finalVirtualWorkTokens,
          );
        let workTokenValues = await helpers.createWorkTokenValues(
          fee,
          amountForEachDay,
          paymentCurrency,
          paymentMethod,
          paymentCoordinates,
          payCycle,
          workTokens,
        );
        const latestWorkTokenValue = await helpers.getLatestWorkTokenValue(workTokenValues);
        if (discountTokenId != null) {
          // workTokenValues
          //   .find(workTokenValue => workTokenValue.id === latestWorkTokenValue.id)
          //   .discountTokenId = discountTokenId;
          workTokenValues = await workTokenValues.map((workTokenValue) => {
            const updatedWorkTokenValue = workTokenValue;
            if (updatedWorkTokenValue.id === latestWorkTokenValue.id) {
              updatedWorkTokenValue.discountTokenId = discountTokenId;
            }
            // return workTokenValue.id === latestWorkTokenValue.id
            //   ? { ...workTokenValue, discountTokenId } : workTokenValue;
            return updatedWorkTokenValue;
          });
          // const workTokenValue = await helpers.getLatestWorkTokenValue(workTokenValues);
          await helpers.updateWorkTokenValue(latestWorkTokenValue.id, { discountTokenId });
        }
        if (noFee) {
          let deductions = [...shiftRequested.deductions];
          if (incomeTax > 0) {
            const taxDeduction = {
              type: 'INCOME_TAX',
              amount: incomeTax,
              enabledDate: moment.utc(payCycleStartTime).add(1, 'days').startOf('day'),
              disabledDate: moment.utc(payCycleStartTime).add(1, 'days').startOf('day'),
              notes: 'Income Tax',
            };
            deductions = [...deductions, taxDeduction];
          }
          await helpers.createDeductionWorkTokensAndWorkTokenValues(
            shiftRequested, worker, payCycle, job.renumerationValue,
            deductions,
          );
        }

        if (!noFee) {
          // notify employer to confirm timesheet
          const title = 'KhuPay request';
          const body = 'a worker has applied for KhuPay. Please confirm timesheet and pay';
          notiService.sendToEmployersInOrganisation(
            new ObjectId(job.organisationId),
            {
              from: worker._id,
              title,
              body,
              userType: notiService.userType.EMPLOYER,
              messageType: notiService.messageType.HIRED,
              data: { jobId, organisationId: job.organisationId, workerId },
              mail: { action: Mail.getActions().CONFIRM },
              analyticsLabel: 'notification_individual_hired',
            },
          );
        }

        info('real WTV %O', workTokenValues);
        return workTokenValues;
      } catch (err) {
        error('mutation:requestJobPay:', err);
        throw new ApolloError(err);
      }
    },
  },
};

module.exports = resolvers;
