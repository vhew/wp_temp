const {
  ApolloError, AuthenticationError,
} = require('apollo-server-koa');
const error = require('debug')('ERROR');
const info = require('debug')('INFO');
const moment = require('moment');
const {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
} = require('graphql-iso-date');
// const {
//   GraphQLJSON,
// } = require('graphql-type-json');
const {
  EmailAddress,
  URL,
  PhoneNumber,
  // PostalCode,
} = require('@okgrow/graphql-scalars');
// const {
//   USCurrency,
// } = require('graphql-currency-scalars');
const firebaseAdmin = require('firebase-admin');
const ErrorHandlingService = require('./../../services/ErrorHandlingService');
const utils = require('../../utils');
const {
  Worker,
  Shift,
  DiscountToken,
} = require('../../../models/mongodb/');
const notifications = require('../../services/NotificationService');
const { workerUserLevels } = require('../../services/AccessControlService');

const resolvers = {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
  EmailAddress,
  URL,
  PhoneNumber,
  Mutation: {
    upgradeWorkerLevel: async (root, data, ctx) => {
      if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');

      let ret;
      try {
        const worker = await Worker.findById(data.id).catch(err => error(err));

        if (!worker) throw new ApolloError('Worker not found', 404, { data });

        const tasksCompleteStatus = {
          LOGIN: true,
          FILL_NRC: !!worker.identification,
          FILL_MARKETPLACE_PROFILE: !!worker.jobSkills
                                  && worker.jobSkills.length > 0
                                  && !!worker.description,
          ADD_PAYMENT_METHOD: !!worker.workerPaymentMethodsIds.length > 0,
          APPLY_TO_JOB: !!worker.jobsAppliedIds.length > 0,
          COMPLETE_PROFILE: false,
          COMPLETE_PERSONALITY_QUIZ: !!worker.personalityType,
          COMPLETE_DIGITAL_LITERACY_QUIZ: !!worker.digitalLiteracyScore,
          COMPLETE_DEMOGRAPHIC_QUIZ: false,
        };

        const lockedLevels = Array.from(workerUserLevels)
          .filter(l => !l.features.every(f => f.tasks.every(t => tasksCompleteStatus[t])));

        const smallestLockedLevel = lockedLevels
          .sort((a, b) => a.userLevel - b.userLevel)
          .shift();
        const newUserLevel = smallestLockedLevel.userLevel - 1;

        const updatedWorker = await Worker.findOneAndUpdate(
          { _id: ctx.user.id },
          { userLevel: newUserLevel },
          { new: true },
        ).catch(err => error(err));

        if (updatedWorker.userLevel !== newUserLevel) { throw new ApolloError('Error updating worker level', 500, { data }); }

        ret = updatedWorker;
      } catch (e) {
        error('mutation:upgradeWorkerLevel:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },
    // updateWorkerLevel: async (root, data, ctx) => {

    // },
    bookmarkJob: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        ret = await Worker.findOneAndUpdate(
          { _id: ctx.user.id },
          {
            $addToSet: {
              jobsBookmarkIds: data.jobId,
            },
          },
          { new: true },
        ).catch(err => error(err));
      } catch (e) {
        error('mutation:bookmarkJob:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    removeBookmarkJob: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        ret = await Worker.findOneAndUpdate(
          { _id: ctx.user.id },
          {
            $pull: {
              jobsBookmarkIds: data.jobId,
            },
          },
          { new: true },
        ).catch(err => error(err));
      } catch (e) {
        error('mutation:removeBookmarkJob:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    async createDiscountToken(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        // construct data
        const update = {};
        Object.keys(data).forEach((key) => {
          update[key] = data[key];
        });
        if (data.expiryDate == null) {
          update.expiryDate = moment.utc().add(3, 'days');
        }
        update.status = 'AVAILABLE';
        update.used = false;
        // create job
        const newDiscountToken = new DiscountToken(update);
        ret = await newDiscountToken.save();
        const title = 'Discount Received';
        const body = `Congratulation you got ${data.discountPercent}% discount valid from
        ${moment(data.expiryDate).add(-14, 'days').format('YYYY-MM-DD')} to
        ${moment(data.expiryDate).format('YYYY-MM-DD')}.
        `;
        const shift = await Shift.findOne({ workerId: data.workerId });
        const message = {
          title,
          body,
          data: {
            senderId: shift.employerId,
            receiverId: data.workerId,
            title,
            body,
            jobId: shift.jobId,
            shiftId: data.id,
          },
          fcm_options: {
            analyticsLabel: 'notification_discount',
          },
        };
        notifications.send(
          {
            userType: 'worker',
            id: data.workerId,
            messageType: 'push',
            message,
          },
        );
      } catch (err) {
        error('mutation:createDiscountToken:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async updateDiscountToken(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await DiscountToken.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateDiscountToken:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    subscribeToFirebaseTopic: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.workerId,
        type: 'Worker',
      };
      if (await utils._isAdmin(ctx.user) || await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation subscribeToFirebaseTopic as user(%s): \n%O', ctx.user.id, data);

      const worker = await Worker.findById(data.workerId);
      const ret = await firebaseAdmin.messaging().subscribeToTopic(worker.fcm, data.topic)
        .catch(err => error('Error subscribing to topic:', err));

      return (ret.successCount > 0);
    },
    unsubscribeFromFirebaseTopic: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.workerId,
        type: 'Worker',
      };
      if (await utils._isAdmin(ctx.user) || await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation subscribeToFirebaseTopic as user(%s): \n%O', ctx.user.id, data);

      const worker = await Worker.findById(data.workerId);
      const ret = await firebaseAdmin.messaging().unsubscribeFromTopic(worker.fcm, data.topic)
        .catch(err => error('Error subscribing to topic:', err));

      return (ret.successCount > 0);
    },
  },
};

module.exports = resolvers;
