/* eslint-disable max-len */
const debug = require('debug')('DEBUG');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const { ObjectId } = require('mongoose').Types;
const moment = require('moment');
const {
  ApolloError, ForbiddenError,
} = require('apollo-server-koa');
const mongoose = require('mongoose');
const jobrank = require('../../jobrank');

const MAX_MATCHING_SCORE = 4;
const MAX_GENDER_MATCH_SCORE = 1;

const {
  Worker,
  AlienWorker,
  Employer,
  Organisation,
  Job,
  Shift,
  // Marketplace,
  WorkTokenValue,
  WorkToken,
  FiatToken,
  TownshipDistanceMatrix,
  ServerConfig,
  DiscountToken,
} = require('../../../models/mongodb');
const {
  Receipt,
} = require('../../modules/Products/model');
const {
  LedgerEntry,
} = require('../../modules/LedgerEntries/model');
const helpers = require('./../../helpers');
const utils = require('./../../utils');
const AccessControlService = require('../../services/AccessControlService');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const FB = require('../../services/FacebookService');

const resolvers = {
  Query: {
    healthcheck: () => true,
    worker: async (root, data, ctx) => {
      info('querying worker %O', ctx);
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        // const target = {
        //   id: data.id,
        //   type: 'Worker',
        // };
        // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        debug('query worker as user(%s): \n%O', ctx.user.id, data);
        const worker = await Worker
          .findById(data.id)
          .catch(e => error(e));

        if (!worker) { throw new ApolloError('Worker not found', 404, { data }); }

        ret = worker;
      } catch (e) {
        error('query:worker:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    workers: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        if (data.organisationId) {
          const target = {
            id: data.organisationId,
            type: 'Organisation',
          };
          if (await utils._isAdmin(ctx.user) || await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
          if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
          await utils
            .checkAccess(ctx, target, 'READ_WORKERS')
            .catch((err) => { throw err; });
        }
        // only admin can query workers without organisationId (commented out for worker recommendation feature)
        // else {
        //   if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
        //   if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
        // }
        debug('query workers as user(%s): \n%O', ctx.user.id, data);

        // get workers belong to this organisation
        let rankedResults;
        if (data.phone) {
          rankedResults = await Worker
            .find({
              phone: data.phone,
            })
            .catch(e => error(e));
        } else if (data.organisationId) {
          const organisationId = mongoose.Types.ObjectId(data.organisationId);
          const unverifiedWorkers = await Worker
            .find({
              createOrganisationId: organisationId,
              verified: false,
              disabled: false,
            }, '_id')
            .catch(e => error(e));
          const unverifiedWorkerIds = unverifiedWorkers.map(unverifiedWorker => unverifiedWorker._id);

          const unverifiedAlienWorkers = await AlienWorker
            .find({
              createOrganisationId: organisationId,
              disabled: false,
            })
            .catch(e => error(e));
          let unverifiedAlienWorkerIds = unverifiedAlienWorkers
            .map(unverifiedAlienWorker => unverifiedAlienWorker._id);
          unverifiedAlienWorkerIds = unverifiedAlienWorkerIds
            .map(unverifiedAlienWorkerId => String(unverifiedAlienWorkerId));

          const shifts = await Shift.find({
            organisationId,
            workerAccepted: true,
            workerId: {
              $exists: true,
            },
          }).catch(err => error(err));
          const shiftWorkerIds = shifts.map(shift => shift.workerId);
          const uniqueWorkerIds = new Set([
            ...unverifiedWorkerIds,
            ...unverifiedAlienWorkerIds,
            ...shiftWorkerIds,
          ]);
          const workerIds = Array.from(uniqueWorkerIds)
            .filter(workerId => !ObjectId.isValid(workerId));
          const alienWorkerIds = Array.from(uniqueWorkerIds)
            .filter(workerId => ObjectId.isValid(workerId));

          debug({ workerIds });
          const workers = await Worker.find({
            _id: { $in: workerIds },
          });

          let alienWorkers = await AlienWorker
            .find({
              _id: alienWorkerIds,
            })
            .catch(e => error(e));
          alienWorkers = alienWorkers.filter(alienWorker => !alienWorker.disabled);
          rankedResults = [...workers, ...alienWorkers];
        } else if (data.jobId) {
          let job;
          if (data.jobId) {
            job = await Job.findById(ObjectId(data.jobId), 'requirementDemographicGender');
          }
          const jobSkillMatchScoreLookup = [
            {
              $lookup: {
                from: 'jobmatchings',
                let: { id: '$_id' },
                pipeline: [{
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$$id', '$workerId'] },
                        { $eq: [ObjectId(data.jobId), '$jobId'] },
                      ],
                    },
                  },
                }],
                as: 'jobSkillMatchScores',
              },
            },
            {
              $addFields: {
                jobSkillMatchScore: {
                  $reduce: {
                    input: '$jobSkillMatchScores',
                    initialValue: 0,
                    in: { $sum: ['$$value', '$$this.score'] },
                  },
                },
              },
            },
          ];
          const avgFeedbackScoreCalc = [{
            $addFields: {
              avgFeedbackScore: {
                $reduce: {
                  input: {
                    $filter: {
                      input: [
                        { $multiply: ['$feedbacks.value.MUST_HIRE', 2] },
                        '$feedbacks.value.WILL_HIRE_AGAIN',
                        '$feedbacks.value.WILL_NOT_HIRE',
                        { $multiply: ['$feedbacks.value.ABSENT', 2] },
                      ],
                      as: 'n',
                      cond: { $gt: ['$$n', 0] },
                    },
                  },
                  initialValue: 0,
                  in: { $add: ['$$value', '$$this'] },
                },
              },
            },
          }];
          const genderMatchScoreLookup = (job && Array.isArray(job.requirementDemographicGender)) ? [{
            $addFields: {
              genderMatchScore: {
                $multiply: [
                  { $toInt: { $in: ['$gender', job.requirementDemographicGender] } },
                  MAX_GENDER_MATCH_SCORE,
                ],
              },
            },
          }] : [];
          const jobMatchingScoreLookup = [{
            $addFields: {
              jobMatchingScore: { $sum: ['$jobSkillMatchScore', '$genderMatchScore'] },
            },
          }];
          const sort = data.jobId ? [{
            $sort: {
              jobSkillMatchScore: -1,
              avgFeedbackScore: -1,
            },
          }] : [];
          const thresholdFilter = data.threshold ? [{
            $match: { jobMatchingScoreLookup: { $gte: data.threshold * (MAX_MATCHING_SCORE + MAX_GENDER_MATCH_SCORE) } },
          }] : [];
          const workerIds = await Worker.aggregate([
            {
              $match: {
                verified: true,
                disabled: false,
              },
            },
            ...jobSkillMatchScoreLookup,
            ...genderMatchScoreLookup,
            ...jobMatchingScoreLookup,
            ...thresholdFilter,
            ...avgFeedbackScoreCalc,
            ...sort,
            {
              $project: {
                _id: 1,
              },
            },
          ]);
          const workers = await Promise.all(workerIds.map(id => Worker.findById(id)));
          rankedResults = workers;
        }
        debug({ rankedResults: rankedResults[0] });
        return rankedResults;
      } catch (e) {
        error('query:workers:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    workerSearch: async (root, data, ctx) => {
      let result;
      try {
        const workerIndex = await Worker.listIndexes();
        info('getIndexes %O', workerIndex);

        if (data.phone && data.workerType == null) {
          const citizens = await Worker
            .find({ phone: data.phone })
            .catch(err => error(err));
          const aliens = await AlienWorker
            .find({ phone: data.phone })
            .catch(err => error(err));
          result = [...citizens, ...aliens];
        }
        if (data.phone && data.workerType === 'CITIZEN') {
          result = await Worker
            .find({ phone: data.phone })
            .catch(err => error(err));
        }
        if (data.phone && data.workerType === 'ALIEN') {
          result = await AlienWorker
            .find({ phone: data.phone })
            .catch(err => error(err));
        }
        // implement nameSearch later
        if (data.name && data.workerType === 'CITIZEN') {
          result = await Worker
            .find({
              $text: {
                $search: data.name,
              },
            }).catch(err => error(err));
        }
      } catch (e) {
        error('query:workerSearch:', e);
        ctx.ctx.throw(500, e);
      }
      return result;
    },
    employer: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Employer',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      debug('query employer as user(%s): \n%O', ctx.user.id, data);
      const result = await Employer.findById(data.id);
      return result;
    },
    job: async (root, data, ctx) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.id,
      //   type: 'Employer',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // debug('query job as user(%s): \n%O', ctx.user.id, data);
      // await utils
      //   .checkAccess(ctx, null, 'READ_JOBS')
      //   .catch((err) => { throw err; });

      let ret;
      try {
        const { id } = data;
        ret = await Job
          .findById(id)
          .catch((e) => {
            throw new ApolloError('Job not found', 404, { data, e });
          });
      } catch (e) {
        error('query:job:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    jobs: async (root, data) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // debug('query jobs as user(%s): \n%O', ctx.user.id, data);

      let jobs = [];
      let hasMore = false;
      let lastJobRankIndex = data.lastJobRankIndex;
      try {
        const jobPage = await jobrank.getJobs(data).catch((e) => {
          error(e);
          throw new ApolloError('Error getting job ranks', 501);
        });

        debug('ranked result: %O', jobPage);
        const jobsIds = jobPage.jobs.map(e => e._id);

        jobs = await Promise.all(jobsIds.map(id => Job.findById(id)));
        hasMore = jobPage.pageInfo.hasMore;
        lastJobRankIndex = jobPage.pageInfo.lastJobRankIndex;
        // throw new Error('error');
      } catch (err) {
        error('query:jobs:', err);
        throw ErrorHandlingService.getError(err);
      }
      // eslint-disable-next-line no-unreachable
      return {
        jobs,
        hasMore,
        lastJobRankIndex,
      };
    },
    townshipsDistance: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        debug('query townshipsDistance as user(%s): \n%O', ctx.user.id, data);
        ret = await TownshipDistanceMatrix.find({
          origin: data.origin,
          destination: data.destinations,
        });
      } catch (e) {
        error('query:townshipsDistance:', e);
        throw ErrorHandlingService.getError(e);
      }
      return ret;
    },
    organisationTransactions: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Organisation',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      debug('query organisationTransactions as user(%s): \n%O', ctx.user.id, data);
    },
    workerTransactions: async (root, data, ctx) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        // const target = {
        //   id: data.id,
        //   type: 'Worker',
        // };
        // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
        // debug('query workerTransactions as user(%s): \n%O', ctx.user.id, data);

        const shifts = await Shift.find({
          workerId: data.id,
          jobId: data.jobId,
        }).catch(e => error(e));
        const job = await Job.findById(data.jobId);
        const worker = await Worker.findById(data.id);
        const organisation = await Organisation.findById(job.organisationId);
        const receipts = await Receipt.find({
          workerId: data.id,
        });
        const workTokens = await WorkToken.find({
          shiftId: {
            $in: [...shifts.map(shift => shift.id), ...receipts.map(receipt => receipt.productId)],
          },
        }).catch(e => error(e));

        if (workTokens.length === 0) {
          ret = [];
        } else {
          const fiatTokens = await FiatToken.aggregate([
            {
              $lookup: {
                from: 'worktokens',
                localField: 'workTokenId',
                foreignField: '_id',
                as: 'worktokens',
              },
            },
            {
              $match: {
                workTokenId: {
                  $in: workTokens.map(workToken => workToken._id),
                },
                // 'worktokens.type': 'DEDUCTION',
              },
            },
            {
              $sort: {
                workTokenId: -1,
              },
            },
            {
              $group: {
                _id: {
                  workTokenType: '$worktokens.type',
                  transactionRef: '$transactionRef',
                },
                amount: {
                  $sum: '$amount',
                },

                disbursedDate: {
                  $last: '$createdAt',
                },
                firstWorkTokenId: {
                  $first: '$workTokenId',
                },
                lastWorkTokenId: {
                  $last: '$workTokenId',
                },
                coordinates: {
                  $first: '$coordinates',
                },
                currency: {
                  $first: '$currency',
                },
                type: {
                  $first: '$worktokens.type',
                },
                transactionRef: {
                  $first: '$transactionRef',
                },
              },
            },
            {
              $sort: {
                disbursedDate: -1,
              },
            },
          ]).catch(err => error(err));

          /* combine khupay & fee */
          const feeTransactions = fiatTokens
            .filter(e => e.coordinates === '+959966617840' || e.coordinates === '+959967730220');

          const paymentTransactions = fiatTokens
            .filter(e => e.coordinates !== '+959966617840'
              && e.coordinates !== '+959967730220'
              && (e._id.workTokenType[0] === 'STANDARD' || e._id.workTokenType[0] === 'PRODUCT'));

          for (let i = 0; i < feeTransactions.length; i += 1) {
            const index = paymentTransactions
              .findIndex(e => e.firstWorkTokenId.toString()
                === feeTransactions[i].firstWorkTokenId.toString());

            if (index > -1) {
              paymentTransactions[index] = {
                ...paymentTransactions[index],
                fee: feeTransactions[i].amount,
              };
            }
          }
          info(paymentTransactions);

          const transactions = await Promise.all(paymentTransactions.map(async (t) => {
            const firstWorkToken = await WorkToken.findById(t.firstWorkTokenId).catch(e => error(e));
            const lastWorkToken = await WorkToken.findById(t.lastWorkTokenId).catch(e => error(e));

            // because of mongo elements default sorting
            const payPeriodStart = lastWorkToken.checkinTime;
            const payPeriodEnd = firstWorkToken.checkoutTime;
            return {
              transactionRef: t._id,
              amount: t.amount,
              fee: t.fee || 0,
              payPeriodStart,
              payPeriodEnd,
              disbursedDate: t.disbursedDate,
              deductions: [],
              bonus: {},
              name: worker.name,
              phone: worker.phone,
              nrc: worker.identification,
              title: job.title,
              salary: job.renumerationValue,
              currency: fiatTokens[0].currency,
              orgName: organisation.name,
            };
          }));
          /* deductions & bonus */
          const deductionWorkTokens = await WorkToken.aggregate([{
            $match: {
              $and: [
                { type: 'DEDUCTION' },
                { shiftId: shifts[0]._id },
              ],
            },
          }, {
            $lookup: {
              from: 'worktokenvalues',
              localField: '_id',
              foreignField: 'workTokenId',
              as: 'workTokenValue',
            },
          }]).catch(e => error(e));

          for (let i = 0; i < deductionWorkTokens.length; i += 1) {
            const index = transactions
              .findIndex(e => moment.utc(e.payPeriodEnd)
                .isSame(moment.utc(deductionWorkTokens[i].checkoutTime), 'day'));

            if (index > -1) {
              transactions[index].deductions.push({
                amount: deductionWorkTokens[i].workTokenValue[0].amount,
                note: deductionWorkTokens[i].workTokenValue[0].notes,
              });
            }
          }

          const bonusWorkTokens = await WorkToken.aggregate([{
            $match: {
              $and: [
                { type: 'BONUS' },
                { shiftId: shifts[0]._id },
              ],
            },
          }, {
            $lookup: {
              from: 'worktokenvalues',
              localField: '_id',
              foreignField: 'workTokenId',
              as: 'workTokenValue',
            },
          }]).catch(e => error(e));

          for (let i = 0; i < bonusWorkTokens.length; i += 1) {
            const index = transactions
              .findIndex(e => moment.utc(e.payPeriodEnd)
                .isSame(moment.utc(bonusWorkTokens[i].checkoutTime), 'day'));

            if (index > -1) {
              transactions[index].bonus = {
                amount: bonusWorkTokens[i].workTokenValue[0].amount,
                note: bonusWorkTokens[i].workTokenValue[0].notes,
              };
            }
          }
          ret = transactions;
        }
      } catch (e) {
        error('query:workerTransactions:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    fiatTokens: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const target = {
          id: data.shiftId,
          type: 'Shift',
        };
        if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
        // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
        info('querying fiatTokens as user(%s): \n%O', ctx.user.id, data);
        if (data.workTokensIds != null && data.workTokensIds.length > 0) {
          const fiatTokens = await FiatToken.find({
            workTokenId: {
              $in: data.workTokensIds,
            },
          }).catch(err => error(err));
          ret = fiatTokens;
        } else {
          const workTokens = await WorkToken.find({ shiftId: data.shiftId });
          const fiatTokens = await FiatToken.find({
            workTokenId: {
              $in: workTokens.map(workToken => workToken._id),
            },
            createdAt: {
              $gte: new Date(data.timeRange.from),
              $lt: new Date(data.timeRange.to),
            },
          }).catch(err => error(err));
          debug('fiatTokens %O', fiatTokens);
          ret = fiatTokens;
        }
      } catch (e) {
        error('query:fiatTokens:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    workTokens: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.shiftId,
      //   type: 'Shift',
      // };
      // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('querying workTokens as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        let {
          filter,
          // orderBy,
          // skip,
          // first,
        } = data;
        filter = filter || {};
        // orderBy = orderBy || null;
        // skip = skip || 0;
        // first = first || null;
        let query = filter.query || {};
        // const advanced = filter.advanced || {};

        // quick fix
        if (filter.query == null) return [];

        query = await helpers.generateGqlToMongoQuery(filter.query) || {};
        let workTokens = [];
        workTokens = await WorkToken.aggregate([
          {
            $lookup: {
              from: 'fiattokens',
              localField: '_id',
              foreignField: 'workTokenId',
              as: 'fiattoken',
            },
          },
          {
            $addFields: {
              isProduct: {
                $cond: { if: { $eq: ['$type', 'PRODUCT'] }, then: true, else: false },
              },

            },
          },
          {
            $addFields: {
              unpaid: {
                $cond: { if: { $eq: [{ $size: '$fiattoken' }, 0] }, then: true, else: false },
              },

            },
          },
          // {
          //   $addFields: {
          //     isFullCheckin: {
          //       $cond: { if: { $and: [{ $eq: ['$confirmed', true] }, { $eq: ['$payNow', false] }] }, then: true, else: false },
          //     },

          //   },
          // },
          {
            $match: query,
          },
        ]);
        for (let i = 0; i < workTokens.length || 0; i += 1) {
          workTokens[i].id = String(workTokens[i]._id);
          delete workTokens[i].isProduct;
          delete workTokens[i].unpaid;
          delete workTokens[i].isFullCheckin;
        }
        if (query.shiftId != null) {
          const shift = await Shift.findById(query.shiftId).catch(err => error(err));
          const organisation = await Organisation.findById(shift.organisationId).catch(err => error(err));
          const { payCycleMonthDays } = organisation;
          const payCycleLength = utils._getPayCycleLength({
            payCycleMonthDays,
          });
          const arbitaryDate = moment.utc().subtract(payCycleLength, 'days');
          const payCycle = utils._getPayCycle({
            payCycleMonthDays,
            arbitaryDate,
          });
          const from = new Date(payCycle.payCycleStartTime);
          const to = new Date(payCycle.payCycleEndTime);
          let leaveTokens = await WorkToken.find({
            shiftId: shift.id,
            type: { $in: utils.leaveTypes },
            checkinTime: {
              $gte: from,
              $lte: to,
            },
          }).catch(err => error(err));

          leaveTokens = leaveTokens.filter((l) => {
            const [reference] = l.references;
            return reference && moment(reference.leaveStart).isBetween(from, to, '[)');
          });

          workTokens = workTokens.concat(leaveTokens);
        }
        ret = workTokens;
      } catch (err) {
        error('query:workTokens:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    // workTokens: async (root, data, ctx) => {
    //   if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
    //   // const target = {
    //   //   id: data.shiftId,
    //   //   type: 'Shift',
    //   // };
    //   // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
    //   // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
    //   // info('querying workTokens as user(%s): \n%O', ctx.user.id, data);

    //   const shift = await Shift.findById(data.shiftId).catch(err => error(err));
    //   if (!shift) ctx.ctx.throw(404, 'Cannot find Shft');

    //   const organisation = await Organisation.findById(shift.organisationId).catch(err => error(err));
    //   if (!organisation) ctx.ctx.throw(404, 'Cannot find Organisation');

    //   const { payCycleMonthDays } = organisation;
    //   let from;
    //   let to;
    //   let payCycle;
    //   let workTokens = [];
    //   // eslint-disable-next-line no-prototype-builtins
    //   if (data.unpaid === true && !data.hasOwnProperty('timeRange')) {
    //     if (data.isProduct === true) {
    //       workTokens = await WorkToken.aggregate([
    //         {
    //           $lookup: {
    //             from: 'fiattokens',
    //             localField: '_id',
    //             foreignField: 'workTokenId',
    //             as: 'fiattoken',
    //           },
    //         },
    //         {
    //           $match: {
    //             workerId: shift.workerId,
    //             type: 'PRODUCT',
    //             $expr: {
    //               $eq: [
    //                 { $size: '$fiattoken' },
    //                 0,
    //               ],
    //             },
    //           },
    //         },
    //       ]);
    //       for (let i = 0; i < workTokens.length || 0; i += 1) {
    //         workTokens[i].id = String(workTokens[i]._id);
    //       }
    //       return workTokens;
    //     }
    //     workTokens = await WorkToken.aggregate([
    //       {
    //         $lookup: {
    //           from: 'fiattokens',
    //           localField: '_id',
    //           foreignField: 'workTokenId',
    //           as: 'fiattoken',
    //         },
    //       },
    //       {
    //         $match: {
    //           shiftId: ObjectId(data.shiftId),
    //           $expr: {
    //             $eq: [
    //               { $size: '$fiattoken' },
    //               0,
    //             ],
    //           },
    //         },
    //       },
    //     ]);
    //     for (let i = 0; i < workTokens.length || 0; i += 1) {
    //       workTokens[i].id = String(workTokens[i]._id);
    //     }
    //     return workTokens;
    //   }
    //   if (data.unpaid === false) {
    //     workTokens = await WorkToken.aggregate([
    //       {
    //         $lookup: {
    //           from: 'fiattokens',
    //           localField: '_id',
    //           foreignField: 'workTokenId',
    //           as: 'fiattoken',
    //         },
    //       },
    //       {
    //         $match: {
    //           shiftId: ObjectId(data.shiftId),
    //           $expr: {
    //             $gt: [
    //               { $size: '$fiattoken' },
    //               0,
    //             ],
    //           },
    //         },
    //       },
    //     ]);
    //     for (let i = 0; i < workTokens.length || 0; i += 1) {
    //       workTokens[i].id = String(workTokens[i]._id);
    //     }
    //     return workTokens;
    //   }

    //   if (data.currentPayCycle != null) {
    //     if (data.currentPayCycle) {
    //       payCycle = utils._getPayCycle({
    //         payCycleMonthDays,
    //         arbitaryDate: moment.utc(),
    //       });
    //     } else {
    //       const payCycleLength = utils._getPayCycleLength({
    //         payCycleMonthDays,
    //       });
    //       const arbitaryDate = moment.utc().subtract(payCycleLength, 'days');
    //       payCycle = utils._getPayCycle({
    //         payCycleMonthDays,
    //         arbitaryDate,
    //       });
    //     }
    //     from = new Date(payCycle.payCycleStartTime);
    //     to = new Date(payCycle.payCycleEndTime);
    //   // eslint-disable-next-line no-prototype-builtins
    //   } else if (data.hasOwnProperty('timeRange')) {
    //     from = new Date(data.timeRange.from);
    //     to = new Date(data.timeRange.to);
    //   }

    //   // eslint-disable-next-line no-prototype-builtins
    //   if (data.hasOwnProperty('confirmStatus')) {
    //     if (data.isProduct != null && data.isProduct === true) {
    //       debug('I am here.....');
    //       workTokens = await WorkToken.find({
    //         type: 'PRODUCT',
    //         confirmed: data.confirmStatus,
    //         workerId: data.workerId,
    //       })
    //         .sort({ _id: 0 })
    //         .catch(err => error(err));
    //       debug('workTokens......', workTokens);
    //       return workTokens;
    //     }
    //     workTokens = await WorkToken.find({
    //       shiftId: data.shiftId,
    //       $or: [
    //         { confirmed: data.confirmStatus },
    //         { type: { $in: ['LEAVE_SICK', 'LEAVE_PERSONAL', 'LEAVE_SPECIAL', 'LEAVE_UNPAID'] } },
    //       ],
    //     })
    //       .sort({ _id: 0 })
    //       .catch(err => error(err));
    //     return workTokens;
    //   }

    //   // eslint-disable-next-line no-prototype-builtins
    //   if (data.hasOwnProperty('isFullCheckIn')) {
    //     workTokens = await WorkToken.find({
    //       shiftId: data.shiftId,
    //       confirmed: true,
    //       payNow: false,
    //     })
    //       .sort({ _id: 0 })
    //       .catch(err => error(err));
    //     return workTokens;
    //   }

    //   // eslint-disable-next-line no-prototype-builtins
    //   if (data.hasOwnProperty('workTokenType')) {
    //     workTokens = await WorkToken.find({
    //       shiftId: data.shiftId,
    //       type: data.workTokenType,
    //     })
    //       .sort({ _id: 0 })
    //       .catch(err => error(err));
    //     return workTokens;
    //   }

    //   workTokens = await WorkToken.find({
    //     shiftId: data.shiftId,
    //     checkinTime: {
    //       $gte: from,
    //       $lt: to,
    //     },
    //     type: { $nin: utils.leaveTypes },
    //   })
    //     .sort({ _id: 0 })
    //     .catch(err => error(err));

    //   let leaveTokens = await WorkToken.find({
    //     shiftId: data.shiftId,
    //     type: { $in: utils.leaveTypes },
    //   }).catch(err => error(err));

    //   leaveTokens = leaveTokens.filter((l) => {
    //     const [reference] = l.references;
    //     return reference && moment(reference.leaveStart).isBetween(from, to, '[)');
    //   });

    //   workTokens = workTokens.concat(leaveTokens);

    //   return workTokens;
    // },

    workTokenValues: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        const target = {
          id: data.shiftId,
          type: 'Shift',
        };
        if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
        info('querying workTokenValues as user(%s): \n%O', ctx.user.id, data);

        const workTokenValues = await WorkTokenValue.find({
          expectedWorkTokenShiftId: data.shiftId,
          expectedWorkTokenCheckinTime: {
            $gte: new Date(data.timeRange.from),
            $lt: new Date(data.timeRange.to),
          },
        }).catch(err => error(err));
        ret = workTokenValues;
      } catch (e) {
        error('query:workTokenValues:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    shifts: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        let {
          filter,
          orderBy,
          skip,
          sort,
        } = data;
        const { first } = data;
        filter = filter || {};
        orderBy = orderBy || 'createdAt';
        skip = skip || 0;
        sort = sort || {
          [orderBy]: 1,
        };
        // first = first || null;
        // let query = filter.query || {};
        const query = helpers.generateGqlToMongoQuery(filter.query) || {};
        const advanced = filter.advanced || {};
        const unpaid = advanced.unpaid || false;
        const dismissedField = sort.dismissed ? [{
          $addFields: {
            dismissed: { $gt: [new Date(), '$endTime'] },
          },
        }] : [];
        // quick fix
        if (filter.query == null) return [];

        const shiftIds = await Shift
          .aggregate([
            {
              $match: query,
            },
            ...dismissedField,
            {
              $sort: sort,
            }, {
              $project: {
                _id: 1,
              },
            },
          ])
          .then(e => e.map(shift => shift._id))
          .catch(e => error(e));

        let filteredShiftIds = [];
        if (unpaid !== false) {
          const shifts = await Promise.all(shiftIds.map(async shiftId => ({
            _id: shiftId,
            unpaid: await helpers.unpaid({ _id: shiftId }),
          })));
          filteredShiftIds = shifts.filter(e => e.unpaid).map(e => e._id);
        } else {
          filteredShiftIds = shiftIds;
        }
        const end = first ? skip + first : filteredShiftIds.length;
        const shiftsToReturn = await Promise.all(Array.from(filteredShiftIds)
          .slice(skip, end)
          .map(e => Shift.findOne({ _id: e })));

        ret = shiftsToReturn;
      } catch (e) {
        error('query:shifts:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    shift: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      const {
        shiftId,
      } = data;

      const shift = await Shift.findById(ObjectId(shiftId)).catch(e => error('query:shift:', 'Shift not found', e));

      return shift;
    },
    // shifts: async (root, data, ctx) => {
    //   // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
    //   let {
    //     filter,
    //     orderBy,
    //     skip,
    //     first,
    //   } = data;
    //   filter = filter || {};
    //   orderBy = orderBy || null;
    //   skip = skip || 0;
    //   first = first || null;
    //   let query = filter.query || {};
    //   const advanced = filter.advanced || {};
    //   const unpaid = advanced.unpaid || false;
    //   query = helpers.generateGqlToMongoQuery(filter.query) || {};
    //   debug('query', query);
    //   const match1 = {
    //     $match: query,
    //   };
    //   const joinWorkTokens = {
    //     $lookup: {
    //       from: 'worktokens',
    //       let: { id: '$_id' },
    //       pipeline: [
    //         {
    //           $match: {
    //             $expr: { $eq: ['$$id', '$shiftId'] },
    //           },
    //         },
    //         {
    //           $sort: {
    //             createdAt: -1,
    //           },
    //         },
    //         {
    //           $limit: 1,
    //         },
    //       ],
    //       as: 'worktokens',
    //     },
    //   };

    //   const joinFiatTokens = {
    //     $lookup: {
    //       from: 'fiattokens',
    //       localField: 'worktokens._id',
    //       foreignField: 'workTokenId',
    //       as: 'fiattokens',
    //     },
    //   };

    //   const joinWorker = {
    //     $lookup: {
    //       from: 'workers',
    //       localField: 'workerId',
    //       foreignField: '_id',
    //       as: 'worker',
    //     },
    //   };

    //   const match2 = {
    //     $match: {
    //       $and: [
    //         {
    //           $expr: {
    //             $eq: [
    //               { $size: '$fiattokens' },
    //               0,
    //             ],
    //           },
    //         },
    //         {
    //           $expr: {
    //             $eq: [
    //               { $size: '$worktokens' },
    //               1,
    //             ],
    //           },
    //         },
    //         {
    //           'worktokens.confirmed': true,
    //         },
    //       ],
    //     },
    //   };

    //   const sort = {
    //     $sort: {
    //       [orderBy]: 1,
    //     },
    //   };
    //   debug('orderBy', sort);
    //   first = {
    //     $limit: first,
    //   };
    //   skip = {
    //     $skip: skip,
    //   };
    //   let pipeline = [
    //     match1,
    //     joinWorker,
    //     sort,
    //     skip,
    //   ];
    //   if (unpaid) {
    //     pipeline = [
    //       match1,
    //       joinWorkTokens,
    //       joinFiatTokens,
    //       joinWorker,
    //       match2,
    //       sort,
    //       skip,
    //     ];
    //   }
    //   if (first.$limit != null) {
    //     pipeline.push(first);
    //   }
    //   const shifts = await Shift.aggregate(pipeline);
    //   for (let i = 0; i < shifts.length || 0; i += 1) {
    //     shifts[i].id = String(shifts[i]._id);
    //   }
    //   return shifts;
    // },

    queryTest: async (root, data) => {
      const filter = helpers.generateGqlToMongoQuery(data.filter) || {};
      debug(filter);
      const shifts = await Shift.aggregate(
        [
          {
            $match: {
              $and: [
                { organisationId: ObjectId('5d490298bbe5a3001c0ec633') },
                // { 'worktokens.confirmed': true },
                // { 'worktokens.payNow': false },
              ],
            },
          },
          {
            $addFields: { unpaid: helpers.unpaid({ _id: '3894798' }) },
          },
        ],
      )
        .limit(10)
        .catch(err => error(err));
      for (let i = 0; i < shifts.length || 0; i += 1) {
        shifts[i].id = String(shifts[i]._id);
      }
      return shifts;
    },

    summaryReport: async (root, data, ctx) => {
      let ret;
      try {
        const fiatTokens = await FiatToken.aggregate([
          {
            $lookup: {
              from: 'worktokens',
              localField: 'workTokenId',
              foreignField: '_id',
              as: 'worktoken',
            },

          },
          {
            $lookup: {
              from: 'shifts',
              localField: 'worktoken.shiftId',
              foreignField: '_id',
              as: 'shift',
            },

          },
          {
            $lookup: {
              from: 'worktokenvalues',
              localField: 'workTokenId',
              foreignField: 'workTokenId',
              as: 'worktokenvalue',
            },
          },
          {
            $match: {
              $and: [
                {
                  'worktoken.checkinTime': {
                    $gte: data.timeRange.from,
                    $lte: data.timeRange.to,
                  },
                },
                {
                  'shift.organisationId': ObjectId(data.organisationId),
                },
              ],
            },
          },
          {
            $facet: {
              total_fee: [
                {
                  $match: {
                    $or: [
                      {
                        coordinates: '+959966617840',
                      },
                      {
                        coordinates: '+959967730220',
                      },
                    ],
                  },
                },
                {
                  $group: {
                    _id: {
                      coordinates: '$coordinates',
                    },
                    amount: {
                      $sum: '$amount',
                    },
                  },
                },

              ],
              total_end_of_month: [
                {
                  $match: {
                    $and: [
                      {
                        'worktokenvalue.fee': 0,
                      },
                    ],
                  },
                },
                {
                  $group: {
                    _id: null,
                    amount: {
                      $sum: '$amount',
                    },
                  },
                },

              ],
              total_khupay: [
                {
                  $match: {
                    $and: [
                      {
                        'worktokenvalue.fee': {
                          $gt: 0,
                        },
                      },
                    ],
                  },
                },
                {
                  $group: {
                    _id: null,
                    amount: {
                      $sum: '$amount',
                    },
                  },
                },

              ],
              total_salary: [
                {
                  $group: {
                    _id: null,
                    amount: {
                      $sum: '$amount',
                    },
                  },
                },

              ],
            },
          },
        ])
          .catch(err => error(err));
          // return ret[0];

        if (fiatTokens && fiatTokens.length === 0) { throw new ApolloError('Fiat tokens no found', 404, { data }); }

        const [fiatToken] = fiatTokens;

        ret = {
          totalFee: fiatToken.total_fee.length === 0 ? 0 : fiatToken.total_fee[0].amount,
          totalEndOfMonth: fiatToken.total_end_of_month.length === 0 ? 0 : fiatToken.total_end_of_month[0].amount,
          totalKhupay: fiatToken.total_khupay.length === 0 ? 0 : fiatToken.total_khupay[0].amount,
          totalSalary: fiatToken.total_salary.length === 0 ? 0 : fiatToken.total_salary[0].amount,
        };
      } catch (e) {
        error('query:summaryReport:', e);
        ctx.ctx.throw(e);
      }
      return ret;
    },

    serverConfig: async (root, data, ctx) => {
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      const serverConfig = await ServerConfig.findOne({
        identifier: data.identifier,
      }).catch(err => error('query:serverConfig:', err));
      return serverConfig;
    },

    discountTokens: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.shiftId,
      //   type: 'Shift',
      // };
      // if (await utils._isShared(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('querying fiatTokens as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        const discountTokens = await DiscountToken.find({ workerId: data.workerId })
          .catch(err => error(err));
        debug('discountTokens %O', discountTokens);
        ret = discountTokens;
      } catch (e) {
        error('query:discountTokens:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    employerUserLevels: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const { employerUserLevels } = AccessControlService;
        debug(employerUserLevels);
        ret = employerUserLevels;
      } catch (e) {
        error('query:employerUserLevels:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    workerUserLevels: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        const { workerUserLevels } = AccessControlService;
        debug(workerUserLevels);
        ret = workerUserLevels;
      } catch (e) {
        error('query:workerUserLevels:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    userLevels: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        const { userType } = data;
        if (userType === 'EMPLOYER') {
          const { employerUserLevels } = AccessControlService;
          ret = employerUserLevels;
        } else {
          const { workerUserLevels } = AccessControlService;
          ret = workerUserLevels;
        }
        debug(ret);
      } catch (e) {
        error('query:userLevels:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    nKhupay: async (root, data, ctx) => {
      let ret;

      try {
        if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { workerId } = data;

        const workerIdFilter = workerId ? { expectedWorkTokenWorkerId: workerId } : {};
        const workerIdGroup = workerId ? {} : { expectedWorkTokenWorkerId: '$expectedWorkTokenWorkerId' };
        const khupayRequests = await WorkTokenValue.aggregate([
          {
            $match: {
              fee: { $gt: 0 },
              createdAt: { $gte: new Date('2020-04-01T00:00:00+00:00') },
              ...workerIdFilter,
            },
          }, {
            $lookup: {
              from: 'worktokens',
              let: {
                workTokenId: '$workTokenId',
              },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $eq: [
                        '$$workTokenId', '$_id',
                      ],
                    },
                  },
                }, {
                  $project: {
                    _id: 0,
                    confirmed: 1,
                  },
                },
              ],
              as: 'workTokens',
            },
          }, {
            $match: {
              'workTokens.confirmed': true,
            },
          }, {
            $group: {
              _id: {
                createdAt: '$createdAt',
                ...workerIdGroup,
              },
              ammount: {
                $first: '$amount',
              },
              fee: {
                $first: '$fee',
              },
            },
          }, {
            $group: {
              _id: {
                $concat: [
                  {
                    $substr: [
                      '$_id.createdAt', 0, 4,
                    ],
                  }, '_', {
                    $substr: [
                      '$_id.createdAt', 5, 2,
                    ],
                  },
                ],
              },
              nKhupay: {
                $sum: 1,
              },
            },
          }, {
            $sort: {
              _id: -1,
            },
          },
        ]);
        ret = khupayRequests;
      } catch (e) {
        error('query:nKhupay:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },

    fbJobPosts: async (root, data, ctx) => {
      let ret = { total: 0, jobPosts: [] };
      try {
        if (!ctx.user) throw new ForbiddenError('Forbidden Query');
        const jobPosts = await FB.getTaggedPosts({ filter: data })
          .then(res => Array.from(res.data)
            .map(post => FB.helpers.addJobLinkAttachment(post))
            .filter(post => !!post.jobLinkAttachment));

        debug({ jobPosts });

        ret = {
          total: jobPosts.length,
          jobPosts,
          summary: FB.helpers.getPostsSummary(jobPosts),
        };
      } catch (e) {
        error('query:fbJobPosts:', e);
        throw ErrorHandlingService.getError(e);
      }
      debug(ret);
      return ret;
    },
    createdWorkers: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        // if (filter.month != null) {

        // }
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else {
          yearMonthFilter = {
            // $and: [
            //   { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
            //   { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            // ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        }
        const createdWorkers = await Worker.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = createdWorkers;
      } catch (e) {
        error('query:createdWorkers:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    createdEmployers: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else {
          yearMonthFilter = {
            // $and: [
            //   { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
            //   { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            // ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        }
        const createdEmployers = await Employer.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = createdEmployers;
      } catch (e) {
        error('query:createdEmployers:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    createdJobs: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else {
          yearMonthFilter = {
            // $and: [
            //   { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
            //   { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            // ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        }
        const createdJobs = await Shift.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = createdJobs;
      } catch (e) {
        error('query:createdJobs:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    jobApplications: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              // { event: 'APPLY_SHIFT' },
              {
                $or: [
                  { workerId: { $exists: true } },
                  { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else {
          yearMonthFilter = {
            $and: [
              {
                $or: [
                  { workerId: { $exists: true } },
                  { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
              // { event: 'APPLY_SHIFT' },
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              // { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        }
        // const jobApplications = await LedgerEntry.aggregate([
        //   {
        //     $match: yearMonthFilter,
        //   },
        //   {
        //     $group: group,
        //   },
        // ]);
        const jobApplications = await Shift.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = jobApplications;
      } catch (e) {
        error('query:jobApplications:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    interviewsScheduled: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              { interviews: { $exists: true, $not: { $size: 0 } } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: { $size: '$interviews' } },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              { interviews: { $exists: true, $not: { $size: 0 } } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: { $size: '$interviews' } },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              { interviews: { $exists: true, $not: { $size: 0 } } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: { $size: '$interviews' } },
          };
        } else {
          yearMonthFilter = {
            $and: [
              { interviews: { $exists: true, $not: { $size: 0 } } },
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              // { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: { $size: '$interviews' } },
          };
        }
        const interviewsScheduled = await Shift.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = interviewsScheduled;
      } catch (e) {
        error('query:interviewsScheduled:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    jobOffers: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else {
          yearMonthFilter = {
            $and: [
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              // { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        }
        const jobApplications = await Shift.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = jobApplications;
      } catch (e) {
        error('query:jobApplications:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    jobAccepted: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        } else {
          yearMonthFilter = {
            $and: [
              {
                $or: [
                  { workerId: { $exists: true } },
                  // { candidatesIds: { $exists: true, $not: { $size: 0 } } },
                ],
              },
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              // { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: { $sum: 1 },
          };
        }
        const jobApplications = await Shift.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = jobApplications;
      } catch (e) {
        error('query:jobApplications:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    hoursWorked: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              { workerId: { $exists: true } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: {
              $sum: {
                $multiply: [{
                  $dateDiff:
                    {
                      startDate: '$startTime',
                      endDate: new Date(),
                      unit: 'day',
                    },

                }, { $subtract: [{ $hour: '$endTime' }, { $hour: '$startTime' }] }],
              },
            },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              { workerId: { $exists: true } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: {
              $sum: {
                $multiply: [{
                  $dateDiff:
                    {
                      startDate: '$startTime',
                      endDate: new Date(),
                      unit: 'day',
                    },

                }, { $subtract: [{ $hour: '$endTime' }, { $hour: '$startTime' }] }],
              },
            },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
              { workerId: { $exists: true } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: {
              $sum: {
                $multiply: [{
                  $dateDiff:
                    {
                      startDate: '$startTime',
                      endDate: new Date(),
                      unit: 'day',
                    },

                }, { $subtract: [{ $hour: '$endTime' }, { $hour: '$startTime' }] }],
              },
            },
          };
        } else {
          yearMonthFilter = {
            $and: [
              { workerId: { $exists: true } },
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              // { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            count: {
              $sum: {
                $multiply: [{
                  $dateDiff:
                    {
                      startDate: '$startTime',
                      endDate: new Date(),
                      unit: 'day',
                    },

                }, { $subtract: [{ $hour: '$endTime' }, { $hour: '$startTime' }] }],
              },
            },
          };
        }
        const jobApplications = await Shift.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = jobApplications;
      } catch (e) {
        error('query:jobApplications:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },

    usdDisbursed: async (root, data, ctx) => {
      let ret;

      try {
        // if (!ctx.user) throw new ForbiddenError('Forbidden Query');

        const { filter, groupBy } = data;
        let yearMonthFilter = {};
        let group = {};
        if (groupBy === 'Daily') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            amount: {
              $sum: '$amount',
            },
            fee: {
              $sum: '$fee',
            },
          };
        } else if (groupBy === 'Weekly') {
          yearMonthFilter = {
            $and: [
              { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            amount: {
              $sum: '$amount',
            },
            fee: {
              $sum: '$fee',
            },
          };
        } else if (groupBy === 'Monthly') {
          yearMonthFilter = {
            $and: [
              // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
              { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            ],
          };
          group = {
            _id: {
              month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            amount: {
              $sum: '$amount',
            },
            fee: {
              $sum: '$fee',
            },
          };
        } else {
          yearMonthFilter = {
            // $and: [
            //   // { $expr: { $eq: [{ $month: '$createdAt' }, filter.month] } },
            //   // { $expr: { $eq: [{ $year: '$createdAt' }, filter.year] } },
            // ],
          };
          group = {
            _id: {
              // month: { $month: '$createdAt' },
              // week: { $week: '$createdAt' },
              // day: { $dayOfMonth: '$createdAt' },
              year: { $year: '$createdAt' },
            },
            amount: {
              $sum: '$amount',
            },
            fee: {
              $sum: '$fee',
            },
          };
        }
        const amountDisbursed = await FiatToken.aggregate([
          {
            $match: yearMonthFilter,
          },
          {
            $group: group,
          },
        ]);
        ret = amountDisbursed;
      } catch (e) {
        error('query:jobApplications:', e);
        throw ErrorHandlingService.getError(e);
      }

      return ret;
    },
    /*
    transactions: async (root, data, ctx) => {
      // TO-DO add pagination on WorkToken level
      const workTokens = await WorkToken.find({ shiftId: data.shiftId });
      const fiatTokens = await FiatToken.aggregate([
        {
          $match: {
            workTokenId: {
              $in: workTokens.map(workToken => workToken._id),
            },
          },
        },
        {
          $group: {
            _id: {
              $dayOfYear: '$createdAt',
            },
            amount: {
              $sum: '$amount',
            },
          },
        },
        {
          $group: {
            tRef: '$_id.transactionRef',
            amount: {
              $sum: '$amount',
            },
          },
        },
      ]).catch(err => error(err));

      info('results: %O', fiatTokens);
      return [];
    },
    */
  },
};

module.exports = resolvers;
