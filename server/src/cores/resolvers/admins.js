const debug = require('debug')('DEBUG');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const firebaseAdmin = require('firebase-admin');
const { ForbiddenError, AuthenticationError } = require('apollo-server-koa');
const { ObjectId } = require('mongoose').Types;
const {
  Worker,
  AlienWorker,
  Organisation,
  WorkTokenValue,
  WorkToken,
  FiatToken,
  ServerConfig,
} = require('../../../models/mongodb');
const helpers = require('./../../helpers');
const utils = require('./../../utils');
const notiService = require('../../services/NotificationService');
const ErrorHandlingService = require('../../services/ErrorHandlingService');

const resolvers = {
  Query: {
    organisationsAdmin: async (root, data, ctx) => {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      let ret;
      try {
        ret = await Organisation
          .find()
          .catch(e => error('Error querying organisationsAdmin', e));
      } catch (e) {
        error('query:organisationsAdmin:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    organisation: async (root, data, ctx) => {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      let ret;
      try {
        const { organisationId } = data;
        ret = await Organisation
          .findById(ObjectId.isValid(organisationId) ? organisationId : ObjectId(organisationId))
          .catch(e => error(`Error getting organisation with id: ${organisationId}`, e));
      } catch (e) {
        error('query:organisation:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
  },

  Mutation: {
    async deleteWorkTokenValues(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      let ret;
      try {
        const workTokensIds = await Promise.all(data.workTokenValuesIds
          .map(async (workTokenValuesId) => {
            const workTokenValue = await WorkTokenValue.findById(workTokenValuesId);
            return workTokenValue.workTokenId;
          })).catch(err => error('error %O', err));
        await helpers.deleteWorkTokenValues(data.workTokenValuesIds);
        const deletedWorkTokens = await helpers.deleteWorkTokens(workTokensIds);
        ret = deletedWorkTokens.length;
      } catch (e) {
        error('mutation:deleteWorkTokenValues:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    async deleteWorkTokensAndWorkTokenValues(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      let ret;
      try {
        const retWorkToken = await WorkToken.deleteMany({
          shiftId: data.shiftId,
          checkinTime: {
            $gte: new Date(data.checkinTime),
            $lte: new Date(data.checkoutTime),
          },
        }).catch(err => error(err));

        let retWorkTokenValue = null;
        if (retWorkToken && retWorkToken.length > 0) {
          retWorkTokenValue = await WorkTokenValue.deleteMany({
            expectedWorkTokenShiftId: data.shiftId,
            expectedWorkTokenCheckinTime: {
              $gte: new Date(data.checkinTime),
              $lte: new Date(data.checkoutTime),
            },
          }).catch(err => error(err));
        }
        ret = retWorkTokenValue != null;
      } catch (e) {
        error('mutation:deleteWorkTokensAndWorkTokenValues:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    async migrateWorker(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation migrateWorker as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const params = {
          srcId: data.oldWorker,
          destId: data.newWorker,
        };
        await helpers.copyWorker(params);
        await helpers.migrateShiftWorker(params);
        await helpers.migrateWorkerPaymentMethods(params);

        if (ObjectId.isValid(data.oldWorker)) {
          await AlienWorker.findOneAndUpdate(
            { _id: data.oldWorker },
            {
              $set: {
                disabled: true,
              },
            },
          ).catch(err => error(err));
        } else {
          await Worker.findOneAndUpdate(
            { _id: data.oldWorker },
            {
              $set: {
                disabled: true,
              },
            },
          ).catch(err => error(err));
        }

        if (ObjectId.isValid(data.newWorker)) {
          ret = await AlienWorker.findById(data.newWorker)
            .catch(err => error(err));
        } else {
          ret = await Worker.findById(data.newWorker)
            .catch(err => error(err));
        }
      } catch (e) {
        error('mutation:migrateWorker:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    async migrateAlienWorkerToWorker(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');

        info('mutation migrateAlienWorkerToWorker as user(%s): \n%O', ctx.user.id, data);

        const alienWorker = await AlienWorker.findById(data.id);
        debug('alienWorker: %O', alienWorker);
        const normalWorker = new Worker({
          _id: `unverified_alien_${data.id}`,
          name: 'dummy worker',
          phone: '+17895551234',
          verified: false,
        });
        const createdNormalWorker = await normalWorker.save();

        const params = {
          srcId: data.id,
          destId: createdNormalWorker._id,
        };
        await helpers.copyWorker(params);
        await helpers.migrateShiftWorker(params);

        ret = await Worker.findOneAndUpdate(
          { _id: createdNormalWorker._id },
          {
            $set: {
              verified: false,
              phone: alienWorker.phone,
              createOrganisationId: alienWorker.createOrganisationId,
            },
          },
          { new: true },
        ).catch(err => error(err));
      } catch (err) {
        error('mutation:migrateAlienWorkerToWorker:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async verifyAlienWorker(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation verifyAlienWorker as user(%s): \n%O', ctx.user.id, data);

        ret = await AlienWorker.findOneAndUpdate(
          { _id: data.alienWorkerId },
          {
            $set: {
              verified: true,
            },
          },
          {
            new: true,
          },
        );
      } catch (err) {
        error('mutation:verifyAlienWorker:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },

    async sendNotification(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      info('mutation sendNotification as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const notification = notiService.generateNotification({
          from: ctx.user.id,
          to: data.workerId,
          title: data.title,
          body: data.body,
          data: data.messageData,
          userType: notiService.userType.WORKER,
          messageType: notiService.messageType.PUSH,
          analyticsLabel: 'notification_individual_hired',
        });

        ret = await notiService.send(notification).catch(e => error(e));
      } catch (e) {
        error('mutation:sendNotification:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    async sendTopicNotification(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      info('mutation sendTopicNotification as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const message = {
          notification: {
            title: data.title,
            body: data.body,
            // click_action: data.click_action,
          },
          webpush: {
            fcm_options: {
              link: data.click_action,
            },
          },
          fcm_options: {
            analyticsLabel: `notification_topic_${data.topic}`,
          },
          topic: data.topic,
        };

        ret = await firebaseAdmin.messaging()
          .send(message)
          .catch(err => error(err));
      } catch (e) {
        error('mutation:sendTopicNotification:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },

    async sendTopicDataNotification(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      info('mutation sendTopicDataNotification as user(%s): \n%O', ctx.user.id, data);

      const message = {
        data: {
          title: data.title,
          body: data.body,
          click_action: data.click_action,
        },
        fcm_options: {
          analyticsLabel: `notification_topic_${data.topic}`,
        },
        topic: data.topic,
      };

      const ret = await firebaseAdmin.messaging()
        .send(message)
        .catch(err => error(err));

      return ret;
    },

    async updateServerConfig(root, data, ctx) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      const ret = await ServerConfig.updateOne({
        identifier: data.identifier,
      }, {
        identifier: data.identifier,
        feePercent: data.feePercent,
      }, {
        upsert: true,
      }).catch(err => error(err));

      return ret;
    },

    async deleteFiatTokens(root, data, ctx) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      const ret = await FiatToken.deleteMany({
        transactionRef: data.transactionRef,
      }).catch(err => error(err));

      return ret.n;
    },

    async updateFeeByRegion(root, data, ctx) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (await utils._isAdmin(ctx.user)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      const ret = await Organisation.updateMany({
        region: data.region,
      }, {
        $set: {
          fee: data.fee,
        },
      }).catch(err => error(err));
      return ret.nModified;
    },
  },
};

module.exports = resolvers;
