/* eslint-disable max-len */
const {
  ApolloError, ForbiddenError, AuthenticationError,
} = require('apollo-server-koa');
const error = require('debug')('ERROR');
const info = require('debug')('INFO');
const debug = require('debug')('DEBUG');
const moment = require('moment');
const {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
} = require('graphql-iso-date');
const {
  EmailAddress,
  URL,
  PhoneNumber,
} = require('@okgrow/graphql-scalars');
const { Notification } = require('../../services/Notification');
const RatingService = require('../../services/RatingService');
const agenda = require('../../scheduler/agenda');
const agendaHelper = require('../../scheduler/helper');

const {
  Employer,
  Organisation,
  Job,
  Shift,
  WorkTokenValue,
  WorkToken,
  FiatToken,
} = require('../../../models/mongodb/');
const { JobContract } = require('../../modules/JobContracts/model');

const utils = require('../../utils');
const helpers = require('../../helpers');
const notiService = require('../../services/NotificationService');
const jobrank = require('../../jobrank');
const ERROR_CODES = require('../../utils/errorCodes');
const AccessControlService = require('../../services/AccessControlService');
const ErrorHandlingService = require('../../services/ErrorHandlingService');
const Mail = require('../../../models/Mail');

const resolvers = {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
  EmailAddress,
  URL,
  PhoneNumber,
  Mutation: {
    async createJob(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation createJob as user(%s): \n%O', ctx.user.id, data);
        await utils
          .checkAccess(ctx, target, 'WRITE_JOBS')
          .catch((err) => { throw err; });

        // construct data
        const update = {};
        const ignoreKeys = [
          'organisationId',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        // special keys
        update.organisationId = data.organisationId;
        // create job
        const newJob = new Job(update);
        ret = await newJob.save();

        if (ret) {
          jobrank.createJobMatchingScore({ jobId: ret._id });
        }
      } catch (err) {
        error('mutation:createJob:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async updateJob(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError(401, 'Not Authorized - invalid access token');
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';

        if (ctx.user.role === 'USER') { // allow only for signing contract
          info('mutation updateJob as user(%s): \n%O', ctx.user.id, data);
          const {
            id, contractId, signingDateWorker, contract, workerId,
          } = data;
          const jobContract = await JobContract.findOneAndUpdate(
            { _id: contractId },
            { $set: { signingDateWorker, contract, workerId } },
            { new: true },
          );
          if (!jobContract) throw new ApolloError('JobContract not found', 404);

          ret = await Job.findOne({ _id: id });
        } else {
          await utils
            .checkAccess(ctx, target, 'WRITE_JOBS')
            .catch((err) => { throw err; });

          // construct data
          const update = {};
          const ignoreKeys = [
            'organisationId',
            'id',
          ];
          Object.keys(data).forEach((key) => {
            if (!ignoreKeys.includes(key)) {
              update[key] = data[key];
            }
          });
          ret = await Job.findOneAndUpdate(
            { _id: data.id },
            update,
            { new: true },
          );
        }
        if (ret) {
          jobrank.createJobMatchingScore({ jobId: data.id });
        }
      } catch (err) {
        error('mutation:updateJob:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async updateJobsMarketplace(
      root, data, ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError(401, 'Not Authorized - invalid access token');
        const { organisationId } = data;
        const target = {
          id: organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';

        if (ctx.user.role !== 'OWNER') throw new AuthenticationError(401, 'Not Authrized - invalid access token');

        ret = Job.updateMany({
          organisationId,
          marketplaceId: utils.NOT_POSTED_MARKETPLACE_ID,
        }, {
          marketplaceId: utils.PUBLIC_MARKETPLACE_ID,
        });
      } catch (err) {
        error('mutation:updateJobsMarketplace:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async addJobShift( // create new shift and add to job.shiftIds
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.jobId,
          type: 'Job',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation addJobShift as user(%s): \n%O', ctx.user.id, data);
        await utils
          .checkAccess(ctx, target, 'WRITE_JOBS')
          .catch((err) => { throw err; });

        // 1. create Shift
        const job = await Job.findById(data.jobId);

        const update = {};
        const ignoreKeys = [];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        update.employerId = ctx.user.id;
        update.organisationId = job.organisationId;

        const newShift = new Shift(update);
        const shift = await newShift.save();

        /*
        This will fail when this operation is called concurrently.
        const key = 'shiftsIds';
        job[key].push(shift._id);
        ret = await Job.findOneAndUpdate(
          { _id: data.jobId },
          job,
          { new: true },
          );
          */

        // 2. add Shift to Job
        ret = await Job.findOneAndUpdate({
          _id: data.jobId,
        }, {
          // The $addToSet operator adds a value to an array unless the value is already present,
          // in which case $addToSet does nothing to that array.
          $addToSet: {
            shiftsIds: shift._id,
          },
        }, {
          new: true,
        }).catch(err => error(err));
      } catch (err) {
        error('mutation:addJobShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async removeJobShift(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.jobId,
          type: 'Job',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation addJobShift as user(%s): \n%O', ctx.user.id, data);
        await utils
          .checkAccess(ctx, target, 'WRITE_JOBS')
          .catch((err) => { throw err; });

        const key = 'shiftIds';
        const update = {};
        const job = await Job.findById(data.jobId);
        const list = [];
        let found = false;
        job[key].forEach((id) => {
          if (String(id) !== data.id) {
            list.push(String(id));
          } else {
            found = true;
          }
        });
        if (!found) {
          throw new ApolloError('Bad request - ID not in list', 501);
        }
        update[key] = list;
        ret = await Job.findOneAndUpdate(
          { _id: data.jobId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:removeJobShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async updateShift(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.id,
          type: 'Shift',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation updateShift as user(%s): \n%O', ctx.user.id, data);

        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Shift.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async addOrganisationPayCycleIdentifier(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Organisation',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation addOrganisationPayCycleIdentifier as user(%s): \n%O', ctx.user.id, data);

      const payCycleIdentifier = moment.utc(data.payCycleIdentifier);
      const organisation = await Organisation.findById(data.id);
      const payCycle = utils._getPayCycle({
        payCycleMonthDays: organisation.payCycleMonthDays,
        arbitaryDate: moment.utc(payCycleIdentifier),
      });
      const { payCycleStartTime, payCycleEndTime } = payCycle;

      // check the payCycleAdvancement is already specified for this pay cycle
      const isAlreadyPresent = await Organisation.find({
        $and: [
          { _id: data.id },
          {
            payCycleAdvancements: {
              $elemMatch: {
                payCycleIdentifier: {
                  $gte: new Date(payCycleStartTime),
                  $lt: new Date(payCycleEndTime),
                },
              },
            },
          },
        ],
      }).catch(err => error(err));
      if (isAlreadyPresent.length > 0) {
        const { DUPLICATE_PAY_CYCLE_IDENTIFIER } = ERROR_CODES;
        const { message, code } = DUPLICATE_PAY_CYCLE_IDENTIFIER;
        throw new ApolloError(message, code);
      }

      const advancedDays = (payCycleEndTime.diff(data.payCycleIdentifier, 'days')) - 1;

      debug('added: %O', {
        payCycleIdentifier: new Date(payCycleIdentifier.toDate()),
        advancedDays,
      });
      const ret = await Organisation.findOneAndUpdate({
        _id: data.id,
      }, {
        $push: {
          payCycleAdvancements: {
            payCycleIdentifier: new Date(payCycleIdentifier.toDate()),
            advancedDays,
          },
        },
      }, { new: true }).catch((err) => {
        error('mutation:addOrganisationPayCycleIdentifier:', err);
        ctx.ctx.throw(500, err);
      });
      return ret;
    },
    async confirmWorkToken(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.shiftId,
        type: 'Shift',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation confirmShift as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const workToken = await WorkToken.findById(data.id, 'shiftId');
        if (workToken == null) {
          throw new ApolloError('Cannot find workToken', 404);
        }
        if (String(workToken.shiftId) !== data.shiftId) {
          throw new ApolloError('Forbidden - shiftId and workTokenId dont match', 401);
          // ctx.ctx.throw(401, 'Forbidden - shiftId and workTokenId dont match');
        }
        ret = await WorkToken.findOneAndUpdate(
          { _id: data.id },
          {
            $set: {
              confirmed: data.confirmed,
              payNow: data.payNow,
            },
          },
          { new: true },
        );
      } catch (err) {
        error('mutation:confirmWorkToken:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async confirmWorkTokens(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.shiftId,
        type: 'Shift',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation confirmShift as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        const shift = await Shift.findById(data.shiftId);
        if (shift == null) throw new ApolloError('Cannot find shift', 404);
        const workToken = await WorkToken.findById(data.workTokenIds[0]);
        if (workToken == null) throw new ApolloError('Cannot find workToken', 404);
        // const shiftIds = await WorkToken
        //   .distinct('shiftId', { _id: { $in: data.workTokenIds } });
        // if (String(shiftIds[0]) !== data.shiftId) {
        //   ctx.ctx.throw(401, 'Forbidden - shiftId and workTokenId dont match');
        // }
        ret = await WorkToken.updateMany(
          { _id: { $in: data.workTokenIds } },
          {
            $set: {
              confirmed: data.confirmed,
              payNow: data.payNow,
            },
          },
          { new: true },
        );
      } catch (err) {
        error('mutation:confirmWorkTokens:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async createFiatTokens(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');

      let ret;
      try {
        const target = {
          id: data.workTokenValueId,
          type: 'WorkTokenValue',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
        info('mutation payWorkToken as user(%s): \n%O', ctx.user.id, data);

        let notifyWorker = false;
        if (data.notifyWorker) {
          notifyWorker = data.notifyWorker;
        }

        // create FiatToken
        const feeCoordinates = '+959966617840';
        const feeMethod = 'ONGO';
        ret = [];
        try {
          const fiatTokensIds = [];
          const workTokenValue = await WorkTokenValue.findById(data.workTokenValueId);
          debug('difference', workTokenValue.amountConflictDifference || 0);
          const workerFiatTokenData = {
            amount: workTokenValue.amount - workTokenValue.fee
              + (workTokenValue.amountConflictDifference || 0),
            coordinates: workTokenValue.coordinates,
            currency: workTokenValue.currency,
            method: workTokenValue.method,
            workTokenId: workTokenValue.workTokenId,
            transactionRef: data.transactionRef,
          };
          const newWorkerFiatToken = new FiatToken(workerFiatTokenData);
          const workerFiatToken = await newWorkerFiatToken.save();
          fiatTokensIds.push(workerFiatToken._id);
          ret.push(workerFiatToken);

          if (workTokenValue.fee > 0) {
            const feeFiatTokenData = {
              amount: workTokenValue.fee,
              coordinates: feeCoordinates,
              currency: workTokenValue.currency,
              method: feeMethod,
              workTokenId: workTokenValue.workTokenId,
              transactionRef: data.feeTransactionRef,
            };
            const newFeeFiatToken = new FiatToken(feeFiatTokenData);
            const feeFiatToken = await newFeeFiatToken.save();
            fiatTokensIds.push(feeFiatToken._id);
            ret.push(feeFiatToken);
          }

          const shift = await Shift.findById(workTokenValue.expectedWorkTokenShiftId);

          // workComplete=true if this WorkToken and shift.endTime is similar
          if (await utils._isWorkComplete(shift, workerFiatToken)) {
            info('shift is complete');
            const updateShiftOperation = await Shift.updateOne(
              { _id: shift._id },
              {
                $set: {
                  workComplete: true,
                  completedAt: new Date(),
                },
              },
            );
            if (updateShiftOperation) {
              await agendaHelper.scheduleAutoRating(shift);
            }
          }

          // notify worker they have been paid
          const title = 'Paid';
          const body = 'You have been paid. Please check.';
          const message = {
            title,
            body,
            data: {
              title,
              body,
              jobId: shift.jobId,
              shiftId: shift._id,
              fiatTokenId: workerFiatToken._id,
            },
            fcm_options: {
              analyticsLabel: 'notification_individual_hired',
            },
          };
          if (notifyWorker) {
            notiService.send(
              {
                userType: 'worker',
                id: shift.workerId,
                messageType: 'hired',
                message,
              },
            );
          }
        } catch (err) {
          error('mutation:createFiatTokens:', err);
          ctx.ctx.throw(500, err);
        }
      } catch (e) {
        error('mutation:createFiatTokens:', e);
        ctx.ctx.throw(500, e);
      }
      return ret;
    },
    async runPayroll(
      root,
      data,
    ) {
      const {
        jobId,
        workerId,
        dryRun,
      } = data;

      const allWorkComplete = await utils._checkShiftsWorkComplete({ jobId, workerId });
      if (allWorkComplete) {
        throw new Error('Job is already in the past');
      }

      const payCycle = await helpers.getPayCycle({ jobId });

      if (dryRun) {
        const ret = await utils._isPayCyclePaid({ jobId, workerId, payCycle });
        return (ret) ? 0 : 1;
      }

      // work-in-progress
      const shifts = await helpers.createShiftsWithinPayCycle({
        payCycle,
        endDate: payCycle.payCycleEndTime,
        jobId,
      });

      // const workTokens = await helpers.createPayrollWorkTokens();

      debug('shifts: %O', shifts);
      return 0;
    },
    async updateWorkTokenValues(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.shiftId,
      //   type: 'Shift',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation confirmShift as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        ret = await WorkTokenValue.updateMany(
          { _id: { $in: data.workTokenValuesIds } },
          {
            $set: {
              amountConflictDifference: data.amountConflictDifference,
            },
          },
          { new: true },
        );
      } catch (err) {
        error('mutation:updateWorkTokenValues:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async updateEmployerLevel(
      root,
      data,
      ctx,
    ) {
      let updatedLevel = 0;
      try {
        if (!await utils._isAdmin(ctx.user)) throw new ForbiddenError('Forbidden mutation');

        const { employerId, level } = data;

        const employerLevels = AccessControlService.employerUserLevels.map(e => e.userLevel);
        if (!employerLevels.includes(level)) throw new ForbiddenError('Invalid Employer Level');

        const employer = await Employer.findOneAndUpdate(
          { _id: employerId },
          {
            $set: {
              userLevel: level,
            },
          },
          { new: true },
        );
        updatedLevel = employer.userLevel;

        if (updatedLevel !== level) throw new ApolloError('Error updating user level', 500);

        // send notifications
        // const notification = notiService.generateNotification({
        // });
        const notification = new Notification({
          from: ctx.user.id,
          to: employerId,
          userType: notiService.userType.EMPLOYER,
          title: 'Account level updated',
          body: `JOBDOH account has been updated to level ${updatedLevel}`,
          data: { updatedLevel },
          mail: { to: employer, action: Mail.getActions().CONFIRM },
          analyticsLabel: 'notification_employer_account_updated',
        });
        notification.send('push_email');

        // EmailService.send(employer.email, title, mail);
      } catch (err) {
        error('mutation:updateEmployerLevel:', err);
        throw ErrorHandlingService.getError(err);
      }
      return updatedLevel;
    },
  },
};

module.exports = resolvers;
