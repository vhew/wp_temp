const {
  ApolloError, ForbiddenError, AuthenticationError,
} = require('apollo-server-koa');
const error = require('debug')('ERROR');
const info = require('debug')('INFO');
const debug = require('debug')('DEBUG');
const warn = require('debug')('WARN');
const moment = require('moment');
const _ = require('lodash');
const { ObjectId } = require('mongoose').Types;
const {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
} = require('graphql-iso-date');
// const {
//   GraphQLJSON,
// } = require('graphql-type-json');
const {
  EmailAddress,
  PhoneNumber,
  // PostalCode,
} = require('@okgrow/graphql-scalars');
const locales = require('../locales/index');
// const {
//   USCurrency,
// } = require('graphql-currency-scalars');
// const firebaseAdmin = require('firebase-admin');
const utils = require('./../utils');
const agendaHelper = require('../scheduler/helper');
const jobrank = require('../jobrank');
const LedgerUtils = require('../modules/LedgerEntries/utils');
const helpers = require('./../helpers');
const { kRegionToCurrency } = require('./../utils/constants');
const {
  Worker,
  AlienWorker,
  Employer,
  Organisation,
  Job,
  Shift,
  JobFilter,
  WorkTokenValue,
  WorkToken,
  FiatToken,
  DiscountToken,
  TownshipDistanceMatrix,
} = require('../../models/mongodb');
const { JobContract } = require('../modules/JobContracts/model');
const { Township } = require('../../src/modules/Locations/model');

const { Marketplace, ExpectedMarketplaceSubscriber } = require('../modules/marketplaces/model');
const { WorkerPaymentMethod } = require('../modules/payments/model');
const { UserPermission } = require('../modules/UserPermissions/model');
const notiService = require('../services/NotificationService');
const { NotificationToOrg } = require('../services/Notification');
const ERROR_CODES = require('../utils/errorCodes');
const ErrorHandlingService = require('../services/ErrorHandlingService');
const ZoomService = require('../services/ZoomService');
const ratingService = require('../services/RatingService');
const Mail = require('../../models/Mail');

// const COST_OF_CAPITAL_PA = 0.40;
// const TIME_COST_PER_DAY = 0.04;
// const NUM_WORK_HOURS = 8.0;
// const NUM_MONTH_DAYS = 30.42;
// const ONGO_WITHDRAW_FEE_MMK = 250;

/*
const _isAdmin = async (user) => {
  let isAdmin = false;
  const authorisedAdminIds = [
    'abc',
    'def',
  ];
  if (authorisedAdminIds.includes(user.id)) {
    isAdmin = true;
  } else {
    // always return true for now
    isAdmin = true;
  }
  return isAdmin;
};
*/

// const _roundTo = (n, digits) => {
//   const multiplicator = 10 ** digits;
//   const nFixed = parseFloat((n * multiplicator).toFixed(11));
//   return Math.round(nFixed) / multiplicator;
// };


// const _getFee = ({
//   salary,
//   numHours,
//   numDays,
//   numDaysToPayCycleEnd,
//   numShiftsPaid,
//   paymentMethod,
// }) => {
//   const feeYearly = salary * COST_OF_CAPITAL_PA;
//   const feeMonthly = feeYearly / 12.0;
//   const feeDaily = feeMonthly / NUM_MONTH_DAYS;
//   const feeHourly = feeDaily / NUM_WORK_HOURS;
//   let fee;
//   if (numHours) {
//     fee = feeHourly * numHours;
//   } else if (numDays) {
//     fee = feeDaily * numDays;
//   }
//   if (numShiftsPaid) {
//     if (paymentMethod && paymentMethod === 'ONGO') {
//       fee += ONGO_WITHDRAW_FEE_MMK / numShiftsPaid;
//     }
//   }
//   if (numDaysToPayCycleEnd) {
//     debug('-----\nbase fee: %f', fee);
//     fee *= (1 + numDaysToPayCycleEnd * TIME_COST_PER_DAY);
//     debug('time adjusted fee: %f', fee);
//   }
//   return fee;
// };

const LEAVE_TYPES = ['LEAVE_SICK', 'LEAVE_PERSONAL', 'LEAVE_SPECIAL', 'LEAVE_UNPAID'];


const resolvers = {
  GraphQLDate,
  GraphQLTime,
  GraphQLDateTime,
  EmailAddress,
  URL,
  PhoneNumber,
  Mutation: {
    async createWorker(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Worker',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');

      info('mutation createWorker as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        // create verified worker
        const update = {};
        update._id = data.id;
        update.name = data.name;
        update.phone = data.phone;
        update.verified = true;
        const newWorker = new Worker(update);
        ret = await newWorker.save();

        // fields we want to copy from unverified worker to verified worker
        let identification = '';
        let defaultWorkerPaymentMethodId;
        let workerPaymentMethodsIds = [];
        let name = update.name;

        // check if there exists an unverified worker with this phoneNumber
        const workers = await Worker.find({
          phone: data.phone,
          verified: false,
        });
        warn(workers);
        if (workers.length > 0) {
          await Promise.all(workers.map(async (worker) => {
            await Shift.updateMany(
              { workerId: worker._id },
              {
                $set: {
                  workerId: data.id,
                },
              },
            );

            await WorkerPaymentMethod.updateMany({
              workerId: worker._id,
            }, {
              $set: {
                workerId: data.id,
              },
            });

            await Worker.findOneAndUpdate(
              { _id: worker._id },
              { adoptedByWorkerId: data.id },
              { new: true },
            );
            identification = worker.identification;
            name = worker.name;
            defaultWorkerPaymentMethodId = worker.defaultWorkerPaymentMethodId;
            workerPaymentMethodsIds = worker.workerPaymentMethodsIds;
          }));
        }

        await Worker.findOneAndUpdate(
          { _id: ret._id },
          {
            identification,
            name,
            defaultWorkerPaymentMethodId,
            workerPaymentMethodsIds,
          },
          { new: true },
        );

        // THIS IS A QUICK HACK FOR CYBERPORT MARKETPLACE DEMO - START
        const subscriptions = await ExpectedMarketplaceSubscriber.find({
          phone: data.phone,
        });
        const marketplacesIds = subscriptions.map(e => e.marketplaceId);
        await Worker.findOneAndUpdate({
          _id: ret._id,
        }, {
          $set: {
            marketplacesIds,
          },
        });
        const publicMarketplaceId = await Marketplace.find({
          organisationId: 'public-marketplace',
        });
        await Worker.findOneAndUpdate({
          _id: ret._id,
        }, {
          $push: {
            marketplacesIds: publicMarketplaceId,
          },
        });
        // THIS IS A QUICK HACK FOR CYBERPORT MARKETPLACE DEMO - END
      } catch (err) {
        error('mutation:createWorker:', err);
        // throw new Error(err);
      }
      return ret;
    },
    async createUnverifiedWorker(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');

        await utils
          .checkAccess(ctx, target, 'WRITE_WORKERS')
          .catch((err) => { throw err; });

        info('mutation createUnverifiedWorker as user(%s): \n%O', ctx.user.id, data);

        const workers = await Worker.find({ phone: data.phone, verified: true });
        if (workers.length > 0) {
          throw new AuthenticationError('worker exists', 500);
        }

        const update = {};
        update._id = `unverified_${data.organisationId}_${data.phone}`;
        update.createOrganisationId = data.organisationId;
        update.name = data.name;
        update.phone = data.phone;
        update.identification = data.identification;
        update.verified = false;
        const newWorker = new Worker(update);
        ret = await newWorker.save();
      } catch (err) {
        error('mutation:createUnverifiedWorker:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async createAlienWorker(
      root,
      data,
      ctx,
    ) {
      // if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // const target = {
      //   id: data.organisationId,
      //   type: 'Organisation',
      // };
      // if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      // if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      // info('mutation createAlienWorker as user(%s): \n%O', ctx.user.id, data);

      // await utils
      // .checkAccess(ctx, null, 'WRITE_WORKERS')
      // eslint-disable-next-line max-len
      // .catch(requiredLevel => ctx.ctx.throw(400, 'Not Authorized - Permission Required', { requiredLevel }));

      let ret;
      try {
        const update = {};
        update.createOrganisationId = data.organisationId;
        update.name = data.name;
        update.identification = data.identification;
        update.identificationFileRef = data.identificationFileRef;
        update.phone = data.phone;
        update.verified = false;
        const newWorker = new AlienWorker(update);
        ret = await newWorker.save();
      } catch (err) {
        error('mutation:createAlienWorker:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async updateWorker(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.id,
          type: 'Worker',
        };
        if (await utils._isAdmin(ctx.user) || await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        await utils
          .checkAccess(ctx, target, 'WRITE_WORKERS')
          .catch((err) => { throw err; });


        info('mutation updateWorker as user(%s): \n%O', ctx.user.id, data);

        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            if (key === 'jobSkills') {
              update[key] = data[key].flat();
            } else {
              update[key] = data[key];
            }
          }
        });

        if (ObjectId.isValid(data.id)) {
          ret = await AlienWorker.findOneAndUpdate(
            { _id: data.id },
            update,
            { new: true },
          );
        } else {
          ret = await Worker.findOneAndUpdate(
            { _id: data.id },
            update,
            { new: true },
          );
        }

        const haveJobMatchingVariables = data.verified || data.jobSkills || data.marketplacesIds;
        if (haveJobMatchingVariables) {
          jobrank.createJobMatchingScore({ workerId: data.id });
        }
      } catch (err) {
        error('mutation:updateWorker:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async updateUnverifiedWorker(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isAdmin(ctx.user) || await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        await utils
          .checkAccess(ctx, target, 'WRITE_WORKERS')
          .catch((err) => { throw err; });


        info('mutation updateUnverifiedWorker as user(%s): \n%O', ctx.user.id, data);
        if (data.phone) {
          debug('update phone %O', data.phone);
          const workers = await Worker.find({ phone: data.phone, verified: true });
          if (workers.length > 0) {
            const { DUPLICATE_VERIFIED_PHONE } = ERROR_CODES;
            const { message, code } = DUPLICATE_VERIFIED_PHONE;
            throw new ApolloError(message, code);
          }
          const unverifiedWorkers = await Worker
            .find({ phone: data.phone, createOrganisationId: data.organisationId });
          if (unverifiedWorkers.length > 0) {
            const { DUPLICATE_UNVERIFIED_PHONE_SAME_ORGANISATION } = ERROR_CODES;
            const { message, code } = DUPLICATE_UNVERIFIED_PHONE_SAME_ORGANISATION;
            throw new ApolloError(message, code);
          }
          const alienWorkers = await AlienWorker.find({
            phone: data.phone, createOrganisationId: data.organisationId,
          });
          if (alienWorkers.length > 0) {
            const { DUPLICATE_ALIEN_PHONE_SAME_ORGANISATION } = ERROR_CODES;
            const { message, code } = DUPLICATE_ALIEN_PHONE_SAME_ORGANISATION;
            throw new ApolloError(message, code);
          }
        }

        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Worker.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateUnverifiedWorker:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async updateAlienWorker(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        await utils
          .checkAccess(ctx, target, 'WRITE_WORKERS')
          .catch((err) => { throw err; });


        info('mutation updateAlienWorker as user(%s): \n%O', ctx.user.id, data);
        if (data.phone) {
          const workers = await Worker.find({
            $or: [
              {
                phone: data.phone, verified: true,
              },
              {
                phone: data.phone, createOrganisationId: data.organisationId,
              },
            ],
          });
          const alienWorkers = await AlienWorker.find({
            phone: data.phone, createOrganisationId: data.organisationId,
          });
          if (workers.length > 0 || alienWorkers.length > 0) {
            throw new ApolloError('worker exists', 500);
          }
        }

        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await AlienWorker.findOneAndUpdate(
          { _id: data.alienWorkerId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateAlienWorker:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async addWorkerJob(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.workerId,
          type: 'Worker',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation addWorkerJob as user(%s): \n%O', ctx.user.id, data);

        // validate JobId
        // check if JobId exists in Job and is current

        let key;
        if (data.to === 'BOOKMARK') {
          key = 'jobsBookmarkIds';
        } else if (data.to === 'APPLIED') {
          key = 'jobsAppliedIds';
        }
        const update = {};
        const worker = await Worker.findById(data.workerId);
        const list = worker[key];
        if (!list.includes(data.id)) {
          list.push(data.id);
          update[key] = list;
          ret = await Worker.findOneAndUpdate(
            { _id: data.workerId },
            update,
            { new: true },
          );
        } else {
          warn('addWorkerJob: duplicate jobId, ignoring');
        }
      } catch (err) {
        error('mutation:addWorkerJob:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async addWorkerJobFilter(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.workerId,
        type: 'Worker',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation addWorkerJobFilter as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        // create jobFilter. TODO: validate fields
        const update = {};
        const ignoreKeys = [];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        const newJobFilter = new JobFilter(update);
        const thisJobFilter = await newJobFilter.save();
        // add jobFilter to worker
        const key = 'jobFiltersIds';
        const worker = await Worker.findById(data.workerId);
        worker[key].push(thisJobFilter._id);
        ret = await Worker.findOneAndUpdate(
          { _id: data.workerId },
          worker,
          { new: true },
        );
      } catch (err) {
        error('mutation:addWorkerJobFilter:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async removeWorkerJobFilter(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.workerId,
        type: 'Worker',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation removeWorkerJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        const key = 'jobFiltersIds';
        const worker = await Worker.findById(data.workerId);
        const list = [];
        let found = false;
        worker[key].forEach((id) => {
          if (String(id) !== data.id) {
            list.push(String(id));
          } else {
            found = true;
          }
        });
        if (!found) {
          ctx.ctx.throw(501, 'Bad request - ID not in list');
        }
        const update = {};
        update[key] = list;
        ret = await Worker.findOneAndUpdate(
          { _id: data.workerId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:removeWorkerJobFilter:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async createEmployer(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Employer',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation createEmployer as user(%s): \n%O', ctx.user.id, data);
      // create new organisation
      // const newOrganisation = new Organisation({
      //   name: data.companyName,
      // });
      // const organisation = await newOrganisation.save();

      let ret;
      try {
        // create new employer
        const update = {};
        update._id = data.id;
        update.name = data.name;
        update.email = data.email;
        // update.organisationsIds = [organisation._id];
        const newEmployer = new Employer(update);
        ret = await newEmployer.save();
      } catch (err) {
        error('mutation:createEmployer:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async updateEmployer(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.id,
          type: 'Employer',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        else if (await utils._isAdmin(ctx.user)) ctx.user.role = 'ADMIN';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation updateEmployer as user(%s): \n%O', ctx.user.id, data);

        const args = Object.assign({}, data);

        if (args.zoomAuthCode) {
          await ZoomService
            .requestAccessToken(args.zoomAuthCode, args.id)
            .catch(() => {
              delete args.zoomAuthCode;
            });
        }

        // construct data
        const update = {};
        const ignoreKeys = [
          'id',
        ];
        Object.keys(args).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = args[key];
          }
        });
        ret = await Employer.findOneAndUpdate(
          { _id: args.id },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateEmployer:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async addEmployerOrganisation(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.employerId,
          type: 'Employer',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation addEmplyerOrganisation as user(%s): \n%O', ctx.user.id, data);

        // create jobFilter. TODO: validate fields
        const update = {};
        const ignoreKeys = [];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        const newOrganisation = new Organisation(update);
        const organisation = await newOrganisation.save();

        // add jobFilter to worker
        const key = 'organisationsIds';
        const employer = await Employer.findById(data.employerId);
        employer[key].push(organisation._id);
        ret = await Employer.findOneAndUpdate(
          { _id: data.employerId },
          employer,
          { new: true },
        );
        await UserPermission.findOneAndUpdate({
          resourceName: 'Organisation',
          resourceId: organisation._id,
          userId: data.employerId,
        }, {
          $set: {
            permissions: [
              // 'READ_TIMESHEET',
              // 'WRITE_TIMESHEET',
              // 'READ_DISBURSEMENT',
              // 'WRITE_DISBURSEMENT',
              // 'READ_WORKERS',
              // 'WRITE_WORKERS',
              'READ_JOBS',
              // 'WRITE_JOBS',
              'READ_MARKETPLACE_JOBS',
              'WRITE_MARKETPLACE_JOBS',
              // 'READ_REPORTS',
              'READ_ORGANISATION_SETTINGS',
              // 'WRTIE_ORGANISATION_SETTINGS',
            ],
          },
        }, {
          upsert: true,
          new: true,
        }).catch(err => error(err));
      } catch (err) {
        error('mutation:addEmployerOrganisation:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async removeEmployerOrganisation(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.employerId,
          type: 'Employer',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation removeEmployerOrganisation as user(%s): \n%O', ctx.user.id, data);

        const key = 'organisationsIds';
        const employer = await Employer.findById(data.employerId);
        const list = [];
        let found = false;
        employer[key].forEach((id) => {
          if (String(id) !== data.id) {
            list.push(data.id);
          } else {
            found = true;
          }
        });
        if (!found) throw new ApolloError('Bad request - ID not in list', 501);

        const update = {};
        update[key] = list;
        ret = await Employer.findOneAndUpdate(
          { _id: data.employerId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:removeEmployerOrganisation:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async updateOrganisation(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.id,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation updateOrganisation as user(%s): \n%O', ctx.user.id, data);

        // construct data
        const update = {};
        const ignoreKeys = [];
        Object.keys(data).forEach((key) => {
          if (!ignoreKeys.includes(key)) {
            update[key] = data[key];
          }
        });
        ret = await Organisation.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:updateOrganisation:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },

    async addOrganisationJob(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation addOrganisationJob as user(%s): \n%O', ctx.user.id, data);
        // validate JobId
        // check if JobId exists in Job and is current

        /*
        let key;
        if (data.to === 'PRIVATE') {
          key = 'jobsPrivateIds';
        } else if (data.to === 'OPEN') {
          key = 'jobsOpenIds';
        }
        */
        const key = 'jobsIds';

        const update = {};
        const organisation = await Organisation.findById(data.organisationId);
        const list = organisation[key];
        list.push(data.id);
        update[key] = list;
        ret = await Organisation.findOneAndUpdate(
          { _id: data.organisationId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:addOrganisationJob:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async removeOrganisationJob(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.organisationId,
          type: 'Organisation',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation removeOrganisationJob as user(%s): \n%O', ctx.user.id, data);

        const key = 'jobsIds';

        const update = {};
        const organisation = await Organisation.findById(data.organisationId);
        const list = [];
        let found = false;
        organisation[key].forEach((id) => {
          if (String(id) !== data.id) {
            list.push(String(id));
          } else {
            found = true;
          }
        });
        if (!found) {
          throw new ApolloError('Bad request - ID not in list', 501);
        }
        update[key] = list;
        ret = await Organisation.findOneAndUpdate(
          { _id: data.organisationId },
          update,
          { new: true },
        );
      } catch (err) {
        error('mutation:removeOrganisationJob:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },

    async applyShift(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      let ret;
      try {
        await utils.checkAccess(ctx, null, 'APPLY_JOB');

        const worker = await Worker.findById(ctx.user.id);
        const shift = await Shift.findById(data.id);
        const job = await Job.findById(shift.jobId);
        const employer = await Employer.findById(shift.employerId);

        utils.checkIfWorkerCanApply(worker, job);

        info('mutation applyShift as user(%s): \n%O', ctx.user.id, data);
        // 1. add Worker to Shift candidates
        ret = await Shift.updateOne({
          _id: data.id,
        }, {
          $addToSet: {
            candidatesIds: ctx.user.id,
          },
        }, {
          new: true,
        });

        const entry = {
          event: 'APPLY_SHIFT',
          collectionName: 'shifts',
          documentId: data.id,
          updatedAttribute: 'candidatesIds',
          update: ctx.user.id,
        };
        await LedgerUtils.createLedgerEntry(entry);

        await Worker.findOneAndUpdate({
          _id: ctx.user.id,
        }, {
          $pull: {
            jobsBookmarkIds: shift.jobId,
          },
          $addToSet: {
            jobsAppliedIds: shift.jobId,
          },
        });

        // 2. notify employer someone has applied
        const language = worker.languageSetting || 'en';
        const title = locales.translate('applyShiftTitle', language);
        let body = locales.translate('applyShiftBody', language);
        body = body.replace('@@@', worker.name);
        body = body.replace('###', job.title);

        new NotificationToOrg({
          organisationId: shift.organisationId,
          from: worker._id,
          to: employer._id,
          title,
          body,
          data: { jobId: shift.jobId, shiftId: data.id },
          mail: { to: employer, action: Mail.getActions().CONFIRM },
          analyticsLabel: 'notification_individual_applied',
        }).send('push_email');
      } catch (err) {
        error('mutation:applyShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret.nModified;
    },

    async rejectShift(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      await utils
        .checkAccess(ctx, null, 'WRITE_JOBS')
        .catch((err) => { throw err; });


      info('mutation rejectShift as user(%s): \n%O', ctx.user.id, data);
      let ret;
      try {
        // remove worker from to shift candidates
        ret = await Shift.updateOne({
          _id: data.id,
        }, {
          $pull: {
            candidatesIds: data.workerId,
            interviews: {
              workerId: data.workerId,
            },
          },
          $addToSet: {
            rejectedCandidatesIds: data.workerId,
          },
        }, {
          new: true,
        });

        const entry = {
          event: 'REJECT_SHIFT',
          collectionName: 'shifts',
          documentId: data.id,
          updatedAttribute: 'candidatesIds',
          update: data.workerId,
        };
        await LedgerUtils.createLedgerEntry(entry);

        const shift = await Shift.findById(data.id);
        await Worker.findOneAndUpdate({
          _id: data.workerId,
        }, {
          $pull: {
            jobsAppliedIds: shift.jobId,
          },
        });

        // notify employer someone has applied
        const job = await Job.findById(shift.jobId);
        const title = 'Not selected for job';
        const body = `Thank you for your interest in ${job.title}.
                      You were not selected for this position. Best of luck with your job search.`;
        const message = {
          title,
          body,
          data: {
            title,
            body,
            // jobId: shift.jobId,
            shiftId: data.id,
          },
          fcm_options: {
            analyticsLabel: 'notification_individual_notHired',
          },
        };
        notiService.send(
          {
            userType: 'worker',
            id: data.workerId,
            messageType: 'push',
            message,
          },
        );
      } catch (err) {
        error('mutation:rejectShift:', err);
        ctx.ctx.throw(500, err);
      }
      return ret.nModified;
    },

    async resignShift(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      await utils
        .checkAccess(ctx, null, 'APPLY_JOB')
        .catch((err) => { throw err; });

      info('mutation resignShift as user(%s): \n%O', ctx.user.id, data);
      try {
        const worker = await Worker.findById(ctx.user.id);
        if (worker.userLevel <= 2) {
          ctx.ctx.throw(500, 'Lower User Level');
        }

        const shift = await Shift.findById(data.id);
        const job = await Job.findById(shift.jobId);
        const employer = await Employer.findById(shift.employerId);
        await Shift.findOneAndUpdate(
          { _id: ObjectId(data.id) },
          {
            $set: {
              resignRequestedDate: moment().toDate(),
              tradeShiftStatus: 'pending',
            },
          },
          { new: true },
        ).catch(err => error(err));

        // 2. notify employer someone has applied
        const language = worker.languageSetting || 'en';
        const title = locales.translate('resignShiftTitle', language);
        let body = locales.translate('resignShiftBody', language);
        body = body.replace('@@@', worker.name);
        body = body.replace('###', job.title);

        notiService.sendToEmployersInOrganisation(
          shift.organisationId,
          {
            from: worker._id,
            to: employer._id,
            title,
            body,
            userType: notiService.userType.EMPLOYER,
            messageType: notiService.messageType.PUSH,
            data: { jobId: shift.jobId, shiftId: data.id },
            // mail: { to: employer, action: Mail.getActions().CONFIRM },
            analyticsLabel: 'notification_individual_applied',
          },
        );
        // setTimeout(() => {
        //   this.withdrawJob(root, {
        //     shiftId: data.id,
        //     workerId: worker.id,
        //   }, ctx);
        // }, 48 * 3600 * 1000); // run after 48 hours
      } catch (err) {
        error('mutation:resignShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return 1;
    },

    async withdrawJob(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      // info('mutation withdrawJob as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        // 🗑 remove workerId from shift's candidateIds list
        const shiftId = ObjectId(data.shiftId);
        ret = await Shift.updateOne({
          _id: shiftId,
        }, {
          $pull: {
            candidatesIds: data.workerId,
          },
          $unset: {
            workerId: 1,
          },
        }, {
          new: true,
        }).catch(e => error(`Error updating shift | id: ${shiftId}`, e));

        const entry = {
          event: 'WITHDRAW_JOB',
          collectionName: 'workers',
          documentId: data.workerId,
          updatedAttribute: 'jobsWithdraw',
          update: data.workerId,
        };
        await LedgerUtils.createLedgerEntry(entry);

        // ➕ add jobId to worker's widrew list and applied list
        const shift = await Shift.findById(shiftId);
        if (!shift) throw new Error(`Error finding shift | id: ${data.id}`);
        const job = await Job.findById(shift.jobId);
        if (!job) throw new Error(`Error finding job | id: ${shift.jobId}`);

        const workerConditions = { _id: data.workerId };
        const addToSet = {
          $addToSet: { jobsWithdrewIds: shift.jobId },
        };
        const pull = {
          $pull: { jobsAppliedIds: shift.jobId },
        };
        await Worker
          .findOneAndUpdate(workerConditions, {
            ...addToSet,
            ...pull,
          })
          .catch(e => error('Error updating worker', workerConditions, addToSet, e));

        // 🔔 notify worker
        const notification = {
          messageType: notiService.messageType.PUSH,
          data: { jobId: shift.jobId, shiftId: data.id },
          analyticsLabel: 'notification_individual_notHired',
        };
        const notificationToWorker = notiService.generateNotification({
          ...notification,
          from: shift.employerId,
          to: data.workerId,
          title: 'Job has already withdrawn',
          body: `${job.title} has already withdrawn.`,
          userType: notiService.userType.WORKER,
        });
        const notificationToEmployer = notiService.generateNotification({
          ...notification,
          from: data.workerId,
          to: shift.employerId,
          title: 'Job has been withdrawn',
          body: `${job.title} has been withdrawn.`,
          userType: notiService.userType.EMPLOYER,
        });
        notiService.send(notificationToWorker);
        notiService.send(notificationToEmployer);
      } catch (err) {
        error('mutation:withdrawJob:', err);
        ctx.ctx.throw(500, err);
      }
      return ret.nModified;
    },

    async acceptShift(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Shift',
      };
      if (await utils._isShared(ctx.user, target)) ctx.user.role = 'SHARED';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation acceptShift as user(%s): \n%O', ctx.user.id, data);

      let ret;
      try {
        ret = await Shift.findOneAndUpdate(
          { _id: data.id },
          {
            $set: {
              workerAccepted: true,
            },
          },
          { new: true },
        );

        // check if all other siblings shifts have workerAccepted
        // if all sibling shifts have workerAccept, update job.shiftsFilled = true
        const shift = await Shift.findById(data.id);
        const worker = await Worker.findById(shift.workerId);
        const job = await Job.findById(shift.jobId);
        const shifts = await Shift.find({
          _id: job.shiftsIds,
        });
        let shiftsFilled = true;
        shifts.forEach((s) => {
          if (!s.workerAccepted) {
            shiftsFilled = false;
          }
        });
        if (shiftsFilled) {
          // update Job to say all shifts are filled
          await Job.findOneAndUpdate(
            { _id: shift.jobId },
            { shiftsFilled },
            { new: true },
          );
        }

        await Worker.findOneAndUpdate({
          _id: ctx.user.id,
        }, {
          $pull: {
            jobsAppliedIds: shift.jobId,
          },
        });
        // notify employer worker has accepted offer
        const language = worker.languageSetting || 'en';
        const title = locales.translate('acceptShiftTitle', language);
        let body = locales.translate('acceptShiftBody', language);
        body = body.replace('@@@', worker.name);
        body = body.replace('###', job.title);
        notiService.sendToEmployersInOrganisation(
          new ObjectId(job.organisationId),
          {
            from: worker._id,
            title,
            body,
            userType: notiService.userType.EMPLOYER,
            messageType: notiService.messageType.NOT_HIRED,
            data: { jobId: shift.jobId, shiftId: shift._id },
            mail: { action: Mail.getActions().CONFIRM },
            analyticsLabel: 'notification_individual_hired',
          },
        );

        const organisation = await Organisation.findById(new ObjectId(job.organisationId));
        const title2 = 'Job Offer';
        // const body = 'This is a job you applied to. You still need to accept this offer.';
        const body2 = `${organisation.name} offered you ${job.title}. You still need to accept this.`;
        const message2 = {
          title: title2,
          body: body2,
          data: {
            title,
            body,
            jobId: shift.jobId,
            shiftId: data.id,
          },
          fcm_options: {
            analyticsLabel: 'notification_individual_hired',
          },
        };
        notiService.send(
          {
            userType: 'worker',
            id: data.workerId,
            messageType: 'hired',
            message: message2,
          },
        );
      } catch (err) {
        error('mutation:acceptShift:', err);
        ctx.ctx.throw(500, err);
      }
      return ret;
    },
    async assignUnverifiedWorkerToShift(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.id,
          type: 'Shift',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation assignUnverifiedWorkerToShift as user(%s): \n%O', ctx.user.id, data);

        // check if worker is unverified
        const worker = Worker.findById(data.workerId);
        if (worker.verified) {
          throw new ForbiddenError('not an unverified worker');
        }

        // update Shift to identify worker and say they accepted
        ret = await Shift.findOneAndUpdate(
          { _id: data.id },
          {
            $set: {
              workerId: data.workerId,
              workerAccepted: true,
            },
          },
          { new: true },
        );
        info(ret);

        // update Job to say all shifts are filled
        await Job.findOneAndUpdate(
          { _id: ret.jobId },
          { shiftsFilled: true },
          { new: true },
        );
      } catch (err) {
        error('mutation:assignUnverifiedWorkerToShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    async assignAlienWorkerToShift(
      root,
      data,
      ctx,
    ) {
      let ret;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.id,
          type: 'Shift',
        };
        if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation assignAlienWorkerToShift as user(%s): \n%O', ctx.user.id, data);

        // update Shift to identify worker and say they accepted
        const update = {
          workerId: data.alienWorkerId,
          workerAccepted: true,
        };
        ret = await Shift.findOneAndUpdate(
          { _id: data.id },
          update,
          { new: true },
        );

        // update Job to say all shifts are filled
        const job = await Job.findOne({
          shiftsIds: data.id,
        })
          .populate({
            path: 'shiftsIds',
            model: 'Shift',
          });
        if (job.shiftsIds.every(shift => shift.workerAccepted)) {
          await Job.findOneAndUpdate({
            _id: job._id,
          }, {
            $set: {
              shiftsFilled: true,
            },
          });
        }
      } catch (err) {
        error('mutation:assignAlienWorkerToShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return ret;
    },
    // deprecated
    async dismissWorkerShift(
      root,
      data,
      ctx,
    ) {
      let updatedShift;
      try {
        if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
        const target = {
          id: data.id,
          type: 'Shift',
        };
        if (await utils._isAdmin(ctx.user) || utils._isShared(ctx.user, target)) ctx.user.role = 'SHARED';
        if (ctx.user.role === 'USER') throw new ForbiddenError('Forbidden mutation');
        info('mutation dismissWorkerShift as user(%s): \n%O', ctx.user.id, data);

        updatedShift = await Shift.findOneAndUpdate(
          { _id: data.id },
          {
            $set: {
              workComplete: true,
              completedAt: new Date(),
            },
          },
          { new: true },
        );
        if (updatedShift) {
          const contract = await JobContract.findOne({
            workerId: updatedShift.workerId,
            jobId: updatedShift.jobId,
          });
          if (contract != null) await agendaHelper.scheduleAutoRating(updatedShift);
        }
        // const job = await Job.findOne({ shiftsIds: data.id });
        // if (job == null) {
        //   throw new ApolloError('job not found');
        // }

        // updatedShift = await Shift.findOneAndUpdate({ _id: data.id }, {
        //   $set: {
        //     dismissReason: data.dismissReason,
        //     workComplete: false, // deprecated / add new logic
        //   },
        // },
        // { new: true }).catch(err => error(err));
      } catch (err) {
        error('mutation:dismissWorkerShift:', err);
        throw ErrorHandlingService.getError(err);
      }
      return updatedShift;
    },
    async rateShiftAsEmployer(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Shift',
      };
      if (await utils._isOwner(ctx.user, target)) ctx.user.role = 'OWNER';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation rateShiftAsEmployer as user(%s): \n%O', ctx.user.id, data);
    },
    async rateShiftAsWorker(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      const target = {
        id: data.id,
        type: 'Shift',
      };
      if (await utils._isShared(ctx.user, target)) ctx.user.role = 'SHARED';
      if (ctx.user.role === 'USER') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation rateShiftAsWorker as user(%s): \n%O', ctx.user.id, data);
    },

    async scheduleOrganisationCredit(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (ctx.user.role !== 'ADMIN') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation creditOrganisation as user(%s): \n%O', ctx.user.id, data);
    },
    async confirmOrganisationCredit(
      root,
      data,
      ctx,
    ) {
      if (!ctx.user) ctx.ctx.throw(401, 'Not Authorized - invalid access token');
      if (ctx.user.role !== 'ADMIN') ctx.ctx.throw(403, 'Forbidden mutation');
      info('mutation confirmOrganisationCreditted as user(%s): \n%O', ctx.user.id, data);
    },
  }, // end Mutation
  // start resolvers
  Employer: {
    organisations: async (employer) => {
      let results;
      try {
        results = await Promise.all(employer.organisationsIds.map(async (id) => {
          const object = await Organisation.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Employer:organisations:', err);
      }
      return results;
    },
    zoomAuthorized: async employer => !!employer.zoomAuthCode,
  },
  Worker: {
    createOrganisation: async (worker) => {
      let result;
      try {
        result = await Organisation.findById(worker.createOrganisationId);
      } catch (err) {
        error('resolver:Worker:createOrganisation:', err);
      }
      return result;
    },
    adoptedByWorker: async (worker) => {
      let result;
      try {
        result = await Worker.findById(worker.adoptedByWorkerId);
      } catch (err) {
        error('resolver:Worker:adoptedByWorker:', err);
      }
      return result;
    },
    jobsBookmarked: async (worker) => {
      let results;
      try {
        if (worker.jobsBookmarkIds == null) {
          return [];
        }
        results = await Promise.all(worker.jobsBookmarkIds.map(async (id) => {
          const object = await Job.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Worker:jobsBookmarked:', err);
      }
      return results;
    },
    jobsApplied: async (worker) => {
      let results;
      try {
        if (worker == null || worker.jobsAppliedIds == null) {
          return [];
        }
        results = await Promise.all(worker.jobsAppliedIds.map(async (id) => {
          const object = await Job.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Worker:jobsApplied:', err);
      }
      return results;
    },
    jobsHired: async (worker) => {
      let jobs;
      try {
        const shifts = await Shift.find({
          $and: [
            { workerId: worker._id },
            { workerAccepted: true },
            // {
            //   endTime: {
            //     $gt: new Date(),
            //   },
            // },
          ],
        });
        const jobsIds = new Set(shifts.map(shift => shift.jobId));
        jobs = await Job.find({
          _id: {
            $in: Array.from(jobsIds),
          },
        }).catch(err => error(err));
      } catch (err) {
        error('resolver:Worker:jobsHired:', err);
      }
      return jobs;
    },
    jobsOffered: async (worker) => {
      let jobs;
      try {
        const shifts = await Shift.find({
          $and: [
            { workerId: worker._id },
            { workerAccepted: false },
          ],
        }).catch(err => error(err));
        const jobsIds = new Set(shifts.map(shift => shift.jobId));
        jobs = await Job.find({
          _id: {
            $in: Array.from(jobsIds),
          },
        }).catch(err => error(err));
      } catch (err) {
        error('resolver:Worker:jobsOffered:', err);
      }
      return jobs;
    },
    jobsNotHired: async (worker) => {
      let results;
      try {
        if (worker.jobsNotHiredIds == null) {
          return [];
        }
        results = await Promise.all(worker.jobsNotHiredIds.map(async (id) => {
          const object = await Job.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Worker:jobsNotHired:', err);
      }
      return results;
    },
    jobsCompleted: async (worker) => {
      let results;
      try {
        const shifts = await Shift.find({
          $and: [
            { workerId: worker._id },
            { workCerroromplete: true }, // deprecated
          ],
        });
        results = await Promise.all(shifts.map(async (shift) => {
          const job = await Job.findById(shift.jobId);
          return job;
        }));
      } catch (err) {
        error('resolver:Worker:jobsCompleted:', err);
      }
      return results;
    },
    jobsWithdrew: async (worker) => {
      let results;
      try {
        if (worker == null || worker.jobsWithdrewIds == null) {
          return [];
        }
        results = await Promise.all(worker.jobsWithdrewIds.map(async (id) => {
          const object = await Job.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Worker:jobsWithdrew:', err);
      }
      return results;
    },
    // jobFilters: async (worker) => {
    //   if (worker.jobFiltersIds == null) {
    //     return [];
    //   }
    //   const results = await Promise.all(worker.jobFiltersIds.map(async (id) => {
    //     const object = await JobFilter.findById(id);
    //     return object;
    //   }));
    //   return results;
    // },
    // lastKhupayStatus: async (worker) => {
    //   let ret;
    //   try {
    //     const [lastWorktokenValue] = await WorkTokenValue.find({
    //       fee: { $gt: 0 },
    //       expectedWorkTokenWorkerId: worker._id,
    //     }).sort({
    //       _id: -1,
    //     }).limit(1);

    //     const lastWorkToken = await WorkToken
    //       .findById(lastWorktokenValue && lastWorktokenValue.workTokenId);

    //     ret = lastWorkToken ? lastWorkToken.confirmed : null;
    //   } catch (e) {
    //     throw new ErrorHandlingService.getError(e);
    //   }
    //   return ret;
    // },
  },
  AlienWorker: {
    createOrganisation: async (worker) => {
      let result;
      try {
        result = await Organisation.findById(worker.createOrganisationId);
      } catch (err) {
        error('resolver:AlienWorker:createOrganisation:', err);
      }
      return result;
    },
    adoptedByWorker: async (worker) => {
      let result;
      try {
        result = await Worker.findById(worker.adoptedByWorkerId);
      } catch (err) {
        error('resolver:AlienWorker:adoptedByWorker:', err);
      }
      return result;
    },
    jobsHired: async (worker) => {
      let results;
      try {
        const shifts = await Shift.find({
          $and: [
            { workerId: worker._id },
            { workerAccepted: true },
          ],
        });

        results = await Promise.all(shifts.map(async (shift) => {
          const job = await Job.findById(shift.jobId);
          return job;
        }));
      } catch (err) {
        error('resolver:AlienWorker:jobsHired:', err);
      }
      return results;
    },
    jobsCompleted: async (worker) => {
      let results;
      try {
        const shifts = await Shift.find({
          $and: [
            { workerId: worker._id },
            { workComplete: true }, // deprecated
          ],
        });

        results = await Promise.all(shifts.map(async (shift) => {
          const job = await Job.findById(shift.jobId);
          return job;
        }));
      } catch (err) {
        error('resolver:AlienWorker:jobsCompleted:', err);
      }
      return results;
    },
  },
  Organisation: {
    jobs: async (organisation) => {
      let results;
      try {
        results = await Promise.all(organisation.jobsIds.map(async (id) => {
          const object = await Job.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Organisation:jobs:', err);
      }
      return results;
    },
  },
  Job: {
    organisation: async (job) => {
      let result;
      try {
        result = await Organisation.findById(job.organisationId);
        throw new Error('error');
      } catch (err) {
        error('resolver:Job:organisation:', err);
      }
      return result;
    },
    shifts: async (job, { uniqueVacancy, workerId }) => {
      let shifts;
      try {
        shifts = await Shift.find({
          jobId: job._id,
        }).catch(err => error(err));

        // if (uniqueVacancy) {
        //   shifts = utils._getUniqueShiftByTimes({ shifts });
        // }

        if (workerId) {
          shifts = shifts.filter(shift => shift.workerId === workerId);
        }
      } catch (err) {
        error('resolver:Job:shifts:', err);
      }
      return shifts;
    },
    hasBeenApplied: async (job, { workerId }) => {
      let results;
      try {
        const worker = await Worker.findById(workerId);
        if (worker == null) return false;

        const jobsIds = helpers.objectIdToString(worker.jobsAppliedIds);
        results = jobsIds.includes(job._id.toString());
      } catch (err) {
        error('resolver:Job:hasBeenApplied:', err);
      }
      return results;
    },
    hasBeenBookmarked: async (job, { workerId }) => {
      let results;
      try {
        const worker = await Worker.findById(workerId);
        if (worker == null) return false;

        const jobsIds = helpers.objectIdToString(worker.jobsBookmarkIds);
        results = jobsIds.includes(job._id.toString());
      } catch (err) {
        error('resolver:Job:hasBeenBookmarked:', err);
      }
      return results;
    },
    currentPayCycle: async (job) => {
      let result;
      try {
        const payCycle = await helpers
          .getPayCycle({ jobId: job._id, ignorePayCycleAdvancement: true });

        payCycle.payCycleEndTime = moment.utc(payCycle.payCycleEndTime)
          .subtract(1, 'days');
        result = payCycle;
      } catch (err) {
        error('resolver:Job:currentPayCycle:', err);
      }

      return result;
    },
    travelTime: async (job, { workerTownship }) => {
      let result;
      try {
        const jobTownship = await Township.findOne(
          { township: job.location && job.location.township },
        );
        const distanceMatrix = await TownshipDistanceMatrix.findOne({
          origin: workerTownship,
          destination: jobTownship && jobTownship.id,
        });
        result = distanceMatrix
          && distanceMatrix.duration !== null ? distanceMatrix.duration / 60 : -1;
      } catch (err) {
        error('resolver:Job:travelTime:', err);
      }
      return result;
    },
    startTime: async (job) => {
      let result;
      try {
        const shift = await Shift.findOne({ jobId: job._id });
        result = shift && shift.startTime;
      } catch (err) {
        error('resolver:Job:startTime:', err);
      }
      return result;
    },
    isOfflineJob: async job => !job.marketplaceId,
    renumerationCurrency: async (job) => {
      let result;
      try {
        const { region } = await Organisation.findById(ObjectId(job.organisationId));
        result = job.renumerationCurrency || kRegionToCurrency[region || 'us'];
      } catch (err) {
        error('resolver:Job:renumerationCurrency:', err);
      }
      return result;
    },
    isRemoteJob: async (job, { workerCountry }) => {
      let result;
      try {
        const organisation = await Organisation.findById(ObjectId(job.organisationId), 'region');
        result = organisation && organisation.region !== _.toLower(workerCountry);
      } catch (err) {
        error('resolver:Job:isRemoteJob:', err);
      }
      return result;
    },
  },
  Shift: {
    worker: async (shift) => {
      let result;
      try {
        const workerId = shift.workerId;
        if (ObjectId.isValid(workerId)) {
          result = await AlienWorker.findById(workerId);
        } else {
          result = await Worker.findById(workerId);
        }
      } catch (err) {
        error('resolver:Shift:worker:', err);
      }
      return result;
    },
    organisation: async (shift) => {
      let result;
      try {
        result = await Organisation.findById(shift.organisationId);
      } catch (err) {
        error('resolver:Shift:organisation:', err);
      }
      return result;
    },
    candidates: async (shift) => {
      let results;
      try {
        results = await Promise.all(shift.candidatesIds.map(async (id) => {
          const object = await Worker.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Shift:candidates:', err);
      }
      return results;
    },
    rejectedCandidates: async (shift) => {
      let results;
      try {
        results = await Promise.all(shift.rejectedCandidatesIds.map(async (id) => {
          const object = await Worker.findById(id);
          return object;
        }));
      } catch (err) {
        error('resolver:Shift:rejectedCandidates:', err);
      }
      return results;
    },
    job: async (shift) => {
      let job;
      try {
        job = await Job.findOne({ _id: shift.jobId });
      } catch (err) {
        error('resolver:Shift:job:', err);
      }
      return job;
    },
    currentTimesheetConfirm: async (shift) => {
      try {
        const organisation = await Organisation.findById(shift.organisationId);
        const workToken = await WorkToken
          .find({ shiftId: shift.id, confirmed: null, type: { $nin: LEAVE_TYPES } })
          .sort({ checkinTime: -1 })
          .limit(1);
        const validWorkToken = workToken.length > 0 ? workToken[0] : null;
        if (validWorkToken == null) {
          return 'none';
        }
        const workTokenValue = await WorkTokenValue.find({ expectedWorkTokenShiftId: shift.id })
          .sort({ expectedWorkTokenCheckinTime: -1 })
          .limit(1);
        const validWorkTokenValue = workTokenValue.length > 0 ? workTokenValue[0] : null;
        const startDate = utils.calendarStartDate(organisation, validWorkToken).format('YYYY-MM-DD');
        const endDate = utils.calendarEndDate(organisation, validWorkToken).format('YYYY-MM-DD');
        debug(validWorkTokenValue);
        if (validWorkToken != null && validWorkToken.confirmed != null) {
          return 'none';
        }
        if (validWorkToken != null
          && validWorkToken.confirm == null
          && validWorkTokenValue
          && validWorkTokenValue.fee > 0) {
          return 'khupay';
        }
        if (validWorkToken != null && validWorkToken.confirm == null
          && validWorkToken.type === 'PRODUCT'
          && validWorkTokenValue.method === 'PRODFEE') {
          return 'productprocessingfee';
        }
        if (validWorkToken != null && validWorkToken.confirm == null
          && validWorkToken.type === 'PRODUCT'
          && validWorkTokenValue.method === 'PRODINST') {
          return 'productinstalment';
        }
        if (validWorkToken != null && validWorkToken.confirm == null
          && moment(validWorkToken.checkinTime).isBetween(startDate, endDate)) {
          return 'currentpayroll';
        }
        if (validWorkToken != null && validWorkToken.confirm == null
          && moment(validWorkToken.checkinTime)
            .isBetween(moment(startDate).add(-1, 'month'), moment(endDate).add(-1, 'month'))) {
          return 'previouspayroll';
        }
      } catch (err) {
        error('resolver:Shift:currentTimesheetConfirm:', err);
      }
      return '';
    },

    unconfirmed: async (shift) => {
      let result;
      try {
        const unconfirmedWorkTokens = await WorkToken.find({
          shiftId: shift._id,
          confirmed: null,
          type: { $nin: LEAVE_TYPES },
          createdAt: {
            $gt: moment().add(-30, 'days').toDate(),
          },
        });
        result = (unconfirmedWorkTokens.length > 0);
      } catch (err) {
        error('resolver:Shift:unconfirmed:', err);
      }
      return result;
    },
    unpaid: async (shift) => {
      let result;
      try {
        const unpaidWorkTokensIds = await helpers
          .getUnpaidWorkTokensIds({ shiftId: shift._id, confirmed: true });
        result = (unpaidWorkTokensIds.length > 0);
      } catch (err) {
        error('resolver:Shift:unpaid:', err);
      }
      return result;
    },
    leaveUnconfirmed: async (shift) => {
      let result;
      try {
        const unconfirmedLeaveTokens = await WorkToken.find({
          shiftId: shift._id,
          type: {
            $in: LEAVE_TYPES,
          },
          confirmed: null,
        });
        result = (unconfirmedLeaveTokens.length > 0);
      } catch (err) {
        error('resolver:Shift:leaveUnconfirmed:', err);
      }
      return result;
    },
    unconfirmedLeaves: async (shift) => {
      let result;
      try {
        const unconfirmedLeaveTokens = await WorkToken.find({
          shiftId: shift._id,
          type: { $in: LEAVE_TYPES },
          confirmed: null,
        });
        result = unconfirmedLeaveTokens;
      } catch (err) {
        error('resolver:Shift:unconfirmedLeaves:', err);
      }
      return result;
    },
    currentLeave: async (shift) => {
      let result;
      try {
        const confirmedLeaveTokens = await WorkToken.find({
          shiftId: shift._id,
          type: { $in: LEAVE_TYPES },
          confirmed: true,
        });
        const today = moment();
        const leaveTokenInToday = confirmedLeaveTokens.find((leaveToken) => {
          const { leaveStart, leaveEnd } = leaveToken.references[0];
          return today.isBetween(leaveStart, leaveEnd, undefined, '[]');
        });
        result = leaveTokenInToday;
      } catch (err) {
        error('resolver:Shift:currentLeave:', err);
      }
      return result;
    },
    pastLeaves: async (shift) => {
      let result;
      try {
        const confirmedLeaveTokens = await WorkToken.find({
          shiftId: shift._id,
          type: { $in: LEAVE_TYPES },
          confirmed: true,
        });
        const today = moment();
        const leaveTokensInPast = confirmedLeaveTokens.filter((leaveToken) => {
          const { leaveEnd } = leaveToken.references[0];
          return moment(leaveEnd).isBefore(today);
        });
        result = leaveTokensInPast;
      } catch (err) {
        error('resolver:Shift:pastLeaves:', err);
      }
      return result;
    },
    futureLeaves: async (shift) => {
      let result;
      try {
        const confirmedLeaveTokens = await WorkToken.find({
          shiftId: shift._id,
          type: { $in: LEAVE_TYPES },
          confirmed: true,
        });
        const today = moment();
        const leaveTokensInFuture = confirmedLeaveTokens.filter((leaveToken) => {
          const { leaveStart } = leaveToken.references[0];
          return moment(leaveStart).isAfter(today);
        });
        result = leaveTokensInFuture;
      } catch (err) {
        error('resolver:Shift:futureLeaves:', err);
      }
      return result;
    },
    rejectedLeaves: async (shift) => {
      let result;
      try {
        const rejectedLeaveTokens = await WorkToken.find({
          shiftId: shift._id,
          type: { $in: LEAVE_TYPES },
          confirmed: false,
        });
        result = rejectedLeaveTokens;
      } catch (err) {
        error('resolver:Shift:rejectedLeaves:', err);
      }
      return result;
    },
    leaveSickUpdateDate: async shift => shift.leaveSickUpdateDate || shift.createdAt,
    leavePersonalUpdateDate: async shift => shift.leavePersonalUpdateDate || shift.createdAt,
    leaveSpecialUpdateDate: async shift => shift.leaveSpecialUpdateDate || shift.createdAt,
  },
  WorkTokenValue: {
    expectedWorkTokenShift: async (workTokenValue) => {
      let result;
      try {
        result = await Shift.findById(workTokenValue.expectedWorkTokenShiftId);
      } catch (err) {
        error('resolver:WorkTokenValue:expectedWorkTokenShift:', err);
      }
      return result;
    },
    expectedWorkTokenWorker: async (workTokenValue) => {
      let result;
      try {
        const workerId = workTokenValue.expectedWorkTokenWorkerId;
        if (ObjectId.isValid(workerId)) {
          result = await AlienWorker.findById(workerId);
        } else {
          result = await Worker.findById(workerId);
        }
      } catch (err) {
        error('resolver:WorkTokenValue:expectedWorkTokenWorker:', err);
      }
      return result;
    },
    workToken: async (workTokenValue) => {
      let result;
      try {
        result = await WorkToken.findById(workTokenValue.workTokenId);
      } catch (err) {
        error('resolver:WorkTokenValue:workToken:', err);
      }
      return result;
    },
    discountToken: async (workTokenValue) => {
      let result;
      try {
        result = await DiscountToken.findById(workTokenValue.discountTokenId);
      } catch (err) {
        error('resolver:WorkTokenValue:discountToken:', err);
      }
      return result;
    },
  },
  WorkToken: {
    shift: async (workToken) => {
      let result;
      try {
        result = await Shift.findById(workToken.shiftId);
      } catch (err) {
        error('resolver:WorkToken:shift:', err);
      }
      return result;
    },
    worker: async (workToken) => {
      let result;
      try {
        const workerId = workToken.workerId;
        if (ObjectId.isValid(workerId)) {
          result = await AlienWorker.findById(workerId);
        } else {
          result = await Worker.findById(workerId);
        }
      } catch (err) {
        error('resolver:WorkToken:worker:', err);
      }
      return result;
    },
    workTokenValue: async (workToken) => {
      let result;
      try {
        result = await WorkTokenValue.findOne({ workTokenId: workToken._id });
      } catch (err) {
        error('resolver:WorkToken:workTokenValue:', err);
      }
      return result;
    },
    fiatTokens: async (workToken) => {
      let result;
      try {
        result = await FiatToken.find({ workTokenId: workToken._id });
      } catch (err) {
        error('resolver:WorkToken:fiatTokens:', err);
      }
      return result;
    },
  },
  FiatToken: {
    workToken: async (fiatToken) => {
      let result;
      try {
        result = await WorkToken.findById(fiatToken.workTokenId);
      } catch (err) {
        error('resolver:FiatToken:workToken:', err);
      }
      return result;
    },
    type: async (fiatToken) => {
      let result;
      try {
        const workToken = await WorkToken.findById(fiatToken.workTokenId);
        result = workToken.type;
      } catch (err) {
        error('resolver:FiatToken:type:', err);
      }
      return result;
    },
    notes: async (fiatToken) => {
      let result;
      try {
        const workTokenValue = await WorkTokenValue.findOne({
          workTokenId: fiatToken.workTokenId,
        });
        info('wtvv %O', workTokenValue);
        result = workTokenValue.notes;
      } catch (err) {
        error('resolver:FiatToken:notes:', err);
      }
      return result;
    },
  },
  Entity: {
    __resolveType(obj) {
      if (obj.organisations) {
        return 'Employer';
      }
      if (obj.jobsBookmark) {
        return 'Worker';
      }
      return null;
    },
  },
  ILedgerEntry: {
    __resolveType(obj) {
      if (obj.checkinTime) {
        return 'WorkToken';
      }
      if (obj.amount) {
        return 'FiatToken';
      }
      return null;
    },
  },
  FBJobPost: {
    nReactions:
      post => (post.reactions && post.reactions.summary && post.reactions.summary.total_count) || 0,
    nComments:
      post => (post.comments && post.comments.summary && post.comments.summary.total_count) || 0,
    nShares:
      post => (post.shares && post.shares.count) || 0,
    worker: async (post) => {
      const historyJobUrl = new URL(post.jobLinkAttachment.jobLink.split('/#'));
      const workerId = historyJobUrl.searchParams.get('workerId');
      const worker = workerId && await Worker.findById(workerId);
      return worker;
    },
    employer: async (post) => {
      const historyJobUrl = new URL(post.jobLinkAttachment.jobLink.split('/#'));
      const employerId = historyJobUrl.searchParams.get('employerId');
      const employer = employerId && await Employer.findById(employerId);
      return employer;
    },
    job: async (post) => {
      const historyJobUrl = new URL(post.jobLinkAttachment.jobLink.split('/#'));
      const [, , jobId] = historyJobUrl.pathname.split('/');
      return jobId && Job.findById(ObjectId(jobId));
    },
  },
};

module.exports = resolvers;
