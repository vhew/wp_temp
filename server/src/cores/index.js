const fs = require('fs');
const { merge } = require('lodash');
const { GraphQLModule } = require('@graphql-modules/core');

const typeDef = fs.readFileSync('./models/graphql/schema.gql', 'utf8');
const resolvers = require('./resolvers.js');
const adminResolvers = require('./resolvers/admins');
const queries = require('./resolvers/queries');
const employerResolvers = require('./resolvers/employers');
const workerResolvers = require('./resolvers/workers');
const sharedResolvers = require('./resolvers/shared');


const CoreModule = new GraphQLModule({
  typeDefs: typeDef,
  resolvers: merge(
    resolvers,
    adminResolvers,
    employerResolvers,
    workerResolvers,
    sharedResolvers,
    queries,
  ),
  context: session => session,
});

module.exports = CoreModule;
