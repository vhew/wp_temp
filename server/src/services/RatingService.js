const { ObjectId } = require('mongoose').Types;
const { Organisation, Job, Worker } = require('../../models/mongodb');
const { FeedbackToken } = require('../modules/Feedback/model');
const { JobContract } = require('../modules/JobContracts/model');
const { NotificationToWorker, NotificationToOrg } = require('./Notification');
const constants = require('../utils/constants');

const service = {
  // 𝑎𝑣𝑒𝑟𝑎𝑔𝑒𝑛𝑒𝑤 = 𝑎𝑣𝑒𝑟𝑎𝑔𝑒𝑜𝑙𝑑 + (𝑣𝑎𝑙𝑢𝑒𝑛𝑒𝑤 − 𝑎𝑣𝑒𝑟𝑎𝑔𝑒𝑜𝑙𝑑) / 𝑠𝑖𝑧𝑒𝑛𝑒𝑤 https://math.stackexchange.com/a/957376
  _getNewAvg: (avgOld, valueNew, sizeNew) => avgOld + (valueNew - avgOld) / sizeNew,
  _calculateWorkerFeedback: (feedbackValue) => {
    const scoreBasedFeedbacks = Object
      .entries(JSON.parse(feedbackValue))
      .filter(([key]) => key !== 'note');
    return scoreBasedFeedbacks
      .map(([_, value]) => parseFloat(value))
      .reduce((a, b) => a + b, 0) / scoreBasedFeedbacks.length;
  },
  _getMockWorkerFeedback: () => '{"WORK_AMBIENCE":3,"JOB_DESCRIPTION_ACCURACY":3,"MANAGEMENT_STYLE":3,"COWORKERS_RELATIONSHIP":3,"WILLINGNESS_TO_RECOMMEND":3}',
  _getMockEmployerFeedback: () => '{"feedback":"WILL_HIRE_AGAIN","note":"auto rating"}',

  rateJob: async (workerId, jobContractId, value, reference) => {
    if (!workerId || !jobContractId || !value) throw Error('invalid args');
    let createdFeedbackToken;
    const fromId = workerId;
    const targetId = jobContractId;
    const jobContract = await JobContract.findById(new ObjectId(jobContractId), 'jobId');
    const job = await Job.findById(jobContract.jobId, 'organisationId');
    const organisation = await Organisation.findById(job.organisationId, 'feedbacks name');
    const nFeedback = (organisation.feedbacks.nFeedback || 0) + 1;
    const avg = service._getNewAvg(
      (organisation.feedbacks.avg || 0),
      service._calculateWorkerFeedback(value),
      nFeedback,
    );
    const lastRatedAt = new Date();
    const updatedOrganisation = await Organisation.findByIdAndUpdate(job.organisationId, {
      feedbacks: { nFeedback, avg, lastRatedAt },
    });

    if (updatedOrganisation) {
      createdFeedbackToken = await FeedbackToken.updateOne(
        { fromId, targetId },
        {
          fromId,
          targetId,
          fromType: 'worker',
          targetType: 'contract',
          feedbackType: 'EmployerRating',
          feedbackValue: value,
          reference,
        },
        { upsert: true },
      );
      new NotificationToOrg({
        organisationId: organisation._id,
        from: workerId,
        title: `A worker rated your job (${organisation.name})`,
        body: `avg: ${service._calculateWorkerFeedback(value)}`,
        analyticsLabel: 'notification_worker_rated_employer',
        data: { jobId: job._id },
      }).send();
    }
    return createdFeedbackToken;
  },
  rateWorker: async (jobContractId, workerId, value, reference) => {
    if (!jobContractId || !workerId || !value) throw Error('invalid args');
    let createdFeedbackToken;
    const $inc = {};
    const $set = {};
    const feedback = JSON.parse(value).feedback;
    $inc[`feedbacks.value.${feedback}`] = 1;
    $set['feedbacks.lastRatedAt'] = new Date();
    const jobContract = await JobContract.findById(new ObjectId(jobContractId), 'jobId');
    const job = await Job.findById(jobContract.jobId, 'organisationId');
    const organisation = await Organisation.findById(job.organisationId, 'name');
    const updateWorkerOperation = await Worker.updateOne({ _id: workerId }, { $inc, $set });
    if (updateWorkerOperation.ok) {
      const fromId = jobContractId;
      const targetId = workerId;
      createdFeedbackToken = await FeedbackToken.updateOne(
        { fromId, targetId },
        {
          fromId,
          targetId,
          fromType: 'employer',
          targetType: 'worker',
          feedbackType: 'WorkerRating',
          feedbackValue: value,
          reference,
        },
        { upsert: true },
      );
      new NotificationToWorker({
        from: constants.authorizedAdminIds[0],
        to: workerId,
        title: `An employer rated you (${organisation.name})`,
        body: `feedback - ${feedback}`,
        analyticsLabel: 'notification_employer_rated_worker',
      }).send();
    }
    return createdFeedbackToken;
  },
};


module.exports = service;
