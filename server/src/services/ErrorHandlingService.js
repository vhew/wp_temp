const logError = require('debug')('ERROR');

const { ApolloError } = require('apollo-server-koa');

const ErrorHandlingService = {
  getError: (err) => {
    if (err.extensions && err.extensions.code) {
      return new ApolloError(err.message, err.extensions.code, { exception: true });
    }
    return new Error(err);
  },
  throw: (e) => {
    const error = ErrorHandlingService.getError(e);
    logError(error);
    throw error;
  },
};

module.exports = ErrorHandlingService;
