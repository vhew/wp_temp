const moment = require('moment');
const R = require('ramda');
const { ForbiddenError, ApolloError } = require('apollo-server-koa');
const number = require('joi/lib/types/number');
const ERROR_CODES = require('../utils/errorCodes');
// nLeaves = number of leaves
const LEAVE_TYPES = Object.freeze([
  'LEAVE_SICK',
  'LEAVE_PERSONAL',
  'LEAVE_SPECIAL',
  'LEAVE_UNPAID',
]);

const service = {
  LEAVE_TYPES,
  // checkLeaveTokenType :: leaveToken -> leaveToken
  checkLeaveTokenType: (leaveToken) => {
    if (!service.isLeaveToken(leaveToken)) throw new ForbiddenError('Forbidden - Invalid Leave Token');
    else return leaveToken;
  },

  checkRemainingLeaves: (remainingLeave) => {
    if (remainingLeave < 0) {
      const { message, code } = ERROR_CODES.INSUFFICIENT_LEAVE;
      throw new ApolloError(message, code);
    } else {
      return remainingLeave;
    }
  },

  isLeaveToken: leaveToken => R.is(Object, leaveToken) && LEAVE_TYPES.includes(leaveToken.type),

  calNRemaindingLeaves: ({ shift, organisation, leaveToken }) => {
    const nAvailableLeaves = R.pipe(
      service._getLeaveData,
      service.calNAvailableLeaves,
    )(shift, organisation, leaveToken);

    const nRequiredLeaves = service.getNLeave(leaveToken);

    return nAvailableLeaves - nRequiredLeaves;
  },

  // calNAvailableLeaves :: ({n, n, n}) -> Number
  calNAvailableLeaves: ({
    nLeaves,
    nDaysAfterLastUpdate,
    nLeavesPerYear,
  }) => nLeaves + (nLeavesPerYear * nDaysAfterLastUpdate / 365),

  getNLeave: (leaveToken) => {
    const { leaveStart, leaveEnd, halfDay } = leaveToken.references[0];
    const a = leaveEnd || leaveStart;
    const b = leaveStart;
    const addends = halfDay ? 0.5 : 1;
    return moment(a).diff(b, 'days') + addends;
  },

  _getLeaveData: (shift, organisation, leaveToken) => {
    const {
      leaveSickDays,
      leavePersonalDays,
      leaveSpecialDays,
      leaveSickUpdateDate,
      leavePersonalUpdateDate,
      leaveSpecialUpdateDate,
    } = shift;

    const {
      leavePersonalAccrualRateDaysPerYear,
      leaveSickAccuralRateDaysPerYear,
      leaveSpecialAccuralRateDaysPerYear,
    } = organisation;

    return [
      {
        type: 'LEAVE_SICK',
        nLeaves: leaveSickDays,
        updatedAt: leaveSickUpdateDate || shift.createdAt,
        nLeavesPerYear: leaveSickAccuralRateDaysPerYear,
      },
      {
        type: 'LEAVE_PERSONAL',
        nLeaves: leavePersonalDays,
        updatedAt: leavePersonalUpdateDate || shift.createdAt,
        nLeavesPerYear: leavePersonalAccrualRateDaysPerYear,
      },
      {
        type: 'LEAVE_SPECIAL',
        nLeaves: leaveSpecialDays,
        updatedAt: leaveSpecialUpdateDate || shift.createdAt,
        nLeavesPerYear: leaveSpecialAccuralRateDaysPerYear,
      },
      {
        type: 'LEAVE_UNPAID',
        nLeaves: Number.POSITIVE_INFINITY,
        updatedAt: shift.createdAt,
        nLeavesPerYear: Number.POSITIVE_INFINITY,
      },
    ]
      .map(e => ({ ...e, nDaysAfterLastUpdate: moment().diff(e.updatedAt, 'days') }))
      .find(R.eqProps('type', leaveToken));
  },
};

module.exports = service;
