const NotificationService = require('./NotificationService');

class Notification {
  constructor({
    from, to, title, body, userType, data = {}, mail, analyticsLabel,
  }) {
    this.from = from;
    this.to = to;
    this.title = title;
    this.body = body;
    this.userType = userType;
    this.data = Object.assign({}, data);
    this.mail = Object.assign({}, mail);
    this.analyticsLabel = analyticsLabel;
  }

  static copyFrom(other) {
    return new Notification({
      from: other.from,
      to: other.to,
      title: other.title,
      body: other.body,
      userType: other.userType,
      data: Object.assign({}, other.data),
      mail: Object.assign({}, other.mail),
      analyticsLabel: other.analyticsLabel,
    });
  }

  set(valuesObj) {
    Object.assign(this, valuesObj);
  }

  _getSendableNotification(messageType) {
    const config = {
      messageType: messageType || NotificationService.messageType.PUSH,
    };
    return NotificationService.generateNotification(
      Object.assign(config, this),
    );
  }

  getData() {
    return {
      from: this.from,
      to: this.to,
      title: this.title,
      body: this.body,
      userType: this.userType,
      data: Object.assign({}, this.data),
      mail: Object.assign({}, this.mail),
      analyticsLabel: this.analyticsLabel,
    };
  }

  async send(messageType) {
    return NotificationService.send(this._getSendableNotification(messageType));
  }

  sendToOrganisation(orgId, messageType) {
    const config = {
      messageType: messageType || NotificationService.messageType.PUSH,
    };
    const notificationData = Object.assign(this, config);
    return NotificationService.sendToEmployersInOrganisation(orgId, notificationData);
  }
}

class NotificationToEmployer extends Notification {
  constructor(args) {
    super(args);
    this.userType = NotificationService.userType.EMPLOYER;
  }
}

class NotificationToWorker extends Notification {
  constructor(args) {
    super(args);
    this.userType = NotificationService.userType.WORKER;
  }
}

class NotificationToOrg extends Notification {
  constructor(args) {
    super(args);
    this.organisationId = args.organisationId;
  }

  send(messageType) {
    return this.sendToOrganisation(this.organisationId, messageType);
  }
}

module.exports = {
  Notification, NotificationToEmployer, NotificationToWorker, NotificationToOrg,
};
