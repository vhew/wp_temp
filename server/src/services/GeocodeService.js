/* global fetch, navigator */
/* eslint-disable camelcase */
const debug = require('debug')('DEBUG');
const error = require('debug')('ERROR');

const geocode = async (query) => {
  const response = await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?${query}&key=${process.env.GMAP_API}`,
  );
  const result = await response.json();
  debug(result);
  return result;
};

const townshipDataFromAddressComponents = (addressComponents) => {
  const townshipData = addressComponents.find(({ types }) => types.some(t => t === 'administrative_area_level_3'))
  || addressComponents.find(({ types }) => types.includes('neighborhood'))
  || addressComponents.find(({ types }) => types.includes('locality'));
  debug(townshipData);
  return {
    longName: townshipData.long_name,
    shortName: townshipData.short_name,
  };
};

const townshipDataFromGeoData = (geoData) => {
  const level3GeoData = geoData.results.find(({ types }) => types.some(t => t === 'administrative_area_level_3'));
  return townshipDataFromAddressComponents(level3GeoData.address_components);
};

const townshipDataOf = async (query) => {
  const geoData = await geocode(query);
  return townshipDataFromGeoData(geoData);
};

const locationDataFromGeodata = (geoData) => {
  const { address_components, geometry } = geoData.results.find(({ types }) => types.includes('administrative_area_level_3'))
  || geoData.results[0];
  const townshipData = address_components.find(({ types }) => types.includes('administrative_area_level_3'))
  || address_components.find(({ types }) => types.includes('neighborhood'))
  || address_components.find(({ types }) => types.includes('locality'))
  || address_components.find(({ types }) => types.includes('establishment'))
  || address_components.find(({ types }) => types.includes('administrative_area_level_2'));
  const stateData = address_components.find(({ types }) => types.includes('administrative_area_level_1'));
  const countryData = address_components.find(({ types }) => types.includes('country'));
  const { lat, lng } = geometry.location;
  if (!stateData) error("can't extract state data");
  if (!townshipData) error("can't extract township data");
  const locationData = {
    coordinates: { lat, lng },
    country: countryData.short_name,
    state: stateData && stateData.long_name.split('Region')[0].trim(),
    township: townshipData && townshipData.long_name.split('Township')[0].trim(),
  };
  return locationData;
};

const locationDataOf = async (query) => {
  const geoData = await geocode(query);
  return locationDataFromGeodata(geoData);
};

const getLocation = () => new Promise((resolve, reject) => {
  if (!('geolocation' in navigator)) {
    reject(new Error('Geolocation is not available.'));
  }
  navigator.geolocation.getCurrentPosition(
    (pos) => { debug(pos); resolve(pos); },
    e => reject(e),
  );
});

module.exports = {
  geocode,
  getLocation,
  locationDataOf,
  locationDataFromGeodata,
  townshipDataOf,
  townshipDataFromGeoData,
  townshipDataFromAddressComponents,
};
