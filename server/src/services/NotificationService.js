const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const warn = require('debug')('WARN');
const debug = require('debug')('DEBUG');
const amqp = require('amqplib');
const { isValidObjectId, Types: { ObjectId } } = require('mongoose');
const {
  Worker,
  Employer,
} = require('../../models/mongodb');
const NotificationMessage = require('../../models/NotificationMessage');
const {
  Notification,
} = require('../modules/models');
const { authorizedAdminIds } = require('../utils/constants');

const _sendNotifications = async (methods, user, userType, message) => {
  const amqpUrl = process.env.AMQP_URL || 'amqp://localhost';
  const exchange = 'notifications';
  let connection;
  let channel;
  try {
    debug('_sendNotifications connecting to rabbitmq at: %s', amqpUrl);
    connection = await amqp.connect(amqpUrl);
    channel = await connection.createChannel();
    await channel.assertExchange(exchange, 'direct', { durable: false });
  } catch (err) {
    error('%O', err);
  }
  // send message for each channel
  await Promise.all(methods.map(async (method) => {
    let data;
    if (method === 'PUSH') {
      if (user.fcm) {
        data = {
          userType,
          coordinates: user.fcm,
          message,
        };
      } else {
        warn('FCM coordinates unknown');
        return;
      }
    } else if (method === 'EMAIL') {
      if (user.email) {
        data = {
          coordinates: user.email,
          message,
        };
      } else {
        warn('email coordinates unknown');
        return;
      }
    } else if (method === 'GOOGLE') {
      if (user.google) {
        data = {
          coordinates: user.google,
          message,
        };
      } else {
        warn('google coordinates unknown');
        return;
      }
    } else if (method === 'PHONE') {
      if (user.phone) {
        data = {
          coordinates: user.phone,
          message,
        };
      } else {
        warn('phone coordinates unknown');
        return;
      }
    } else if (method === 'FACEBOOK') {
      if (user.facebook) {
        data = {
          coordinates: user.facebook,
          message,
        };
      } else {
        warn('facebook coordinates unknown');
        return;
      }
    }
    // send message
    try {
      await channel.publish(exchange, method, new Buffer.from(JSON.stringify(data)));
      const notification = new Notification({
        senderId: data.message.senderId,
        receiverId: data.message.receiverId,
        userType: data.userType,
        title: data.message.title,
        body: data.message.body,
        isRead: false,
      });
      await notification.save();

      info('Queued notification %s: %O', method, data);
    } catch (err) {
      error('%O', err);
    }
  })); // Promise.all

  // discconect from rabitmq
  try {
    await channel.close();
    await connection.close();
  } catch (err) {
    error('%O', err);
  }
  return true;
};

const notifications = {
  send: async ({
    userType,
    id,
    messageType,
    message,
  }) => {
    try {
      debug('notification.send: %s %s %s %O', userType, id, messageType, message);
      if (process.env.NODE_ENV === 'testing') {
        debug('NODE_ENV = testing. not sending notification');
        return true;
      }

      // get the relevant user
      let user;
      if (userType === 'worker') {
        user = await Worker.findById(id, 'phone fcm facebook email google notificationHiredUpdates notificationNotHiredUpdates notificationChat notificationJobWatch');
      } else if (userType === 'employer') {
        user = await Employer.findById(id, 'phone fcm facebook email google notificationHiredUpdates notificationNotHiredUpdates notificationChat');
      } else {
        warn('userType invalid');
        return false;
      }
      debug('user: %O', user);
      if (!user) { return false; }

      // send notifications according to the user notification preferences
      if (messageType === notifications.messageType.HIRED) {
        debug('hired notification preferences: %O', user.notificationHiredUpdates);
        await _sendNotifications(user.notificationHiredUpdates, user, userType, message);
      } else if (messageType === notifications.messageType.NOT_HIRED) {
        debug('Not hired notification preferences: %O', user.notificationNotHiredUpdates);
        await _sendNotifications(['PUSH', 'EMAIL'], user, userType, message);
      } else if (messageType === notifications.messageType.CHAT) {
        debug('chat notification preferences: %O', user.notificationChat);
        await _sendNotifications(user.notificationChat, user, userType, message);
      } else if (messageType === notifications.messageType.JOB_WATCH) {
        debug('jobwatch notification preferences: %O', user.notificationJobWatch);
        await _sendNotifications(user.notificationJobWatch, user, userType, message);
      } else if (messageType === notifications.messageType.WORKER_TIMESHEET) {
        await _sendNotifications(['PUSH'], user, userType, message);
      } else if (messageType === notifications.messageType.PUSH) {
        await _sendNotifications(['PUSH'], user, userType, message);
      } else if (messageType === notifications.messageType.PUSH_EMAIL) {
        await _sendNotifications(['PUSH', 'EMAIL'], user, userType, message);
      } else {
        return false;
      }
    } catch (e) {
      error('Error sending notification', e);
    }
    return true;
  },

  sendToEmployersInOrganisation: async (orgId, args) => {
    const validOrgId = isValidObjectId(orgId) && ObjectId(orgId);
    const employers = await Employer.find({ organisationsIds: { $in: validOrgId } }, '_id email');
    debug({ employers });
    const result = await Promise.all(
      employers.map(
        employer => notifications.send(
          notifications.generateNotification({
            ...args,
            to: employer._id,
            mail: { ...args.mail, to: employer },
            userType: notifications.userType.EMPLOYER,
          }),
        ),
      ),
    );
    return result;
  },

  sendToUnregisteredPerson: async ({
    notificationType,
    message,
    coordinates,
  }) => {
    if (notificationType === 'EMAIL') {
      await _sendNotifications(
        ['EMAIL'],
        { coordinates, email: coordinates },
        message,
      );
    }
  },

  generateNotification: ({
    from, to, title, body, userType, messageType, data = {}, mail, analyticsLabel,
  }) => {
    const validFrom = from || authorizedAdminIds[0];
    return {
      id: to,
      userType,
      messageType,
      message: new NotificationMessage(
        validFrom, to,
        title,
        body,
        { ...data, senderId: validFrom, receiverId: to },
        { mail, analyticsLabel },
      ),
    };
  },

  messageType: {
    HIRED: 'hired',
    NOT_HIRED: 'not_hired',
    CHAT: 'chat',
    JOB_WATCH: 'job_watch',
    WORKER_TIMESHEET: 'worker_timesheet',
    PUSH: 'push',
    PUSH_EMAIL: 'push_email',
  },

  userType: {
    EMPLOYER: 'employer',
    WORKER: 'worker',
  },
};

module.exports = Object.assign({}, notifications);
