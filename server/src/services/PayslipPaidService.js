const {
  Shift,
  WorkToken,
} = require('../../models/mongodb/index');

const PayslipPaidService = {
  getTotalPaid: async ({
    workerId,
    jobId,
    from,
    to,
  }) => {
    const shifts = await Shift.find({
      workerId,
      jobId,
    });

    const ret = await WorkToken.aggregate([
      {
        $match: {
          workerId,
          shiftId: {
            $in: shifts.map(e => e._id),
          },
          checkinTime: {
            $gte: new Date(from),
          },
          checkoutTime: {
            $lt: new Date(to),
          },
          type: {
            $in: ['STANDARD', 'BONUS'],
          },
        },
      },
      {
        $lookup: {
          from: 'fiattokens',
          localField: '_id',
          foreignField: 'workTokenId',
          as: 'fiatTokens',
        },
      },
      {
        $unwind: '$fiatTokens',
      },
      {
        $match: {
          'fiatTokens.coordinates': {
            $nin: ['+959966617840', '+959967730220'],
          },
        },
      },
      {
        $group: {
          _id: '$workerId',
          amount: {
            $sum: '$fiatTokens.amount',
          },
        },
      },
    ]);

    if (ret.length === 0) {
      throw new Error('PayslipPaidService.getTotalPaid NULL');
    }
    return ret[0];
  },

  getDisbursementDetail: async ({
    workerId,
    jobId,
    from,
    to,
  }) => {
    const shifts = await Shift.find({
      workerId,
      jobId,
    });

    const ret = await WorkToken.aggregate([
      {
        $match: {
          workerId,
          shiftId: {
            $in: shifts.map(e => e._id),
          },
          checkinTime: {
            $gte: new Date(from),
          },
          checkoutTime: {
            $lt: new Date(to),
          },
        },
      },
      {
        $lookup: {
          from: 'fiattokens',
          localField: '_id',
          foreignField: 'workTokenId',
          as: 'fiatTokens',
        },
      },
      {
        $facet: {
          totalKhuPay: [
            {
              $match: {
                $expr: {
                  $eq: [
                    { $size: '$fiatTokens' },
                    2,
                  ],
                },
              },
            },
            {
              $unwind: '$fiatTokens',
            },
            {
              $group: {
                _id: '$workerId',
                fee: {
                  $sum: {
                    $cond: [
                      {
                        $in: [
                          '$fiatTokens.coordinates',
                          ['+959966617840', '+959967730220'],
                        ],
                      },
                      '$fiatTokens.amount',
                      0,
                    ],
                  },
                },
                amount: {
                  $sum: {
                    $cond: [
                      {
                        $in: [
                          '$fiatTokens.coordinates',
                          ['+959966617840', '+959967730220'],
                        ],
                      },
                      0,
                      '$fiatTokens.amount',
                    ],
                  },
                },
              },
            },
          ],
          totalBonusDeduction: [
            {
              $match: {
                $expr: {
                  $eq: [
                    { $size: '$fiatTokens' },
                    1,
                  ],
                },
              },
            },
            {
              $unwind: '$fiatTokens',
            },
            {
              $group: {
                _id: '$workerId',
                bonus: {
                  $sum: {
                    $cond: [
                      {
                        $in: [
                          '$type',
                          ['BONUS'],
                        ],
                      },
                      '$fiatTokens.amount',
                      0,
                    ],
                  },
                },
                deductions: {
                  $sum: {
                    $cond: [
                      {
                        $in: [
                          '$type',
                          ['DEDUCTION'],
                        ],
                      },
                      '$fiatTokens.amount',
                      0,
                    ],
                  },
                },
              },
            },
          ],
        },
      },
    ]);
    if (ret.length === 0) {
      throw new Error('PayslipPaidService.getDisbursemntDetail NULL');
    }
    return ret[0];
  },
};

module.exports = PayslipPaidService;
