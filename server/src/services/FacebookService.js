const error = require('debug')('ERROR');
const moment = require('moment');
const { default: Axios } = require('axios');

const axios = Axios.create({
  baseURL: 'https://graph.facebook.com/v10.0/',
});

const USER_ID = '964958430980862'; // Karen Swe
const PAGE_ID_JOBDOH_MM = '1983362101899581';

// access token has no expiration date
// https://developers.facebook.com/docs/facebook-login/access-tokens/refreshing#get-a-long-lived-page-access-token
const LONG_LIVED_PAGE_ACCESS_TOKEN = 'EAAChTcdCmHsBAGORr51UbFM2TnpK9WdtiZBAc5UeQWGvU2wNYuyP0V6oOgsiBrpp9g8BVzqUyX8SYBsbUdnFCiVXgg06OqUFx13yilGBwSTxbdsIEjK8yxBhoZC9DUBa0HIkx878CRRphb4cJSCMMEIoiHCFIYPGN7GbqKUztJDexzVmMx';


const helpers = {
  addJobLinkAttachment: (p) => {
    const validDomains = ['beta-worker.jobdoh.com', 'worker.jobdoh.com'];
    const post = JSON.parse(JSON.stringify(p));
    const jobLinkAttachment = post.attachments
    && post.attachments.data
    && post.attachments.data.find((attachment) => {
      const url = new URL(attachment.url || '').searchParams.get('u');
      const matches = `${url}`.match(/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]/);
      return validDomains.includes(matches && matches[0]);
    });
    if (jobLinkAttachment) {
      const jobLink = new URL(jobLinkAttachment.url || '').searchParams.get('u');
      jobLinkAttachment.jobLink = jobLink;
    }
    return { ...post, jobLinkAttachment };
  },

  /**
   * @param {[*]} posts
   */
  getPostsSummary: (posts) => {
    const p = Array.from(posts).filter(e => !!e);
    const nPosts = p.length;
    const nReactions = helpers.getTotalReactionsOf(p);
    const avgReactions = nReactions / nPosts;
    const nShares = helpers.getTotalSharesOf(p);
    const avgShares = nShares / nPosts;
    const nComments = helpers.getTotalCommentsOf(p);
    const avgComments = nComments / nPosts;

    return {
      nPosts, nReactions, avgReactions, nShares, avgShares, nComments, avgComments,
    };
  },

  getTotalReactionsOf: posts => Array.from(posts).reduce((a, b) => {
    const aReactions = (a.reactions && a.reactions.summary && a.reactions.summary.total_count) || 0;
    const bReactions = (b.reactions && b.reactions.summary && b.reactions.summary.total_count) || 0;
    return aReactions + bReactions;
  }, {}),

  getTotalCommentsOf: posts => Array.from(posts).reduce((a, b) => {
    const aComments = (a.comments && a.comments.summary && a.comments.summary.total_count) || 0;
    const bComments = (b.comments && b.comments.summary && b.comments.summary.total_count) || 0;
    return aComments + bComments;
  }, {}),

  getTotalSharesOf: posts => Array.from(posts).reduce((a, b) => {
    const aShares = (a.shares && a.shares.count) || 0;
    const bShares = (b.shares && b.shares.count) || 0;
    return aShares + bShares;
  }, {}),
};

const FBService = {
  /**
     * @returns {Promise<
     * { data: { data: Array, paging: { cursors: { before: String, after: String } } } } >}
     */
  getTaggedPosts: ({ fields = 'id,created_time', filter }) => axios.get(`${PAGE_ID_JOBDOH_MM}/tagged`, { params: { fields, access_token: LONG_LIVED_PAGE_ACCESS_TOKEN } })
    .then(async ({ data }) => {
      let posts = data.data;
      if (filter.startDate || filter.endDate) {
        const start = moment(filter.startDate || new Date());
        const end = moment(filter.endDate || new Date());
        posts = data.data.filter(post => moment(post.created_time).isBetween(start, end, undefined, '[]'));
      }
      const postsWithData = await Promise.all(posts.map(post => FBService.getPost(post.id)));
      return { ...data, data: postsWithData.filter(e => !!e) };
    }),

  getPost: postId => axios.get(`${postId}/`, {
    params: {
      fields: 'permalink_url,status_type,created_time,shares.summary(true),reactions.limit(0).summary(true),attachments,comments.limit(0).summary(true),from{id, name}',
      access_token: LONG_LIVED_PAGE_ACCESS_TOKEN,
    },
  }).then(res => res.data),

  /**
   * @param {String} postId
   * @returns { Promise<Array> }
   */
  getPostAttachments: postId => axios.get(`${postId}/attachments`, { params: { access_token: LONG_LIVED_PAGE_ACCESS_TOKEN } })
    .then(r => Array.from(r.data.data)),

  /**
   * @returns { Promise<{accessToken: String}> }
   */
  getPageAccessToken: async accessToken => axios.get(`${USER_ID}/accounts`, { params: { access_token: accessToken } })
    .then(r => r.data[0]),

  _getPostsWithAttachments: async (posts) => {
    const postsWithAttachments = await Promise.all(Array.from(posts).map(async (post) => {
      const attachments = await FBService.getPostAttachments(post.id);
      return { ...post, attachments };
    }));
    return postsWithAttachments;
  },
};


module.exports = { ...FBService, helpers };
