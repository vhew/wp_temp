const debug = require('debug')('DEBUG');
const logError = require('debug')('ERROR');
const axios = require('axios').default;

const { Employer } = require('./../../models/mongodb');

const getZoomClientIdB64 = () => {
  const env = process.env.NODE_ENV;
  if (env === 'production') return 'eWt6WEFXb3JUMGVKdmtTZ2Q1TmJZZzpQNW9FNTZrc2NQZEVwang1VmdxTHlYWEV2Y0FYY1kybw==';
  if (env === 'staging') return 'RlhLMUNJZktTY2EwbW1pOHZzS0ZBOnlWNzViOXJhekw1cDZEMDRIaTlRSEJhemZPYTNYNTNy';
  return 'QzFNVktWbWxTX0NUdEhVYVlnQW41ZzpsQkdKZTdIejBUdGtQbnh3ZGZrd0dYb29NSmJMUDVxYw==';
};

const redirectUri = () => {
  const env = process.env.NODE_ENV;
  if (env === 'production') return 'https://employer.jobdoh.com';
  if (env === 'staging') return 'https://staging-employer.jobdoh.com';
  return 'https://localhost:3030';
};

const ZoomService = {
  /**
   *
   * @param {String} code zoomAuthCode
   * @returns
   */
  requestAccessToken: async (code, employerId) => {
    try {
      debug('requestAccessToken', { code });
      const grantType = 'authorization_code';
      const url = `https://zoom.us/oauth/token?grant_type=${grantType}&code=${code}&redirect_uri=${redirectUri()}`;
      const body = {};
      const headers = {
        Authorization: `Basic ${getZoomClientIdB64()}`,
      };
      const { data } = await axios
        .post(url, body, { headers })
        .catch(e => logError(e.response.status, e.response.data));
      if (employerId) {
        await ZoomService._updateEmployerZoomAuth(employerId, data);
      }
      return data;
    } catch (e) {
      logError(e);
    } return null;
  },
  refreshAccessToken: async (refreshToken, employerId) => {
    try {
      if (!employerId) return null;
      debug('refreshAccessToken', { refreshToken, employerId });
      const grantType = 'refresh_token';
      const url = `https://zoom.us/oauth/token?grant_type=${grantType}&refresh_token=${refreshToken}`;
      const body = {};
      const headers = {
        Authorization: `Basic ${getZoomClientIdB64()}`,
      };
      const { data } = await axios.post(url, body, { headers });

      debug('refreshAccessToken', data);
      if (data && employerId) {
        await ZoomService._updateEmployerZoomAuth(employerId, data);
      }
      return data;
    } catch (e) {
      logError(e);
      throw e;
    }
  },
  /**
   *
   * @param {String} accessToken Zoom access token
   * @param {{
   *    topic: String
   *    type: Number
   *    start_time: String
   *    duration: Number
   *    timezone: String
   *    password: String
   *    agenda: String
   *    recurrence: {}
   *    settings: {}
   * }} meetingData https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingcreate#request-body
   * @returns
   */
  scheduleMeeting: async (accessToken, meetingData) => {
    try {
      debug('scheduleMeeting', { accessToken, meetingData });
      const userId = 'me';
      const url = `https://api.zoom.us/v2/users/${userId}/meetings`;
      const body = {
        ...meetingData,
        type: 2,
      };
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
      };
      return await axios
        .post(url, body, { headers })
        .catch(e => logError(e.response.status, e.response.data));
    } catch (e) {
      logError(e);
    } return null;
  },
  _updateEmployerZoomAuth: async (employerId, zoomAuth) => {
    try {
      debug('updateEmployerZoomAuth', { employerId, zoomAuth });
      return await Employer
        .updateOne(
          { _id: employerId },
          {
            $set: {
              zoomAccessToken: zoomAuth.access_token,
              zoomRefreshToken: zoomAuth.refresh_token,
            },
          },
        );
    } catch (e) {
      logError(e);
    } return null;
  },
};

module.exports = ZoomService;
