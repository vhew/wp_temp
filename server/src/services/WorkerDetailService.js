const debug = require('debug')('DEBUG');
const {
  AlienWorker,
  Worker,
} = require('../../models/mongodb/index');
const {
  WorkerPaymentMethod,
} = require('../modules/payments/model');

const WorkerDetailService = {
  getPaymentMethods: (workerId) => {
    debug(workerId);
  },

  getPaymentMethod: async ({ workerPaymentMethodId }) => {
    const ret = await WorkerPaymentMethod.findById(workerPaymentMethodId)
      .populate({
        path: 'paymentMethodId',
        ref: 'PaymentMethod',
      });
    return {
      coordinates: ret.coordinates,
      currency: ret.paymentMethodId.currency,
      paymentMethod: ret.paymentMethodId.name,
    };
  },

  getDefaultWorkerPaymentMethod: async ({
    workerId,
  }) => {
    let worker = await Worker.findById(workerId);
    if (worker == null) {
      worker = await AlienWorker.findById(workerId);
    }
    const ret = await WorkerPaymentMethod.findById(worker.defaultWorkerPaymentMethodId)
      .populate({
        path: 'paymentMethodId',
        ref: 'PaymentMethod',
      });

    if (ret == null) {
      throw new Error('WorkerPaymentMethod is not found');
    }
    return {
      coordinates: ret.coordinates,
      currency: ret.paymentMethodId.currency,
      paymentMethod: ret.paymentMethodId.name,
    };
  },
};

module.exports = WorkerDetailService;
