const nodemailer = require('nodemailer');
const Mailgen = require('mailgen');

const EMAIL = 'jobdoh.noreply@gmail.com';
const PASSWORD = 'jobdoh.N0rep1y';

const transporter = nodemailer.createTransport({
  service: 'Gmail',
  secure: true,
  auth: {
    user: EMAIL,
    pass: PASSWORD,
  },
});

const MailGenerator = new Mailgen({
  theme: 'default',
  product: {
    name: 'JOBDOH',
    link: 'https://page.jobdoh.com/',
  },
});

const { NODE_ENV } = process.env;

const EmailService = {
  send: (to, subject, response) => {
    const message = {
      from: EMAIL,
      to: NODE_ENV === 'production' ? to : EMAIL,
      subject,
      html: MailGenerator.generate(response),
    };

    return transporter.sendMail(message);
  },
};

module.exports = EmailService;
