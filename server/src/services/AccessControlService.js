const error = require('debug')('ERROR');
const {
  Employer,
  Organisation,
  Shift,
} = require('../../models/mongodb/index');

const userLevels = {
  workerUserLevels: [
    {
      userLevel: 1,
      features: [
        {
          name: 'VIEW_JOBS',
          description: 'View job cards in your country',
          tasks: [],
          permissions: ['READ_JOBS', 'WRITE_WORKERS'],
        },
      ],
    },
    {
      userLevel: 2,
      features: [
        {
          name: 'VIEW_JOB_DETAIL',
          description: 'View job detail',
          tasks: ['LOGIN'],
          permissions: [],
        },
      ],
    },
    {
      userLevel: 3,
      features: [
        {
          name: 'APPLY_JOB',
          description: 'Apply to job',
          tasks: ['FILL_NRC', 'FILL_MARKETPLACE_PROFILE'],
          permissions: ['APPLY_JOB'],
        },
        {
          name: 'FILTER_JOBS',
          description: 'Filter jobs',
          tasks: [],
          permissions: [],
        },
      ],
    },
    {
      userLevel: 4,
      features: [
        {
          name: 'FACEBOOK_GROUP_INVITATION',
          description: 'Facebook group invitation',
          tasks: [],
          permissions: [],
        },
      ],
    },
    {
      userLevel: 5,
      features: [
        {
          name: 'PREMIUM_CUSTOMER_SUPPORT',
          description: 'Premium customer support & feedback via chatgroup invitation',
          tasks: ['APPLY_TO_JOB'],
          permissions: [],
        },
      ],
    },
    {
      userLevel: 6,
      features: [
        {
          name: '24_7_JOB_SEARCH_NOTIFICATION',
          description: '24/7 job search via notifications',
          tasks: ['COMPLETE_PROFILE', 'COMPLETE_PERSONALITY_QUIZ', 'COMPLETE_DIGITAL_LITERACY_QUIZ', 'COMPLETE_DEMOGRAPHIC_QUIZ'],
          permissions: [],
        },
      ],
    },
  ],
  employerUserLevels: [
    {
      userLevel: 1,
      adminLevel: false,
      title: 'Welcome',
      description: '',
      features: [
        {
          name: 'CREATE_PUBLIC_JOB',
          description: 'Create a job',
          permissions: ['READ_JOBS', 'WRITE_JOBS'],
        },
      ],
    },
    {
      userLevel: 2,
      adminLevel: false,
      title: 'Basic Hiring',
      description: 'Wow you created your first job. Now get workers to apply!',
      features: [
        {
          name: 'POST_JOB',
          description: 'Publish jobs',
          permissions: ['READ_JOBS', 'WRITE_JOBS'],
        },
        // {
        //   name: 'CREATE_MARKETPLACE_JOB',
        //   description: 'Create marketplace jobs',
        //   permissions: ['READ_JOBS', 'WRITE_MARKETPLACE_JOBS'],
        // },
        {
          name: 'REVIEW_ACCEPT_APPLICANTS',
          description: 'Receive and screen candidates\' profiles',
          permissions: ['READ_JOBS', 'WRITE_JOBS'],
        },
        {
          name: 'VIEW_PAST_JOBS',
          description: 'See past jobs',
          permissions: ['READ_JOBS'],
        },
      ],
    },
    {
      userLevel: 3,
      adminLevel: false,
      title: 'Outsourcing your payroll disbursement',
      description: 'Hired!  Your talents are ready to start.  Would you like us to help you pay them?  Please select the payroll method.  Don’t worry, the talents will be paid only after completing their work but we can help you manage the hassle.',
      features: [
        {
          name: 'MARKETPLACE_CHECK_IN',
          description: 'Support check-in/outs',
          permissions: ['CHECK_IN'],
        },
        {
          name: 'TIMESHEET',
          description: 'View and confirm timesheet',
          permissions: ['READ_TIMESHEET', 'WRITE_TIMESHEET'],
        },
        {
          name: 'REPORT',
          description: 'View payroll reports',
          permissions: ['READ_REPORTS'],
        },
      ],
    },
    {
      userLevel: 4,
      adminLevel: false,
      title: 'Running your internal HR operations',
      description: 'Want to have more time to work on more important things than payroll disbursement? Or have up to 28 days to pay your staff without affecting the company morale?  Use our KhuPay service. If you also want to outsource payroll calculation, contact us <a href="https://www.facebook.com/jobdohmyanmar/" target="_">here</a>.',
      features: [
        {
          name: 'CREATE_OFFLINE_JOB',
          description: 'Add and manage existing employees',
          permissions: ['READ_JOBS', 'WRITE_JOBS'],
        },
        {
          name: 'ROSTER',
          description: 'Manage roster',
          permissions: ['READ_WORKERS', 'WRITE_WORKERS'],
        },
        {
          name: 'FULL_CHECKIN',
          description: 'Unlock all check-in modules',
          permissions: ['CHECK_IN_HYBRID', 'CHECK_IN_FULL'],
        },
        {
          name: 'LEAVE_SYS',
          description: 'Manage leave requests',
          permissions: ['LEAVE_SYS'],
        },
        {
          name: 'OUTSOURCE_PAYROLL_CALCULATION',
          description: 'Outsource the payroll calculation',
          permissions: [],
        },
      ],
    },
    {
      userLevel: 5,
      adminLevel: false,
      title: 'Managing multiple entities',
      description: 'Do you or your hiring officers need to manage multiple organisations centrally?',
      features: [
        {
          name: 'EXTRA_ORGANISATIONS',
          description: ' Create subsidiary companies',
          permissions: ['CREATE_MULTIPLE_ORGANISATION'],
        },
        {
          name: 'CREATE_PRIVATE_MARKETPLACE_JOBS',
          description: 'Create internal jobs',
          permissions: ['READ_PRIVATE_MARKETPLACE_JOBS', 'WRITE_PRIVATE_MARKETPLACE_JOBS'],
        },
      ],
    },
    {
      userLevel: 6,
      adminLevel: false,
      title: 'Self-managing payroll and disbursements',
      description: 'Do you want to save on our services charges, but retain access to our financing?\nOr do you want to generate income by using our system to service your own customers?',
      features: [
        {
          name: 'DISBURSEMENT',
          description: 'Use the system we built for yourself to calculate and disburse payroll',
          permissions: ['READ_DISBURSEMENT', 'WRITE_DISBURSEMENT'],
        },
        {
          name: 'ACCESS_LOW_FINACING_RATE',
          description: 'Get access to our low rate payroll financing offers',
          permissions: [],
        },
      ],

    },

    {
      userLevel: 7,
      title: '',
      description: '',
      adminLevel: true,
      features: [],
    },
  ],
};

const AccessControlService = {
  _getWorkerLevel: permission => userLevels.workerUserLevels
    .find(l => l.features.some(f => f.permissions && f.permissions.includes(permission))),
  _getEmployerLevel: permission => userLevels.employerUserLevels
    .find(e => e.features.some(f => f.permissions.includes(permission))),
  _isOwner: async (user, target) => {
    let isOwner = false;

    try {
      if (target.type === 'Job' || target.type === 'Shift') {
        let jobId = target.id;
        if (target.type === 'Shift') {
          const shift = await Shift.findById(target.id);
          jobId = shift.jobId;
        }
        // if by worker
        const shift = await Shift.find({
          workerId: user.id,
          jobId,
        });
        if (shift.length > 0) {
          isOwner = true;
          return isOwner;
        }

        // if by employer
        const organisation = await Organisation.findOne({
          jobsIds: jobId,
        });
        const employer = await Employer.findById(user.id);
        const organisationsIds = employer.organisationsIds.map(e => e.toString());
        if (organisationsIds.includes(organisation._id.toString())) {
          isOwner = true;
          return isOwner;
        }
      }
    } catch (err) {
      error(err);
      throw new Error('AccessControlError', err);
    }
    return isOwner;
  },

};

module.exports = { ...AccessControlService, ...userLevels };
