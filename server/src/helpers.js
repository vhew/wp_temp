const {
  ApolloError,
} = require('apollo-server-koa');
const debug = require('debug')('DEBUG');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const moment = require('moment');
const { ObjectId } = require('mongoose').Types;
const {
  Worker,
  AlienWorker,
  Organisation,
  Job,
  Shift,
  WorkTokenValue,
  WorkToken,
  FiatToken,
} = require('../models/mongodb/');
const { WorkerPaymentMethod } = require('./modules/payments/model');
const gqlToMongoKeys = require('./gql_to_mongo_keys');
const utils = require('./utils');

const LEAVE_TYPES = ['LEAVE_SICK', 'LEAVE_PERSONAL', 'LEAVE_SPECIAL', 'LEAVE_UNPAID'];

const helpers = {
  objectIdToString(objectIds) {
    return objectIds.map(objectId => String(objectId));
  },
  async deleteWorkTokens(
    workTokensIds,
  ) {
    const ret = await Promise.all(workTokensIds
      .map(workTokensId => WorkToken.deleteOne({ _id: workTokensId })))
      .catch(err => error('error %O', err));
    return ret;
  },

  async deleteWorkTokenValues(
    workTokenValuesIds,
  ) {
    const ret = await Promise.all(workTokenValuesIds
      .map(workTokenValuesId => WorkTokenValue.deleteOne({ _id: workTokenValuesId })))
      .catch(err => error('error %O', err));
    return ret;
  },

  async copyWorker({
    srcId,
    destId,
  }) {
    info('calling copyWorker helper with %O, %O', srcId, destId);

    let sourceWorker;
    if (ObjectId.isValid(srcId)) {
      sourceWorker = await AlienWorker.findById(srcId);
    } else {
      sourceWorker = await Worker.findById(srcId);
    }
    debug('sourceWorker %O', sourceWorker);

    // attributes to copy
    const {
      jobsBookmarkIds,
      notificationHiredUpdates,
      notificationNotHiredUpdates,
      notificationChat,
      notificationJobWatch,
      name,
      identification,
      createOrganisationId,
    } = sourceWorker;

    let ret;
    if (ObjectId.isValid(destId)) {
      ret = await AlienWorker.update(
        {
          _id: destId,
        },
        {
          $set: {
            jobsBookmarkIds,
            notificationHiredUpdates,
            notificationNotHiredUpdates,
            notificationChat,
            notificationJobWatch,
            name,
            identification,
            createOrganisationId,
          },
        },
      );
    } else {
      ret = await Worker.findOneAndUpdate(
        {
          _id: destId,
        },
        {
          $set: {
            jobsBookmarkIds,
            notificationHiredUpdates,
            notificationNotHiredUpdates,
            notificationChat,
            notificationJobWatch,
            name,
            identification,
          },
        },
      );
    }
    return ret;
  },

  async migrateShiftWorker({
    srcId,
    destId,
  }) {
    const ret = await Shift.updateMany(
      { workerId: srcId },
      {
        $set: {
          workerId: destId,
        },
        $addToSet: {
          disabledWorkersIds: srcId,
        },
      },
    ).catch(err => error(err));
    return ret;
  },

  migrateWorkerPaymentMethods: async ({
    srcId,
    destId,
  }) => {
    let srcWorker;
    if (ObjectId.isValid(srcId)) {
      srcWorker = await AlienWorker.findById(srcId);
    } else {
      srcWorker = await Worker.findById(srcId);
    }

    if (ObjectId.isValid(destId)) {
      await AlienWorker.findOneAndUpdate({
        _id: destId,
      }, {
        $set: {
          defaultWorkerPaymentMethodId: srcWorker.defaultWorkerPaymentMethodId,
          workerPaymentMethodsIds: srcWorker.workerPaymentMethodsIds,
        },
      }).catch(err => error(err));
    } else {
      await Worker.findOneAndUpdate({
        _id: destId,
      }, {
        $set: {
          defaultWorkerPaymentMethodId: srcWorker.defaultWorkerPaymentMethodId,
          workerPaymentMethodsIds: srcWorker.workerPaymentMethodsIds,
        },
      }).catch(err => error(err));
    }

    const ret = await WorkerPaymentMethod.updateMany({
      workerId: srcId,
    }, {
      $set: {
        workerId: destId,
      },
    }).catch(err => error(err));
    return ret;
  },

  getPayCycle: async ({ jobId, ignorePayCycleAdvancement }) => {
    let organisation = {};
    try {
      const job = await Job.findById(jobId);
      organisation = await Organisation.findById(job.organisationId);
    } catch (err) {
      error(err);
      throw new ApolloError(err);
    }
    let payCycle = utils._getPayCycle({
      payCycleMonthDays: organisation.payCycleMonthDays,
      arbitaryDate: moment.utc(),
    });

    let arbitaryDate = moment.utc();
    if (!utils._isTodayPayCycleAdvancement({
      payCycle,
      payCycleAdvancements: organisation.payCycleAdvancements,
      dateToBeChecked: moment.utc(),
    }) && !ignorePayCycleAdvancement) {
      arbitaryDate = arbitaryDate.subtract(1, 'months');
    }

    payCycle = utils._getPayCycle({
      payCycleMonthDays: organisation.payCycleMonthDays,
      arbitaryDate,
    });
    return payCycle;
  },

  createShiftsWithinPayCycle: async ({
    payCycle,
    endDate,
    jobId,
  }) => {
    let shift = {};
    let nonWorkWeekDays = [];
    let accountingStyle = '';
    try {
      const job = await Job.findById(jobId);
      nonWorkWeekDays = job.nonWorkWeekDays;
      if (job.type === 'FULL_TIME') {
        shift = await Shift.findById(job.shiftsIds[0]);
      }

      if (job.type === 'PART_TIME') {
        //
      }

      const organisation = await Organisation.findById(job.organisationId);
      accountingStyle = organisation.accountingStyle;
    } catch (err) {
      error(err);
      throw new Error(err);
    }

    const shifts = [];
    let realShift;
    const { payCycleStartTime, payCycleEndTime } = payCycle;
    const startDay = (moment.utc(payCycleStartTime).isAfter(moment.utc(shift.startTime)))
      ? moment.utc(payCycleStartTime) : moment.utc(shift.startTime);
    for (let thisDay = moment.utc(startDay);
      thisDay.isBefore(moment.utc(payCycleEndTime))
      && thisDay.isSameOrBefore(moment.utc(shift.endTime));
      thisDay.add(1, 'days')) {
      // realShift = Object.assign({}, shift);
      if (thisDay.isSameOrAfter(moment.utc(endDate), 'day')) {
        const todayShiftFinishedTime = moment.utc()
          .set('hour', moment.utc(shift.endTime).get('hour'))
          .set('minute', moment.utc(shift.endTime).get('minute'));

        debug('endDate %O, todayShiftFinishedTime %O', endDate, todayShiftFinishedTime);
        if (endDate.isBefore(todayShiftFinishedTime) || thisDay.isAfter(endDate)) {
          break;
        }
      }
      const startTime = moment.utc(thisDay)
        .set('hour', moment.utc(shift.startTime).get('hour'))
        .set('minute', moment.utc(shift.startTime).get('minute'));
      const endTime = moment.utc(thisDay)
        .set('hour', moment.utc(shift.endTime).get('hour'))
        .set('minute', moment.utc(shift.endTime).get('minute'));

      realShift = {
        _id: shift._id,
        workerId: shift.workerId,
        employerId: shift.employerId,
        startTime,
        endTime,
      };
      if (accountingStyle === 'actual' && nonWorkWeekDays.includes(thisDay.day())) {
        realShift.isWorkday = false;
      } else {
        realShift.isWorkday = true;
      }
      shifts.push(realShift);
    }
    return shifts;
  },

  calculateVirtualWorkToken: (
    shifts,
  ) => {
    const virtualWorkTokens = shifts.map((shift) => {
      const virtualWorkToken = {
        checkinTime: shift.startTime,
        checkoutTime: shift.endTime,
        type: 'VIRTUAL_STANDARD',
        shiftId: shift._id,
        workerId: shift.workerId,
        confirmed: null,
      };

      if (!shift.isWorkday) {
        virtualWorkToken.type = 'VIRTUAL_MISSING';
      }
      return virtualWorkToken;
    });
    return virtualWorkTokens.filter(virtualWorkToken => virtualWorkToken.type === 'VIRTUAL_STANDARD');
  },

  compareVirtualWorkTokensWithRealWorkTokens: async (payCycle, virtualWorkTokens) => {
    const { payCycleStartTime, payCycleEndTime } = payCycle;
    const realWorkTokens = await WorkToken.find({
      shiftId: virtualWorkTokens[0].shiftId,
      checkinTime: {
        $gte: payCycleStartTime,
        $lt: payCycleEndTime,
      },
      // payNow: true,
    }).catch(err => error(err));
    info('real WT %O', realWorkTokens);

    const filtered = virtualWorkTokens.filter(virtualWorkToken => !realWorkTokens
      .some(realWorkToken => utils
        ._isWorkTokenMatch(realWorkToken, virtualWorkToken)));
    info('filtered WT %O', filtered);
    return filtered;
  },

  createVirtualWorkTokenValues: async (
    fee,
    amount,
    currency,
    method,
    coordinates,
    payCycle,
    virtualWorkTokens,
  ) => {
    const virtualWorkTokenValues = virtualWorkTokens.map((virtualWorkToken) => {
      const virtualWorkTokenValue = {
        expectedWorkTokenShiftId: virtualWorkToken.shiftId,
        expectedWorkTokenWorkerId: virtualWorkToken.workerId,
        expectedWorkTokenType: virtualWorkToken.type,
        expectedWorkTokenCheckinTime: virtualWorkToken.checkinTime.toDate().toISOString(),
        expectedWorkTokenCheckoutTime: virtualWorkToken.checkoutTime.toDate().toISOString(),
        confirmRequest: true,
        amount,
        fee,
        currency,
        method,
        coordinates,
      };
      return virtualWorkTokenValue;
    });
    return virtualWorkTokenValues;
  },

  removeFeeAlreadyConfirmedPayNowFalse: async (shiftId) => {
    const workTokens = await WorkToken.find({
      shiftId,
      confirmed: true,
      payNow: false,
    }).catch(err => error(err));

    const ret = await WorkTokenValue.updateMany({
      workTokenId: {
        $in: workTokens.map(workToken => workToken._id),
      },
    }, {
      $set: {
        fee: 0,
      },
    }).catch(err => error(err));
    return ret.nModified;
  },

  getPayNowFalseWorkTokenValues: async (shiftId) => {
    const payNowFalseWorkTokens = await WorkToken.find({
      shiftId,
      confirmed: true,
      payNow: false,
    }).catch(err => error(err));

    const workTokenValues = await WorkTokenValue.find({
      workTokenId: {
        $in: payNowFalseWorkTokens.map(workToken => workToken._id),
      },
    }).catch(err => error(err));
    return workTokenValues;
  },

  updateAlreadyConfirmedPayNowFalseWorkTokens: async (numShiftsPay, shiftId) => {
    const payNowFalseWorkTokens = await WorkToken.find({
      shiftId,
      confirmed: true,
      payNow: false,
    }).catch(err => error(err));

    const workTokens = payNowFalseWorkTokens.slice(0, numShiftsPay);

    const ret = await WorkToken.updateMany({
      _id: {
        $in: workTokens.map(workToken => workToken._id),
      },
    }, {
      $set: {
        payNow: true,
      },
    }).catch(err => error(err));

    return ret.nModified;
  },

  createWorkTokens: async (numShiftsPay, virtualWorkTokens) => {
    if (numShiftsPay > virtualWorkTokens.length) {
      throw new ApolloError('Not enough shifts to satify requested KhuPay');
    }

    let requestedWorkTokens;
    // eslint-disable-next-line no-restricted-globals
    if (numShiftsPay != null && !isNaN(numShiftsPay)) {
      requestedWorkTokens = virtualWorkTokens.slice(0, numShiftsPay);
    } else {
      requestedWorkTokens = virtualWorkTokens;
    }
    const workTokens = await Promise.all(requestedWorkTokens
      .map((requestedWorkToken) => {
        // eslint-disable-next-line no-param-reassign
        requestedWorkToken.type = 'STANDARD';
        const workToken = new WorkToken(requestedWorkToken);
        return workToken.save();
      })).catch(err => error(err));
    return workTokens;
  },
  // Phoenix Created
  updateWorkTokenValue: async (
    workTokenValueId,
    updateData,
  ) => {
    const ret = await WorkTokenValue.findOneAndUpdate(
      { _id: workTokenValueId },
      updateData,
      { new: true },
    );
    return ret;
  },
  getLatestWorkTokenValue: async (
    workTokenValues,
  // eslint-disable-next-line arrow-body-style
  ) => {
    // const moments = WorkTokenValues.map(token => moment(token.expectedWorkTokenCheckinTime));
    // const maxDate = moment.max(moments);
    return workTokenValues[0];
  },

  createWorkTokenValues: async (
    fee,
    amount,
    currency,
    method,
    coordinates,
    payCycle,
    workTokens,
  ) => {
    const workTokenValues = await Promise.all(workTokens.map((workToken) => {
      const workTokenValue = new WorkTokenValue({
        expectedWorkTokenShiftId: workToken.shiftId,
        expectedWorkTokenWorkerId: workToken.workerId,
        expectedWorkTokenType: 'STANDARD',
        expectedWorkTokenCheckinTime: workToken.checkinTime,
        expectedWorkTokenCheckoutTime: workToken.checkinTime,
        workTokenId: workToken._id,
        confirmRequest: true,
        amount,
        fee,
        currency,
        method,
        coordinates,
      });
      return workTokenValue.save();
    })).catch(err => error(err));
    return workTokenValues;
  },

  createDeductionWorkTokensAndWorkTokenValues: async (
    shift, worker, payCycle, renumerationValue, deductions,
  ) => {
    const DEDUCTION_COORDINATES = '+454213209153';
    let { payCycleStartTime, payCycleEndTime } = payCycle;
    payCycleStartTime = moment.utc(payCycleStartTime);
    payCycleEndTime = moment.utc(payCycleEndTime);
    const checkinTime = moment.utc(payCycleStartTime)
      .set('hour', moment.utc(shift.startTime).get('hour'))
      .set('minute', moment.utc(shift.startTime).get('minute'));
    const checkoutTime = moment.utc(payCycleEndTime)
      .subtract(1, 'days')
      .set('hour', moment.utc(shift.endTime).get('hour'))
      .set('minute', moment.utc(shift.endTime).get('minute'));

    const isCurrentPayCycleDeductions = (deduction) => {
      const enabledDate = moment.utc(deduction.enabledDate);
      const disabledDate = moment.utc(deduction.disabledDate);
      if (enabledDate.isSame(disabledDate)) {
        if (payCycleStartTime.isBefore(enabledDate) && payCycleEndTime.isAfter(enabledDate)) {
          return true;
        }
      } else if (enabledDate.isBefore(payCycleEndTime) && disabledDate.isAfter(payCycleEndTime)) {
        return true;
      }
      return false;
    };
    const currentPayCycleDeductions = deductions.filter(isCurrentPayCycleDeductions);

    const workTokenValues = await Promise.all(currentPayCycleDeductions
      .map(async (currentPayCycleDeduction) => {
        const workToken = new WorkToken({
          confirmed: true,
          payNow: true,
          checkinTime,
          checkoutTime,
          type: 'DEDUCTION',
          shiftId: shift._id,
          workerId: worker._id,
        });
        const createdWorkToken = await workToken.save();

        let amount;
        if (currentPayCycleDeduction.type === 'PERCENTAGE') {
          const percentage = parseInt(currentPayCycleDeduction.amount, 10);
          amount = renumerationValue * percentage / 100;
        } else {
          amount = currentPayCycleDeduction.amount;
        }
        const workTokenValue = new WorkTokenValue({
          confirmRequest: false,
          expectedWorkTokenShiftId: shift._id,
          expectedWorkTokenWorkerId: worker._id,
          expectedWorkTokenType: 'DEDUCTION',
          expectedWorkTokenCheckinTime: createdWorkToken.checkinTime,
          expectedWorkTokenCheckoutTime: createdWorkToken.checkoutTime,
          workTokenId: createdWorkToken._id,
          amount,
          fee: 0,
          currency: worker.paymentCurrency,
          method: worker.paymentMethod,
          coordinates: DEDUCTION_COORDINATES,
          notes: currentPayCycleDeduction.notes,
        });
        return workTokenValue.save();
      }));
    return workTokenValues;
  },

  // work-in-progress
  createPayrollWorkTokens: async ({ shiftId, payCycle }) => {
    const workTokensInPayCycle = await WorkToken.find({
      shiftId,
      checkinTime: {
        $gte: payCycle.payCycleStartTime,
      },
    }).catch(err => error(err));
    return workTokensInPayCycle;
  },

  getUnpaidWorkTokensIds: async ({ shiftId, confirmed }) => {
    let workTokens;
    const shift = await Shift.findById(shiftId);
    if (confirmed) {
      workTokens = await WorkToken.find({
        // shiftId, temp remove
        workerId: shift.workerId,
        confirmed,
        type: { $nin: LEAVE_TYPES },
        createdAt: {
          $gt: moment().add(-2, 'months').toDate(),
        },
        payNow: true,
      }).catch(err => error(err));
    } else {
      workTokens = await WorkToken.find({
        shiftId,
        type: { $nin: LEAVE_TYPES },
        createdAt: {
          // $gt: new Date('2020-03-01'),
          $gt: moment().add(-2, 'months').toDate(),
        },
      }).catch(err => error(err));
    }
    const workTokensIds = helpers
      .objectIdToString(workTokens.map(workToken => workToken._id));

    const fiatTokens = await FiatToken.find({
      workTokenId: {
        $in: workTokensIds,
      },
    }).catch(err => error(err));

    const paidWorkTokensIds = helpers
      .objectIdToString(fiatTokens.map(fiatToken => fiatToken.workTokenId));

    const unpaidWorkTokensIds = workTokensIds
      .filter(workTokenId => !paidWorkTokensIds.includes(workTokenId));

    return unpaidWorkTokensIds;
  },
  renameKeys: (keysMap, obj) => Object.keys(obj).reduce(
    (acc, key) => ({
      ...acc,
      ...{ [keysMap[key] || key]: obj[key] },
    }),
    {},
  ),
  renameJson(json, oldkey, newkey) {
    return Object.keys(json)
      .reduce((s, item) => (item === oldkey ? ({ ...s, [newkey]: json[oldkey] })
        : ({ ...s, [item]: json[item] })), {});
  },

  generateGqlToMongoQuery: (obj) => {
    const queryObject = obj;
    if (obj == null) {
      return queryObject;
    }
    Object.keys(queryObject).forEach((key) => {
      let newKey = '';
      if (typeof queryObject[key] === 'string' && ObjectId.isValid(queryObject[key])) {
        queryObject[key] = new ObjectId(queryObject[key]);
        debug('changesKey', queryObject[key]);
      }
      if (typeof queryObject[key] === 'string' && moment(queryObject[key], moment.ISO_8601, true).isValid()) {
        queryObject[key] = new Date(queryObject[key]);
      }
      Object.keys(gqlToMongoKeys).forEach((gqlKey) => {
        if (key === gqlKey) {
          Object.defineProperty(queryObject, gqlToMongoKeys[gqlKey],
            Object.getOwnPropertyDescriptor(queryObject, key));
          delete queryObject[key];
          newKey = gqlToMongoKeys[gqlKey];
        }
      });
      if (typeof queryObject[newKey] === 'object') {
        debug('I am here');
        helpers.generateGqlToMongoQuery(queryObject[newKey]);
      }
      if (typeof queryObject[key] === 'object') {
        helpers.generateGqlToMongoQuery(queryObject[key]);
      }
      if (Array.isArray(queryObject[key])) {
        helpers.generateGqlToMongoQuery(queryObject[key]);
      }
    });
    return queryObject;
  },

  unpaid: async (shift) => {
    const unpaidWorkTokensIds = await helpers
      .getUnpaidWorkTokensIds({ shiftId: shift._id, confirmed: true });
    return (unpaidWorkTokensIds.length > 0) || false;
  },

  workerHasWithdrawnJob: async (jobId, workerId) => {
    try {
      const worker = await Worker.findOne({ id: workerId });
      if (!worker) throw new Error(`Error finding worker. id: ${workerId}`);
      return worker.jobsWithdrewIds.includes(jobId);
    } catch (err) {
      throw new ApolloError(err);
    }
  },

};

module.exports = helpers;
