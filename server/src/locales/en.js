
const en = {
  applyShiftTitle: 'Application Received',
  applyShiftBody: '@@@ has applied to ###.',
  offerShiftTitle: 'Job Offer',
  offerShiftBody: '@@@ offered you the position of ###. Go accept to job offer!',
  acceptShiftTitle: 'Job Offer Accepted',
  acceptShiftBody: '@@@ has accepted job offer to ###.',
  resignShiftTitle: 'RESIGNATION Request',
  resignShiftBody: '@@@ has request to resign from ###.',
  tradeShiftTitle: 'Urgent',
  tradeShiftBody: '@@@ requested you to work their shift in ### job. Please accept or reject ASAP.',
  tradeShiftOwnerTitle: '',
  tradeShiftOwnerBody: 'Request to work on your shift has been sent. Recommand you also call them ($$$) to let them know.',
  tradeShiftConfirmTitle: 'Request Accepted',
  tradeShiftConfirmBody: '@@@ accepted to work on your shift in ### job.',
  tradeShiftRejectTitle: 'Request Rejected',
  tradeShiftRejectBody: '@@@ rejected to work on your shift in ### job.',
};
module.exports = en;
