
const mm = {
  applyShiftTitle: 'mm Application Received',
  applyShiftBody: 'mm @@@ has applied to ###.',
  offerShiftTitle: 'mm Job Offer',
  offerShiftBody: 'mm @@@ offered you ###. You still need to accept this.',
  acceptShiftTitle: 'mm Job Offer Accepted',
  acceptShiftBody: 'mm @@@ has accepted job offer to ###.',
  resignShiftTitle: 'RESIGNATION Request',
  resignShiftBody: '@@@ has request to resign from ###.',
  tradeShiftTitle: 'Urgent',
  tradeShiftBody: '@@@ requested you to work their shift in ### job. Please accept or reject ASAP.',
  tradeShiftOwnerTitle: '',
  tradeShiftOwnerBody: 'Request to work on your shift has been sent. Recommand you also call them ($$$) to let them know.',
  tradeShiftConfirmTitle: 'Request Accepted',
  tradeShiftConfirmBody: '@@@ accepted to work on your shift in ### job.',
};
module.exports = mm;
