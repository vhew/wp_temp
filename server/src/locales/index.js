/* eslint-disable camelcase */

const en = require('./en');
const vn = require('./vn');
const mm = require('./mm');
const zg = require('./zg');
const zh_Hant = require('./zh_Hant');

const locales = {
  en,
  mm,
  vn,
  zg,
  zh_Hant,
  translate: (langVariableName, lang) => {
    let translation;
    if (lang === 'en') {
      translation = locales.en[langVariableName];
    }
    if (lang === 'vn') {
      translation = locales.vn[langVariableName];
    }
    if (lang === 'mm') {
      translation = locales.mm[langVariableName];
    }
    if (lang === 'zg') {
      translation = locales.zg[langVariableName];
    }
    if (lang === 'zh_Hant') {
      translation = locales.zh_Hant[langVariableName];
    }
    return translation;
  },
};

module.exports = locales;
