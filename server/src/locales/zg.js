
const zg = {
  applyShiftTitle: 'zg Application Received',
  applyShiftBody: 'zg @@@ has applied to ###.',
  offerShiftTitle: 'zg Job Offer',
  offerShiftBody: 'zg @@@ offered you ###. You still need to accept this.',
  acceptShiftTitle: 'zg Job Offer Accepted',
  acceptShiftBody: 'zg @@@ has accepted job offer to ###.',
  resignShiftTitle: 'RESIGNATION Request',
  resignShiftBody: '@@@ has request to resign from ###.',
  tradeShiftTitle: 'Urgent',
  tradeShiftBody: '@@@ requested you to work their shift in ### job. Please accept or reject ASAP.',
  tradeShiftOwnerTitle: '',
  tradeShiftOwnerBody: 'Request to work on your shift has been sent. Recommand you also call them ($$$) to let them know.',
  tradeShiftConfirmTitle: 'Request Accepted',
  tradeShiftConfirmBody: '@@@ accepted to work on your shift in ### job.',
};
module.exports = zg;
