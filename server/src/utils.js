const debug = require('debug')('DEBUG');
const error = require('debug')('ERROR');
const { ApolloError, AuthenticationError } = require('apollo-server-koa');
const { log } = require('debug');
const moment = require('moment');
const { ObjectId } = require('mongoose').Types;
const {
  Worker,
  Employer,
  Organisation,
  Job,
  Shift,
  WorkTokenValue,
  WorkToken,
} = require('../models/mongodb/');
const { ExpectedMarketplaceSubscriber } = require('../src/modules/marketplaces/model');
const { _getEmployerLevel, _getWorkerLevel } = require('./services/AccessControlService');
const { UNAUTHORIZED_ACCESS } = require('./utils/errorCodes');
const TAX_CONFIGURATION_REFERENCES = require('./utils/taxConfigurationReferences');
const constants = require('./utils/constants');
const errorCodes = require('./utils/errorCodes');
const ZoomService = require('./services/ZoomService');

const utils = {
  _isAdmin: async (user) => {
    let isAdmin = false;

    if (constants.authorizedAdminIds.includes(user.id)) {
      isAdmin = true;
    }
    debug(user, 'is admin: ', isAdmin);
    return isAdmin;
  },

  _isOwner: async (user, target) => {
    let isOwner = false;
    // const organisationJobKeys = [
    //   'jobsIds',
    // ];
    // isOwner if user has same id as target
    if (target.type === 'Employer') {
      // isOwner if target.id is me
      if (user.id === target.id) {
        isOwner = true;
      }
    } else if (target.type === 'Worker') {
      // isOwner if target.id is me
      if (user.id === target.id) {
        isOwner = true;
      } else {
        // if this worker is not verified and has createOrganisationId
        const worker = await Worker.findById(target.id, 'verified createOrganisationId');
        if (!worker.verified) {
          // make sure worker.createOrganisationId is one of my organisations
          const employer = await Employer.findById(user.id, 'organisationsIds');
          if (employer) {
            const organisationsIds = employer.organisationsIds.map(obj => String(obj));
            if (organisationsIds.includes(String(worker.createOrganisationId))) {
              isOwner = true;
            }
          }
        }
      }
      // isOwner if can traverse from Employer to Organisation
    } else if (target.type === 'Organisation') {
      const employer = await Employer.findById(user.id, 'organisationsIds');
      if (employer) {
        const organisationsIds = employer.organisationsIds.map(obj => String(obj));
        if (organisationsIds.includes(target.id)) {
          isOwner = true;
        }
      }
      // isOwner if can traverse from Employer to Job
    } else if (target.type === 'Job') {
      const employer = await Employer.findById(user.id, 'organisationsIds');
      if (employer) {
        // look through each organisation for jobs
        const organisationsIds = employer.organisationsIds.map(obj => String(obj));
        const job = await Job.findById(target.id);
        if (organisationsIds.includes(job.organisationId)) {
          isOwner = true;
        }
      }
      // isOwner if can traverse from Employer to Shift
    } else if (target.type === 'Shift') {
      const job = await Job.findOne({ shiftsIds: target.id });
      const employer = await Employer.findById(user.id, 'organisationsIds');
      employer.organisationsIds.map((organisationId) => {
        if (String(organisationId) === String(job.organisationId)) {
          isOwner = true;
        }
        return organisationId;
      });
    } else if (target.type === 'WorkTokenValue') {
      try {
        const workTokenValue = await WorkTokenValue.findById(target.id, 'expectedWorkTokenShiftId');
        const shift = await Shift.findById(workTokenValue.expectedWorkTokenShiftId, 'jobId');
        const job = await Job.findById(shift.jobId, 'organisationId');
        const employer = await Employer.findById(user.id, 'organisationsIds');
        debug(job);
        debug(employer);
        if (employer) {
          const organisationsIds = employer.organisationsIds.map(obj => String(obj));
          if (organisationsIds.includes(String(job.organisationId))) {
            isOwner = true;
          }
        }
      } catch (err) {
        error(err);
      }
    }
    return isOwner;
  },

  isWorker: async (id) => {
    const worker = await Worker.findById(id, '_id');
    return !!worker;
  },

  calendarStartDate(organisation, worktoken) {
    const payCycle = organisation.payCycleMonthDays;
    const today = moment();
    let startTime = moment().set('date', payCycle[0]);
    const payCycleStart = moment().set('date', payCycle[0]);
    if (worktoken != null) {
      startTime = moment(worktoken.checkinTime).add(1, 'day');
      if (startTime >= payCycleStart) {
        return payCycleStart;
      }
      return payCycleStart.add(-1, 'M');
    }
    this.$log.debug('startTime... ', startTime);
    this.$log.debug('payCycleStart... ', payCycleStart);
    if (today >= startTime) {
      return payCycleStart;
    }
    // this.$log.debug(moment(this.filteredWorkTokens[0].checkinTime).format('YYYY-MM-DD'));
    return payCycleStart.add(-1, 'M');
  },

  calendarEndDate(organisation, worktoken) {
    const payCycle = organisation.payCycleMonthDays;
    let startTime = moment().set('date', payCycle[0]);
    const today = moment();
    const payCycleStart = moment().set('date', payCycle[0]);
    if (worktoken != null) {
      startTime = moment(worktoken.checkinTime).add(1, 'day');
      if (startTime >= payCycleStart) {
        return moment().set('date', payCycle[1]).add(1, 'M');
      }
      return moment().set('date', payCycle[1]);
    }
    if (today >= startTime) {
      return moment().set('date', payCycle[1])
        .add(1, 'M');
    }
    return moment().set('date', payCycle[1]);
  },

  _isShared: async (user, target) => {
    let isShared = false;
    // isShared if user has same id Shift.worker
    if (target.type === 'Shift') {
      const shift = await Shift.findById(target.id, 'workerId organisationId');
      if (shift) {
        if (user.id === String(shift.workerId)) {
          isShared = true;
        } else {
          const employer = await Employer.findById(user.id, 'organisationsIds');
          if (employer) {
            if (employer.organisationsIds.map(obj => String(obj))
              .includes(String(shift.organisationId))) {
              isShared = true;
            }
          }
        }
      }
    }
    return isShared;
  },

  _getFeeSimple: ({
    amount,
    feePercent,
  }) => {
    const fee = amount * feePercent / 100;
    return fee;
  },

  _isTodayPayCycleAdvancement: ({
    payCycle,
    payCycleAdvancements,
    dateToBeChecked,
  }) => {
    let { payCycleStartTime, payCycleEndTime } = payCycle;
    payCycleStartTime = moment.utc(payCycleStartTime);
    payCycleEndTime = moment.utc(payCycleEndTime);

    const currentPayCycleAdvancement = payCycleAdvancements
      .find((payCycleAdvancement) => {
        const payCycleIdentifier = moment.utc(payCycleAdvancement.payCycleIdentifier);
        return (payCycleStartTime.isBefore(payCycleIdentifier)
          && payCycleEndTime.isAfter(payCycleIdentifier));
      });
    if (currentPayCycleAdvancement == null) return false;

    const { payCycleIdentifier } = currentPayCycleAdvancement;
    const momentDateToBeChecked = moment.utc(dateToBeChecked).startOf('day');

    return (momentDateToBeChecked.isSameOrAfter(payCycleIdentifier)
      && momentDateToBeChecked.isSameOrBefore(payCycleEndTime));
  },

  _getPayCycleIdentifier: ({
    payCycle,
    payCycleAdvancements,
  }) => {
    let { payCycleStartTime, payCycleEndTime } = payCycle;
    payCycleStartTime = moment.utc(payCycleStartTime);
    payCycleEndTime = moment.utc(payCycleEndTime);

    const currentPayCycleAdvancement = payCycleAdvancements
      .find((payCycleAdvancement) => {
        const payCycleIdentifier = moment.utc(payCycleAdvancement.payCycleIdentifier);
        return (payCycleStartTime.isBefore(payCycleIdentifier)
          && payCycleEndTime.isAfter(payCycleIdentifier));
      });
    if (currentPayCycleAdvancement == null) return null;

    const { payCycleIdentifier } = currentPayCycleAdvancement;
    return payCycleIdentifier;
  },

  _getPayCycle: ({
    payCycleMonthDays,
    arbitaryDate,
    predeterminedPayCycleEndTime,
  }) => {
    debug('calling _getPayCycle with payCycleMonthDays %O, arbitaryDate %O, predeterminedPayCycleEndTime %O',
      payCycleMonthDays, arbitaryDate, predeterminedPayCycleEndTime);
    let payCycleEndTime;
    let payCycleStartTime;
    const arbitaryDateNumber = arbitaryDate.date();

    for (let i = 0; i < payCycleMonthDays.length - 1; i += 1) {
      if (payCycleMonthDays[i] < arbitaryDateNumber
          && arbitaryDateNumber <= payCycleMonthDays[i + 1]) {
        payCycleStartTime = moment.utc(arbitaryDate).set('date', payCycleMonthDays[i]).startOf('day');
        payCycleEndTime = moment.utc(arbitaryDate).set('date', payCycleMonthDays[i + 1]).startOf('day');
      }
    }

    const [payCycleDayNumber] = payCycleMonthDays;
    if (payCycleStartTime == null && payCycleEndTime == null) {
      payCycleEndTime = moment.utc(arbitaryDate)
        .set('date', payCycleMonthDays[payCycleMonthDays.length - 1])
        .startOf('day');
      payCycleEndTime.add(1, 'month');
      payCycleStartTime = moment.utc(arbitaryDate)
        .set('date', payCycleMonthDays[payCycleMonthDays.length - 2])
        .startOf('day');
    }
    if (arbitaryDate.date() < payCycleDayNumber) {
      payCycleStartTime.subtract(1, 'month');
      payCycleEndTime.subtract(1, 'month');
    }

    if (predeterminedPayCycleEndTime && moment.utc(predeterminedPayCycleEndTime).startOf('day')
      .isBefore(payCycleEndTime)) {
      payCycleEndTime = moment.utc(predeterminedPayCycleEndTime).startOf('day');
    }

    return {
      payCycleEndTime,
      payCycleStartTime,
    };
  },

  _getPayCycleLength: ({
    payCycleMonthDays,
  }) => {
    if (payCycleMonthDays.length === 2) {
      return 30;
    }
    const length = payCycleMonthDays[1] - payCycleMonthDays[0];
    return length;
  },

  _isWorkTokenMatch: (workToken1, workToken2) => {
    // debug('_isWorkTokenMatch: \nworkToken1: %O\nworkToken2: %O', workToken1, workToken2);
    const varianceSec = 60 * 60; // 1hr
    let isMatch = false;
    let checkinTimeMin = moment.utc(workToken2.checkinTime);
    let checkinTimeMax = moment.utc(workToken2.checkinTime);
    let checkoutTimeMin = moment.utc(workToken2.checkoutTime);
    let checkoutTimeMax = moment.utc(workToken2.checkoutTime);
    checkinTimeMin = checkinTimeMin.subtract(varianceSec, 'seconds');
    checkinTimeMax = checkinTimeMax.add(varianceSec, 'seconds');
    checkoutTimeMin = checkoutTimeMin.subtract(varianceSec, 'seconds');
    checkoutTimeMax = checkoutTimeMax.add(varianceSec, 'seconds');
    const workToken1CheckinTime = moment.utc(workToken1.checkinTime);
    const workToken1CheckoutTime = moment.utc(workToken1.checkoutTime);
    try {
      if (workToken1CheckinTime.isBetween(checkinTimeMin, checkinTimeMax)
          && workToken1CheckoutTime.isBetween(checkoutTimeMin, checkoutTimeMax)) {
        isMatch = true;
      }
    } catch (err) {
      error(err);
    }
    return isMatch;
  },

  // deprecated
  _isWorkComplete: async (shift, fiatToken) => {
    // debug('_isWorkComplete: \nshift: %O\nfiatToken: %O', shift, fiatToken);
    let workComplete = false;
    // const shiftEndTime = moment.utc(shift.endTime);
    const workToken = await WorkToken.findById(fiatToken.workTokenId);
    const workTokenFromShift = {
      checkinTime: shift.startTime,
      checkoutTime: shift.endTime,
    };
    if (utils._isWorkTokenMatch(workToken, workTokenFromShift)) {
      workComplete = true;
    }
    return workComplete;
  },

  _calculateTotalWorkDays: async ({
    accountingStyle,
    payCycle,
    job,
    workerId,
  }) => {
    const { payCycleStartTime, payCycleEndTime } = payCycle;

    if (job.type === 'FULL_TIME') {
      if (accountingStyle === 'simple') {
        return payCycleEndTime.diff(payCycleStartTime, 'days');
      }

      let totalWorkDays = 0;
      for (let thisDay = moment.utc(payCycleStartTime);
        thisDay.isBefore(moment.utc(payCycleEndTime));
        thisDay.add(1, 'days')) {
        if (!job.fulltimeNonWorkWeekDays.includes(thisDay.day())) {
          totalWorkDays += 1;
        }
      }
      return totalWorkDays;
    }

    if (job.type === 'PART_TIME') {
      const shifts = await Shift.find({
        jobId: job.id,
        workerId,
        startTime: {
          $gte: payCycleStartTime,
          $lt: payCycleEndTime,
        },
      });
      const totalShifts = shifts.length;
      return totalShifts;
    }
    return 0;
  },

  _getAdjustedRenumeration: (payCycle, renumerationValue, deductions) => {
    debug(
      'calling _getAdjustedRenumeration with payCycle %O, renumerationValue %O, deductions %O',
      payCycle, renumerationValue, deductions,
    );

    let { payCycleStartTime, payCycleEndTime } = payCycle;
    payCycleStartTime = moment.utc(payCycleStartTime);
    payCycleEndTime = moment.utc(payCycleEndTime);

    const isCurrentPayCycleDeductions = (deduction) => {
      const enabledDate = moment.utc(deduction.enabledDate);
      const disabledDate = moment.utc(deduction.disabledDate);
      if (enabledDate.isSame(disabledDate)) {
        if (payCycleStartTime.isBefore(enabledDate) && payCycleEndTime.isAfter(enabledDate)) {
          return true;
        }
      } else if (enabledDate.isBefore(payCycleEndTime) && disabledDate.isAfter(payCycleEndTime)) {
        return true;
      }
      return false;
    };
    const currentPayCycleDeductions = deductions.filter(isCurrentPayCycleDeductions);
    debug('currentPayCycleDeductions %O', currentPayCycleDeductions);

    let adjustedRenumerationValue = renumerationValue;
    currentPayCycleDeductions.forEach((currentPayCycleDeduction) => {
      const { amount } = currentPayCycleDeduction;
      if (currentPayCycleDeduction.type === 'PERCENTAGE') {
        const percentage = parseInt(amount, 10);
        adjustedRenumerationValue -= (renumerationValue * percentage / 100);
      } else {
        const fixedAmount = parseInt(amount, 10);
        adjustedRenumerationValue -= fixedAmount;
      }
    });
    return adjustedRenumerationValue;
  },

  _getSSBFeePerMonth: ({
    renumerationValue,
  }) => {
    const MM_TAX_CONFIG = TAX_CONFIGURATION_REFERENCES.MYANMAR;
    const { maxSalaryLimit } = MM_TAX_CONFIG.socialSecurityBoardFee;
    if (renumerationValue > maxSalaryLimit) {
      return (maxSalaryLimit * 2 / 100);
    }
    return (renumerationValue * 2 / 100);
  },

  _getIncomeTaxPerMonth: ({
    taxConfigurationReference,
    taxConfigurations,
    renumerationValue,
  }) => {
    debug('calling _getIncomeTaxPerMonth with %O %O %O', taxConfigurationReference, taxConfigurations, renumerationValue);
    const salaryPerAnnum = (renumerationValue * 12);

    const totalDeductions = (taxConfigurations.parents * taxConfigurationReference.parentRelief)
      + (taxConfigurations.spouse * taxConfigurationReference.spouseRelief)
      + (taxConfigurations.children * taxConfigurationReference.childRelief)
      + (salaryPerAnnum * (parseInt(taxConfigurationReference.personalRelief, 10) / 100))
      + taxConfigurations.socialSecurityFee;
    debug('totalDeductions %O', totalDeductions);

    const taxableIncome = salaryPerAnnum - totalDeductions;
    debug('taxableIncome %O', taxableIncome);

    let totalTax = 0;
    for (let i = 0; i < taxConfigurationReference.taxPercentages.length; i += 1) {
      const { min, max, percent } = taxConfigurationReference.taxPercentages[i];
      debug('%O - %O -> %O', min, max, percent);
      if (taxableIncome < min) {
        break;
      }
      if (taxableIncome > max) {
        const taxForThisRange = (max - min + 1) * percent / 100;
        debug('%O - %O tax: %O', min, max, taxForThisRange);
        totalTax += taxForThisRange;
      } else {
        const taxForThisRange = (taxableIncome - min + 1) * percent / 100;
        debug('%O - %O tax: %O', min, taxableIncome, taxForThisRange);
        totalTax += taxForThisRange;
      }
    }
    return parseFloat((totalTax / 12).toFixed(2));
  },

  getWorkerShiftsIds: async ({
    jobId,
    workerId,
  }) => {
    try {
      const shifts = await Shift.find({
        jobId,
        workerId,
      });
      const shiftsIds = shifts.map(shift => shift._id);
      return shiftsIds;
    } catch (err) {
      error(err);
      throw new Error(err);
    }
  },

  _checkShiftsWorkComplete: async ({
    jobId,
    workerId,
  }) => {
    const shifts = await Shift.find({
      jobId,
      workerId,
    }).catch((err) => {
      error(err);
      throw new Error(err);
    });

    return shifts.every(shift => shift.workComplete); // deprecated
  },

  _isPayCyclePaid: async ({
    jobId,
    workerId,
    payCycle,
  }) => {
    let job = {};
    let organisation = {};
    try {
      job = await Job.findById(jobId);
      organisation = await Organisation.findById(job.organisationId);
    } catch (err) {
      error(err);
      throw new Error(err);
    }
    const shiftsIds = await utils.getWorkerShiftsIds({ jobId, workerId });
    const { payCycleStartTime, payCycleEndTime } = payCycle;
    const workTokens = await WorkToken.find({
      shiftId: {
        $in: shiftsIds,
      },
      checkinTime: {
        $gte: payCycleStartTime,
        $lt: payCycleEndTime,
      },
    }).catch((err) => {
      error(err);
      throw new Error(err);
    });

    const totalWorkDays = await utils._calculateTotalWorkDays({
      accountingStyle: organisation.accountingStyle,
      payCycle,
      job,
      workerId,
    });

    if (workTokens.length >= totalWorkDays) {
      return true;
    }
    return false;
  },

  _getUniqueShiftByTimes: ({ shifts }) => {
    const startTimes = shifts.map(shift => shift.startTime);
    const uniqueStartTimes = startTimes
      .map(startTime => startTime.toString())
      .filter((startTime, i, arr) => arr.indexOf(startTime) === i)
      .map(startTime => startTime.toString());

    const endTimes = shifts.map(shift => shift.endTime);
    const uniqueEndTimes = endTimes
      .map(endTime => endTime.toString())
      .filter((endTime, i, arr) => arr.indexOf(endTime) === i)
      .map(endTime => endTime.toString());

    if (uniqueStartTimes.length !== uniqueEndTimes.length) {
      throw new Error('Faulty algorithm');
    }

    const uniqueShifts = [];
    let unique = {};
    for (let i = 0; i < uniqueEndTimes.length; i += 1) {
      unique = shifts.find(shift => shift.startTime.toString() === uniqueStartTimes[i]
        && shift.endTime.toString() === uniqueEndTimes[i]);

      uniqueShifts.push(unique);
    }
    return uniqueShifts;
  },

  checkAccess: async (ctx, target, requiredPermission) => new Promise(
    async (resolve, reject) => {
      let canAccess = false;
      let requiredLevel;
      let unauthorizedAccessError;
      try {
        // const { id, type } = target;
        const employer = await Employer.findById(ctx.user.id);
        const worker = await Worker.findById(ctx.user.id);
        log(ctx, target, requiredPermission);
        if (employer) {
          requiredLevel = _getEmployerLevel(requiredPermission);
          if (!requiredLevel) { reject(new ApolloError('Required Level Not Found', 500)); return; }
          if (requiredLevel.userLevel <= employer.userLevel) {
            canAccess = true;
          }
        } else if (worker) {
          requiredLevel = _getWorkerLevel(requiredPermission);
          if (!requiredLevel) { reject(new ApolloError('Required Level Not Found', 500)); return; }
          if (requiredLevel.userLevel <= worker.userLevel) {
            canAccess = true;
          }
        } else if (await utils._isAdmin(ctx.user)) {
          canAccess = true;
        } else {
          reject(new ApolloError('Not Authorized - invalid access token', 401));
          return;
        }
        // if (canAccess) {
        //   resolve(true);
        // }
        unauthorizedAccessError = new ApolloError(
          `${UNAUTHORIZED_ACCESS.message} ${requiredLevel != null ? requiredLevel.userLevel : 1}`,
          UNAUTHORIZED_ACCESS.code,
        );
      } catch (err) {
        reject(err);
        return;
      }
      if (canAccess) {
        resolve(true);
      } else {
        reject(unauthorizedAccessError);
      }
    },
  ),

  checkUser: (ctx) => {
    if (!ctx.user) throw new AuthenticationError('Not Authorized - invalid access token');
    return true;
  },

  checkIfWorkerCanApply: (worker, job) => {
    const jobHasBeenWithdrawn = worker.jobsWithdrewIds.map(String).includes(String(job._id));

    const jobHasBeenApplied = worker.jobsAppliedIds.map(String).includes(String(job._id));

    const jobIsExpired = moment().isAfter(job.listingEndDate);

    if (jobHasBeenWithdrawn || jobHasBeenApplied || jobIsExpired) {
      const { message, code } = errorCodes.APPLYING_INVALID_JOB;
      throw new ApolloError(message, code);
    }
    return true;
  },

  passJobFilter: (job, jobFilters) => {
    if (!jobFilters) return true;

    const {
      jobType,
      renumerationValue,
      renumerationType,
      startTime,
      jobCategories,
      maxTravelTime,
      affiliateIds,
    } = jobFilters;

    const jobRoleIDs = jobCategories
    && jobCategories.map(category => category.jobRoles.map(role => role.id)).flat();

    let matchJobType = true;
    if (jobType) {
      const { id, specialTypeIDs } = jobType;
      if (specialTypeIDs && specialTypeIDs.length > 0) {
        matchJobType = specialTypeIDs.includes(job.type);
      } else {
        matchJobType = job.type.includes(id);
      }
    }
    let matchJobRole = true;
    if (jobRoleIDs && jobRoleIDs.length > 0) {
      matchJobRole = jobRoleIDs.some(
        id => id === job.jobRoleId.toString(),
      );
    }
    let matchStartTime = true;
    if (startTime) {
      const jobStartDate = job.isOpenEnded
        ? new Date(job.listingEndDate)
        : new Date(job.startTime);
      matchStartTime = Date.now() <= jobStartDate && jobStartDate <= startTime;
    }
    let matchTravelTime = true;
    const isAbroadJob = jobType && jobType.specialTypeIDs.includes('FULL_TIME_ABROAD');
    if (!isAbroadJob && maxTravelTime < 180) {
      matchTravelTime = job.travelTime <= maxTravelTime;
    }
    let matchRValue = true;
    if (renumerationValue) {
      // get duration of times in hour
      const getDuration = (start, end) => {
        const [startHour, startMinute] = start.split(':').map(v => Number.parseInt(v, 10));
        const [endHour, endMinute] = end.split(':').map(v => Number.parseInt(v, 10));
        return (endHour + endMinute / 60) - (startHour + startMinute / 60);
      };
      if (renumerationType === 'MONTHLY') {
        if (job.paymentOption === 'HOURLY') { // convert hourly to monthly
          const payPerMonth = job.renumerationValue * 160;
          return payPerMonth >= renumerationValue;
        }
        matchRValue = job.renumerationValue >= renumerationValue;
      } else {
        if (job.paymentOption === 'MONTHLY') { // convert monthly to hourly
          const payPerDay = job.renumerationValue / 30;
          const startHour = job.fulltimeStandardHoursStart.slice(0, 5);
          const endHour = job.fulltimeStandardHoursEnd.slice(0, 5);
          const payPerHour = payPerDay / getDuration(startHour, endHour);
          const pass = payPerHour >= renumerationValue;
          return pass;
        }
        matchRValue = job.renumerationValue >= renumerationValue;
      }
    }
    let matchAffiliates = true;
    if (affiliateIds && affiliateIds.length > 0) {
      matchAffiliates = job.affiliateIds && job.affiliateIds
        .map(objId => objId.toString())
        .some(id => affiliateIds.includes(id));
    }

    return matchJobType
    && matchJobRole
    && matchStartTime
    && matchTravelTime
    && matchRValue
    && matchAffiliates;
  },


  /**
   *
   * @param {{
   *  _id: *
   *  zoomAuthCode: String
   *  zoomAccessToken: String
   *  zoomRefreshToken: String
   * }} employer
   * @param {*} interviewData
   *
   * @returns {Promise<String>}
   */
  // TODO: move under ZoomService
  getZoomInterviewUrl: async (employer, interviewData) => {
    try {
      debug('getZoomInterviewUrl', { employer, interviewData });
      const { access_token: accessToken } = await ZoomService
        .refreshAccessToken(employer.zoomRefreshToken, employer._id);
      const { startTime, endTime } = interviewData;
      const meetingData = {
        topic: 'Job Interview',
        type: 2,
        start_time: moment(startTime).toISOString(),
        duration: moment(startTime).diff(endTime, 'minutes'),
        agenda: 'Job interview scheduled by JOBDOH',
      };
      const res1 = await ZoomService.scheduleMeeting(accessToken, meetingData);
      return res1.data.join_url;
    } catch (e) {
      error(e);
    } return null;
  },

  hasDuplicates: (array, targetProp) => {
    let unique;
    if (targetProp) unique = [...new Set(array.map(e => e[targetProp]))];
    else unique = [...new Set(Array.from(array))];

    return unique.length !== array.length;
  },
  // postPublicJobs: (organisationsIds) => {
  //   try {
  //     const ret = Job.updateMany({
  //       organisationId: { $in: organisationsIds },
  //     });
  //   } catch (e) {
  //     throw new ApolloError(e);
  //   }
  // }

  createMarketplaceSubscriber: async (marketplaceId, subscriber) => {
    let createdSubscriber;
    try {
      createdSubscriber = await
      new ExpectedMarketplaceSubscriber({ ...subscriber, marketplaceId }).save();
      if (createdSubscriber) {
        await Worker.updateMany(
          { phone: subscriber.phone },
          { $addToSet: { marketplacesIds: marketplaceId } },
        );
      }
    } catch (e) {
      error(e);
    }
    return createdSubscriber;
  },

  updateMarketplaceSubscriber: async (subscriber) => {
    let updatedSubscriber;
    try {
      const s = subscriber;
      updatedSubscriber = await ExpectedMarketplaceSubscriber.update(
        { phone: s.phone },
        { name: s.name, identification: s.identification, email: s.email },
      );
    } catch (e) {
      error(e);
    }
    return updatedSubscriber;
  },

  deleteMarketplaceSubscriber: async (marketplaceId, subscriber) => {
    let deletedSubscriber;
    try {
      deletedSubscriber = await ExpectedMarketplaceSubscriber.deleteMany({
        marketplaceId,
        phone: subscriber.phone,
      });
      if (deletedSubscriber) {
        await Worker.updateMany(
          {
            marketplacesIds: { $in: [marketplaceId] },
            phone: subscriber.phone,
          },
          { $pull: { marketplacesIds: marketplaceId } },
        );
      }
    } catch (e) {
      error(e);
    }
    return deletedSubscriber;
  },

  createMarketplaceSubscribers: async (marketplaceId, newSubscribers = []) => {
    const createdSubscribers = await Promise.all(
      newSubscribers.map(
        async subscriber => utils.createMarketplaceSubscriber(marketplaceId, subscriber),
      ),
    );
    const success = createdSubscribers.filter(e => !!e).length;
    const total = newSubscribers.length;
    return { success, total };
  },

  updateMarketplaceSubscribers: async (marketplaceId, changedSubscribers = []) => {
    const updatedSubscribers = await Promise.all(
      changedSubscribers.map(
        subscriber => utils.updateMarketplaceSubscriber(subscriber),
      ),
    );
    const success = updatedSubscribers.filter(e => !!e).length;
    const total = changedSubscribers.length;
    return { success, total };
  },

  deleteMarketplaceSubscribers: async (marketplaceId, deleteSubscribers = []) => {
    const deletedSubscribers = await Promise.all(
      deleteSubscribers.map(
        async subscriber => utils.deleteMarketplaceSubscriber(marketplaceId, subscriber),
      ),
    );
    const success = deletedSubscribers.filter(e => !!e).length;
    const total = deleteSubscribers.length;
    return { success, total };
  },

  getEmployerOf: async ({ shiftId, jobId }, projection) => {
    let employer;
    try {
      let organisationId;
      if (jobId) {
        const job = await Job.findById(jobId, 'organisationId');
        organisationId = job && job.organisationId;
      } else if (shiftId) {
        const shift = await Shift.findById(shiftId, 'organisationId');
        organisationId = shift && shift.organisationId;
      }
      if (organisationId) {
        employer = await Employer.findOne({
          organisationsIds: { $in: [ObjectId(organisationId)] },
        }, projection);
      }
    } catch (e) {
      error(e);
    }
    return employer;
  },

  // deprecated use from utils/constants.js
  // authorisedAdminIds: [
  //   // '34KyJCLznHWHvvLTX6yID7clV6a2', // jobdoh.employer1@gmail.com
  //   // 'GniPsb6Vr0QLicFPKch8lE18hAw1', // lightcode2018@gmail.com
  //   // 'upK3BhyyuLPIdN204sauQn1QU5G3', // phoenixjames05@gmail.com
  //   '7APDOVEepLhBVEaZF5XYDLezfG43', // jobdoh.operations@gmail.com
  //   '34KyJCLznHWHvvLTX6yID7clV6a2', // jobdoh.employer1@gmail.com
  //   'lFvCWJ5DZgNN52uYVE3yQjb1kya2', // myanmar@jobdoh.com
  //   '2Aw1enPwipeyLiwQvbvOdN1vxDi2', // jobdoh.backend@gmail.com
  // ],
  leaveTypes: ['LEAVE_SICK', 'LEAVE_PERSONAL', 'LEAVE_SPECIAL', 'LEAVE_UNPAID'],
  NOT_POSTED_MARKETPLACE_ID: ObjectId('5fdb677bb9aa853f791bab64'),
  PUBLIC_MARKETPLACE_ID: ObjectId('5e1d54053998d418ddc644ba'),
};

module.exports = utils;
