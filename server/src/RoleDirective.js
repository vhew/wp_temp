const { SchemaDirectiveVisitor } = require('graphql-tools');
const { defaultFieldResolver } = require('graphql');

class RoleDirective extends SchemaDirectiveVisitor {
  visitObject(type) {
    type._requiredAuthRole = this.args.role; // eslint-disable-line no-param-reassign
    this.ensureFieldsWrapped(type);
  }

  visitFieldDefinition(field, details) {
    field._requiredRole = this.args.role; // eslint-disable-line no-param-reassign
    this.ensureFieldsWrapped(details.objectType);
  }

  // eslint-disable-next-line class-methods-use-this
  ensureFieldsWrapped(objectType) {
    // Mark the GraphQLObjectType object to avoid re-wrapping:
    if (objectType._authFieldsWrapped) return;
    // eslint-disable-next-line no-param-reassign
    objectType._authFieldsWrapped = true;

    const fields = objectType.getFields();

    Object.keys(fields).forEach((fieldName) => {
      const field = fields[fieldName];
      const { resolve = defaultFieldResolver } = field;
      field.resolve = async (...args) => {
        // Get the required Role from the field first, falling back
        // to the objectType if no Role is required by the field:
        const requiredRole = field._requiredAuthRole;

        if (!requiredRole) {
          return resolve.apply(this, args);
        }

        const { user } = args[2];
        if (user.role !== requiredRole && user.role !== 'ADMIN') {
          throw new Error('not authorized');
        }

        return resolve.apply(this, args);
      };
    });
  }


  // visitEnumValue(value) {
  //   const requiredRole = this.args.role;

  //   const { resolve = defaultFieldResolver } = value;


  //   // eslint-disable-next-line no-param-reassign
  //   value.resolve = (...args) => { // eslint-disable-line no-param-reassign
  //     // if role does not match requiredRole, do not resolve value
  //     const { user } = args[2];
  //     const details = args[3];
  //     if (user.role !== requiredRole && user.role !== 'ADMIN') {
  //       throw new ForbiddenError(`Forbidden access to field: ${details.fieldName}`);
  //     }

  //     // call the original resolve function
  //     return resolve.apply(this, args);
  //   };
  // }
}


module.exports = RoleDirective;
