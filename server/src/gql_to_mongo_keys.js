const mapping = {
  AND: '$and',
  OR: '$or',
  GT: '$gt',
  GTE: '$gte',
  LT: '$lt',
  LTE: '$lte',
  IN: '$in',
  EQ: '$eq',
};
module.exports = mapping;
