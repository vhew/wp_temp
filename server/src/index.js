const debug = require('debug')('DEBUG');
const info = require('debug')('INFO');
const error = require('debug')('ERROR');
const fs = require('fs');
// const https = require('https');
const http = require('http');
const ip = require('ip');
const Koa = require('koa');
const helmet = require('koa-helmet');
// const Logger = require('koa-pino-logger');
const { ApolloServer } = require('apollo-server-koa');
const firebaseAdmin = require('firebase-admin');
const healthCheckPlugin = require('koa-simple-healthcheck');
const AppModule = require('./modules/index');
const { getDbUrl, connectDatabase } = require('./utils/db');
const config = require('../config/api');

const dbUrl = getDbUrl();
connectDatabase(dbUrl);

// initialise Firebase Admin
firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert({
    projectId: 'dotted-howl-158112',
    clientEmail: 'firebase-adminsdk-w9gi0@dotted-howl-158112.iam.gserviceaccount.com',
    clientId: '100062061334108703140',
    privateKeyId: 'a8d8fc156d06e73397b1afce3ace2d207e78c524',
    privateKey: config.server.firebasePrivateKey.replace(/\\n/g, '\n'),
    authUri: 'https://accounts.google.com/o/oauth2/auth',
    tokenUri: 'https://oauth2.googleapis.com/token',
    authProviderX509CertUrl: 'https://www.googleapis.com/oauth2/v1/certs',
    clientX509CertUrl: 'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-w9gi0%40dotted-howl-158112.iam.gserviceaccount.com',
  }),
  databaseURL: 'https://dotted-howl-158112.firebaseio.com',
});

const employerFirebaseApp = firebaseAdmin.initializeApp({
  apiKey: 'AIzaSyA5m_XMaeAHs3nars-eKmF5CLuYdYDVsyQ',
  authDomain: 'jobdoh2employer.firebaseapp.com',
  databaseURL: 'https://jobdoh2employer.firebaseio.com',
  projectId: 'jobdoh2employer',
  storageBucket: 'jobdoh2employer.appspot.com',
  messagingSenderId: '996263494846',
  appId: '1:996263494846:web:9d65cd2bcd09ca862b4aef',
  measurementId: 'G-GWLX7HBZXS',
}, 'employerFirebaseApp');

const operationsFirebaseApp = firebaseAdmin.initializeApp({
  apiKey: 'AIzaSyBUaOKdLBhnRsST8sIlSP_o-mEvfy2hx6c',
  authDomain: 'jobdoh2operations.firebaseapp.com',
  databaseURL: 'https://jobdoh2operations.firebaseio.com',
  projectId: 'jobdoh2operations',
  storageBucket: 'jobdoh2operations.appspot.com',
  messagingSenderId: '150905851962',
  appId: '1:150905851962:web:e004c1f334f619c4b06503',
  measurementId: 'G-L9PSTN67DT',
}, 'operationsFirebaseApp');
// define directives used by the graphql Schema

// verify the user supplied token with firebase
const getUser = async ({ token, app }) => {
  if (process.env.NODE_ENV === 'development') {
    if (token === 'development') {
      return {
        id: 'development',
      };
    }
  }

  if (token === null || token.length < 10) return null;

  let decodedToken;
  if (app === 'client_employer') {
    decodedToken = await employerFirebaseApp.auth().verifyIdToken(token);
  } else if (app === 'client_operations') {
    decodedToken = await operationsFirebaseApp.auth().verifyIdToken(token);
  } else {
    decodedToken = await firebaseAdmin.auth().verifyIdToken(token);
  }


  let user = null;
  if (decodedToken) {
    debug('user decodedToken: %O', decodedToken);
    user = {
      id: decodedToken.user_id,
      role: 'USER',
    };
  }
  return user;
};

// Init koa
const app = new Koa();
app.use(helmet());
// app.use(Logger({ level: config.logger.level }));
app.use(healthCheckPlugin({
  path: '/health',
  healthy: () => ({
    everything: 'is ok',
  }),
}));
// apply GraphQL to koa

const versions = fs.readFileSync('./versions', 'utf8').toString().split('\n');

const { schema } = AppModule;
const apollo = new ApolloServer({
  introspection: true,
  schema,
  engine: {
    apiKey: 'service:pwa-pwa:hUEG3xCNbGF1jo7errOQdA',
    schemaTag: 'staging',
  },
  context: async ({ ctx }) => {
    if (ctx.headers.version != null && !versions.includes(ctx.headers.version)) {
      ctx.throw(400, 'Incompatible version');
    }
    if (ctx.headers.authorization === 'undefined') {
      return {};
    }
    const user = await getUser({
      token: ctx.headers.authorization || '',
      app: ctx.headers.app || '',
    });
    return {
      user,
      ctx,
    };
  },
  // context: async ({ ctx }) => ({
  //   user: await getUser(ctx.headers.authorization || ''),
  //   ctx,
  // }),
  uploads: false,
});
apollo.applyMiddleware({ app, path: '/v1' });

// init http/https server
/*
const runHTTPS = (config.server.https && process.env.NODE_ENV === 'production');
let server;
if (runHTTPS) {
  server = https.createServer(
    {
      key: fs.readFileSync(`./ssl/${config.server.https.key}`),
      cert: fs.readFileSync(`./ssl/${config.server.https.cert}`),
      ca: fs.readFileSync(`./ssl/${config.server.https.ca}`),
    },
    app.callback(),
  );
} else {
  server = http.createServer(app.callback());
}
*/
const runHTTPS = false;
const server = http.createServer(app.callback());

// add subscription support
apollo.installSubscriptionHandlers(server);

const instance = server.listen(config.server.port, (err) => {
  if (err) {
    error('%O', err);
  } else {
    const protocol = runHTTPS ? 'https' : 'http';
    const myIP = ip.address();
    info('API server started on %s', `${protocol}://${myIP}:${config.server.port}${apollo.graphqlPath}`);
  }
});

module.exports = instance;
