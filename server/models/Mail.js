class Mail {
  constructor(person, intro, option = {}) {
    this.to = person.email;
    this.body = {
      name: person.name,
      intro,
      action: option.action,
    };
  }

  static getActions() {
    return {
      CONFIRM: {
        instructions: 'Please go to the app to confirm it.',
        button: {
          color: '#22BC66', // Optional action button color
          text: 'GO TO EMPLOYER APP',
          link: 'https://employer.jobdoh.com',
        },
      },
    };
  }
}

module.exports = Mail;
