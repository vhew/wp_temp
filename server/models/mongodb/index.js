const mongoose = require('mongoose');

const WorkerSchema = new mongoose.Schema(
  {
    // MINIMUM PROFILE
    _id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    verified: {
      type: Boolean,
      required: true,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    createOrganisationId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Organisation',
    },
    adoptedByWorkerId: {
      type: String,
    },
    jobsBookmarkIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Job',
      },
    ],
    addToCartProductsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Job',
      },
    ],
    jobsAppliedIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Job',
      },
    ],
    jobsWithdrewIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Job',
      },
    ],
    jobsNotHiredIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Job',
      },
    ],
    marketplacesIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Marketplace',
      },
    ],
    identification: {
      type: String,
    },
    identificationBackFileRef: {
      type: String,
    },
    identificationFrontFileRef: {
      type: String,
    },

    // COMMUNICATION PROFILE
    phone: {
      type: String,
      required: true,
    },
    lockCode: {
      type: String,
    },
    languageSetting: {
      type: String,
    },
    region: { type: String },
    fcm: { type: String },
    facebook: { type: String },
    email: { type: String },
    google: { type: String },
    linkin: { type: String },

    // NOTIFICATION PREFERENCES
    notificationHiredUpdates: [{ type: String }],
    notificationNotHiredUpdates: [{ type: String }],
    notificationChat: [{ type: String }],
    notificationJobWatch: [{ type: String }],
    jobSkillsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'JobSkill',
      },
    ],
    jobSkills: [
      {
        type: Object,
        default: [],
      },
    ],
    // jobFiltersIds: [{
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: 'JobFilter',
    // }],
    jobFilters: [
      {
        type: Object,
        default: [],
      },
    ],
    locationFrom: {
      type: String,
    },

    // FINANCE PROFILE
    paymentMethod: { type: String }, // TO BE DEPRECATED
    paymentCurrency: { type: String }, // TO BE DEPRECATED
    paymentCoordinates: { type: String }, // TO BE DEPRECATED

    secondaryPaymentMethod: { type: String }, // TO BE DEPRECATED
    secondaryPaymentCurrency: { type: String }, // TO BE DEPRECATED
    secondaryPaymentCoordinates: { type: String }, // TO BE DEPRECATED

    paycycleUseAlternatePayment: {
      // TO BE DEPRECATED
      type: Boolean,
      default: false,
    },
    alternatePaymentMethod: { type: String }, // TO BE DEPRECATED
    alternatePaymentCurrency: { type: String }, // TO BE DEPRECATED
    alternatePaymentCoordinates: { type: String }, // TO BE DEPRECATED

    paymentMethods: [
      // TO BE DEPRECATED
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PaymentMethod',
      },
    ],
    advancedPayPaymentMethodId: {
      // TO BE DEPRECATED
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PaymentMethod',
    },
    payCyclePaymentMethodId: {
      // TO BE DEPRECATED
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PaymentMethod',
    },

    profileUrl: {
      type: String,
    },
    youtubeUrl: {
      type: String,
    },
    cvFileUrl: {
      type: String,
    },
    description: {
      type: String,
    },
    jobHistories: [{ type: Object }],
    // MARKETING PROFILE
    // qualificationX: { type: String },
    qualificationIdentificationFileRef: {
      type: String,
    },

    digitalLiteracyScore: {
      type: Number,
    },
    personalityType: {
      type: String,
    },
    conscientiousnessLevel: {
      type: Number,
    },
    // SURVEY PROFILE
    ageGroup: {
      type: String,
    },
    gender: {
      type: String,
    },
    householdSize: {
      type: Number,
    },
    householdIncome: {
      type: Number,
    },
    maritalStatus: {
      type: String,
    },
    highestEducationStatus: {
      type: String,
    },
    childrenCount: {
      type: Number,
    },
    race: {
      type: String,
    },
    isOwnHouse: {
      type: Boolean,
    },
    workerPaymentMethodsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'WorkerPaymentMethod',
      },
    ],
    defaultWorkerPaymentMethodId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'WorkerPaymentMethod',
    },
    userLevel: {
      type: Number,
      default: 1,
    },
    surveyTriggers: {
      type: Object,
      default: {},
    },
    feedbacks: {
      type: Object,
      default: {
        value: {},
        lastRatedAt: null,
      },
    },
  },
  { timestamps: true },
);

const AlienWorkerSchema = new mongoose.Schema(
  {
    // MINIMUM PROFILE
    name: {
      type: String,
      required: true,
    },
    verified: {
      type: Boolean,
      required: true,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    createOrganisationId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Organisation',
    },
    adoptedByWorkerId: {
      type: String,
    },
    identification: {
      type: String,
      required: true,
    },
    identificationFileRef: {
      type: String,
    },

    // COMMUNICATION PROFILE
    phone: {
      type: String,
    },
    // NOTIFICATION PREFERENCES
    locationFrom: {
      type: String,
    },
    workerPaymentMethodsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'WorkerPaymentMethod',
      },
    ],
    defaultWorkerPaymentMethodId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'WorkerPaymentMethod',
    },

    // FINANCE PROFILE
    paymentMethod: { type: String }, // TO BE DEPRECATED
    paymentCurrency: { type: String }, // TO BE DEPRECATED
    paymentCoordinates: { type: String }, // TO BE DEPRECATED
    paycycleUseAlternatePayment: {
      // TO BE DEPRECATED
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true },
);

const EmployerSchema = new mongoose.Schema(
  {
    _id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    organisationsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Organisation',
        required: true,
      },
    ],
    userLevel: {
      type: Number,
      default: 1,
    },
    access: {
      type: Object,
      default: {
        readable: true,
        writable: true,
      },
    },
    affiliateIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Affiliate',
        default: [],
      },
    ],

    // COMMUNICATION PROFILE
    phone: { type: String },
    fcm: { type: String },
    facebook: { type: String },
    email: {
      type: String,
      required: true,
    },
    google: { type: String },
    zoomAuthCode: { type: String },
    zoomAccessToken: { type: String },
    zoomRefreshToken: { type: String },

    // NOTIFICATION PREFERENCES
    notificationHiredUpdates: [{ type: String }],
    notificationNotHiredUpdates: [{ type: String }],
    notificationChat: [{ type: String }],
    workerWatchNotifyThreshold: { type: Number },
    surveyTriggers: { type: Object, default: {} },
  },
  { timestamps: true },
);

const OrganisationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    jobsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Job',
      },
    ],
    marketplacesIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Marketplace',
      },
    ],

    // FINANCE PROFILE
    payrollMethod: { type: String }, // TO BE DEPRECATED
    payrollCurrency: { type: String }, // TO BE DEPRECATED
    payrollManagerName: { type: String },
    payrollManagerContact: { type: String },
    payCycleMonthDays: {
      type: Array,
      required: true,
      default: [1, 1],
    },
    payCycleAdvancements: {
      type: Array,
    },
    accountingStyle: {
      type: String,
      default: 'simple',
    },
    region: { type: String },
    fee: { type: Number },
    // MARKETING PROFILE
    description: { type: String },
    settings: { type: Array },
    phone: {
      type: String,
    },
    address: {
      type: String,
    },
    logoUrl: { type: String },
    industryType: { type: String },
    videoUrl: { type: String },
    websiteUrl: { type: String },
    socialMedia: {
      type: Object,
      default: {
        facebook: null,
        twitter: null,
        instagram: null,
        linkedIn: null,
      },
    },
    businessRegFileUrl: { type: String },
    businessRegNumber: { type: String },
    payrollOfficerContact: { type: String },
    disbursementPaymentType: { type: String },
    payrollContractFileUrl: { type: String },
    surveys: { type: Object, default: {} },
    billingDetails: { type: String },
    payrollOfficerPhone: { type: String },
    payrollOfficerEmail: { type: String },
    leaveSickAccuralRateDaysPerYear: { type: Number, default: 0 },
    leavePersonalAccrualRateDaysPerYear: { type: Number, default: 0 },
    leaveSpecialAccuralRateDaysPerYear: { type: Number, default: 0 },
    numberOfStuff: { type: Number },
    approxTotalSalary: { type: Number },
    companyAge: { type: Number },
    organisationRelationChartUrl: { type: String },
    reasonForSelfPayrollAndDisbursement: { type: String },
    feedbacks: {
      type: Object,
      default: {
        avg: 0,
        nFeedback: 0,
        lastRatedAt: null,
      },
    },
  },
  { timestamps: true },
);

const JobSchema = new mongoose.Schema(
  {
    organisationId: {
      type: String,
      ref: 'Organisation',
      required: true,
    },
    marketplaceId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Marketplace',
    },
    title: {
      type: String,
      required: true,
    },
    description: { type: String },
    requirementDescription: { type: String },
    address: { type: String },
    location: { type: Object },
    locationCoordinates: { type: Object },
    type: {
      type: String,
      required: true,
    },
    role: { type: String },
    jobRoleId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'JobRole',
    },
    paymentMethodsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PaymentMethod',
      },
    ],
    paymentOption: {
      type: String,
    },
    renumerationCurrency: {
      type: String,
      default: 'MMK',
    },
    renumerationValue: {
      type: Number,
      required: true,
      default: 0,
    },
    score: { type: Number, default: 0 },
    private: {
      type: Boolean,
    },

    isOpenEnded: { type: Boolean },
    allowFlexibleTime: { type: Boolean },
    listingStartDate: { type: Date },
    listingEndDate: { type: Date },

    // CREATE (type specific)
    fulltimeStandardHoursStart: {
      type: String,
      required: true,
      default: '09:00+06:30',
    },
    fulltimeStandardHoursEnd: {
      type: String,
      required: true,
      default: '18:00+06:30',
    },
    fulltimeOvertimeRenumerationPerHour: {
      type: Number,
      required: true,
      default: 0,
    },
    fulltimeLeavePersonalAccruePerDay: {
      type: Number,
      required: true,
      default: 0,
    },
    fulltimeLeaveSickAccruePerDay: {
      type: Number,
      required: true,
      default: 0,
    },
    fulltimePublicHolidays: {
      type: Array,
      required: true,
      default: [],
    },
    fulltimeNonWorkWeekDays: {
      type: Array,
      required: true,
      default: [0, 6],
    },
    hoursPerWeek: { type: Number },
    payCycleGraceDays: {
      type: Number,
      required: true,
      default: 0,
    },
    benefits: [{ type: String }],
    jobTypeConversion: [{ type: String }],
    contractAcceptanceTest: { type: String },
    contractId: { type: String },
    contract: { type: String },
    contractAlt: { type: String },

    // OPEN -> HIRED -> WORKING (WORKED & PAID) -> COMPLETE (Rated)
    shiftsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shift',
      },
    ],
    shiftsFilled: { type: Boolean, default: false },
    payDate: {
      type: Date,
    },
    allowAdvancedPay: {
      type: Boolean,
    },
    minShiftsBalance: {
      type: Number,
      required: true,
      default: 0,
    },
    // REQUIREMENTS
    requirementNumberOfContracts: { type: Number },
    requirementSpokenLanguages: [{ type: String }],
    requirementWrittenLanguages: [{ type: String }],
    requirementDocuments: [{ type: String }],
    requirementScreeningChannels: [{ type: String }],
    requirementRequiredSkills: [{ type: String }],
    jobSkillRequirements: [
      {
        type: Object,
        default: [],
      },
    ],
    requirementVenueWorked: [{ type: String }],
    requirementMinExperienceLevel: { type: String },
    requirementPreferredSkills: [{ type: String }],
    requirementDressCodes: [{ type: String }],
    requirementCustomDressCode: { type: String },
    requirementDemographicAgeGroup: [{ type: String }],
    requirementDemographicGender: [{ type: String, default: [] }],
    requirementExperienceLevel: { type: String },
    isPortfolioRequired: { type: Boolean },
    notes: { type: String },
    proxyCompanyName: { type: String },
    proxyCompanyDescription: { type: String },
    proxyEmployerName: { type: String },
    proxyCompanyContact: { type: String },
    proxyCompanyAddress: { type: String },
    proxyCompanyWebsiteUrl: { type: String },
    affiliateIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Affiliate',
        default: [],
      },
    ],
    purpose: { type: String },
    workingDays: { type: Array },
    workingHours: { type: Array },
    lunchHour: { type: Array },
    sickLeaveDays: { type: Number },
    personalLeaveDays: { type: Number },
    specialLeaveDays: { type: Number },
    availableCountries: [{ type: String, default: [] }],
  },
  { timestamps: true },
);

const ShiftSchema = new mongoose.Schema(
  {
    siblingShiftsIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shift',
      },
    ],
    jobId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Job',
      required: true,
    },
    startTime: {
      type: Date,
      required: true,
    },
    endTime: { type: Date },

    // OPEN -> HIRED -> WORKING (WORKED & PAID) -> COMPLETE (Rated)
    candidatesIds: [
      {
        type: String,
      },
    ],
    rejectedCandidatesIds: [
      {
        type: String,
      },
    ],
    interviews: [{ type: Object }],
    employerId: {
      type: String,
    },
    organisationId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Organisation',
    },
    workerId: {
      type: String,
    },
    disabledWorkersIds: [
      {
        type: String,
      },
    ],
    workerAccepted: {
      type: Boolean,
      default: false,
    },
    workComplete: {
      type: Boolean,
      default: false,
    },
    divisible: {
      type: Boolean,
      default: true,
    },
    deductions: [
      {
        type: Object,
      },
    ],
    taxConfigurations: {
      type: Object,
    },
    employerRatingByWorker: { type: Number },
    workerRatingByEmployer: { type: Number },
    resignRequestedDate: {
      type: Date,
      // default: new Date().toISOString(),
    },
    tradeShiftStatus: { type: String },
    leaveSickDays: {
      type: Number,
      default: 0,
    },
    leaveSickUpdateDate: {
      type: Date,
      // default: new Date().toISOString(),
    },
    leavePersonalDays: {
      type: Number,
      default: 0,
    },
    leavePersonalUpdateDate: {
      type: Date,
      // default: new Date().toISOString(),
    },
    leaveSpecialDays: {
      type: Number,
      default: 0,
    },
    leaveSpecialUpdateDate: {
      type: Date,
      // default: new Date().toISOString(),
    },
    dismissReason: {
      type: String,
    },
    salary: {
      type: Number,
      default: 0,
    },
    renumerationCurrency: {
      type: String,
    },
    completedAt: { type: Date },
  },
  { timestamps: true },
);

const JobFilterSchema = new mongoose.Schema(
  {
    orderBy: {
      type: String,
      required: true,
    },
    startingInHoursMax: {
      type: Number,
      required: true,
    },
    jobTypes: [
      {
        type: String,
        required: true,
      },
    ],
    roleTypes: [
      {
        type: String,
        required: true,
      },
    ],
    salaryMin: {
      type: Number,
      required: true,
    },
    salaryCurrency: {
      type: String,
      required: true,
    },
    travelMinutesMax: {
      type: Number,
      required: true,
    },
    jobWatchNotifyThreshold: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true },
);

const WorkTokenValueSchema = new mongoose.Schema(
  {
    expectedWorkTokenShiftId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Shift',
      required: true,
    },
    expectedWorkTokenWorkerId: {
      type: 'String',
      required: true,
    },
    expectedWorkTokenType: {
      type: String,
      required: true,
    },
    expectedWorkTokenCheckinTime: {
      type: Date,
      required: true,
    },
    expectedWorkTokenCheckoutTime: {
      type: Date,
      required: true,
    },
    workTokenId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'WorkToken',
    },
    confirmRequest: {
      type: Boolean,
      default: true,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    amountConflictDifference: {
      type: Number,
      required: false,
    },
    fee: {
      type: Number,
      required: true,
    },
    currency: {
      type: String,
      required: true,
    },
    notes: {
      type: String,
    },
    method: {
      type: String,
      required: true,
    },
    coordinates: {
      type: String,
      required: true,
    },
    discountTokenId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'DiscountToken',
    },
  },
  { timestamps: true },
);

const WorkTokenSchema = new mongoose.Schema(
  {
    shiftId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Shift',
    },
    workerId: {
      type: 'String',
    },
    productId: {
      type: 'String',
    },
    checkinTime: { type: Date },
    checkoutTime: { type: Date },
    confirmed: {
      type: Boolean,
      default: null,
    },
    payNow: {
      type: Boolean,
      default: false,
    },
    type: { type: String },
    references: { type: Array },
  },
  { timestamps: true },
);

const FiatTokenSchema = new mongoose.Schema(
  {
    amount: {
      type: Number,
      required: true,
    },
    currency: {
      type: String,
      required: true,
    },
    method: {
      type: String,
      required: true,
    },
    coordinates: {
      type: String,
      required: true,
    },
    transactionRef: {
      type: String,
      required: true,
    },
    workTokenId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'WorkToken',
      required: true,
    },
  },
  { timestamps: true },
);

const DiscountTokenSchema = new mongoose.Schema(
  {
    expiryDate: { type: Date },
    discountPercent: {
      type: Number,
      required: true,
    },
    discountFixed: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    workerId: {
      type: String,
    },
    used: {
      type: Boolean,
    },
  },
  { timestamps: true },
);

const TownshipDistanceMatrixSchema = new mongoose.Schema({
  origin: { type: String },
  originMM: { type: String },
  destination: { type: String },
  destinationMM: { type: String },
  distance: { type: Number },
  duration: { type: Number },
});

const JobMatchingSchema = new mongoose.Schema({
  workerId: { type: String },
  jobId: { type: mongoose.Types.ObjectId },
  score: { type: Number },
});

const ServerConfigSchema = new mongoose.Schema({
  identifier: {
    type: String,
  },
  feePercent: {
    type: Number,
  },
});
// Create indexes

WorkerSchema.index({ name: 'text' });
WorkTokenSchema.index({ shiftId: 1, checkinTime: -1 });
WorkTokenSchema.index({ confirmed: -1 });
WorkTokenSchema.index({ type: 1 });
WorkTokenSchema.index({ createdAt: -1 });
WorkTokenSchema.index({ payNow: -1 });
WorkTokenValueSchema.index({
  expectedWorkTokenShiftId: 1,
  expectedWorkTokenCheckinTime: -1,
});
WorkTokenValueSchema.index({ workTokenId: -1 });
WorkTokenValueSchema.index({ fee: -1, expectedWorkTokenWorkerId: -1 });
JobSchema.index({ organisationId: 1 });
JobSchema.index({ shiftsFilled: 1 });
JobSchema.index({ marketplaceId: 1 });
JobSchema.index({ proxyCompanyName: 1 });
JobSchema.index({
  listingStartDate: 1,
  listingEndDate: 1,
});
JobMatchingSchema.index({ workerId: 1 });
JobMatchingSchema.index({ jobId: 1 });
// JobSchema.index({ locationCoordinates: '2dsphere' });
FiatTokenSchema.index({ createdAt: -1 });
FiatTokenSchema.index({ workTokenId: -1 });
ShiftSchema.index({ workerId: 1, jobId: 1 });
ShiftSchema.index({ jobId: 1 });
ShiftSchema.index({ organisationId: 1 });
TownshipDistanceMatrixSchema.index({ origin: 1, destination: 1 });

// Declare Model to mongoose with Schema

const Worker = mongoose.model('Worker', WorkerSchema);
const AlienWorker = mongoose.model('AlienWorker', AlienWorkerSchema);
const Employer = mongoose.model('Employer', EmployerSchema);
const Organisation = mongoose.model('Organisation', OrganisationSchema);
const Job = mongoose.model('Job', JobSchema);
const Shift = mongoose.model('Shift', ShiftSchema);
const JobFilter = mongoose.model('JobFilter', JobFilterSchema);
const WorkTokenValue = mongoose.model('WorkTokenValue', WorkTokenValueSchema);
const WorkToken = mongoose.model('WorkToken', WorkTokenSchema);
const FiatToken = mongoose.model('FiatToken', FiatTokenSchema);
const TownshipDistanceMatrix = mongoose.model(
  'TownshipDistanceMatrix',
  TownshipDistanceMatrixSchema,
);
const ServerConfig = mongoose.model('ServerConfig', ServerConfigSchema);
const DiscountToken = mongoose.model('DiscountToken', DiscountTokenSchema);
const JobMatching = mongoose.model('JobMatching', JobMatchingSchema);

// Export Model to be used in Node
module.exports = {
  Worker,
  AlienWorker,
  Employer,
  Organisation,
  Job,
  Shift,
  JobFilter,
  WorkTokenValue,
  WorkToken,
  FiatToken,
  TownshipDistanceMatrix,
  ServerConfig,
  DiscountToken,
  JobMatching,
  // Schemas
  WorkerSchema,
  JobSchema,
};
