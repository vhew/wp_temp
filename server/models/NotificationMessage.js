const Mail = require('./Mail');

// const messages = {
//   INTERVIEW_REQUEST: data => ({
//     title: 'Request',
//     body: `Interivew request from ${data.title}`,
//   }),
// };
module.exports = class NotificationMessage {
  /**
     *
     * @param {String} title
     * @param {String} body
     * @param {*} data
     * @param {*} mail
     */
  constructor(from, to, title, body, data = {}, { mail, analyticsLabel }) {
    this.senderId = from;
    this.receiverId = to;
    this.title = title || 'Notification';
    this.body = body || 'Notification Body';
    this.data = { ...data, title, body };
    if (mail && mail.to) this.mail = new Mail(mail.to, body, { action: mail.action });
    if (analyticsLabel) this.fcm_options = { analyticsLabel };
  }
};
