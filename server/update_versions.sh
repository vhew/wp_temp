#!/bin/bash
if [ "$NODE_ENV" == "development" ]
then
  echo "development_version" > versions
elif [ "$NODE_ENV" == "staging" ]
then
  git rev-list master -n 5 > versions
elif [ "$NODE_ENV" == "production" ]
then
  git rev-list production -n 5 > versions
fi

